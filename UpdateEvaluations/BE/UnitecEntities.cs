﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace UpdateEvaluations.BE
{



    #region Common Entities

    public static class ApplicationProperty
    {
        public static string ConnectionString
        {
            set;
            get;
        }

        public static UnitecConfiguration Settings
        {
            set;
            get;
        }
    }

    [Serializable]
    public abstract class EntityCollection<EntityClass> : IEnumerable<EntityClass>
    {
        #region Private Member

        List<EntityClass> _entities;

        #endregion

        #region Property

        public EntityClass this[int Index]
        {
            get
            {
                return _entities[Index];
            }
            set
            {
                _entities[Index] = value;
            }
        }

        #endregion

        public EntityCollection(EntityClass[] initialElements)
        {
            _entities = new List<EntityClass>();
            this.AddRange(initialElements);
        }

        public EntityCollection()
        { _entities = new List<EntityClass>(); }

        public EntityCollection(IEnumerable<EntityClass> entities)
        {
            _entities = new List<EntityClass>();
            this.AddRange(entities);
        }

        public int Count
        {
            get { return _entities.Count; }
        }

        public abstract EntityClass this[string ID] { get; }
        public virtual void Add(EntityClass entity)
        {
            _entities.Add(entity);
        }

        public virtual void Add(params EntityClass[] entities)
        {
            for (int i = 0; i < entities.Length; i++)
                _entities.Add(entities[i]);
        }

        public virtual void AddRange(IEnumerable<EntityClass> entities)
        {
            foreach (EntityClass entity in entities)
                _entities.Add(entity);
        }

        public virtual void Clear()
        {
            if (_entities != null)
                _entities.Clear();
        }

        public IEnumerator<EntityClass> GetEnumerator()
        {
            return _entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Import(EntityClass[] Data)
        {
            if (Data != null)
                for (int i = 0; i < Data.Length; i++)
                    if (Data[i] != null)
                        Add(Data[i]);
        }

        public virtual void Insert(EntityClass Entity, int Index)
        {
            _entities.Insert(Index, Entity);
        }

        public bool IsEmpty()
        {
            return _entities.Count == 0;
        }

        public virtual bool Remove(int Index)
        {
            if (_entities.Count > Index)
            {
                _entities.RemoveAt(Index);
                return true;
            }
            return false;
        }

        public virtual bool Remove(EntityClass entity)
        {
            if (_entities.Contains(entity))
            {
                _entities.Remove(entity);
                return true;
            }
            return false;
        }
        public void Set(EntityClass[] Data)
        {
            List<EntityClass> rt = new List<EntityClass>();
            if (Data != null)
                for (int i = 0; i < Data.Length; i++)
                    if (Data[i] != null)
                        rt.Add(Data[i]);
            this.Clear();
            this.AddRange(rt);
        }

        public void Set(IEnumerable<EntityClass> Data)
        {
            List<EntityClass> rt = new List<EntityClass>();
            if (Data != null)
                for (int i = 0; i < Data.Count<EntityClass>(); i++)
                    if (Data.ElementAt<EntityClass>(i) != null)
                        rt.Add(Data.ElementAt<EntityClass>(i));
            this.Clear();
            this.AddRange(rt);
        }

        public EntityClass[] ToArray()
        {
            EntityClass[] temp = new EntityClass[_entities.Count];
            for (int i = 0; i < temp.Length; i++)
                temp[i] = _entities[i];
            return temp;
        }
    }

    public class UnitecConfiguration
    {
        public string adminDisable;
        public string AvailableLanguages;
        public int bCryptWorkFactor;
        public string DefaultLanguage;
        public string EmailReplyTo;
        public bool useLDAP;
        public bool useDbEncryption;
        public int gradesToAllowExclusion;
        public string googleApiKey;
        public string LdapConnectionString;
        public string reminderEmailBody;
        public string reminderEmailSubject;
        public string credentialsEmailSubject;
        public string credentialsEmailBody;

        public string EmailSmtpAlternativeAlertAddress;
        public string EmailSmtpAlternativeHost;
        public string EmailSmtpAlternativePassword;
        public string EmailSmtpAlternativePort;
        public string EmailSmtpAlternativeSenderAddress;
        public string EmailSmtpAlternativeSenderName;
        public string EmailSmtpAlternativeUsername;
        public string EmailSmtpAlternativeUseSSL;
        public string EmailSmtpHost;
        public string EmailSmtpPassword;
        public string EmailSmtpPort;
        public string EmailSmtpSenderAddress;
        public string EmailSmtpSenderName;
        public string EmailSmtpUsername;
        public string EmailSmtpUseSSL;
        public string EncryptionKey;
        public int ForgotPasswordKeyDurationDays;
        public int InvitationKeyDurationDays;
        public string InvitationSiteAddress;
        public bool isTestSite;
        public string NotifyKey;
        public string NotifySiteAddress;
        public int PasswordHistoryCount;
        public int PasswordHistoryMinDays;
        public string RecoverSiteAddress;
        public double TokenDurationMinutes;
        public double TwoFactorAuthenticationTokenDurationMinutes;
        public string ZeroLanguage;
        public decimal Tolerance;
        public decimal ToleranceAbsolute;
    }
    public class ValidationException : Exception
    {
        public string errorCode;

        public ValidationException(string errorCode, string Message)
            : base(Message)
        {
            this.errorCode = errorCode;
        }
    }

    #endregion

    #region Evaluations

    public enum EmployeeAccessesDataOrdering
    {
        EmployeeName, Department, Contract, StaffDate, LastAccess, LastAccessDuration
    }

    public enum AssessorTableOrdering
    {
        Surname, AverageGrade, GradesReceived, Reminders, AutoOrManualPairing, DepartmentDescription,
        CurrentJob, ContractType, ContractStartDate, ContractEndDate, ContractDaysLeft, EmployeeRole,
        Workplace, Company, Commuter, ExternalEmployee, WorkplaceAlgorithm, ExtensionNotes
    }

    public enum EvaluationStatus
    {
        Imminent = 0,
        Underway = 1,
        Expired = 2,
        Closed = 3
    }

    public enum EvalueeTableOrdering
    {
        Surname, AverageGrade, GradesReceived, Reminders, AutoOrManualPairing, DepartmentDescription,
        CurrentJob, ContractType, ContractStartDate, ContractEndDate, ContractDaysLeft, EmployeeRole,
        Workplace, Company, IsProposedAssessor, Commuter, ExternalEmployee, WorkplaceAlgorithm, ExtensionNotes, IsDowngraded, yearAverage
    }

    public enum PairingStatus
    {
        Proposed = 0,
        Accepted = 1,
        Rejected = 2
    }

    public enum PromotionStatus
    {
        Proposed = 0,
        Accepted = 1,
        Rejected = 2
    }

    public struct GradeTrend
    {
        public const string Up = "up";
        public const string Down = "down";
        public const string Equal = "equal";
    }

    public class EmployeeAccessesData
    {
        public int EvaluationId { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployeeName { get; set; }
        public string Department { get; set; }
        public DateTime StaffDate { get; set; }
        public string Contract { get; set; }
        public DateTime? LastAccess { get; set; }
        public long? LastAccessDuration { get; set; }
        public bool HasPhoto { get; set; }
        public string Id => EmployeeNumber;
    }

    public class EmployeeAccessDetailsData
    {
        public int EvaluationId { get; set; }
        public DateTime LoginDate { get; set; }
        public int? VideoViewDuration { get; set; }
        public DateTime StaffDate { get; set; }
        public string CriterionDescription { get; set; }
        public decimal Grade { get; set; }
        public string Trend { get; set; }
        public string Notes { get; set; }
        public bool HasPhoto { get; set; }
        public int AccessId { get; set; }
        public string CriterionCode { get; set; }
    }

    public class EmployeeAccessDetailsDataSheet
    {
        //public EmployeeAccessDetailsDataSheet()
        //{
        //    CriterionNotes = new List<EmployeeAccessDetailsDataNotes>();
        //}

        public string EmployeeNumber { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string Nationality { get; set; }
        public string Domicile { get; set; }
        public string BloodType { get; set; }
        public DateTime? HiringDate { get; set; }
        public DateTime? TerminationDate { get; set; }
        public string ContractType { get; set; }
        public string Company { get; set; }
        public string Site { get; set; }
        public string Job { get; set; }
        public string ProtectedCategory { get; set; }
        public string ProtectedCategoryType { get; set; }
        public string Motivations { get; set; }
        public int? CompanyDistance { get; set; }
        public string Qualification { get; set; }
        public string TestResults { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }
        public int? SessionDuration { get; set; }
        public int? VideoViewDuration { get; set; }
        public int VideoViewsCount { get; set; }
        public DateTime StaffDate { get; set; }
        public string Notes { get; set; }
        public string PointImprovement { get; set; }
        public string RecognizeYourself { get; set; }
        //public ICollection<EmployeeAccessDetailsDataNotes> CriterionNotes { get; set; }
        public bool HasPhoto { get; set; }
        public int AccessId { get; set; }
    }

    public class EmployeeAccessDetailsDataNotes
    {
        public string Note { get; set; }
        public string CriterionCode { get; set; }
        public string CriterionDescription { get; set; }
    }

    public class EmployeeAccesses
    {
        public EmployeeAccesses()
        {
            EmployeeAccessDetails = new List<EmployeeAccessDetails>();
        }


        public int AccessId { get; set; }
        public DateTime LoginDate { get; set; }
        public long? LoginDuration { get; set; }
        public DateTime? LogoutDate { get; set; }
        public string Notes { get; set; }
        public string RecognizeYourself { get; set; }
        public string PointImprovement { get; set; }
        public string Approach { get; set; }
        public string ActivitiesPartecipation { get; set; }
        public string WillingnessInvolved { get; set; }
        public int EvaluationId { get; set; }
        public string EmployeeNumber { get; set; }

        public ICollection<EmployeeAccessDetails> EmployeeAccessDetails { get; set; }
    }

    public class EmployeeAccessDetails
    {
        public int AccessDetailId { get; set; }
        public string CriterionCode { get; set; }
        public int? VideoViewDuration { get; set; }
        public DateTime? VideoViewDate { get; set; }
        public decimal Grade { get; set; }
        public string Trend { get; set; }
        public int AccessId { get; set; }
    }

    public class EvaluationTrend
    {
        public EvaluationStatus Status { get; set; }
        public DateTime StaffDate { get; set; }
        public string CriterionCode { get; set; }
        public string CriterionDescription { get; set; }
        public string VideoPath { get; set; }
        public bool VideoEnable { get; set; }
        public DateTime? VideoDateView { get; set; }
        public string EmployeeNumber { get; set; }
        public decimal Grade { get; set; }
        public string AssessorId { get; set; }
        public string Trend { get; set; } = GradeTrend.Equal;
        public decimal? GradeTrendDifference { get; set; }
    }

    public class AssessorStatistics
    {
        public int evaluationsDone { get; set; }
        public int evaluationsPlanned { get; set; }
    }

    public class AuthenticationResult_LDAP
    {
        public bool authenticationSuccessful { get; set; }
        public Employee employeeData { get; set; }
        public string errorMessage { get; set; }
        public string userGroups { get; set; }
        public bool isHttpsLogin { get; set; }
        //public bool authenticationSuccessful { get; set; }
        //public bool authenticationSuccessful { get; set; }
    }

    public class Contract
    {
        public string company { get; set; }
        public DateTime contractEndDate { get; set; }
        public DateTime contractStartDate { get; set; }
        public string contractType { get; set; }
        public string employeeId { get; set; }
        public string extensionNotes { get; set; }
        public string extensions { get; set; }
        public int contractExpiresInDays { get; set; }
    }

    public class Contracts : EntityCollection<Contract>
    {
        public override Contract this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.employeeId == ID);
            }
        }
    }

    public class DefaultMessagesConfiguration
    {
        public string conf1;
    }

    public class Department
    {
        public string departmentCode { get; set; }
        public string departmentDescription { get; set; }
    }

    public class Departments : EntityCollection<Department>
    {
        public override Department this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.departmentCode == ID);
            }
        }
    }

    public class Employee
    {
        public DateTime BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public string BloodType { get; set; }
        public string Citizenship { get; set; }
        public bool Commuter { get; set; }
        public string Company { get; set; }
        public string ContractType { get; set; }
        public string CurrentJob { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentDescription { get; set; }
        public int DistanceFromWork { get; set; }
        public bool DoNotEvaluate { get; set; }
        public string Education { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployeeState { get; set; }
        public int ExtensionsLeft { get; set; }
        public DateTime HiringDate { get; set; }
        public string Home_Address1 { get; set; }
        public string Home_Address2 { get; set; }
        public string Home_CAP { get; set; }
        public string Home_City { get; set; }
        public string Home_Province { get; set; }
        public string Home_State { get; set; }
        public bool IsAssessor { get; set; }
        public bool IsHeadOfDepartment { get; set; }
        public bool IsDirector { get; set; }
        public string UserType { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }
        public int NumberOfExtensions { get; set; }
        public byte[] ObjectSID { get; set; }
        public byte[] Photo { get; set; }
        public bool ProbationaryPeriodCompleted { get; set; }
        public int RequiredMinDaysBusinessTrips { get; set; }
        public string RoleType { get; set; }
        public string SpecialCategory { get; set; }
        public string SpecialCategoryNotes { get; set; }
        public string SpecialCategoryType { get; set; }
        public string Surname { get; set; }
        public DateTime TerminationDate { get; set; }
        public string TestResult { get; set; }
        public int TotalBusinessTripDays { get; set; }
        public int TotalMonths { get; set; }
        public string Workplace { get; set; }
        public string WorkplaceAddress1 { get; set; }
        public string WorkplaceAddress2 { get; set; }
        public int WorkplaceAlgorithm { get; set; }
        public string WorkplaceCAP { get; set; }
        public string WorkplaceCity { get; set; }
        public string WorkplaceCountryCode { get; set; }
        public string WorkplaceProvince { get; set; }
        public string Password { get; set; }
        public Contract ContractData { get; set; }
        public string Email { get; set; }
        public bool ExternalEmployee { get; set; }
        public string ExtensionNotes { get; set; }
        public GradeStatistics GradeStats { get; set; }
        public bool isDowngraded { get; set; }
        public string TruancyRate { get; set; }
        public int RoleId { get; set; }
        public int OpenResearch { get; set; }
        public double yearAverage { get; set; }
        public List<int> PrivilegeIdList { get; set; }
        public string getFullName()
        {
            return this.Name + " " + this.Surname;
        }


        public Employee()
        {
            PrivilegeIdList = new List<int>();
        }
    }

    public class EmployeeAssessmentData
    {
        public string Company { get; set; }
        public string ContractType { get; set; }
        public string CurrentJob { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentDescription { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime HiringDate { get; set; }
        public string Name { get; set; }
        public byte[] Photo { get; set; }
        public string Surname { get; set; }
        public DateTime TerminationDate { get; set; }
    }

    public class EmployeeFilteringData
    {
        public decimal averageGrade;
        public string company;
        public DateTime contractEndDate;
        public int contractExpiresInDays;
        public DateTime contractStartDate;
        public string contractType;
        public string currentJob;
        public string departmentCode;
        public string departmentDescription;
        public string employeeId;
        public int gradesDone;
        public bool hasManualPairing;
        public bool isProposedAssessor;
        public DateTime mostRecentGrade;
        public string name;
        public int reminders;
        public string role;
        public string surname;
        public string workplace;
        public bool commuter;
        public bool externalEmployee;
        public int workplaceAlgorithm;
        public string extensionNotes;
        public bool isDowngraded;
        public int OpenResearch;
        public decimal YearAverage;
    }

    public class EmployeeHeader
    {
        public string employeeId;
        public string fullName;
    }

    public class EmployeeName
    {
        public string employeeId;
        public string name;
        public string surname;
        public string departmentCode;
    }

    public class EmployeeNames : EntityCollection<EmployeeName>
    {
        public override EmployeeName this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.employeeId == ID);
            }
        }

        public string toNameLines()
        {
            StringBuilder sb = new StringBuilder();

            if (Count > 0)
            {
                foreach (EmployeeName en in this.ToArray())
                    sb.Append(en.name + " " + en.surname + ", ");
            }

            return sb.ToString().Trim(',');
        }
    }

    public class EmployeePersonalInfo
    {
        public string EmployeeNumber { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }

    public class Employees : EntityCollection<Employee>
    {
        public override Employee this[string ID]
        {
            get
            {
                if (this.IsEmpty())
                    return null;
                return this.FirstOrDefault(x => x.EmployeeNumber == ID);
            }
        }

        public void removeEmployees(List<string> employeeIdList)
        {
            if (employeeIdList != null && employeeIdList.Count > 0)
            {
                foreach (string seil in employeeIdList)
                {
                    if (!string.IsNullOrWhiteSpace(seil))
                    {
                        Employee morituro = this[seil];
                        if (morituro != null)
                            this.Remove(morituro);
                    }
                }
            }
        }
    }

    public class ErrorLog
    {
        public string logDescription { get; set; }
        public string logMessage { get; set; }
        public string logType { get; set; }
    }

    public class Evaluation
    {
        public Evaluation()
        {
            departments = new Departments();
        }

        public DateTime ClosureDate { get; set; }
        public Departments departments { get; set; }
        public DateTime EndDate { get; set; }
        public int EvaluationId { get; set; }
        public string Notes { get; set; }
        public DateTime StaffDate { get; set; }
        public DateTime StartDate { get; set; }
        public EvaluationStatus Status { get; set; }
    }

    /// <summary>
    /// Entity for save Evaluation Summary with average
    /// value of year and total
    /// </summary>
    public class EvaluationSummary
    {
        public string EmployeeNumber { get; set; }

        public decimal TotalAverage { get; set; }

        public decimal YearAverage { get; set; }

        public DateTime ModifyDate { get; set; }


        public EvaluationSummary()
        {
            ModifyDate = new DateTime();
        }
    }

    public class EvaluationConnection
    {
        public string Commission { get; set; }
        public DateTime ConnectionDate { get; set; }
        public string ConnectionType { get; set; }
        public string EmployeeCode { get; set; }
        public string ODP { get; set; }
        public string Task { get; set; }
    }

    public class EvaluationConnectionFrequencies : EntityCollection<EvaluationConnectionFrequency>
    {
        public override EvaluationConnectionFrequency this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.employee1.ToString() == ID);
            }
        }

        public EvaluationConnectionFrequency findByCodes(string ID1, string ID2)
        {
            var theEnum = this.GetEnumerator();
            while (theEnum.MoveNext())
            {
                if ((theEnum.Current.employee1 == ID1 && theEnum.Current.employee2 == ID2) ||
                    (theEnum.Current.employee1 == ID2 && theEnum.Current.employee2 == ID1))
                    return theEnum.Current;
            }
            return null;
        }
    }

    public class EvaluationConnectionFrequency
    {
        public string employee1 { get; set; }
        public string employee2 { get; set; }
        public int frequency { get; set; }
    }

    public class EvaluationConnections : EntityCollection<EvaluationConnection>
    {
        public override EvaluationConnection this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.Task.ToString() == ID);
            }
        }
    }

    public class EvaluationCriteria : EntityCollection<EvaluationCriterion>
    {
        public override EvaluationCriterion this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.CriterionCode.ToString() == ID);
            }
        }

        public EvaluationCriterion findByCode(string code, out string parentCode)
        {
            IEnumerator<EvaluationCriterion> theEnum = this.GetEnumerator();
            while (theEnum.MoveNext())
            {
                if (theEnum.Current.CriterionCode.Equals(code))
                {
                    parentCode = null;
                    return theEnum.Current;
                }
                else
                {
                    EvaluationCriterion crit = theEnum.Current.Subcriteria[code];
                    if (crit != null)
                    {
                        parentCode = theEnum.Current.CriterionCode;
                        return crit;
                    }
                }
            }
            parentCode = null;
            return null;
        }
    }

    public class EvaluationCriteria_Multilanguage : EntityCollection<EvaluationCriterion_Multilanguage>
    {
        public override EvaluationCriterion_Multilanguage this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.CriterionCode.ToString() == ID);
            }
        }

        public EvaluationCriterion_Multilanguage findByCode(string code, out string parentCode)
        {
            IEnumerator<EvaluationCriterion_Multilanguage> theEnum = this.GetEnumerator();
            while (theEnum.MoveNext())
            {
                if (theEnum.Current.CriterionCode.Equals(code))
                {
                    parentCode = null;
                    return theEnum.Current;
                }
                else
                {
                    EvaluationCriterion_Multilanguage crit = theEnum.Current.Subcriteria[code];
                    if (crit != null)
                    {
                        parentCode = theEnum.Current.CriterionCode;
                        return crit;
                    }
                }
            }
            parentCode = null;
            return null;
        }
    }

    public class EvaluationCriterion
    {
        public EvaluationCriterion()
        {
            Subcriteria = new EvaluationCriteria();
        }

        public EvaluationCriterion(EvaluationCriterion_Multilanguage ecm)
        {
            CriterionCode = ecm.CriterionCode;
            CriterionWeight = ecm.CriterionWeight;
            ValidFrom = ecm.ValidFrom;
            ValidTo = ecm.ValidTo;
            Ordering = ecm.Ordering;
            Expanded = ecm.Expanded;
            MacroareaName = ecm.macroAreaName;
            MacroAreaOrder = ecm.macroAreaOrder;
            MacroAreaId = ecm.macroAreaId;
            Subcriteria = new EvaluationCriteria();
        }

        public string CriterionCode { get; set; }
        public string CriterionDescription { get; set; }
        public int CriterionWeight { get; set; }
        public bool Expanded { get; set; }
        public int Ordering { get; set; }
        public string MacroareaName { get; set; }
        public int MacroAreaOrder { get; set; }
        public int MacroAreaId { get; set; }
        public EvaluationCriteria Subcriteria { get; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
    }

    public class EvaluationCriterion_Multilanguage
    {
        public EvaluationCriterion_Multilanguage()
        {
            Subcriteria = new EvaluationCriteria_Multilanguage();
            CriterionDescription = new Dictionary<string, string>();
            VideoPaths = new Dictionary<string, string>();
            VideoEnables = new Dictionary<string, bool>();
        }

        public string CriterionCode { get; set; }
        public Dictionary<string, string> CriterionDescription { get; set; }
        public int CriterionWeight { get; set; }
        public bool Expanded { get; set; }
        public int Ordering { get; set; }
        public EvaluationCriteria_Multilanguage Subcriteria { get; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public Dictionary<string, string> VideoPaths { get; set; }
        public Dictionary<string, bool> VideoEnables { get; set; }
        public string macroAreaName { get; set; }
        public int macroAreaOrder { get; set; }
        public int macroAreaId { get; set; }
    }

    public class EvaluationData
    {
        public string conf1;
    }

    public class EvaluationDetail
    {
        public string AssessorCode { get; set; }
        public string AssessorName { get; set; }
        public string AssessorSurname { get; set; }
        public DateTime EvaluationDate { get; set; }
        public string EvaluationTermCode { get; set; }
        public string EvaluationTermDescription { get; set; }
        public decimal Grade { get; set; }
        public string UserCode { get; set; }
        public string UserFullName { get; set; }
    }

    public class EvaluationDetails : EntityCollection<EvaluationDetail>
    {
        public override EvaluationDetail this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.UserCode.ToString() == ID);
            }
        }
    }

    public class EvaluationGrades
    {
        public EvaluationGrades()
        {
            grades = new List<EvaluationTermsAndNotes>();
        }

        public decimal averageGrade { get; set; }
        public bool canSelfEvaluate { get; set; }
        public string employeeId { get; set; }
        public PromotionStatus? employeePromotionStatus { get; set; }
        public Evaluation evaluation { get; set; }
        public List<EvaluationTermsAndNotes> grades { get; set; }
        public int gradesDone { get; set; }
        public int gradesPlanned { get; set; }
        public int numberOfReminders { get; set; }
        public decimal pastAverage { get; set; }
        public bool canBePromoted { get; set; }
        public string AutoOrManual { get; set; }
        public Employee employeeData { get; set; }
        public bool hasChildRows { get; set; }
        public bool hasLogAccess { get; set; }

    }

    public class GradeStatistics
    {
        public decimal averageGrade { get; set; }
        public int gradesPlanned { get; set; }
        public int gradesDone { get; set; }
    }

    public class EvaluationHistory
    {
        public string conf1;
    }

    public class EvaluationPairing
    {
        public string AssessorId { get; set; }
        public string AutoOrManual { get; set; }
        public int EvaluationId { get; set; }
        public string EvalueeId { get; set; }
        public string NoteCode { get; set; }
        public string Notes { get; set; }
        public int PairingId { get; set; }
        public int RemindersSent { get; set; }
        public PairingStatus Status { get; set; }
    }

    public class EvaluationReceived
    {

        public int ContractDuration { get; set; }
        public DateTime ContractEndDate { get; set; }
        public DateTime ContractStartDate { get; set; }
        public string ContractType { get; set; }
        public string EmployeeNumber { get; set; }
        public DateTime EvaluationDate { get; set; }
        public int EvaluationId { get; set; }
        public string EvaluationNotes { get; set; }
        public DateTime NextEvaluation { get; set; }
        public int NumberOfAssessors { get; set; }
        public decimal Result { get; set; }
    }

    public class EvaluationRulesConfiguration
    {
        public double maximumAssignableRate { get; set; }
        public double minimumAssignableRate { get; set; }
    }

    public class Evaluations : EntityCollection<Evaluation>
    {
        public override Evaluation this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.EvaluationId.ToString() == ID);
            }
        }

        public List<string> getIdList()
        {
            List<string> ret = null;
            if (this.Any())
            {
                ret = this.Select(x => x.EvaluationId.ToString()).ToList();
            }
            return ret;
        }
    }

    public class EvaluationsReceived : EntityCollection<EvaluationReceived>
    {
        /// <summary>
        /// Deprecated. Use the DateTime search. Gets a specific evaluation 
        /// by specifying the evaluation date as a string 
        /// like this: theCollection[evalDate.ToString("s")]
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override EvaluationReceived this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.EvaluationDate.ToString() == ID);
            }
        }

        /// <summary>
        /// Gets a specific evaluation by date
        /// </summary>
        /// <param name="evaluationDate"></param>
        /// <returns></returns>
        public EvaluationReceived this[DateTime evaluationDate]
        {
            get
            {
                return this.FirstOrDefault(x => x.EvaluationDate == evaluationDate);
            }
        }
    }

    public class EvaluationTerm
    {
        public EvaluationTerm()
        {
            SubTerms = new EvaluationTerms();
        }

        public string AssessorId { get; set; }
        public int EvaluationId { get; set; }
        public string EvalueeId { get; set; }
        public decimal Grade { get; set; }
        public DateTime LastModified { get; set; }
        public EvaluationTerms SubTerms { get; set; }
        public string TermCode { get; set; }
        public string TermDescription { get; set; }
        public int TermId { get; set; }
        public double CriterionWeight { get; set; }
        public string MacroArea { get; set; }
    }

    public class EvaluationTerms : EntityCollection<EvaluationTerm>
    {
        public override EvaluationTerm this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.TermCode == ID);
            }
        }

        public EvaluationTerm this[int ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.TermId == ID);
            }
        }
    }

    public class EvaluationTermsAndNotes
    {
        public Employee Assessor { get; set; }
        public bool AssessorCanSelfEvaluate { get; set; }
        public AssessorStatistics AssessorStatistics { get; set; }
        public decimal AverageGrade { get; set; }
        public Employee Evaluee { get; set; }
        public bool EvalueeCanSelfEvaluate { get; set; }
        public EvaluationPairing Pairing { get; set; }
        public EvaluationTerms Terms { get; set; }
    }

    public class EvaluationType1Notes
    {
        public int evaluationId { get; set; }
        public string note { get; set; }
        public string userId { get; set; }
    }

    public class FilterModel
    {
        public FilterModel()
        {
            NamesList = new List<string>();
            SitesList = new List<string>();
            DepartmentList = new List<string>();
            WorkplaceAlgorithm = new List<int>();
            ContractType = new List<string>();
        }

        public List<string> DepartmentList { get; set; }
        public int FilterId { get; set; }
        public string FilterName { get; set; }
        public bool IsAllEvaluation { get; set; }
        public bool IsAssessorHidden { get; set; }
        public bool IsAverageEvaluation { get; set; }
        public bool IsDepartment { get; set; }
        public bool IsEvaluated { get; set; }
        public bool IsEvaluationDate { get; set; }
        public bool IsEvaluationReceived { get; set; }
        public bool IsEvaluationReminded { get; set; }
        //  public bool ISOpenResearch { get; set; }
        public bool IsExcludeCompiledEvaluations { get; set; }
        // Gruppo 1: cerca solo i voti ricevuti dai valutatori che hanno votato
        // Gruppo 1: cerca in tutte le valutazioni e non solo in quella specificata
        public bool IsEvaluationUnexpressed { get; set; }

        public bool IsMaximumExcluded { get; set; }
        // Gruppo 1: cerca solo le accoppiate e i voti (che sono nulli) dei valutatori che non hanno votato
        // Gruppo 1: cerca solo i voti ricevuti dai valutatori che sono stati sollecitati
        public bool IsMinimumExcluded { get; set; }

        public bool IsNames { get; set; }
        // Gruppo 2: elimina dai risultati il valutatore che ha dato il voto medio più basso
        // Gruppo 2: elimina dai risultati il valutatore che ha dato il voto medio più alto
        public bool IsNumberEvaluationReceived { get; set; }

        public bool IsOnlySuggestedAssessors { get; set; }
        //dice se considerare il parametro successivo
        //Lista dei reparti dei valutati da restituire
        public bool IsSite { get; set; }

        public bool IsStaffMemberHidden { get; set; }
        public double? MaximumAverageEvaluationNumber { get; set; }

        public int? MaximumEvaluationNumber { get; set; }

        // Elimina dai risultati i valutati che hanno ricevuto più valutazioni del parametro
        //dice se considerare i 2 parametri successivi
        public double? MinimumAverageEvaluationNumber { get; set; }

        //dice se considerare i 2 parametri successivi
        public int? MinimumEvaluationNumber { get; set; }  // Elimina dai risultati i valutati che hanno ricevuto meno valutazioni del parametro
                                                           //dice se considerare il parametro successivo
        public List<string> NamesList { get; set; }

        // Elimina dai risultati i valutati che hanno ricevuto un voto medio più basso del parametro
        // Elimina dai risultati i valutati che hanno ricevuto un voto medio più alto del parametro
        //dice se considerare il parametro successivo
        public int? NumberBeforeEvaluationDate { get; set; }   //Elimina dai risultati i valutati che hanno ricevuto una valutazione negli ultimi X giorni
                                                               //Lista dei nomi dei valutati da restituire
                                                               //dice se considerare il parametro successivo
        public List<string> SitesList { get; set; }             //Lista delle sedi dei valutati da restituire
                                                                //Restituisce solo i valutati che sono stati proposti per la promozione a valutatore o rifiutati

        public bool IsCommuter { get; set; }
        public bool IsContractType { get; set; }
        public bool IsExternalEmployee { get; set; }
        public bool IsWorkplaceAlgorithm { get; set; }
        public bool Commuter { get; set; }
        public List<string> ContractType { get; set; }
        public bool ExternalEmployee { get; set; }
        public List<int> WorkplaceAlgorithm { get; set; }
        public bool IsOpenResearch { get; set; }

        public bool IsColorEnable { get; set; }
        public bool IsWhite { get; set; }
        public bool IsRed { get; set; }
        public bool IsOrange { get; set; }
        public bool IsPurple { get; set; }
        public bool IsGreen { get; set; }
    }

    public class FilterModels : EntityCollection<FilterModel>
    {
        public override FilterModel this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.FilterName == ID);
            }
        }
    }

    public class GridColumn
    {
        public string columnName { get; set; }
        public bool visible { get; set; }
    }

    public class GridColumns : EntityCollection<GridColumn>
    {
        public override GridColumn this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.columnName == ID);
            }
        }
    }

    public class PlannedAssessment
    {
        public bool alreadyReminded { get; set; }
        public EvaluationCriteria evaluationCriteria { get; set; }
        public EvaluationPairing evaluationPairing { get; set; }
        public EvaluationTerms evaluationTerms { get; set; }
        public EmployeeAssessmentData evaluee { get; set; }
    }

    public class PlannedAssessments : EntityCollection<PlannedAssessment>
    {
        public override PlannedAssessment this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.evaluee.EmployeeNumber.ToString() == ID);
            }
        }
    }

    public class PrivacyConfiguration
    {
        public List<EmployeeHeader> employees;

        public PrivacyConfiguration()
        {
            employees = new List<EmployeeHeader>();
        }
    }
    public class PromotionRulesConfiguration
    {
        public double minimumAverageRate { get; set; }
        public double minimumSeniority { get; set; }
    }
    public class ProposedAssessor
    {
        public decimal averageGrade { get; set; }
        public Employee employeeData { get; set; }
        public int gradesDone { get; set; }
        public int gradesPlanned { get; set; }
        public decimal pastAverage { get; set; }
        public PromotionStatus promotionStatus { get; set; }
    }

    public class ProposedAssessors : EntityCollection<ProposedAssessor>
    {
        public override ProposedAssessor this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.employeeData.EmployeeNumber == ID);
            }
        }
    }

    public class Reminder
    {
        public string assessorId { get; set; }
        public int evaluationId { get; set; }
        public string evalueeId { get; set; }
        public DateTime reminderDate { get; set; }
        public int reminderId { get; set; }
    }

    public class Reminders : EntityCollection<Reminder>
    {
        public override Reminder this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.reminderId.ToString() == ID);
            }
        }

        public bool containsEvalueeId(string evalueeId)
        {
            return this.Any(x => x.evalueeId == evalueeId);
        }
    }

    public class RemindersConfiguration
    {
        public bool isEnabled { get; set; }
        public int numberDaysBeforeReminder { get; set; }
        public int numberEvaluationsBeforeReminder { get; set; }
    }

    public class StaffMember
    {
        public string employeeId;
        public string fullName;
    }

    public class StaffMembersConfiguration
    {
        public List<StaffMember> members;

        public StaffMembersConfiguration()
        {
            members = new List<StaffMember>();
        }
    }


    public class Type2NoteDefinition
    {
        public string noteCode { get; set; }
        public string noteText { get; set; }
    }


    public class Type2NoteDefinitions : EntityCollection<Type2NoteDefinition>
    {
        public override Type2NoteDefinition this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.noteCode == ID);
            }
        }
    }

    public class UserUISettings
    {
        public int assessorsPageSize { get; set; }
        public int evalueesPageSize { get; set; }
    }

    public class CustomFilterHeader
    {
        public int customFilterId;
        public string customFilterName;
    }

    public class CustomFilter
    {
        public CustomFilterHeader customFilterHeader;
        public FilterModel customFilterModel;

        public CustomFilter()
        {
            customFilterHeader = new CustomFilterHeader();
            customFilterModel = new FilterModel();
        }
    }

    public class CustomFilterHeaders : EntityCollection<CustomFilterHeader>
    {
        public override CustomFilterHeader this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.customFilterId.ToString() == ID);
            }
        }
    }

    /// <summary>
    /// Collection monthly messages
    /// </summary>
    public class MonthlyLoginMessages : EntityCollection<MonthlyLoginMessage>
    {
        public override MonthlyLoginMessage this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.Code == ID);
            }
        }
    }

    /// <summary>
    /// Monthly message
    /// </summary>
    public class MonthlyLoginMessage
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Message { get; set; }
    }

    /// <summary>
    /// Monthly message multilanguage
    /// </summary>
    public class MonthlyLoginMessages_Multilanguage : EntityCollection<MonthlyLoginMessage_Multilanguage>
    {
        public override MonthlyLoginMessage_Multilanguage this[string ID]
        {
            get
            {
                return this.FirstOrDefault(x => x.Code == ID);
            }
        }
    }

    /// <summary>
    /// Collection monthly messages multilanguage
    /// </summary>
    public class MonthlyLoginMessage_Multilanguage
    {
        public MonthlyLoginMessage_Multilanguage()
        {
            Messages = new Dictionary<string, string>();
        }

        public int Id { get; set; }

        public string Code { get; set; }

        public IDictionary<string, string> Messages { get; set; }
    }

    //#region messages multilanguages
    ///// <summary>
    ///// Collection messages
    ///// </summary>
    //public class LoginMessages : EntityCollection<LoginMessage>
    //{
    //    public override LoginMessage this[string ID]
    //    {
    //        get
    //        {
    //            return this.FirstOrDefault(x => x.LanguageCode == ID);
    //        }
    //    }
    //}

    ///// <summary>
    ///// message
    ///// </summary>
    //public class LoginMessage
    //{
    //    public string LanguageCode { get; set; }

    //    public string Message { get; set; }
    //}

    ///// <summary>
    ///// message multilanguage
    ///// </summary>
    //public class LoginMessages_Multilanguage : EntityCollection<LoginMessage_Multilanguage>
    //{
    //    public override LoginMessage_Multilanguage this[string ID]
    //    {
    //        get
    //        {
    //            return this.FirstOrDefault(x => x.LanguageCode == ID);
    //        }
    //    }
    //}

    ///// <summary>
    ///// Collection messages multilanguage
    ///// </summary>
    //public class LoginMessage_Multilanguage
    //{
    //    public LoginMessage_Multilanguage()
    //    {
    //        Messages = new Dictionary<string, string>();
    //    }

    //    public string LanguageCode { get; set; }

    //    public IDictionary<string, string> Messages { get; set; }
    //}
    //#endregion


    /// <summary>
    /// Logout video multilanguage
    /// </summary>
    public class LogoutVideo_Multilanguage
    {
        public int Id { get; set; }

        public string LanguageCode { get; set; }

        public string Path { get; set; }

        public bool Enable { get; set; }

        public bool VideoUploaded { get; set; } = false;

        public bool VideoDeleted { get; set; } = false;
    }
    #endregion

    #region Notifications


    public enum CP_NotificationConfirmationStatus
    {
        No = 0,
        Yes = 1,
        FinalNo = 2,
        Cancel = 3
    }

    public enum CP_NotificationRequestSetting
    {
        SendOnce = 0,
        SendAndWaitForConfirmation = 1,
        DontSendNow = 2
    }


    public class CP_SystemNotification
    {
        public int notificationId;
        public string templateId;
        public DateTime dateInserted;
        public string name;
        public string result;
        public string notificationEvent;
        public string notificationModule;
        public string notificationMethod;
        public string failureReason;
        public DateTime dateSent;
        public string recipientName;
        public string recipientAddress;
        public string htmlBody;
        public string plaintextBody;
        public string messageSubject;
        public string objectId;
        public string cpUsername;
        public string buttonOkText;
        public string buttonKoText;
        public string buttonFinalNoText;
        public DateTime confirmedOn;
        public CP_NotificationConfirmationStatus confirmationStatus;
        public DateTime sendOn;
        public string buttonOkUrl;
    }

    #endregion

    #region Users
    public class User
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
    }
    #endregion

    #region Privileges

    public class Privileges
    {
        public int PrivilegeId { get; set; }
        public int WorkplaceAlgorithm { get; set; }
        public string PrivilegeName { get; set; }
        public string PrivilegeNotes { get; set; }
    }


    #endregion
}


