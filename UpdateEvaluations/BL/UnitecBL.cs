﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Sockets;
using System.Security.Cryptography;

using System.Web;
using System.Xml;
using UnitecCommons;
using UpdateEvaluations.BE;
using UpdateEvaluations.DA;

namespace UpdateEvaluations.BL
{
    public class UnitecBL
    {
        #region Global members

        UnitecConfiguration applicationConfiguration;
        UnitecDA DA;
        LogFactory logFactory;
        /*
        public static Logger logger
        {
            get { return LogManager.GetCurrentClassLogger(); }
        }
        */
        Logger logger;

        #endregion

        #region Users
        public ICollection<User> getNoSystemUsers()
        {
            logger.Info("getNoSystemUsers");

            return DA.getNoSystemUsers();
        }

        public ICollection<User> getNoSystemUsersToAdd()
        {
            logger.Info("getNoSystemUsersToAdd");

            return DA.getNoSystemUsersToAdd();
        }

        public User getNoSystemUserById(string id)
        {
            logger.Info($"getNoSystemUserById - id: {id}");

            return DA.getNoSystemUserById(id);
        }

        public bool deleteNoSystemUser(string id)
        {
            logger.Info($"deleteNoSystemUser - id: {id}");

            return DA.deleteNoSystemUser(id);
        }

        public bool updateOrCreateNoSystemUser(User user)
        {
            logger.Info("updateOrCreateNoSystemUser");

            return DA.updateOrCreateNoSystemUser(user);
        }

        public string sendMailCredentials(string id)
        {
            var result = string.Empty;

            var u = getNoSystemUserById(id);
            if (u != null)
            {
                string emailBody = buildCredentialsBody(applicationConfiguration.credentialsEmailBody,
                                                    u.Name + " " + u.Lastname, u.Username, u.Password);

                result = SendMessage2(applicationConfiguration.credentialsEmailSubject, emailBody, emailBody.Replace("<br>", @"\r\n"), u.Email);
            }

            return result;
        }

        private decimal tolerance;

        private decimal toleranceAbsolute;
        #endregion

        #region Common methods

        public UnitecBL(string connectionString, UnitecConfiguration applicationConfiguration)
        {
            DA = new UnitecDA(connectionString, applicationConfiguration);
            this.applicationConfiguration = applicationConfiguration;
            logFactory = new LogFactory();
            //logFactory.Configuration = new NLog.Config.XmlLoggingConfiguration(@"D:\Cityphone\UnitecValutazionePersonale\UnitecCore\NLog.config", true, logFactory);
            logger = logFactory.GetCurrentClassLogger();
            if (string.IsNullOrWhiteSpace(applicationConfiguration.EmailSmtpHost))
                DA.getEmailConfiguration(this.applicationConfiguration);
            string subject, body;
            DA.getNotificationTemplate("StandardReminder", out subject, out body);
            this.applicationConfiguration.reminderEmailSubject = subject;
            this.applicationConfiguration.reminderEmailBody = body;
            this.tolerance = applicationConfiguration.Tolerance;
            this.toleranceAbsolute = applicationConfiguration.ToleranceAbsolute;

            DA.getNotificationTemplate("Credentials", out subject, out body);
            this.applicationConfiguration.credentialsEmailSubject = subject;
            this.applicationConfiguration.credentialsEmailBody = body;
        }

        #endregion

        #region Employee Accesses
        public EmployeeAccessDetailsDataSheet getEmployeesAccessDetailsDataSheet(int accessId, string lang)
        {
            logger.Info($"getEmployeesAccessDetailsDataSheet - accessId={accessId} - lang={lang}");

            return DA.getEmployeesAccessDetailsDataSheet(accessId, lang);
        }

        public ICollection<EmployeeAccessDetailsData> getEmployeesAccessDetailsData(int? evaluationId, string employeeNumber, string language)
        {
            logger.Info($"getEmployeesAccessesData - evaluationId={evaluationId} - employeeNumber={employeeNumber} - language={language}");

            ICollection<EmployeeAccessDetailsData> ret = DA.getEmployeesAccessDetailsData(evaluationId, employeeNumber, language);

            List<EmployeeAccessDetailsData> newRet = null;
            if (ret != null && ret.Count > 0)
            {
                string _trendToleranceAbsolute = System.Configuration.ConfigurationManager.AppSettings["TrendToleranceAbsolute"];
                decimal trendToleranceAbsolute = decimal.Parse(_trendToleranceAbsolute.Replace(".", ","));

                newRet = new List<EmployeeAccessDetailsData>();

                EvaluationGrades currEvaluationGrades = DA.getEvalueeGradesSimplified(getEvaluation(evaluationId.Value), getEmployee(employeeNumber, false), false, new Dictionary<string, Employee>(), new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), false);

                EvaluationGrades pastEvaluationGrades = null;
                var pastEvaluation = DA.getLastEvalueeHistoryRecord(employeeNumber, evaluationId.Value);
                if (pastEvaluation != null)
                    pastEvaluationGrades = DA.getEvalueeGradesSimplified(getEvaluation(pastEvaluation.EvaluationId), getEmployee(employeeNumber, false), false, new Dictionary<string, Employee>(), new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), false);

                foreach (EmployeeAccessDetailsData a in ret)
                {
                    EmployeeAccessDetailsData curr = a;
                    curr.Trend = DA.GetTrendAbsolutePerVoce(employeeNumber, (int)evaluationId, curr.CriterionCode, trendToleranceAbsolute, currEvaluationGrades.grades, pastEvaluationGrades.grades);

                    newRet.Add(curr);
                }
            }

            return (newRet == null ? ret : newRet);
        }

        public ICollection<EmployeeAccessesData> getEmployeesAccessesData(int? evaluationId, string[] employeeNumbers, int orderColumn, bool orderAsc, int start, int topRecord, out int totRecords)
        {
            logger.Info($"getEmployeesAccessesData - evaluationId={evaluationId}");

            var res = DA.getEmployeesAccessesData(evaluationId, employeeNumbers);

            switch (orderColumn)
            {
                case (int)EmployeeAccessesDataOrdering.Contract:
                    res = orderAsc
                        ? res.OrderBy(x => x.Contract).ThenByDescending(x => x.EmployeeName).ToList()
                        : res.OrderByDescending(x => x.Contract).ThenByDescending(x => x.EmployeeName).ToList();
                    break;
                case (int)EmployeeAccessesDataOrdering.Department:
                    res = orderAsc
                        ? res.OrderBy(x => x.Department).ThenByDescending(x => x.EmployeeName).ToList()
                        : res.OrderByDescending(x => x.Department).ThenByDescending(x => x.EmployeeName).ToList();
                    break;
                case (int)EmployeeAccessesDataOrdering.StaffDate:
                    res = orderAsc
                        ? res.OrderBy(x => x.StaffDate).ThenByDescending(x => x.EmployeeName).ToList()
                        : res.OrderByDescending(x => x.StaffDate).ThenByDescending(x => x.EmployeeName).ToList();
                    break;
                case (int)EmployeeAccessesDataOrdering.LastAccess:
                    res = orderAsc
                        ? res.OrderBy(x => x.LastAccess).ThenByDescending(x => x.EmployeeName).ToList()
                        : res.OrderByDescending(x => x.LastAccess).ThenByDescending(x => x.EmployeeName).ToList();
                    break;
                case (int)EmployeeAccessesDataOrdering.LastAccessDuration:
                    res = orderAsc
                        ? res.OrderBy(x => x.LastAccessDuration).ThenByDescending(x => x.EmployeeName).ToList()
                        : res.OrderByDescending(x => x.LastAccessDuration).ThenByDescending(x => x.EmployeeName).ToList();
                    break;
                default:
                    res = orderAsc
                        ? res.OrderBy(x => x.EmployeeName).ToList()
                        : res.OrderByDescending(x => x.EmployeeName).ToList();
                    break;
            }

            totRecords = res.Count();

            res = res.Skip(start).Take(topRecord).ToList();

            return res;
        }

        /// <summary>
        /// Current opened Access
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="evaluationId"></param>
        /// <returns></returns>
        public int GetCurrentAccessId(string employeeNumber, int evaluationId)
        {
            logger.Info($"GetCurrentAccessId - employeeNumber={employeeNumber} - evaluationId={evaluationId}");

            return DA.GetCurrentAccessId(employeeNumber, evaluationId);
        }

        /// <summary>
        /// Last closed Access
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="evaluationId"></param>
        /// <returns></returns>
        public int GetLastClosedAccessId(string employeeNumber, int evaluationId)
        {
            logger.Info($"GetLastClosedAccessId - employeeNumber={employeeNumber} - evaluationId={evaluationId}");

            return DA.GetLastClosedAccessId(employeeNumber, evaluationId);
        }

        /// <summary>
        /// Create a new access
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="evaluationId"></param>
        /// <returns></returns>
        public int InsertAccess(string employeeNumber, int evaluationId)
        {
            logger.Info($"InsertAccess - employeeNumber={employeeNumber} - evaluationId={evaluationId}");

            return DA.InsertAccess(employeeNumber, evaluationId);
        }

        /// <summary>
        /// Insert a notes to access
        /// </summary>
        /// <param name="accessId"></param>
        /// <param name="notes"></param>
        /// <param name="pointImprovement"></param>
        /// <param name="recognizeYourself"></param>
        /// <returns></returns>
        public bool InsertOrUpdateNotesAccess(int accessId, string notes, string pointImprovement, string recognizeYourself)
        {
            logger.Info($"InsertOrUpdateNotesAccess - accessId={accessId}");

            return DA.InsertOrUpdateNotesAccess(accessId, notes, pointImprovement, recognizeYourself);
        }

        /// <summary>
        /// Insert logout date and duration of access session
        /// </summary>
        /// <param name="accessId"></param>
        /// <returns></returns>
        public bool InsertLogoutAccess(int accessId)
        {
            logger.Info($"InsertLogoutAccess - accessId={accessId}");

            DateTime logoutDate = DateTime.Now;
            DateTime loginDate = DA.GetLoginDate(accessId);

            long loginDuration = Convert.ToInt64((logoutDate - loginDate).TotalSeconds);

            return DA.InsertLogoutAccess(accessId, logoutDate, loginDuration);
        }

        /// <summary>
        /// Create details for access
        /// </summary>
        /// <param name="details"></param>
        /// <param name="accessId"></param>
        /// <returns></returns>
        public bool InsertOrUpdateCriterionAccessDetail(EmployeeAccessDetails detail, int accessId)
        {
            logger.Info($"InsertCriterionAccessDetail (ONE) - accessId={accessId}");

            return DA.InsertOrUpdateCriterionAccessDetail(accessId, detail.CriterionCode,
                detail.VideoViewDuration, detail.Grade, detail.Trend);
        }

        /// <summary>
        /// Create details for access
        /// </summary>
        /// <param name="details"></param>
        /// <param name="accessId"></param>
        /// <returns></returns>
        public bool InsertOrUpdateCriterionAccessDetails(ICollection<EmployeeAccessDetails> details, int accessId)
        {
            logger.Info($"InsertCriterionAccessDetails (MANY) - accessId={accessId}");

            var result = false;
            DA.BeginTransaction();
            try
            {
                foreach (var d in details)
                {
                    result = DA.InsertOrUpdateCriterionAccessDetail(accessId, d.CriterionCode, d.VideoViewDuration, d.Grade, d.Trend);

                    if (!result)
                        break;
                }

                if (result)
                    DA.CommitTransaction();
                else
                {
                    DA.RollbackTransaction();

                    logger.Error("InsertCriterionAccessDetail - KO");
                }

                logger.Info("InsertCriterionAccessDetail - OK");
            }
            catch (Exception ex)
            {
                DA.RollbackTransaction();

                logger.Error("InsertCriterionAccessDetail - KO");
            }

            return result;
        }
        #endregion

        #region Evaluations

        /// <summary>
        /// Get evaluation term
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="lang"></param>
        /// <param name="evaluationId">if evaluation id is null, it's take the last</param>
        /// <param name="topRecord"></param>
        /// <returns></returns>
        public ICollection<EvaluationTrend> getEvaluationTerm(string employeeNumber, string lang,
            string[] criterionCode, int? evaluationId = null)
        {
            return DA.getEvaluationTerm(employeeNumber, lang, criterionCode, tolerance, evaluationId);
        }

        /// <summary>
        /// Get worst evaluation term
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="lang"></param>
        /// <param name="evaluationId">if evaluation id is null, it's take the last</param>
        /// <param name="topRecord"></param>
        /// <returns></returns>
        public ICollection<EvaluationTrend> getWorstEvaluationTerm(string employeeNumber, string lang,
            int? evaluationId = null, int? topRecord = 3)
        {
            /* var result = getEvaluationTrends(employeeNumber, lang, evaluationId);

             result = result.OrderBy(x => x.Grade).ThenBy(x => x.Trend).ToList();

             if (topRecord.HasValue)
                 result = result.Take(topRecord.Value).ToList();

             return result;*/

            return DA.getWorstEvaluationTerm(employeeNumber, lang, tolerance, evaluationId, topRecord);
        }

        /// <summary>
        /// Get worst Macro evaluation term
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="lang"></param>
        /// <param name="evaluationId">if evaluation id is null, it's take the last</param>
        /// <param name="topRecord"></param>
        /// <returns></returns>
        public ICollection<EvaluationTrend> getWorstMacroEvaluationTerm(string employeeNumber, string lang,
            int? evaluationId = null, int? topRecord = 1)
        {
            return DA.getWorstMacroEvaluationTerm(employeeNumber, lang, tolerance, toleranceAbsolute, evaluationId, topRecord);
        }

        /// <summary>
        /// Get worst Sub evaluation term
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="lang"></param>
        /// <param name="evaluationId">if evaluation id is null, it's take the last</param>
        /// <param name="topRecord"></param>
        /// <returns></returns>
        public ICollection<EvaluationTrend> getWorstSubEvaluationTerm(string employeeNumber, string lang,
            int? evaluationId = null, int? topRecord = 3)
        {
            return DA.getWorstSubEvaluationTerm(employeeNumber, lang, tolerance, toleranceAbsolute, evaluationId, topRecord);
        }

        /// <summary>
        /// Get best evaluation term
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="lang"></param>
        /// <param name="evaluationId">if evaluation id is null, it's take the last</param>
        /// <param name="topRecord"></param>
        /// <returns></returns>
        public ICollection<EvaluationTrend> getBestEvaluationTerm(string employeeNumber, string lang,
            int? evaluationId = null, int? topRecord = 3)
        {
            /*var result = getEvaluationTrends(employeeNumber, lang, evaluationId);

            result = result.OrderByDescending(x => x.Grade).ThenBy(x => x.Trend).ToList();

            if (topRecord.HasValue)
                result = result.Take(topRecord.Value).ToList();

            return result;*/

            return DA.getBestEvaluationTerm(employeeNumber, lang, tolerance, evaluationId, topRecord);
        }

        /// <summary>
        /// Get best evaluation term over grade limit
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="lang"></param>
        /// <param name="gradeLimit"></param>
        /// <param name="evaluationId">if evaluation id is null, it's take the last</param>
        /// <param name="topRecord"></param>
        /// <returns></returns>
        public ICollection<EvaluationTrend> getBestEvaluationTermOverGradeLimit(string employeeNumber,
            string lang, decimal gradeLimit,
            int? evaluationId = null, int? topRecord = 3)
        {
            /* var result = getEvaluationTrends(employeeNumber, lang, evaluationId);

             result = result.Where(x => x.Grade >= gradeLimit).OrderByDescending(x => x.Grade).ThenBy(x => x.Trend).ToList();

             if (topRecord.HasValue)
                 result = result.Take(topRecord.Value).ToList();

             return result;*/

            return DA.getBestEvaluationTermOverGradeLimit(employeeNumber, lang, gradeLimit, tolerance, toleranceAbsolute, evaluationId, topRecord);
        }

        private ICollection<EvaluationTrend> getEvaluationTrends(string employeeNumber, string lang,
            int? evaluationId = null)
        {
            if (!evaluationId.HasValue)
                evaluationId = getLastEvaluationId(employeeNumber);

            var eval = getEvaluation(evaluationId.Value);

            var accesId = GetCurrentAccessId(employeeNumber, evaluationId.Value);

            EvaluationCriteria_Multilanguage theCriteria = DA.getEvaluationCriteriaConfiguration(evaluationId.Value, true);

            int gradesDone = 0;
            EvaluationGrades ret = new EvaluationGrades();

            EvaluationTermsAndNotes theTerms = new EvaluationTermsAndNotes();

            theTerms.Terms = DA.getEvaluationTerms(evaluationId.Value, employeeNumber, null, theCriteria);

            if (theTerms.Terms.Count > 0)
            {
                theTerms.AverageGrade = DA.computeAverage(theTerms.Terms);
                if (theTerms.AverageGrade > 0)
                    gradesDone++;
            }
            ret.grades.Add(theTerms);

            if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);

            ICollection<EvaluationTrend> result = new List<EvaluationTrend>();

            foreach (var c in theCriteria)
            {
                var r = ret.grades.FirstOrDefault(x => x.Terms.Any(y => y.TermCode == c.CriterionCode));

                if (r != null)
                {
                    var g = r.Terms.First(x => x.TermCode == c.CriterionCode);
                    bool videoEnable = false;
                    string videoPath = string.Empty;
                    string description = string.Empty;
                    c.VideoEnables.TryGetValue(lang, out videoEnable);
                    c.VideoPaths.TryGetValue(lang, out videoPath);
                    c.CriterionDescription.TryGetValue(lang, out description);

                    result.Add(new EvaluationTrend
                    {
                        CriterionCode = c.CriterionCode,
                        CriterionDescription = description,
                        EmployeeNumber = employeeNumber,
                        Grade = g.Grade,
                        Trend = DA.GetTrend(employeeNumber, evaluationId.Value, tolerance),
                        StaffDate = eval.StaffDate,
                        VideoEnable = videoEnable,
                        VideoPath = videoPath,
                        Status = eval.Status,
                        VideoDateView = DA.getVideoViewDate(accesId, c.CriterionCode)
                    });
                }
            }

            return result;
        }

        public DateTime getVideoViewDate(int accessId, string criterionCode)
        {
            return DA.getVideoViewDate(accessId, criterionCode);
        }

        private static void SetWorstVoices(int? topRecord, EvaluationTerm sv, ref int countWorst, ref List<EvaluationTerm> worstVoice)
        {
            if (worstVoice.Count < topRecord.Value)
            {
                var value = sv.Grade.ToString("#.00", CultureInfo.InvariantCulture);
                if (value != ".00")
                {
                    countWorst = countWorst + 1;
                    worstVoice.Add(sv);
                    worstVoice = worstVoice.OrderByDescending(w => w.Grade).ToList();
                }
            }
            else
            {
                var value = sv.Grade.ToString("#.00", CultureInfo.InvariantCulture);
                if (value != ".00")
                {
                    countWorst = countWorst + 1;
                    if (double.Parse(worstVoice.FirstOrDefault().Grade.ToString("#.00", CultureInfo.InvariantCulture)) >
                        double.Parse(value.Replace('.', ',')))
                    {
                        worstVoice.Remove(worstVoice.First());
                        worstVoice.Add(sv);
                        worstVoice = worstVoice.OrderByDescending(w => w.Grade).ToList();
                    }
                }
            }
        }

        public ICollection<EvaluationTrend> getWorstEvaluationTermSameGrade(string employeeNumber, string lang, decimal grade,
            int? evaluationId = null, int? topRecord = 3, string[] criteriaToExclude = null)
        {
            return DA.getWorstEvaluationTermSameGrade(employeeNumber, lang, grade, tolerance, evaluationId, topRecord, criteriaToExclude);
        }

        /// <summary>
        /// Add or Update record in EvaluationSummary
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="totalAverage"></param> 
        /// <param name="yearAverage"></param>
        public void addOrUpdateEvaluationSummary(Employee employee, decimal totalAverage, decimal yearAverage)
        {
            EvaluationSummary evaluationSummary = getEvaluationSummary(employee.EmployeeNumber);

            if (evaluationSummary == null)
                DA.insertEvaluationSummary(employee.EmployeeNumber, totalAverage, yearAverage);
            else
                DA.updateEvaluationsSummary(new EvaluationSummary()
                {
                    EmployeeNumber = employee.EmployeeNumber,
                    TotalAverage = totalAverage,
                    YearAverage = yearAverage
                });
        }

        public void addEvaluationsSummary(Employee employee, decimal totalAverage, decimal yearAverage)
        {
            DA.insertEvaluationSummary(employee.EmployeeNumber, totalAverage, yearAverage);
        }

        public void updateEvaluationsSummary(EvaluationSummary evaluationSummary)
        {
            DA.updateEvaluationsSummary(evaluationSummary);
        }

        public EvaluationSummary getEvaluationSummary(string employeeNumber)
        {
            return DA.getEvaluationsSummary(employeeNumber);

        }


        /// <summary>
        /// Calculate Evaluation Summary
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public decimal calculateEvaluationSummary(EvaluationsReceived evaluationReceived, int? year, int? month)
        {
            EvaluationsReceived history = evaluationReceived;

            List<EvaluationReceived> historyList = history
                .ToList()
                .Where(s => s.Result > 0)
                .OrderBy(s => s.EvaluationDate)
                .ToList();

            if (year.HasValue && !month.HasValue)
                historyList = historyList
                    .Where(s => s.EvaluationDate.Year == year).ToList();

            if (year.HasValue && month.HasValue)
            {
                DateTime startDate = new DateTime(year.Value - 1, month.Value, 1);

                historyList = historyList.Where(s => s.EvaluationDate >= startDate)
                    .ToList();
            }

            var average = 0.0m;

            historyList.ForEach(s => average += s.Result);

            if (historyList.Count > 0)
                return average / historyList.Count;
            else
                return 0m;

        }

        /// <summary>
        /// Delete Evaluations Summary
        /// </summary>
        /// <param name="employeeNumber"></param>
        public void deleteEvaluationsSummary(string employeeNumber)
        {
            DA.deleteEvaluationSummary(employeeNumber);
        }



        /// <summary>
        /// Update both year and total average 
        /// </summary>
        /// <param name="employees"></param>
        /// <param name="evaluationDate"></param>
        public void updateEvaluationsSummaries(Employees employees, DateTime evaluationDate)
        {
            foreach (Employee employee in employees)
            {
                try
                {
                    EvaluationsReceived evaluationReceived = getEvalueeHistoryWithoutJoin(employee.EmployeeNumber);

                    decimal yearAverage = calculateEvaluationSummary(evaluationReceived, evaluationDate.Year, evaluationDate.Month);
                    decimal totalAverage = calculateEvaluationSummary(evaluationReceived, null, null);
                    logger.Info($"Updating employee n.{employee.EmployeeNumber}");

                    if (yearAverage > 0 || totalAverage > 0)
                        addOrUpdateEvaluationSummary(employee, totalAverage, yearAverage);
                }
                catch (Exception ex)
                {
                    logger.Error($"Error during update summary for employee : {ex}");
                }

            }
        }

        public void addEvalueesToEmptyEvaluations()
        {
            Evaluations allEval = getAllEvaluations();
            foreach (Evaluation ev in allEval)
            {
                if (ValueOperations.isNullOrEmpty(getAllEvalueesIdList(ev.EvaluationId)))
                {
                    List<string> evalueesFromHistory = DA.getEvalueesFromHistory(ev.StartDate, ev.EndDate);
                    if (!ValueOperations.isNullOrEmpty(evalueesFromHistory))
                    {
                        foreach (string s in evalueesFromHistory)
                        {
                            DA.insertEvalueeProposedPromotion(ev.EvaluationId, s, false);
                        }
                    }
                }
            }
        }


   

        public string buildReminderBody(string reminderTemplate, string recipientName, string evalueesNames)
        {
            string ret = reminderTemplate;
            ret = ret.Replace("((REM01))", recipientName);
            ret = ret.Replace("((REM02))", evalueesNames);
            return ret;
        }

        public string buildCredentialsBody(string reminderTemplate, string recipientName, string username, string password)
        {
            string ret = reminderTemplate;
            ret = ret.Replace("((REM01))", recipientName);
            ret = ret.Replace("((REM03))", username);
            ret = ret.Replace("((REM04))", password);
            return ret;
        }

        public bool canSelfEvaluate(int evaluationId, string employeeId)
        {
            return DA.canSelfEvaluate(evaluationId, employeeId);
        }

        /// <summary>
        /// Closes the current evaluation
        /// </summary>
        public void closeCurrentEvaluation()
        {
            logger.Info("CloseCurrentEvaluation");
            DA.updateEvaluationStatus(DA.getCurrentEvaluationId(), EvaluationStatus.Closed);
        }

        public void closeEvaluation(int evaluationId)
        {
            logger.Info("CloseEvaluation");
            DA.updateEvaluationStatus(evaluationId, EvaluationStatus.Closed);
        }

        //public decimal computeAverage(EvaluationTerms terms)
        //{
        //    decimal total = 0;
        //    int numTerms = 0;
        //    double totalWeight = 0;
        //    foreach (EvaluationTerm et in terms)
        //    {
        //        decimal subtotal = 0;
        //        int numSubterms = 0;
        //        if (et.SubTerms.Any())
        //        {
        //            foreach (EvaluationTerm st in et.SubTerms)
        //            {
        //                if (st.Grade > 0)
        //                {
        //                    subtotal += st.Grade;
        //                    numSubterms++;
        //                }
        //            }
        //        }

        //        if (et.Grade > 0)
        //        {
        //            subtotal += et.Grade;
        //            numSubterms++;
        //        }
        //        else
        //        {
        //            if (subtotal > 0 && numSubterms > 0)
        //                et.Grade = subtotal / numSubterms;
        //            else
        //                et.Grade = 0;
        //        }

        //        if (et.CriterionWeight > 0 && numSubterms > 0)
        //        {
        //            total += (subtotal / numSubterms) * Convert.ToDecimal(et.CriterionWeight);
        //            totalWeight += et.CriterionWeight;
        //            numTerms++;
        //        }

        //    }

        //    if (numTerms > 0 && totalWeight > 0)
        //        return total / Convert.ToDecimal(totalWeight);
        //    else
        //        return 0;
        //}

      

        public DateTime? computeNextEvaluationDate(Employee emp, out string nextDateResult)
        {
            logger.Info("compute next evaluation date for " + emp.EmployeeNumber);
            DateTime? ret = null;
            DateTime nextEvaluationDate = DA.getNextEvaluationStaffDate();
            if ("debug".Equals(emp.EmployeeNumber) ||
                "demo".Equals(emp.EmployeeNumber) ||
                "lenis".Equals(emp.EmployeeNumber))
            {
                nextDateResult = "debug user";
                return null;
            }
            nextDateResult = "Inactive";
            if ("Terminato".Equals(emp.EmployeeState))
                nextDateResult = "Terminated";
            if (emp.DoNotEvaluate)
                nextDateResult = "Do not evaluate";

            Contract con = DA.getCurrentEmployeeContract(emp.EmployeeNumber);
            if (con.contractStartDate > nextEvaluationDate)
            {
                nextDateResult = "Hiring date is in the future";
                emp.DoNotEvaluate = true;
            }

            if (!(emp.DoNotEvaluate || "Inattivo".Equals(emp.EmployeeState) || "Terminato".Equals(emp.EmployeeState)))
            {
                switch (emp.WorkplaceAlgorithm)
                {
                    case 0:
                        if (emp.Commuter)
                        {
                            ret = nextEvaluationDate;
                            nextDateResult = "Workplace algorithm 0 - Commuter. Next evaluation date: " + nextEvaluationDate;
                        }
                        else
                        {
                            if (emp.ContractType.Equals("Tempo Indeterminato") || emp.ContractType.Equals("T.I."))
                            {
                                if (emp.ProbationaryPeriodCompleted)
                                {
                                    ret = DA.getNextEvaluationStaffDateForDepartment(emp.DepartmentCode);
                                    nextDateResult = "Workplace algorithm 0 - Permanent contract - Probationary period completed. Next evaluation date for the department " + emp.DepartmentCode + ": " + ret;
                                }
                                else
                                {
                                    ret = nextEvaluationDate;
                                    nextDateResult = "Workplace algorithm 0 - Permanent contract - Probationary period not completed. Next evaluation date: " + ret;
                                }
                            }
                            else
                            {
                                if (con != null)
                                {
                                    EvaluationReceived received = DA.getLastEvalueeHistoryRecord(emp.EmployeeNumber, getCurrentEvaluationId(), true);
                                    DateTime lastEvaluationDate = ValueOperations.getEpoch();
                                    if (received != null)
                                        lastEvaluationDate = received.EvaluationDate;

                                    //if ((nextEvaluationDate - con.contractStartDate).TotalDays < 120)
                                    if (con.contractStartDate.AddMonths(4) >= lastEvaluationDate || con.contractStartDate.AddMonths(4).Month == lastEvaluationDate.Month)
                                    {
                                        ret = nextEvaluationDate;
                                        nextDateResult = "Workplace algorithm 0 - Fixed-term contract - Less than 4 months since hiring. Next evaluation date: " + ret;
                                    }
                                    else
                                    {
                                        //if ((con.contractEndDate - nextEvaluationDate).TotalDays <= 120)
                                        if (lastEvaluationDate.AddMonths(4) >= con.contractEndDate || lastEvaluationDate.AddMonths(4).Month >= con.contractEndDate.Month)
                                        {
                                            ret = nextEvaluationDate;
                                            nextDateResult = "Workplace algorithm 0 - Fixed-term contract - Less than 4 months to termination. Next evaluation date: " + ret;
                                        }
                                        else if (lastEvaluationDate.AddYears(1) < con.contractEndDate) //else if ((con.contractEndDate - nextEvaluationDate).TotalDays >= 365)
                                        {
                                            //if (lastEvaluationDate.AddMonths(3) < nextEvaluationDate)
                                            if (lastEvaluationDate.AddMonths(3) < nextEvaluationDate || lastEvaluationDate.AddMonths(3).Month == nextEvaluationDate.Month)
                                            {
                                                ret = nextEvaluationDate;
                                                nextDateResult = "Workplace algorithm 0 - Fixed-term contract - More than 1 year to termination - More than 3 months since last evaluation. Next evaluation date: " + ret;
                                            }
                                            else
                                            {
                                                ret = lastEvaluationDate.AddMonths(3);
                                                nextDateResult = "Workplace algorithm 0 - Fixed-term contract - More than 1 year to termination - Less than 3 months since last evaluation. Next evaluation date: " + ret;
                                            }
                                        }
                                        else
                                        {
                                            //if (lastEvaluationDate.AddMonths(2) < nextEvaluationDate)
                                            if (lastEvaluationDate.AddMonths(2) < nextEvaluationDate || lastEvaluationDate.AddMonths(2).Month == nextEvaluationDate.Month)
                                            {
                                                ret = nextEvaluationDate;
                                                nextDateResult = "Workplace algorithm 0 - Fixed-term contract - Less than 1 year to termination - More than 2 months since last evaluation. Next evaluation date: " + ret;
                                            }
                                            else
                                            {
                                                ret = lastEvaluationDate.AddMonths(2);
                                                nextDateResult = "Workplace algorithm 0 - Fixed-term contract - Less than 1 year to termination - Less than 2 months since last evaluation. Next evaluation date: " + ret;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case 1:
                        if (!string.IsNullOrEmpty(emp.ContractType) && (emp.ContractType.Equals("Tempo Indeterminato") || emp.ContractType.Equals("T.I.")))
                        {
                            ret = nextEvaluationDate;
                            nextDateResult = "Workplace algorithm 1 - Permanent. Next evaluation date: " + ret;
                        }
                        else
                        {
                            ret = nextEvaluationDate;
                            nextDateResult = "Workplace algorithm 1 - Fixed-term contract. Next evaluation date: " + ret;
                        }
                        break;
                    case 2:
                        if (!string.IsNullOrEmpty(emp.ContractType) && (emp.ContractType.Equals("Tempo Indeterminato") || emp.ContractType.Equals("T.I.")))
                        {
                            ret = nextEvaluationDate;
                            nextDateResult = "Workplace algorithm 2 - Permanent contract. Next evaluation date: " + ret;
                        }
                        else
                        {
                            ret = nextEvaluationDate;
                            nextDateResult = "Workplace algorithm 2 - Fixed-term contract. Next evaluation date: " + ret;
                        }
                        break;
                }
            }

            if (ret.HasValue)
                logger.Info("next evaluation Date=" + ret.Value.ToString("s"));
            return ret;
        }

        public void deleteEvaluation(int evaluationId)
        {
            logger.Info("delete evaluation " + evaluationId);
            DA.BeginTransaction();
            try
            {
                DA.deleteEvaluation(evaluationId);
                DA.deleteDepartmentsToEvaluate(evaluationId);
                DA.CommitTransaction();
                logger.Info("evaluation deleted");
            }
            catch (Exception ex)
            {
                DA.RollbackTransaction();
            }
        }

        /// <summary>
        /// Delete the pairing between assessor and evaluee (when manually 
        /// pairing the two)
        /// </summary>
        /// <param name="assessor"></param>
        /// <param name="evaluee"></param>
        public void deleteEvaluationPairing(int evaluationId, string assessorId, string evalueeId)
        {
            logger.Info("delete evaluation pairing (" + evaluationId + "," + assessorId + "," + evalueeId + ")");
            DA.deleteEvaluationPairing(evaluationId, assessorId, evalueeId);
            logger.Info("Pairing deleted");
            updateGradeStatistics(evaluationId, evalueeId);
            updateAssessorGradeStatistics(evaluationId, assessorId);
        }

        public void deleteSelfEvaluationPairing(int evaluationId, string employeeId)
        {
            logger.Info("delete self evaluation pairing (" + evaluationId + "," + employeeId + ")");
            DA.deleteEvaluationPairing(evaluationId, employeeId, employeeId);
            logger.Info("pairing deleted");
            updateGradeStatistics(evaluationId, employeeId);
            updateAssessorGradeStatistics(evaluationId, employeeId);
        }

        public void deleteType2Note(string noteCode)
        {
            logger.Info("delete type 2 note " + noteCode);
            DA.deleteType2Note(noteCode);
            logger.Info("note deleted");
        }

        public void generateReminders_Automatic()
        {
            logger.Info("generating reminders");
            RemindersConfiguration conf = getRemindersConfiguration();

            if (conf.isEnabled)
            {
                int evaluationId = getCurrentEvaluationId();
                Evaluation currentEvaluation = getEvaluation(evaluationId);
                if ((currentEvaluation.EndDate - DateTime.Now).TotalDays < conf.numberDaysBeforeReminder &&
                    (currentEvaluation.EndDate - DateTime.Now).TotalDays + 1 > conf.numberDaysBeforeReminder)
                {
                    Reminders theReminders = new Reminders();
                    int totalEmployees, totalEmployeesFiltered;
                    List<EvaluationGrades> theGrades = getAllEvalueeGrades(evaluationId, null, 0, 500000,
                            EvalueeTableOrdering.Surname, true, out totalEmployees, out totalEmployeesFiltered, false);

                    if (!ValueOperations.isNullOrEmpty(theGrades))
                    {
                        foreach (EvaluationGrades g in theGrades)
                        {
                            if (g.gradesDone < conf.numberEvaluationsBeforeReminder)
                            {
                                //Notify all the assessors who didn't yet grade the evaluee
                                if (!ValueOperations.isNullOrEmpty(g.grades))
                                {
                                    foreach (EvaluationTermsAndNotes tn in g.grades)
                                    {
                                        if (tn.Terms == null || tn.Terms.Count == 0)
                                        {
                                            theReminders.Add(new Reminder()
                                            {
                                                assessorId = tn.Pairing.AssessorId,
                                                evaluationId = tn.Pairing.EvaluationId,
                                                evalueeId = tn.Pairing.EvalueeId,
                                                reminderDate = DateTime.Now
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }

                    sendReminders(theReminders);
                }
            }
        }

        public List<EvaluationGrades> getAssessorGradesTable(int evaluationId, FilterModel filter, int offset, int pageSize,
                                                            AssessorTableOrdering ordering, bool ascendingOrder,
                                                            out int totalEmployees, out int totalEmployeesFiltered, bool getPhoto)
        {
            if (filter != null && (filter.IsMaximumExcluded || filter.IsMinimumExcluded))
                return getAllAssessorGrades(evaluationId, filter, offset, pageSize, ordering, ascendingOrder,
                                                    out totalEmployees, out totalEmployeesFiltered, getPhoto);
            else
                return getAssessorGradesTable_filtered(evaluationId, filter, offset, pageSize, ordering, ascendingOrder,
                                                    out totalEmployees, out totalEmployeesFiltered, getPhoto);
        }


        public List<EvaluationGrades> getAllAssessorGrades(int evaluationId, FilterModel filter, int offset, int pageSize,
                                                        AssessorTableOrdering ordering, bool ascendingOrder,
                                                        out int totalEmployees, out int totalEmployeesFiltered, bool getPhoto)
        {
            logger.Info("getAllAssessorGrades of evaluation " + evaluationId);
            List<EvaluationGrades> ret = new List<EvaluationGrades>();

            List<string> theEmployeeIdList = new List<string>();
            List<EmployeeFilteringData> filteringData = DA.getAssessorFilteringData(evaluationId);

            totalEmployees = filteringData.Count;
            totalEmployeesFiltered = filteringData.Count;

            if (filter != null)
            {
                /*
                 * IsDepartment
                 * Only return the evaluees from the specified departments
                 */
                if (filter.IsDepartment && filteringData.Any())
                {
                    filteringData = (from x in filteringData
                                     where (filter.DepartmentList.Contains(x.departmentCode) ||
                                         filter.DepartmentList.Contains(x.departmentDescription))
                                     select x).ToList();
                }

                /*
                 * IsNames
                 * Only return the evaluees with the specified names
                 */
                if (filter.IsNames && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.NamesList.Contains((x.surname + " " + x.name).ToUpper()) select x).ToList();
                }

                /*
                 * IsSites
                 * Only return the evaluees with the specified sites
                 */
                if (filter.IsSite && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.SitesList.Contains(x.workplace) select x).ToList();
                }

                /*
                 * IsStaffMemberHidden
                 * Only return the evaluees which are not staff members
                 */
                if (filter.IsStaffMemberHidden && filteringData.Any())
                {
                    StaffMembersConfiguration staffMembers = getStaffMembersConfiguration();
                    IEnumerable<string> staffMemberIdList = from x in staffMembers.members select x.employeeId;
                    filteringData = (from x in filteringData where !staffMemberIdList.Contains(x.employeeId) select x).ToList();
                }

                /*
                 * IsEvaluationDate
                 * Only return the evaluees who received grades in the last X days
                 */
                if (filter.IsEvaluationDate && filter.NumberBeforeEvaluationDate.HasValue && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.mostRecentGrade > DateTime.Now.AddDays(-filter.NumberBeforeEvaluationDate.Value) select x).ToList();
                }


                /*
                 * IsEvaluationReceived
                 * Only return the evaluees who have received grades
                 *
                 * IsEvaluationReminded
                 * Only return the evaluees whose assessors have received reminders
                 *
                 * IsEvaluationUnexpressed
                 * Only return the evaluees who have not received grades
                 */
                if (!filter.IsAllEvaluation)
                {
                    if (filteringData.Any())
                    {
                        if (filter.IsEvaluationReminded)
                        {
                            if (filter.IsEvaluationReceived)
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 select x).ToList();
                                }
                                else
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 && x.gradesDone > 0 select x).ToList();
                                }
                            }
                            else
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 && x.gradesDone == 0 select x).ToList();
                                }
                                else
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 select x).ToList();
                                }
                            }
                        }
                        else
                        {
                            if (filter.IsEvaluationReceived)
                            {
                                if (!filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.gradesDone > 0 select x).ToList();
                                }
                            }
                            else
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.gradesDone == 0 select x).ToList();
                                }
                            }
                        }
                    }
                }

                /*
                 * IsNumberEvaluationReceived
                 * Return only the evaluees who have been graded by a number of assessors in the specified bounds
                 */
                if (filter.IsNumberEvaluationReceived && filteringData.Any())
                {
                    int maxValue = filter.MaximumEvaluationNumber.HasValue ? filter.MaximumEvaluationNumber.Value : 9999;
                    int minValue = filter.MinimumEvaluationNumber.HasValue ? filter.MinimumEvaluationNumber.Value : -1;
                    filteringData = (from x in filteringData where x.gradesDone >= minValue && x.gradesDone <= maxValue select x).ToList();
                }

                /*
                 * Commuter
                 * Return only the evaluees which are commuters
                 */
                if (filter.IsCommuter && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.commuter == filter.Commuter select x).ToList();
                }

                /*
                 * ExternalEmployee
                 * Return only the evaluees which are external
                 */
                if (filter.IsExternalEmployee && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.externalEmployee == filter.ExternalEmployee select x).ToList();
                }

                /*
                 * WorkplaceAlgorithm
                 * Return only the evaluees which have the specified workplace algorithm
                 */
                if (filter.IsWorkplaceAlgorithm && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.WorkplaceAlgorithm.Contains(x.workplaceAlgorithm) select x).ToList();
                }

                /*
                 * ContractType
                 * Return only the evaluees which have the specified contract type
                 */
                if (filter.IsContractType && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.ContractType.Contains(x.contractType) select x).ToList();
                }
                if (filter.IsOpenResearch && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.OpenResearch == 1 select x).ToList();
                }
            }

            totalEmployeesFiltered = filteringData.Count;

            if (filteringData.Any())
            {
                switch (ordering)
                {
                    case AssessorTableOrdering.AutoOrManualPairing:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.hasManualPairing).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.hasManualPairing).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.AverageGrade:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.averageGrade).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.averageGrade).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Company:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.company).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.company).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ContractDaysLeft:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractExpiresInDays).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractExpiresInDays).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ContractEndDate:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractEndDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractEndDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ContractStartDate:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractStartDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractStartDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ContractType:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractType).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractType).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.CurrentJob:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.currentJob).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.currentJob).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.DepartmentDescription:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.departmentDescription).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.departmentDescription).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.EmployeeRole:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.role).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.role).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.GradesReceived:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.gradesDone).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.gradesDone).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Reminders:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.reminders).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.reminders).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Surname:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Workplace:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.workplace).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.workplace).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.WorkplaceAlgorithm:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.workplaceAlgorithm).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.workplaceAlgorithm).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Commuter:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.commuter).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.commuter).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ExternalEmployee:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.externalEmployee).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.externalEmployee).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ExtensionNotes:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.extensionNotes).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.extensionNotes).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                }

                List<string> employeeIdSorted = filteringData.Select(x => x.employeeId).ToList();
                Employees theEmployees = new Employees();
                if (!ValueOperations.isNullOrEmpty(employeeIdSorted))
                {
                    List<Employee> employeesUnsorted = DA.getEmployees(employeeIdSorted, true).ToList();
                    if (!ValueOperations.isNullOrEmpty(employeesUnsorted))
                        employeesUnsorted = employeesUnsorted.OrderBy(x => employeeIdSorted.IndexOf(x.EmployeeNumber)).ToList();
                    if (!ValueOperations.isNullOrEmpty(employeesUnsorted))
                        theEmployees.AddRange(employeesUnsorted);
                }


                Evaluation theEvaluation = getEvaluation(evaluationId);
                if (theEmployees != null && !theEmployees.IsEmpty())
                {
                    Dictionary<string, Employee> evaluees = new Dictionary<string, Employee>();
                    Dictionary<string, bool> evalueesCanSelfEvaluate = new Dictionary<string, bool>();
                    Dictionary<string, AssessorStatistics> evalueeStatisticsStore = new Dictionary<string, AssessorStatistics>();

                    foreach (Employee emp in theEmployees)
                    {
                        ret.Add(getAssessorGrades(theEvaluation, emp, false, evaluees, evalueesCanSelfEvaluate,
                                                evalueeStatisticsStore, getPhoto));
                    }
                }
            }
            return ret;
        }


        public List<EvaluationGrades> getAssessorGradesTable_filtered(int evaluationId, FilterModel filter, int offset, int pageSize,
                                                AssessorTableOrdering ordering, bool ascendingOrder,
                                                out int totalEmployees, out int totalEmployeesFiltered, bool getPhoto)
        {
            logger.Info("getAllAssessorGrades of evaluation " + evaluationId);
            List<EvaluationGrades> ret = new List<EvaluationGrades>();

            List<string> theEmployeeIdList = new List<string>();
            List<EmployeeFilteringData> filteringData = DA.getAssessorFilteringData(evaluationId);

            totalEmployees = filteringData.Count;
            totalEmployeesFiltered = filteringData.Count;

            if (filter != null)
            {
                /*
                 * IsDepartment
                 * Only return the evaluees from the specified departments
                 */
                if (filter.IsDepartment && filteringData.Any())
                {
                    filteringData = (from x in filteringData
                                     where (filter.DepartmentList.Contains(x.departmentCode) ||
                                         filter.DepartmentList.Contains(x.departmentDescription))
                                     select x).ToList();
                }

                /*
                 * IsNames
                 * Only return the evaluees with the specified names
                 */
                if (filter.IsNames && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.NamesList.Contains((x.surname + " " + x.name).ToUpper()) select x).ToList();
                }

                /*
                 * IsSites
                 * Only return the evaluees with the specified sites
                 */
                if (filter.IsSite && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.SitesList.Contains(x.workplace) select x).ToList();
                }

                /*
                 * IsStaffMemberHidden
                 * Only return the evaluees which are not staff members
                 */
                if (filter.IsStaffMemberHidden && filteringData.Any())
                {
                    StaffMembersConfiguration staffMembers = getStaffMembersConfiguration();
                    IEnumerable<string> staffMemberIdList = from x in staffMembers.members select x.employeeId;
                    filteringData = (from x in filteringData where !staffMemberIdList.Contains(x.employeeId) select x).ToList();
                }

                /*
                 * IsEvaluationDate
                 * Only return the evaluees who received grades in the last X days
                 */
                if (filter.IsEvaluationDate && filter.NumberBeforeEvaluationDate.HasValue && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.mostRecentGrade > DateTime.Now.AddDays(-filter.NumberBeforeEvaluationDate.Value) select x).ToList();
                }


                /*
                 * IsEvaluationReceived
                 * Only return the evaluees who have received grades
                 *
                 * IsEvaluationReminded
                 * Only return the evaluees whose assessors have received reminders
                 *
                 * IsEvaluationUnexpressed
                 * Only return the evaluees who have not received grades
                 */
                if (!filter.IsAllEvaluation)
                {
                    if (filteringData.Any())
                    {
                        if (filter.IsEvaluationReminded)
                        {
                            if (filter.IsEvaluationReceived)
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 select x).ToList();
                                }
                                else
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 && x.gradesDone > 0 select x).ToList();
                                }
                            }
                            else
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 && x.gradesDone == 0 select x).ToList();
                                }
                                else
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 select x).ToList();
                                }
                            }
                        }
                        else
                        {
                            if (filter.IsEvaluationReceived)
                            {
                                if (!filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.gradesDone > 0 select x).ToList();
                                }
                            }
                            else
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.gradesDone == 0 select x).ToList();
                                }
                            }
                        }
                    }
                }

                /*
                 * IsNumberEvaluationReceived
                 * Return only the evaluees who have been graded by a number of assessors in the specified bounds
                 */
                if (filter.IsNumberEvaluationReceived && filteringData.Any())
                {
                    int maxValue = filter.MaximumEvaluationNumber.HasValue ? filter.MaximumEvaluationNumber.Value : 9999;
                    int minValue = filter.MinimumEvaluationNumber.HasValue ? filter.MinimumEvaluationNumber.Value : -1;
                    filteringData = (from x in filteringData where x.gradesDone >= minValue && x.gradesDone <= maxValue select x).ToList();
                }

                /*
                 * Commuter
                 * Return only the evaluees which are commuters
                 */
                if (filter.IsCommuter && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.commuter == filter.Commuter select x).ToList();
                }

                /*
                 * ExternalEmployee
                 * Return only the evaluees which are external
                 */
                if (filter.IsExternalEmployee && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.externalEmployee == filter.ExternalEmployee select x).ToList();
                }

                /*
                 * WorkplaceAlgorithm
                 * Return only the evaluees which have the specified workplace algorithm
                 */
                if (filter.IsWorkplaceAlgorithm && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.WorkplaceAlgorithm.Contains(x.workplaceAlgorithm) select x).ToList();
                }

                /*
                 * ContractType
                 * Return only the evaluees which have the specified contract type
                 */
                if (filter.IsContractType && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.ContractType.Contains(x.contractType) select x).ToList();
                }
                if (filter.IsOpenResearch && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.OpenResearch == 1 select x).ToList();
                }
            }

            totalEmployeesFiltered = filteringData.Count;

            if (filteringData.Any())
            {
                switch (ordering)
                {
                    case AssessorTableOrdering.AutoOrManualPairing:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.hasManualPairing).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.hasManualPairing).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.AverageGrade:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.averageGrade).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.averageGrade).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Company:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.company).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.company).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ContractDaysLeft:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractExpiresInDays).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractExpiresInDays).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ContractEndDate:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractEndDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractEndDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ContractStartDate:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractStartDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractStartDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ContractType:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractType).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractType).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.CurrentJob:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.currentJob).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.currentJob).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.DepartmentDescription:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.departmentDescription).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.departmentDescription).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.EmployeeRole:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.role).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.role).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.GradesReceived:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.gradesDone).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.gradesDone).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Reminders:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.reminders).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.reminders).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Surname:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Workplace:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.workplace).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.workplace).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.WorkplaceAlgorithm:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.workplaceAlgorithm).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.workplaceAlgorithm).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.Commuter:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.commuter).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.commuter).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ExternalEmployee:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.externalEmployee).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.externalEmployee).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case AssessorTableOrdering.ExtensionNotes:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.extensionNotes).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.extensionNotes).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                }

                List<string> employeeIdSorted = filteringData.Select(x => x.employeeId).ToList();
                Employees theEmployees = new Employees();
                if (!ValueOperations.isNullOrEmpty(employeeIdSorted))
                {
                    List<Employee> employeesUnsorted = DA.getEmployees(employeeIdSorted, true).ToList();
                    if (!ValueOperations.isNullOrEmpty(employeesUnsorted))
                        employeesUnsorted = employeesUnsorted.OrderBy(x => employeeIdSorted.IndexOf(x.EmployeeNumber)).ToList();
                    if (!ValueOperations.isNullOrEmpty(employeesUnsorted))
                        theEmployees.AddRange(employeesUnsorted);
                }


                Evaluation theEvaluation = getEvaluation(evaluationId);
                if (theEmployees != null && !theEmployees.IsEmpty())
                {
                    foreach (Employee emp in theEmployees)
                    {
                        emp.GradeStats = DA.getAssessorGradeStatistics(evaluationId, emp.EmployeeNumber);
                        ret.Add(getAssessorGradesForTable(theEvaluation, emp, false, getPhoto));
                    }
                }
            }
            return ret;
        }

        public Employees getAllAssessors(int evaluationId)
        {
            logger.Info("getAllAssessors of evaluation " + evaluationId);
            Employees ret = new Employees();
            List<string> employeeIdList = DA.getAllAssessors(evaluationId);
            ret = DA.getEmployees(employeeIdList, false);
            return ret;
        }

        public Dictionary<string, string> getAllContractTypes()
        {
            return DA.getAllContractTypes();
        }

        public Departments getAllDepartments()
        {
            logger.Info("getAllDepartments");
            return DA.getAllDepartments();
        }

        public Employees getAllEmployees(bool getPhoto)
        {
            logger.Info("getAllEmployees");
            Employees ret = DA.getAllEmployees(getPhoto);
            return ret;
        }

        public List<EmployeePersonalInfo> getAllEmployeesPersonalInfo()
        {
            logger.Info("getAllEmployeesPersonalInfo");
            List<EmployeePersonalInfo> ret = DA.getEmployeePersonalInfo();
            return ret;
        }

        public Evaluations getAllEvaluations()
        {
            logger.Info("getAllEvaluations");
            Evaluations ret = DA.getAllEvaluations();
            if (!ret.IsEmpty())
            {
                foreach (Evaluation e in ret)
                    e.departments = DA.getDepartmentsToEvaluate(e.EvaluationId);
            }
            return ret;
        }

        public List<EvaluationGrades> getEvalueeGradesTable(int evaluationId, FilterModel filter, int offset, int pageSize,
                                                            EvalueeTableOrdering ordering, bool ascendingOrder,
                                                            out int totalEmployees, out int totalEmployeesFiltered, bool getPhoto)
        {
            if (filter != null && (filter.IsMaximumExcluded || filter.IsMinimumExcluded))
                return getAllEvalueeGrades(evaluationId, filter, offset, pageSize, ordering, ascendingOrder,
                                                    out totalEmployees, out totalEmployeesFiltered, getPhoto);
            else
                return getEvalueeGradesTable_filtered(evaluationId, filter, offset, pageSize, ordering, ascendingOrder,
                                                    out totalEmployees, out totalEmployeesFiltered, getPhoto);
        }

        public List<EvaluationGrades> getEvalueeGradesTable_filtered(int evaluationId, FilterModel filter, int offset, int pageSize,
                                                            EvalueeTableOrdering ordering, bool ascendingOrder,
                                                            out int totalEmployees, out int totalEmployeesFiltered, bool getPhoto)
        {
            DateTime startDate = DateTime.Now;
            logger.Info("getAllEvalueeGrades of evaluation " + evaluationId);
            List<EvaluationGrades> ret = new List<EvaluationGrades>();

            DateTime beforeFilteringData = DateTime.Now;
            List<string> theEmployeeIdList = new List<string>(); // DA.getAllEvaluees_ordered(evaluationId, offset, pageSize, ordering, ascendingOrder);
            List<EmployeeFilteringData> filteringData = DA.getEvalueeFilteringData(evaluationId);
            totalEmployees = filteringData.Count;
            totalEmployeesFiltered = filteringData.Count;
            logger.Info("getAllEvalueeGrades time for filtering data (ms): " + ((int)(DateTime.Now - beforeFilteringData).TotalMilliseconds));
            DateTime beforeFiltering = DateTime.Now;
            if (filter != null)
            {
                /*
                 * IsAverageEvaluation
                 * Remove from results the evaluees who received average grades lower or higher than 
                 * the specified bounds
                 */
                if (filter.IsAverageEvaluation && filteringData.Any())
                {
                    decimal maxValue = Convert.ToDecimal(filter.MaximumAverageEvaluationNumber.HasValue ? filter.MaximumAverageEvaluationNumber.Value : 9999);
                    decimal minValue = Convert.ToDecimal(filter.MinimumAverageEvaluationNumber.HasValue ? filter.MinimumAverageEvaluationNumber.Value : -1);
                    filteringData = (from x in filteringData where x.averageGrade >= minValue && x.averageGrade <= maxValue select x).ToList();
                }

                /*
                 * IsDepartment
                 * Only return the evaluees from the specified departments
                 */
                if (filter.IsDepartment && filteringData.Any())
                {
                    filteringData = (from x in filteringData
                                     where (filter.DepartmentList.Contains(x.departmentCode) ||
                                         filter.DepartmentList.Contains(x.departmentDescription))
                                     select x).ToList();
                }

                /*
                 * IsNames
                 * Only return the evaluees with the specified names
                 */
                if (filter.IsNames && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.NamesList.Contains((x.surname + " " + x.name).ToUpper()) select x).ToList();
                }

                /*
                 * IsSites
                 * Only return the evaluees with the specified sites
                 */
                if (filter.IsSite && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.SitesList.Contains(x.workplace) select x).ToList();
                }

                // IsExcludeCompiledEvaluations 
                if (filter.IsExcludeCompiledEvaluations && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.gradesDone < 3 select x).ToList();
                }

                /*
                 * IsStaffMemberHidden
                 * Only return the evaluees which are not staff members
                 */
                if (filter.IsStaffMemberHidden && filteringData.Any())
                {
                    StaffMembersConfiguration staffMembers = getStaffMembersConfiguration();
                    IEnumerable<string> staffMemberIdList = from x in staffMembers.members select x.employeeId;
                    filteringData = (from x in filteringData where !staffMemberIdList.Contains(x.employeeId) select x).ToList();
                }

                /*
                 * IsEvaluationDate
                 * Only return the evaluees who received grades in the last X days
                 */
                if (filter.IsEvaluationDate && filter.NumberBeforeEvaluationDate.HasValue && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.mostRecentGrade > DateTime.Now.AddDays(-filter.NumberBeforeEvaluationDate.Value) select x).ToList();
                }

                /*
                 * IsEvaluationReceived
                 * Only return the evaluees who have received grades
                 *
                 * IsEvaluationReminded
                 * Only return the evaluees whose assessors have received reminders
                 *
                 * IsEvaluationUnexpressed
                 * Only return the evaluees who have not received grades
                 */
                if (!filter.IsAllEvaluation)
                {
                    if (filteringData.Any())
                    {
                        if (filter.IsEvaluationReminded)
                        {
                            if (filter.IsEvaluationReceived)
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 select x).ToList();
                                }
                                else
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 && x.gradesDone > 0 select x).ToList();
                                }
                            }
                            else
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 && x.gradesDone == 0 select x).ToList();
                                }
                                else
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 select x).ToList();
                                }
                            }
                        }
                        else
                        {
                            if (filter.IsEvaluationReceived)
                            {
                                if (!filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.gradesDone > 0 select x).ToList();
                                }
                            }
                            else
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.gradesDone == 0 select x).ToList();
                                }
                            }
                        }
                    }
                }
                /*
                 * IsNumberEvaluationReceived
                 * Return only the evaluees who have been graded by a number of assessors in the specified bounds
                 */
                if (filter.IsNumberEvaluationReceived && filteringData.Any())
                {
                    int maxValue = filter.MaximumEvaluationNumber.HasValue ? filter.MaximumEvaluationNumber.Value : 9999;
                    int minValue = filter.MinimumEvaluationNumber.HasValue ? filter.MinimumEvaluationNumber.Value : -1;
                    filteringData = (from x in filteringData where x.gradesDone >= minValue && x.gradesDone <= maxValue select x).ToList();
                }

                /*
                 * IsOnlySuggestedAssessors
                 * Return only the evaluees who have been proposed or rejected for a promotion to assessor
                 */
                if (filter.IsOnlySuggestedAssessors && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.isProposedAssessor select x).ToList();

                }



                /*
                 * Commuter
                 * Return only the evaluees which are commuters
                 */
                if (filter.IsCommuter && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.commuter == filter.Commuter select x).ToList();
                }

                /*
                 * ExternalEmployee
                 * Return only the evaluees which are external
                 */
                if (filter.IsExternalEmployee && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.externalEmployee == filter.ExternalEmployee select x).ToList();
                }

                /*
                 * WorkplaceAlgorithm
                 * Return only the evaluees which have the specified workplace algorithm
                 */
                if (filter.IsWorkplaceAlgorithm && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.WorkplaceAlgorithm.Contains(x.workplaceAlgorithm) select x).ToList();
                }

                /*
                 * ContractType
                 * Return only the evaluees which have the specified contract type
                 */
                if (filter.IsContractType && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.ContractType.Contains(x.contractType) select x).ToList();
                }

                if (filter.IsOpenResearch && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.OpenResearch == 1 select x).ToList();
                }
            }

            totalEmployeesFiltered = filteringData.Count;

            logger.Info("getAllEvalueeGrades time for filtering (ms): " + ((int)(DateTime.Now - beforeFiltering).TotalMilliseconds));
            DateTime beforeSorting = DateTime.Now;

            if (filteringData.Any())
            {
                switch (ordering)
                {
                    case EvalueeTableOrdering.AutoOrManualPairing:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.hasManualPairing).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.hasManualPairing).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.AverageGrade:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.averageGrade).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.averageGrade).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Company:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.company).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.company).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ContractDaysLeft:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractExpiresInDays).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractExpiresInDays).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ContractEndDate:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractEndDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractEndDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ContractStartDate:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractStartDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractStartDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ContractType:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractType).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractType).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.CurrentJob:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.currentJob).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.currentJob).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.DepartmentDescription:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.departmentDescription).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.departmentDescription).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.EmployeeRole:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.role).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.role).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.GradesReceived:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.gradesDone).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.gradesDone).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.IsProposedAssessor:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.isProposedAssessor).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.isProposedAssessor).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Reminders:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.reminders).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.reminders).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Surname:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Workplace:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.workplace).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.workplace).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.WorkplaceAlgorithm:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.workplaceAlgorithm).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.workplaceAlgorithm).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Commuter:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.commuter).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.commuter).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ExternalEmployee:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.externalEmployee).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.externalEmployee).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ExtensionNotes:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.extensionNotes).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.extensionNotes).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.IsDowngraded:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.isDowngraded).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.isDowngraded).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                }

                List<string> employeeIdSorted = filteringData.Select(x => x.employeeId).ToList();
                Employees theEmployees = new Employees();
                if (!ValueOperations.isNullOrEmpty(employeeIdSorted))
                {
                    List<Employee> employeesUnsorted = DA.getEmployees(employeeIdSorted, getPhoto).ToList();
                    if (!ValueOperations.isNullOrEmpty(employeesUnsorted))
                    {
                        employeesUnsorted = employeesUnsorted.OrderBy(x => employeeIdSorted.IndexOf(x.EmployeeNumber)).ToList();
                        theEmployees.AddRange(employeesUnsorted);
                    }
                }

                logger.Info("getAllEvalueeGrades time for sorting (ms): " + ((int)(DateTime.Now - beforeSorting).TotalMilliseconds));
                DateTime beforeGrades = DateTime.Now;

                Evaluation theEvaluation = getEvaluation(evaluationId);
                if (theEmployees != null && !theEmployees.IsEmpty())
                {
                    foreach (Employee emp in theEmployees)
                    {
                        emp.GradeStats = DA.getGradeStatistics(evaluationId, emp.EmployeeNumber);
                        ret.Add(getEvalueeGradesForTable(theEvaluation, emp, false, getPhoto, GetDowngradedAssessors(evaluationId)));
                    }

                    if (filter != null)
                    {
                        int gradesForMaxExclusion = Math.Max(2, applicationConfiguration.gradesToAllowExclusion);
                        int gradesForMinExclusion = gradesForMaxExclusion;
                        if (filter.IsMaximumExcluded)
                            gradesForMinExclusion--;
                        /*
                         * IsMaximumExcluded
                         * Remove from the grades the assessor who gave the highest grade and recompute the average
                         */
                        if (filter.IsMaximumExcluded && ret.Any())
                        {
                            foreach (EvaluationGrades eg in ret)
                            {
                                if (eg.grades.Count(num => num.AverageGrade > 0) >= gradesForMaxExclusion)
                                {
                                    eg.grades = eg.grades.OrderByDescending(x => x.AverageGrade).Skip(1).ToList();
                                    if (eg.grades.Count(num => num.AverageGrade > 0) > 0)
                                        eg.averageGrade = eg.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                                    else
                                        eg.averageGrade = 0;
                                }
                            }
                        }

                        /*
                         * IsMinimumExcluded
                         * Remove from the grades the assessor who gave the lowest grade and recompute the average
                         */
                        if (filter.IsMinimumExcluded && ret.Any())
                        {
                            foreach (EvaluationGrades eg in ret)
                            {
                                if (eg.grades.Count(num => num.AverageGrade > 0) >= gradesForMinExclusion)
                                {
                                    //eg.grades = eg.grades.OrderBy(x => x.AverageGrade).Skip(1).ToList();
                                    eg.grades.Remove(eg.grades.Where(x => x.AverageGrade > 0).OrderBy(x => x.AverageGrade).First());
                                    if (eg.grades.Count(num => num.AverageGrade > 0) > 0)
                                        eg.averageGrade = eg.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                                    else
                                        eg.averageGrade = 0;
                                }
                            }
                        }
                    }
                }
                logger.Info("getAllEvalueeGrades time for grades (ms): " + ((int)(DateTime.Now - beforeGrades).TotalMilliseconds));
            }
            logger.Info("time for getAllEvalueeGrades (ms): " + ((int)(DateTime.Now - startDate).TotalMilliseconds));
            return ret;
        }



        public List<EvaluationGrades> getAllEvalueeGrades(int evaluationId, FilterModel filter, int offset, int pageSize,
                                                            EvalueeTableOrdering ordering, bool ascendingOrder,
                                                            out int totalEmployees, out int totalEmployeesFiltered, bool getPhoto)
        {
            DateTime startDate = DateTime.Now;
            logger.Info("getAllEvalueeGrades of evaluation " + evaluationId);
            List<EvaluationGrades> ret = new List<EvaluationGrades>();

            DateTime beforeFilteringData = DateTime.Now;
            List<string> theEmployeeIdList = new List<string>(); // DA.getAllEvaluees_ordered(evaluationId, offset, pageSize, ordering, ascendingOrder);
            List<EmployeeFilteringData> filteringData = DA.getEvalueeFilteringData(evaluationId);
            totalEmployees = filteringData.Count;
            totalEmployeesFiltered = filteringData.Count;
            logger.Info("getAllEvalueeGrades time for filtering data (ms): " + ((int)(DateTime.Now - beforeFilteringData).TotalMilliseconds));
            DateTime beforeFiltering = DateTime.Now;
            if (filter != null)
            {
                /*
                 * IsAverageEvaluation
                 * Remove from results the evaluees who received average grades lower or higher than 
                 * the specified bounds
                 */
                if (filter.IsAverageEvaluation && filteringData.Any())
                {
                    decimal maxValue = Convert.ToDecimal(filter.MaximumAverageEvaluationNumber.HasValue ? filter.MaximumAverageEvaluationNumber.Value : 9999);
                    decimal minValue = Convert.ToDecimal(filter.MinimumAverageEvaluationNumber.HasValue ? filter.MinimumAverageEvaluationNumber.Value : -1);
                    filteringData = (from x in filteringData where x.averageGrade >= minValue && x.averageGrade <= maxValue select x).ToList();
                }

                /*
                 * IsDepartment
                 * Only return the evaluees from the specified departments
                 */
                if (filter.IsDepartment && filteringData.Any())
                {
                    filteringData = (from x in filteringData
                                     where (filter.DepartmentList.Contains(x.departmentCode) ||
                                         filter.DepartmentList.Contains(x.departmentDescription))
                                     select x).ToList();
                }

                /*
                 * IsNames
                 * Only return the evaluees with the specified names
                 */
                if (filter.IsNames && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.NamesList.Contains((x.surname + " " + x.name).ToUpper()) select x).ToList();
                }

                /*
                 * IsSites
                 * Only return the evaluees with the specified sites
                 */
                if (filter.IsSite && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.SitesList.Contains(x.workplace) select x).ToList();
                }

                /*
                 * IsStaffMemberHidden
                 * Only return the evaluees which are not staff members
                 */
                if (filter.IsStaffMemberHidden && filteringData.Any())
                {
                    StaffMembersConfiguration staffMembers = getStaffMembersConfiguration();
                    IEnumerable<string> staffMemberIdList = from x in staffMembers.members select x.employeeId;
                    filteringData = (from x in filteringData where !staffMemberIdList.Contains(x.employeeId) select x).ToList();
                }

                /*
                 * IsEvaluationDate
                 * Only return the evaluees who received grades in the last X days
                 */
                if (filter.IsEvaluationDate && filter.NumberBeforeEvaluationDate.HasValue && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.mostRecentGrade > DateTime.Now.AddDays(-filter.NumberBeforeEvaluationDate.Value) select x).ToList();
                }

                /*
                 * IsEvaluationReceived
                 * Only return the evaluees who have received grades
                 *
                 * IsEvaluationReminded
                 * Only return the evaluees whose assessors have received reminders
                 *
                 * IsEvaluationUnexpressed
                 * Only return the evaluees who have not received grades
                 */
                if (!filter.IsAllEvaluation)
                {
                    if (filteringData.Any())
                    {
                        if (filter.IsEvaluationReminded)
                        {
                            if (filter.IsEvaluationReceived)
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 select x).ToList();
                                }
                                else
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 && x.gradesDone > 0 select x).ToList();
                                }
                            }
                            else
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 && x.gradesDone == 0 select x).ToList();
                                }
                                else
                                {
                                    filteringData = (from x in filteringData where x.reminders > 0 select x).ToList();
                                }
                            }
                        }
                        else
                        {
                            if (filter.IsEvaluationReceived)
                            {
                                if (!filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.gradesDone > 0 select x).ToList();
                                }
                            }
                            else
                            {
                                if (filter.IsEvaluationUnexpressed)
                                {
                                    filteringData = (from x in filteringData where x.gradesDone == 0 select x).ToList();
                                }
                            }
                        }
                    }
                }
                /*
                 * IsNumberEvaluationReceived
                 * Return only the evaluees who have been graded by a number of assessors in the specified bounds
                 */
                if (filter.IsNumberEvaluationReceived && filteringData.Any())
                {
                    int maxValue = filter.MaximumEvaluationNumber.HasValue ? filter.MaximumEvaluationNumber.Value : 9999;
                    int minValue = filter.MinimumEvaluationNumber.HasValue ? filter.MinimumEvaluationNumber.Value : -1;
                    filteringData = (from x in filteringData where x.gradesDone >= minValue && x.gradesDone <= maxValue select x).ToList();
                }

                /*
                 * IsOnlySuggestedAssessors
                 * Return only the evaluees who have been proposed or rejected for a promotion to assessor
                 */
                if (filter.IsOnlySuggestedAssessors && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.isProposedAssessor select x).ToList();
                }

                /*
                 * Commuter
                 * Return only the evaluees which are commuters
                 */
                if (filter.IsCommuter && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.commuter == filter.Commuter select x).ToList();
                }

                /*
                 * ExternalEmployee
                 * Return only the evaluees which are external
                 */
                if (filter.IsExternalEmployee && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.externalEmployee == filter.ExternalEmployee select x).ToList();
                }

                /*
                 * WorkplaceAlgorithm
                 * Return only the evaluees which have the specified workplace algorithm
                 */
                if (filter.IsWorkplaceAlgorithm && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.WorkplaceAlgorithm.Contains(x.workplaceAlgorithm) select x).ToList();
                }

                /*
                 * ContractType
                 * Return only the evaluees which have the specified contract type
                 */
                if (filter.IsContractType && filteringData.Any())
                {
                    filteringData = (from x in filteringData where filter.ContractType.Contains(x.contractType) select x).ToList();
                }

                if (filter.IsOpenResearch && filteringData.Any())
                {
                    filteringData = (from x in filteringData where x.OpenResearch == 1 select x).ToList();
                }





            }

            totalEmployeesFiltered = filteringData.Count;

            logger.Info("getAllEvalueeGrades time for filtering (ms): " + ((int)(DateTime.Now - beforeFiltering).TotalMilliseconds));
            DateTime beforeSorting = DateTime.Now;

            if (filteringData.Any())
            {
                switch (ordering)
                {
                    case EvalueeTableOrdering.AutoOrManualPairing:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.hasManualPairing).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.hasManualPairing).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.AverageGrade:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.averageGrade).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.averageGrade).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Company:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.company).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.company).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ContractDaysLeft:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractExpiresInDays).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractExpiresInDays).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ContractEndDate:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractEndDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractEndDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ContractStartDate:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractStartDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractStartDate).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ContractType:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.contractType).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.contractType).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.CurrentJob:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.currentJob).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.currentJob).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.DepartmentDescription:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.departmentDescription).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.departmentDescription).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.EmployeeRole:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.role).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.role).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.GradesReceived:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.gradesDone).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.gradesDone).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.IsProposedAssessor:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.isProposedAssessor).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.isProposedAssessor).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Reminders:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.reminders).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.reminders).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Surname:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Workplace:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.workplace).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.workplace).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.WorkplaceAlgorithm:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.workplaceAlgorithm).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.workplaceAlgorithm).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.Commuter:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.commuter).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.commuter).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ExternalEmployee:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.externalEmployee).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.externalEmployee).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.ExtensionNotes:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.extensionNotes).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.extensionNotes).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;
                    case EvalueeTableOrdering.IsDowngraded:
                        if (ascendingOrder)
                            filteringData = filteringData.OrderBy(x => x.isDowngraded).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        else
                            filteringData = filteringData.OrderByDescending(x => x.isDowngraded).ThenBy(y => y.surname).Skip(Math.Min(filteringData.Count, offset)).Take(Math.Min(filteringData.Count - offset, pageSize)).ToList();
                        break;

                }

                List<string> employeeIdSorted = filteringData.Select(x => x.employeeId).ToList();
                Employees theEmployees = new Employees();
                if (!ValueOperations.isNullOrEmpty(employeeIdSorted))
                {
                    List<Employee> employeesUnsorted = DA.getEmployees(employeeIdSorted, getPhoto).ToList();
                    if (!ValueOperations.isNullOrEmpty(employeesUnsorted))
                    {
                        employeesUnsorted = employeesUnsorted.OrderBy(x => employeeIdSorted.IndexOf(x.EmployeeNumber)).ToList();
                        theEmployees.AddRange(employeesUnsorted);
                    }
                }

                logger.Info("getAllEvalueeGrades time for sorting (ms): " + ((int)(DateTime.Now - beforeSorting).TotalMilliseconds));
                DateTime beforeGrades = DateTime.Now;

                Evaluation theEvaluation = getEvaluation(evaluationId);
                if (theEmployees != null && !theEmployees.IsEmpty())
                {
                    Dictionary<string, Employee> assessors = new Dictionary<string, Employee>();
                    Dictionary<string, bool> assessorsCanSelfEvaluate = new Dictionary<string, bool>();
                    Dictionary<string, AssessorStatistics> assessorStatisticsStore = new Dictionary<string, AssessorStatistics>();

                    foreach (Employee emp in theEmployees)
                    {
                        emp.GradeStats = DA.getGradeStatistics(evaluationId, emp.EmployeeNumber);
                        ret.Add(getEvalueeGrades(theEvaluation, emp, false, assessors, assessorsCanSelfEvaluate, assessorStatisticsStore, getPhoto, GetDowngradedAssessors(evaluationId)));
                    }

                    if (filter != null)
                    {
                        int gradesForMaxExclusion = Math.Max(2, applicationConfiguration.gradesToAllowExclusion);
                        int gradesForMinExclusion = gradesForMaxExclusion;
                        if (filter.IsMaximumExcluded)
                            gradesForMinExclusion--;
                        /*
                         * IsMaximumExcluded
                         * Remove from the grades the assessor who gave the highest grade and recompute the average
                         */
                        if (filter.IsMaximumExcluded && ret.Any())
                        {
                            foreach (EvaluationGrades eg in ret)
                            {
                                if (eg.grades.Count(num => num.AverageGrade > 0) >= gradesForMaxExclusion)
                                {
                                    eg.grades = eg.grades.OrderByDescending(x => x.AverageGrade).Skip(1).ToList();
                                    if (eg.grades.Count(num => num.AverageGrade > 0) > 0)
                                        eg.averageGrade = eg.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                                    else
                                        eg.averageGrade = 0;
                                }
                            }
                        }

                        /*
                         * IsMinimumExcluded
                         * Remove from the grades the assessor who gave the lowest grade and recompute the average
                         */
                        if (filter.IsMinimumExcluded && ret.Any())
                        {
                            foreach (EvaluationGrades eg in ret)
                            {
                                if (eg.grades.Count(num => num.AverageGrade > 0) >= gradesForMinExclusion)
                                {
                                    //eg.grades = eg.grades.OrderBy(x => x.AverageGrade).Skip(1).ToList();
                                    eg.grades.Remove(eg.grades.Where(x => x.AverageGrade > 0).OrderBy(x => x.AverageGrade).First());
                                    if (eg.grades.Count(num => num.AverageGrade > 0) > 0)
                                        eg.averageGrade = eg.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                                    else
                                        eg.averageGrade = 0;
                                }
                            }
                        }
                    }
                }
                logger.Info("getAllEvalueeGrades time for grades (ms): " + ((int)(DateTime.Now - beforeGrades).TotalMilliseconds));
            }
            logger.Info("time for getAllEvalueeGrades (ms): " + ((int)(DateTime.Now - startDate).TotalMilliseconds));
            return ret;
        }

        public Employees getAllEvaluees(int evaluationId, bool getPhoto)
        {
            logger.Info("getAllEvaluees of evaluation " + evaluationId);
            Employees ret = new Employees();
            List<string> employeeIdList = DA.getPromotionEvaluees(evaluationId);
            if (!ValueOperations.isNullOrEmpty(employeeIdList))
            {
                ret = DA.getEmployees(employeeIdList, getPhoto);
            }

            return ret;
        }

        public List<string> getAllEvalueesIdList(int evaluationId)
        {
            logger.Info("getAllEvalueesIdList of evaluation " + evaluationId);
            List<string> ret = DA.getPromotionEvaluees(evaluationId);
            return ret;
        }

        public Employees getAllEvalueesInHistory()
        {
            logger.Info("getAllEvalueesInHistory");
            Employees ret = new Employees();
            List<string> employeeIdList = getAllEvalueesInHistoryIdList();
            if (!ValueOperations.isNullOrEmpty(employeeIdList))
            {
                ret = DA.getEmployees(employeeIdList, false);
            }

            return ret;
        }

        public List<string> getAllEvalueesInHistoryIdList()
        {
            logger.Info("getAllEvalueesInHistoryIdList");
            List<string> ret = DA.getPromotionEvaluees_allHistory();
            return ret;
        }

        public EmployeeNames getAllEvalueeNamesInHistory(List<int> accessibleWorkplacesAlgorithmList)
        {
            logger.Info("getAllEvalueeNamesInHistory");
            /*
            List<string> employeeIdList = DA.getPromotionEvaluees_allHistory();
            EmployeeNames ret = DA.getEmployeeNames(employeeIdList);
            */
            EmployeeNames ret = DA.getAllEmployeeNames(accessibleWorkplacesAlgorithmList);
            return ret;
        }

        public List<Privileges> GetAllPrivilegesWithWorkplaceAlgorithm()
        {
            return DA.GetAllPrivilegesWithWorkplaceAlgorithm();
        }

        public List<int> GetAccessibleWorkplacesAlgorithmList(int roleId, List<int> privilegeIdList, List<int> accessibleWorkplacesAlgorithmPrivilegeIdList)
        {
            if (roleId <= 0)
                return null;

            List<int> ret = null;

            List<int> workplacesAccessPrivilegeIdList = accessibleWorkplacesAlgorithmPrivilegeIdList;               //lista dei privilegi che, tra tutti, riguardano le sedi di lavoro
            List<Privileges> privilegesWithWorkplaceAlgorithm = this.GetAllPrivilegesWithWorkplaceAlgorithm();      //lista dei privilegi che hanno un WorkplaceAlgorithm associato

            foreach (var prvId in privilegeIdList)
            {
                //se non ha privilegi del tipo riguardante le sedi di lavoro, manco lo considero
                if (!workplacesAccessPrivilegeIdList.Contains(prvId))
                    continue;

                if (ret == null)
                    ret = new List<int>();

                //prendo i privilegi associati ad un WorkplaceAlgorithm
                Privileges t = privilegesWithWorkplaceAlgorithm.Where(k => k.PrivilegeId == prvId).SingleOrDefault();
                if (t != null)
                    ret.Add(t.WorkplaceAlgorithm);
            }

            return ret;
        }

        public Employees getAllNonEvaluees(int evaluationId, bool getPhoto)
        {
            logger.Info("getAllNonEvaluees of evaluation " + evaluationId);
            Employees ret = new Employees();
            List<string> employeeIdList = DA.getAllNonEvalueesIdList(evaluationId);
            if (!ValueOperations.isNullOrEmpty(employeeIdList))
            {
                ret = DA.getEmployees(employeeIdList, getPhoto);
            }

            return ret;
        }

        public List<string> getAllSites()
        {
            logger.Info("getAllSites");
            return DA.getAllSites();
        }

        public Dictionary<int, string> getAllWorkplaceAlgorithms()
        {
            return DA.getAllWorkplaceAlgorithms();
        }

        public PlannedAssessments getAssessmentData(int evaluationId, string assessorId, string language, bool orderByCompany, out Departments assessmentDepartments)
        {
            DateTime beforeRet = DateTime.Now;
            logger.Info("getAssessmentData of evaluation " + evaluationId + " assessor " + assessorId);
            assessmentDepartments = new Departments();
            PlannedAssessments ret = new PlannedAssessments();
            DateTime beforePairings = DateTime.Now;
            List<EvaluationPairing> pairings = DA.getAssessorPairings(evaluationId, assessorId, false);
            List<EvaluationPairing> selfEval = DA.getAssessorPairings(evaluationId, assessorId, true);
            if (DA.isEvaluee(evaluationId, assessorId))
            {
                logger.Info("time for pairings (ms): " + (int)(DateTime.Now - beforePairings).TotalMilliseconds);
                if (ValueOperations.isNullOrEmpty(pairings))
                    pairings = selfEval;
                else
                {
                    if (!ValueOperations.isNullOrEmpty(selfEval))
                        pairings.AddRange(selfEval);
                }
            }

            if (!ValueOperations.isNullOrEmpty(pairings))
            {
                DateTime beforeCriteria = DateTime.Now;
                EvaluationCriteria criteria = getEvaluationCriteria(evaluationId, language);
                EvaluationCriteria_Multilanguage criteriaMultilanguage = getEvaluationCriteriaConfiguration(evaluationId);
                logger.Info("time for criteria (ms): " + (int)(DateTime.Now - beforeCriteria).TotalMilliseconds);
                DateTime beforeAssessmentData = DateTime.Now;
                List<EmployeeAssessmentData> theEmployees = DA.getEmployeeAssessmentData(pairings.Select(x => x.EvalueeId).ToList());
                if (!ValueOperations.isNullOrEmpty(theEmployees))
                {
                    foreach (EmployeeAssessmentData ead in theEmployees)
                    {
                        if (assessmentDepartments[ead.DepartmentCode] == null)
                        {
                            assessmentDepartments.Add(new Department()
                            {
                                departmentCode = ead.DepartmentCode,
                                departmentDescription = ead.DepartmentDescription
                            });
                        }
                    }
                }
                logger.Info("time for employee data (ms): " + (int)(DateTime.Now - beforeAssessmentData).TotalMilliseconds);

                DateTime beforeGraded = DateTime.Now;
                List<string> gradedEvaluees = DA.getGradedEvaluees(evaluationId, assessorId);
                logger.Info("time for graded evaluees (ms): " + (int)(DateTime.Now - beforeGraded).TotalMilliseconds);

                Reminders theReminders = DA.getReminders(evaluationId, assessorId, true);

                DateTime beforeLoop = DateTime.Now;
                foreach (EvaluationPairing ep in pairings)
                {
                    PlannedAssessment pa = new PlannedAssessment();
                    pa.alreadyReminded = theReminders.containsEvalueeId(ep.EvalueeId);
                    pa.evaluationCriteria = criteria;
                    pa.evaluationPairing = ep;
                    DateTime beforeTerms = DateTime.Now;
                    if (gradedEvaluees.Contains(ep.EvalueeId))
                        pa.evaluationTerms = DA.getEvaluationTerms(evaluationId, ep.EvalueeId, assessorId, criteriaMultilanguage);
                    else
                        pa.evaluationTerms = new EvaluationTerms();
                    logger.Info("time for terms (ms): " + (int)(DateTime.Now - beforeTerms).TotalMilliseconds);
                    pa.evaluee = theEmployees.FirstOrDefault(x => x.EmployeeNumber.Equals(ep.EvalueeId));
                    if (pa != null && pa.evaluee != null)
                        ret.Add(pa);
                }
                logger.Info("time for loop (ms): " + (int)(DateTime.Now - beforeLoop).TotalMilliseconds);
            }

            if (!ret.IsEmpty())
            {
                PlannedAssessments ret2 = new PlannedAssessments();
                PlannedAssessment selfEvaluation = ret[assessorId];
                if (selfEvaluation != null)
                {
                    ret2.Add(selfEvaluation);
                    ret.Remove(selfEvaluation);
                }
                if (ret.Any())
                {
                    if (orderByCompany)
                    {
                        ret2.AddRange(ret.OrderBy(x => x.evaluee.Company).ThenBy(x => x.evaluee.DepartmentCode).ThenBy(x => x.evaluee.Surname).ThenBy(x => x.evaluee.Name).ToList());
                    }
                    else
                    {
                        ret2.AddRange(ret.OrderBy(x => x.evaluee.Surname).ThenBy(x => x.evaluee.Name).ToList());
                    }
                }
                ret = ret2;
            }

            logger.Info("time for the entire procedure (ms): " + (int)(DateTime.Now - beforeRet).TotalMilliseconds);
            return ret;
        }

        /// <summary>
        /// Get the columns to show for a specific assessor
        /// </summary>
        /// <returns></returns>
        public GridColumns getAssessorColumns(string userId)
        {
            logger.Info("getAssessorColumns of user " + userId);
            return DA.getGridColumns("Assessors", userId);
        }

        /// <summary>
        /// Get the filters for a specific assessor
        /// </summary>
        /// <param name="assessorId"></param>
        /// <returns></returns>
        public FilterModel getAssessorFilter(string assessorId)
        {
            logger.Info("getAssessorFilter of user " + assessorId);
            FilterModel ret = DA.getFilters(assessorId, "Assessors");
            if (ret == null)
            {
                logger.Info("inserting filter for user " + assessorId);
                DA.insertFilter(assessorId, "Assessors");
                ret = DA.getFilters(assessorId, "Assessors");
            }
            return ret;
        }

        public List<EmployeeFilteringData> getAssessorFilteringDataSimplified(int evaluationId)
        {
            return DA.getAssessorFilteringDataSimplified(evaluationId);
        }


        public List<EmployeeFilteringData> getAssessorFilteringData(int evaluationId)
        {
            return DA.getAssessorFilteringData(evaluationId);
        }

        /// <summary>
        /// Get the terms and the values for each term of the current evaluation
        ///  complete with notes. if getSelfEvaluation is true will get the self-evaluation grades,
        ///  otherwise the method will get all the "normal" grades given by assessors
        /// </summary>
        /// <returns></returns>
        public EvaluationGrades getAssessorGrades(Evaluation theEvaluation, Employee theAssessor, bool getSelfEvaluations,
                                                    Dictionary<string, Employee> evaluees,
                                                    Dictionary<string, bool> evalueesCanSelfEvaluate,
                                                    Dictionary<string, AssessorStatistics> evalueeStatisticsStore, bool getPhoto)
        {
            string assessorId = theAssessor.EmployeeNumber;
            int evaluationId = theEvaluation.EvaluationId;
            DateTime beforeStart = DateTime.Now;
            logger.Info("get assessor grades of user " + assessorId + " evaluation " + evaluationId + " selfEvaluation=" + getSelfEvaluations);
            EvaluationGrades ret = new EvaluationGrades();
            ret.evaluation = theEvaluation;
            ret.employeeId = assessorId;
            bool assessorCanSelfEvaluate = canSelfEvaluate(evaluationId, assessorId);
            ret.canSelfEvaluate = assessorCanSelfEvaluate;
            AssessorStatistics stats = getAssessorStatistics(evaluationId, assessorId);
            logger.Info("time for assessor data (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            DateTime beforePairings = DateTime.Now;
            List<EvaluationPairing> pairings = DA.getAssessorPairings(evaluationId, assessorId, getSelfEvaluations);
            List<string> gradedEvaluees = DA.getGradedEvaluees(evaluationId, assessorId);
            logger.Info("time for pairings (ms): " + (int)(DateTime.Now - beforePairings).TotalMilliseconds);
            DateTime beforeCriteria = DateTime.Now;
            EvaluationCriteria_Multilanguage criteriaMultilanguage = getEvaluationCriteriaConfiguration(evaluationId);
            logger.Info("time for criteria (ms): " + (int)(DateTime.Now - beforeCriteria).TotalMilliseconds);
            int gradesDone = 0;
            if (!ValueOperations.isNullOrEmpty(pairings))
            {
                foreach (EvaluationPairing ep in pairings)
                {
                    EvaluationTermsAndNotes theTerms = new EvaluationTermsAndNotes();
                    DateTime beforeTerms = DateTime.Now;
                    if (gradedEvaluees.Contains(ep.EvalueeId))
                        theTerms.Terms = DA.getEvaluationTerms(evaluationId, ep.EvalueeId, ep.AssessorId, criteriaMultilanguage);
                    else
                        theTerms.Terms = new EvaluationTerms();
                    logger.Info("time for terms (ms): " + (int)(DateTime.Now - beforeTerms).TotalMilliseconds);
                    DateTime beforeEmployees = DateTime.Now;
                    if (!evaluees.ContainsKey(ep.EvalueeId))
                        evaluees[ep.EvalueeId] = DA.getEmployee(ep.EvalueeId, getPhoto);
                    theTerms.Evaluee = evaluees[ep.EvalueeId];
                    if (theTerms.Evaluee != null)
                    {
                        theTerms.Assessor = theAssessor;
                        logger.Info("time for employees (ms): " + (int)(DateTime.Now - beforeEmployees).TotalMilliseconds);
                        DateTime beforeSelfEvaluations = DateTime.Now;
                        if (!evalueesCanSelfEvaluate.ContainsKey(ep.EvalueeId))
                            evalueesCanSelfEvaluate[ep.EvalueeId] = canSelfEvaluate(evaluationId, ep.EvalueeId);
                        theTerms.EvalueeCanSelfEvaluate = evalueesCanSelfEvaluate[ep.EvalueeId];
                        theTerms.AssessorCanSelfEvaluate = assessorCanSelfEvaluate;
                        theTerms.Pairing = ep;
                        theTerms.Pairing.AutoOrManual = theTerms.Pairing.AutoOrManual.Equals("Auto") ? "auto" : "man";
                        logger.Info("time for self evaluations (ms): " + (int)(DateTime.Now - beforeSelfEvaluations).TotalMilliseconds);
                        DateTime beforeAssessorStatistics = DateTime.Now;
                        if (!evalueeStatisticsStore.ContainsKey(ep.EvalueeId))
                            evalueeStatisticsStore[ep.EvalueeId] = getEvalueeStatistics(evaluationId, ep.EvalueeId);
                        theTerms.AssessorStatistics = evalueeStatisticsStore[ep.EvalueeId];
                        if (theTerms.Terms.Count > 0)
                        {
                            theTerms.AverageGrade = DA.computeAverage(theTerms.Terms);
                            if (theTerms.AverageGrade > 0)
                                gradesDone++;
                        }
                        ret.grades.Add(theTerms);
                        logger.Info("time for evaluee statistics (ms): " + (int)(DateTime.Now - beforeAssessorStatistics).TotalMilliseconds);
                    }
                }
                if (ret.grades.Count > 0)
                    ret.grades = ret.grades.OrderBy(x => x.Evaluee.Surname).ThenBy(x => x.Evaluee.Name).ToList();
                if (theAssessor.GradeStats != null && theAssessor.GradeStats.averageGrade > 0)
                    ret.averageGrade = theAssessor.GradeStats.averageGrade;
                else
                {
                    if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                        ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                }
                ret.gradesPlanned = (theAssessor.GradeStats != null && theAssessor.GradeStats.gradesPlanned > 0 ? theAssessor.GradeStats.gradesPlanned : pairings.Count);
                ret.gradesDone = (theAssessor.GradeStats != null && theAssessor.GradeStats.gradesDone > 0 ? theAssessor.GradeStats.gradesDone : gradesDone);
                ret.AutoOrManual = ret.grades.Count(x => x.Pairing.AutoOrManual.Equals("man") || x.Pairing.AutoOrManual.Equals("Manual")) > 0 ? "man" : "auto";
                ret.hasChildRows = ret.gradesPlanned > 0;
            }
            else
                logger.Info("no pairings");

            ret.numberOfReminders = DA.getReminders(evaluationId, assessorId, true).Count;

            logger.Info("time for whole procedure (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            return ret;
        }


        public EvaluationGrades getAssessorGradesForTable(Evaluation theEvaluation, Employee theAssessor, bool getSelfEvaluations,
                                                             bool getPhoto)
        {
            string assessorId = theAssessor.EmployeeNumber;
            int evaluationId = theEvaluation.EvaluationId;
            DateTime beforeStart = DateTime.Now;
            logger.Info("get assessor grades of user " + assessorId + " evaluation " + evaluationId + " selfEvaluation=" + getSelfEvaluations);
            EvaluationGrades ret = new EvaluationGrades();
            ret.evaluation = theEvaluation;
            ret.employeeId = assessorId;
            ret.employeeData = theAssessor;
            bool assessorCanSelfEvaluate = canSelfEvaluate(evaluationId, assessorId);
            ret.canSelfEvaluate = assessorCanSelfEvaluate;
            AssessorStatistics stats = getAssessorStatistics(evaluationId, assessorId);
            logger.Info("time for assessor data (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            DateTime beforePairings = DateTime.Now;
            List<EvaluationPairing> pairings = DA.getAssessorPairings(evaluationId, assessorId, getSelfEvaluations);
            List<string> gradedEvaluees = DA.getGradedEvaluees(evaluationId, assessorId);
            logger.Info("time for pairings (ms): " + (int)(DateTime.Now - beforePairings).TotalMilliseconds);
            DateTime beforeCriteria = DateTime.Now;
            EvaluationCriteria_Multilanguage criteriaMultilanguage = getEvaluationCriteriaConfiguration(evaluationId);
            logger.Info("time for criteria (ms): " + (int)(DateTime.Now - beforeCriteria).TotalMilliseconds);
            int gradesDone = 0;
            if (theAssessor.GradeStats != null && theAssessor.GradeStats.averageGrade > 0)
                ret.averageGrade = theAssessor.GradeStats.averageGrade;
            else
            {
                if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                    ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
            }
            ret.gradesPlanned = (theAssessor.GradeStats != null && theAssessor.GradeStats.gradesPlanned > 0 ? theAssessor.GradeStats.gradesPlanned : pairings.Count);
            ret.gradesDone = (theAssessor.GradeStats != null && theAssessor.GradeStats.gradesDone > 0 ? theAssessor.GradeStats.gradesDone : gradesDone);
            ret.AutoOrManual = ret.grades.Count(x => x.Pairing.AutoOrManual.Equals("man") || x.Pairing.AutoOrManual.Equals("Manual")) > 0 ? "man" : "auto";
            ret.hasChildRows = ret.gradesPlanned > 0;

            ret.numberOfReminders = DA.getReminders(evaluationId, assessorId, true).Count;

            logger.Info("time for whole procedure (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            return ret;
        }

        /// <summary>
        /// Get the data for the assessor grid, filtered as specified in the 
        /// filters parameter
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public EvaluationData getAssessorGrid(FilterModels filters)
        {
            return new EvaluationData();
        }

        public AssessorStatistics getAssessorStatistics(int evaluationId, string assessorId)
        {
            logger.Info("getAssessorStatistics of user " + assessorId);
            AssessorStatistics ret = new AssessorStatistics();

            List<EvaluationPairing> pairings = DA.getAssessorPairings(evaluationId, assessorId, false);
            ret.evaluationsPlanned = pairings.Count;
            ret.evaluationsDone = DA.countEvaluationsDone(evaluationId, assessorId);
            return ret;
        }

        public AssessorStatistics getAssessorStatisticsSimplified(int evaluationId, string assessorId)
        {
            logger.Info("getAssessorStatistics of user " + assessorId);
            AssessorStatistics ret = new AssessorStatistics();

            int count = DA.getCountAssessorPairings(evaluationId, assessorId, false);
            ret.evaluationsPlanned = count;
            ret.evaluationsDone = DA.countEvaluationsDone(evaluationId, assessorId);
            return ret;
        }

        public Contract getCurrentEmployeeContract(string employeeId)
        {
            logger.Info("get contract of user " + employeeId);
            return DA.getCurrentEmployeeContract(employeeId);
        }

        public Evaluation getCurrentEvaluation()
        {
            int evaluationId = getCurrentEvaluationId();
            Evaluation currentEvaluation = getEvaluation(evaluationId);
            return currentEvaluation;
        }

        public int getCurrentEvaluationId()
        {
            logger.Info("getcurrentEvaluationId");
            return DA.getCurrentEvaluationId();
        }

        public int getLastEvaluationId(string employeeNumber)
        {
            logger.Info($"getLastEvaluationId - employeeNumber: {employeeNumber}");
            return DA.getLastEvaluationId(employeeNumber);
        }

        public int? getBeforeRequestEvaluationId(string employeeNumber, int? evaluationId = null)
        {
            if (!evaluationId.HasValue)
            {
                evaluationId = getLastEvaluationId(employeeNumber);
            }

            logger.Info($"getBeforeRequestEvaluationId - employeeNumber: {employeeNumber} - evaluationId: {evaluationId}");

            return DA.getBeforeRequestEvaluationId(employeeNumber, evaluationId.Value);
        }

        public DefaultMessagesConfiguration getDefaultMessagesConfiguration()
        {
            return new DefaultMessagesConfiguration();
        }

        /// <summary>
        /// Get the full personal data of an employee
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public Employee getEmployee(string employeeId, bool getPhoto)
        {
            logger.Info("get employee " + employeeId);
            return DA.getEmployee(employeeId, getPhoto);
        }

        public Contracts getEmployeeContractHistory(string employeeId)
        {
            logger.Info("get contract history of user " + employeeId);
            return DA.getEmployeeContractHistory(employeeId);
        }

        public byte[] getEmployeePhoto(string employeeId)
        {
            logger.Info("get employee photo " + employeeId);
            return DA.getEmployee(employeeId, true)?.Photo;
        }

        public byte[] getPhoto(string employeeId)
        {
            logger.Info("get employee photo " + employeeId);
            return DA.getPhoto(employeeId);
        }

        public Evaluation getEvaluation(int evaluationId)
        {
            logger.Info("getting evaluation " + evaluationId + " data");
            Evaluation ret = DA.getEvaluation(evaluationId);
            ret.departments = DA.getDepartmentsToEvaluate(evaluationId);
            return ret;
        }

        /// <summary>
        /// Executes the [VALUTAZIONI_LEGAMI_TASK_ODS] stored procedure to find out 
        /// connections between employees who worked together (after working together 
        /// on 4 tasks, each of them can evaluate the teammate)
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public EvaluationConnections getEvaluationConnections(DateTime dateFrom, DateTime dateTo)
        {
            logger.Info("get evaluation connections from " + dateFrom.ToString("s") + " to " + dateTo.ToString("s"));
            return DA.getEvaluationConnections(dateFrom, dateTo);
        }

        public EvaluationCriteria getEvaluationCriteria(int evaluationId, string language)
        {
            EvaluationCriteria ret = new EvaluationCriteria();
            logger.Info("get evaluation criteria configuration of evaluation " + evaluationId);
            EvaluationCriteria_Multilanguage ecm = DA.getEvaluationCriteriaConfiguration(evaluationId);
            if (ecm != null && !ecm.IsEmpty())
            {
                foreach (EvaluationCriterion_Multilanguage critM in ecm)
                {
                    EvaluationCriterion newCrit = new EvaluationCriterion(critM);
                    string neededValue = "";
                    if (!critM.CriterionDescription.TryGetValue(language, out neededValue) || string.IsNullOrEmpty(neededValue))
                        critM.CriterionDescription.TryGetValue(applicationConfiguration.DefaultLanguage, out neededValue);
                    if (string.IsNullOrEmpty(neededValue) && !string.IsNullOrEmpty(applicationConfiguration.ZeroLanguage))
                        critM.CriterionDescription.TryGetValue(applicationConfiguration.ZeroLanguage, out neededValue);
                    if (string.IsNullOrEmpty(neededValue))
                        neededValue = critM.CriterionCode;
                    newCrit.CriterionDescription = neededValue;
                    ret.Add(newCrit);
                    if (critM.Subcriteria != null && !critM.Subcriteria.IsEmpty())
                    {
                        foreach (EvaluationCriterion_Multilanguage subcritM in critM.Subcriteria)
                        {
                            EvaluationCriterion newSubcrit = new EvaluationCriterion(subcritM);
                            string neededSubValue = "";
                            if (!subcritM.CriterionDescription.TryGetValue(language, out neededSubValue) || string.IsNullOrEmpty(neededSubValue))
                                subcritM.CriterionDescription.TryGetValue(applicationConfiguration.DefaultLanguage, out neededSubValue);
                            if (string.IsNullOrEmpty(neededSubValue) && !string.IsNullOrEmpty(applicationConfiguration.ZeroLanguage))
                                subcritM.CriterionDescription.TryGetValue(applicationConfiguration.ZeroLanguage, out neededSubValue);
                            if (string.IsNullOrEmpty(neededSubValue))
                                neededSubValue = subcritM.CriterionCode;
                            newSubcrit.CriterionDescription = neededSubValue;
                            newCrit.Subcriteria.Add(newSubcrit);
                        }
                    }
                }
            }
            return ret;
        }

        public EvaluationCriteria_Multilanguage getEvaluationCriteriaConfiguration(int evaluationId, bool callSlowMethod = false)
        {
            logger.Info("get evaluation criteria configuration of evaluation " + evaluationId);
            return DA.getEvaluationCriteriaConfiguration(evaluationId, callSlowMethod);
        }

        public EvaluationRulesConfiguration getEvaluationRulesConfiguration()
        {
            logger.Info("get evaluation rules configuration");
            return DA.getEvaluationRulesConfiguration();
        }

        /// <summary>
        /// Get the status of an evaluation
        /// </summary>
        /// <param name="evaluationID"></param>
        /// <returns></returns>
        public EvaluationStatus getEvaluationStatus(int evaluationID)
        {
            logger.Info("get status of evaluation " + evaluationID);
            return DA.getEvaluationStatus(evaluationID);
        }

        /// <summary>
        /// Get the columns to show for each evaluee
        /// </summary>
        /// <returns></returns>
        public GridColumns getEvalueeColumns(string userId)
        {
            logger.Info("get evaluee columns of user " + userId);
            return DA.getGridColumns("Evaluees", userId);
        }

        public List<EmployeeFilteringData> getEvalueeFilteringDataSimplified(int evaluationId, List<int> accessibleWorkplacesAlgorithmList = null)
        {
            return DA.getEvalueeFilteringDataSimplified(evaluationId, accessibleWorkplacesAlgorithmList);
        }

        public List<EmployeeFilteringData> getEvalueeFilteringAccessesDataSimplified(int evaluationId)
        {
            return DA.getEvalueeFilteringAccessesDataSimplified(evaluationId);
        }

        public List<EmployeeFilteringData> getEvalueeFilteringAccessesDataSimplified()
        {
            return DA.getEvalueeFilteringAccessesDataSimplified();
        }

        public List<EmployeeFilteringData> getEvalueeFilteringData(int evaluationId)
        {
            return DA.getEvalueeFilteringData(evaluationId);
        }

        /// <summary>
        /// Get the filters for a specific evaluee
        /// </summary>
        /// <param name="evalueeId"></param>
        /// <returns></returns>
        public FilterModel getEvalueeFilters(string evalueeId)
        {
            logger.Info("getEvalueeFilter of user " + evalueeId);
            FilterModel ret = DA.getFilters(evalueeId, "Evaluees");
            if (ret == null)
            {
                logger.Info("inserting filter");
                DA.insertFilter(evalueeId, "Evaluees");
                ret = DA.getFilters(evalueeId, "Evaluees");
            }
            return ret;
        }

        public EvaluationGrades getAssessorGrades(int evaluationId, string assessorId, bool getSelfEvaluations, bool getPhoto)
        {
            return getAssessorGrades(DA.getEvaluation(evaluationId), DA.getEmployee(assessorId, getPhoto), getSelfEvaluations, new Dictionary<string, Employee>(),
                                    new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), getPhoto);
        }

        public EvaluationGrades getEvalueeGrades(int evaluationId, string evalueeId, bool getSelfEvaluations, bool getPhoto)
        {
            return getEvalueeGrades(evaluationId, evalueeId, getSelfEvaluations, new Dictionary<string, Employee>(),
                                    new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), getPhoto);
        }

        public EvaluationGrades getEvalueeGrades(int evaluationId, string evalueeId, bool getSelfEvaluations,
                                                    Dictionary<string, Employee> assessors,
                                                    Dictionary<string, bool> assessorsCanSelfEvaluate,
                                                    Dictionary<string, AssessorStatistics> assessorStatisticsStore, bool getPhoto)
        {
            return getEvalueeGrades(DA.getEvaluation(evaluationId), DA.getEmployee(evalueeId, getPhoto), getSelfEvaluations, assessors,
                                    assessorsCanSelfEvaluate, assessorStatisticsStore, getPhoto, GetDowngradedAssessors(evaluationId));
        }

        public EvaluationGrades getEvalueeGradesSimplified(int evaluationId, string assessorId, bool getSelfEvaluations, bool getPhoto)
        {
            return DA.getEvalueeGradesSimplified(DA.getEvaluation(evaluationId), DA.getEmployee(assessorId, getPhoto), getSelfEvaluations, new Dictionary<string, Employee>(),
                                    new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), getPhoto);
        }

        //private EvaluationGrades getEvalueeGradesSimplified(Evaluation theEvaluation, Employee theEvaluee, bool getSelfEvaluations,
        //                                           Dictionary<string, Employee> assessors,
        //                                           Dictionary<string, bool> assessorsCanSelfEvaluate,
        //                                           Dictionary<string, AssessorStatistics> assessorStatisticsStore, bool getPhoto)
        //{
        //    string evalueeId = theEvaluee.EmployeeNumber;
        //    int evaluationId = theEvaluation.EvaluationId;
        //    DateTime beforeStart = DateTime.Now;
        //    logger.Info("get evaluee grades of user " + evalueeId + " evaluation " + evaluationId + " selfEvaluation=" + getSelfEvaluations);
        //    EvaluationGrades ret = new EvaluationGrades();
        //    ret.evaluation = theEvaluation;
        //    ret.employeeId = evalueeId;
        //    ret.employeeData = theEvaluee;
        //    logger.Info("time for evaluee data (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
        //    DateTime beforePairings = DateTime.Now;
        //    //Chiamo il metodo per le accoppiate, ma senza calcolare i reminder che sono l'operazione più onerosa
        //    List<EvaluationPairing> pairings = DA.getEvalueePairingsWithoutReminders(evaluationId, evalueeId, getSelfEvaluations);

        //    List<string> assessorsWhoGraded = DA.getAssessorsWhoGraded(evaluationId, evalueeId);
        //    logger.Info("time for pairings (ms): " + (int)(DateTime.Now - beforePairings).TotalMilliseconds);
        //    DateTime beforeCriteria = DateTime.Now;
        //    EvaluationCriteria_Multilanguage criteriaMultilanguage = getEvaluationCriteriaConfiguration(evaluationId);
        //    logger.Info("time for criteria (ms): " + (int)(DateTime.Now - beforeCriteria).TotalMilliseconds);
        //    int gradesDone = 0;
        //    if (!ValueOperations.isNullOrEmpty(pairings))
        //    {
        //        foreach (EvaluationPairing ep in pairings)
        //        {
        //            EvaluationTermsAndNotes theTerms = new EvaluationTermsAndNotes();
        //            DateTime beforeTerms = DateTime.Now;
        //            if (assessorsWhoGraded.Contains(ep.AssessorId))
        //                theTerms.Terms = DA.getEvaluationTerms(evaluationId, evalueeId, ep.AssessorId, criteriaMultilanguage);
        //            else
        //                theTerms.Terms = new EvaluationTerms();
        //            logger.Info("time for terms (ms): " + (int)(DateTime.Now - beforeTerms).TotalMilliseconds);
        //            DateTime beforeEmployees = DateTime.Now;
        //            if (!assessors.ContainsKey(ep.AssessorId))
        //                assessors[ep.AssessorId] = DA.getEmployeeSimplified(ep.AssessorId);
        //            theTerms.Assessor = assessors[ep.AssessorId];
        //            if (theTerms.Assessor != null)
        //            {
        //                theTerms.Evaluee = theEvaluee;
        //                logger.Info("time for employees (ms): " + (int)(DateTime.Now - beforeEmployees).TotalMilliseconds);
        //                DateTime beforeSelfEvaluations = DateTime.Now;
        //                if (!assessorsCanSelfEvaluate.ContainsKey(ep.AssessorId))
        //                    assessorsCanSelfEvaluate[ep.AssessorId] = canSelfEvaluate(evaluationId, ep.AssessorId);
        //                theTerms.AssessorCanSelfEvaluate = assessorsCanSelfEvaluate[ep.AssessorId];
        //                theTerms.Pairing = ep;
        //                logger.Info("time for self evaluations (ms): " + (int)(DateTime.Now - beforeSelfEvaluations).TotalMilliseconds);
        //                DateTime beforeAssessorStatistics = DateTime.Now;
        //                if (theTerms.Terms.Count > 0)
        //                {
        //                    theTerms.AverageGrade = computeAverage(theTerms.Terms);
        //                    if (theTerms.AverageGrade > 0)
        //                        gradesDone++;
        //                }
        //                ret.grades.Add(theTerms);
        //                logger.Info("time for assessor statistics (ms): " + (int)(DateTime.Now - beforeAssessorStatistics).TotalMilliseconds);
        //            }
        //        }
        //        if (ret.grades.Count > 0)
        //            ret.grades = ret.grades.OrderBy(x => x.Assessor.Surname).ThenBy(x => x.Assessor.Name).ToList();
        //        if (theEvaluee.GradeStats != null && theEvaluee.GradeStats.averageGrade > 0)
        //            ret.averageGrade = theEvaluee.GradeStats.averageGrade;
        //        else
        //        {
        //            if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
        //                ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
        //        }

        //    }
        //    else
        //        logger.Info("no pairings");
        //    return ret;
        //}

        public EvaluationGrades getEvalueeGradesHistorySimplified(Evaluation evaluation, Employee evaluee, bool getSelfEvaluations, bool getPhoto)
        {
            return getEvalueeGradesHistorySimplified(evaluation, evaluee, getSelfEvaluations, new Dictionary<string, Employee>(),
                                    new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), getPhoto);
        }

        public Employee GetEmployeeSimplified(string employeeNumber)
        {
            return DA.getEmployeeSimplified(employeeNumber);
        }


        private EvaluationGrades getEvalueeGradesHistorySimplified(Evaluation theEvaluation, Employee theEvaluee, bool getSelfEvaluations,
                                                 Dictionary<string, Employee> assessors,
                                                 Dictionary<string, bool> assessorsCanSelfEvaluate,
                                                 Dictionary<string, AssessorStatistics> assessorStatisticsStore, bool getPhoto)
        {
            string evalueeId = theEvaluee.EmployeeNumber;
            int evaluationId = theEvaluation.EvaluationId;
            DateTime beforeStart = DateTime.Now;
            logger.Info("get evaluee grades of user " + evalueeId + " evaluation " + evaluationId + " selfEvaluation=" + getSelfEvaluations);
            EvaluationGrades ret = new EvaluationGrades();
            ret.evaluation = theEvaluation;
            ret.employeeId = evalueeId;
            ret.employeeData = theEvaluee;

            logger.Info("time for evaluee data (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            DateTime beforePairings = DateTime.Now;
            List<EvaluationPairing> pairings = DA.getEvalueePairingsWithoutReminders(evaluationId, evalueeId, getSelfEvaluations);

            List<string> assessorsWhoGraded = DA.getAssessorsWhoGraded(evaluationId, evalueeId);
            logger.Info("time for pairings (ms): " + (int)(DateTime.Now - beforePairings).TotalMilliseconds);
            DateTime beforeCriteria = DateTime.Now;
            EvaluationCriteria_Multilanguage criteriaMultilanguage = getEvaluationCriteriaConfiguration(evaluationId);
            logger.Info("time for criteria (ms): " + (int)(DateTime.Now - beforeCriteria).TotalMilliseconds);
            int gradesDone = 0;
            if (!ValueOperations.isNullOrEmpty(pairings))
            {
                foreach (EvaluationPairing ep in pairings)
                {
                    EvaluationTermsAndNotes theTerms = new EvaluationTermsAndNotes();
                    DateTime beforeTerms = DateTime.Now;
                    if (assessorsWhoGraded.Contains(ep.AssessorId))
                        theTerms.Terms = DA.getEvaluationTerms(evaluationId, evalueeId, ep.AssessorId, criteriaMultilanguage);
                    else
                        theTerms.Terms = new EvaluationTerms();
                    logger.Info("time for terms (ms): " + (int)(DateTime.Now - beforeTerms).TotalMilliseconds);
                    DateTime beforeEmployees = DateTime.Now;
                    if (!assessors.ContainsKey(ep.AssessorId))
                        assessors[ep.AssessorId] = DA.getEmployeeSimplified(ep.AssessorId);
                    theTerms.Assessor = assessors[ep.AssessorId];
                    if (theTerms.Assessor != null)
                    {
                        theTerms.Evaluee = theEvaluee;
                        logger.Info("time for employees (ms): " + (int)(DateTime.Now - beforeEmployees).TotalMilliseconds);
                        DateTime beforeSelfEvaluations = DateTime.Now;
                        theTerms.Pairing = ep;
                        logger.Info("time for self evaluations (ms): " + (int)(DateTime.Now - beforeSelfEvaluations).TotalMilliseconds);
                        DateTime beforeAssessorStatistics = DateTime.Now;
                        if (!assessorStatisticsStore.ContainsKey(ep.AssessorId))
                            assessorStatisticsStore[ep.AssessorId] = getAssessorStatisticsSimplified(evaluationId, ep.AssessorId);
                        theTerms.AssessorStatistics = assessorStatisticsStore[ep.AssessorId];
                        if (theTerms.Terms.Count > 0)
                        {
                            theTerms.AverageGrade = DA.computeAverage(theTerms.Terms);
                            if (theTerms.AverageGrade > 0)
                                gradesDone++;
                        }
                        ret.grades.Add(theTerms);
                        logger.Info("time for assessor statistics (ms): " + (int)(DateTime.Now - beforeAssessorStatistics).TotalMilliseconds);
                    }
                }
                if (ret.grades.Count > 0)
                    ret.grades = ret.grades.OrderBy(x => x.Assessor.Surname).ThenBy(x => x.Assessor.Name).ToList();
                if (theEvaluee.GradeStats != null && theEvaluee.GradeStats.averageGrade > 0)
                    ret.averageGrade = theEvaluee.GradeStats.averageGrade;
                else
                {
                    if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                        ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                }
                ret.gradesPlanned = (theEvaluee.GradeStats != null && theEvaluee.GradeStats.gradesPlanned > 0 ? theEvaluee.GradeStats.gradesPlanned : pairings.Count);
                ret.gradesDone = (theEvaluee.GradeStats != null && theEvaluee.GradeStats.gradesDone > 0 ? theEvaluee.GradeStats.gradesDone : gradesDone);

            }
            else
                logger.Info("no pairings");
            return ret;
        }

        /// <summary>
        /// Get the terms and the values for each term of the current evaluation
        ///  complete with notes. if getSelfEvaluation is true will get the self-evaluation grades,
        ///  otherwise the method will get all the "normal" grades given by assessors
        /// </summary>
        /// <returns></returns>
        public EvaluationGrades getEvalueeGrades(Evaluation theEvaluation, Employee theEvaluee, bool getSelfEvaluations,
                                                    Dictionary<string, Employee> assessors,
                                                    Dictionary<string, bool> assessorsCanSelfEvaluate,
                                                    Dictionary<string, AssessorStatistics> assessorStatisticsStore, bool getPhoto, List<string> downGraded)
        {
            string evalueeId = theEvaluee.EmployeeNumber;
            int evaluationId = theEvaluation.EvaluationId;
            DateTime beforeStart = DateTime.Now;
            logger.Info("get evaluee grades of user " + evalueeId + " evaluation " + evaluationId + " selfEvaluation=" + getSelfEvaluations);
            bool isDowngraded = false;
            if (downGraded.Any() && downGraded.Contains(theEvaluee.EmployeeNumber))
            {
                isDowngraded = true;

            }
            theEvaluee.isDowngraded = isDowngraded;
            EvaluationGrades ret = new EvaluationGrades();
            ret.evaluation = theEvaluation;
            ret.employeeId = evalueeId;
            ret.employeeData = theEvaluee;
            bool evalueeCanSelfEvaluate = canSelfEvaluate(evaluationId, evalueeId);
            ret.canSelfEvaluate = evalueeCanSelfEvaluate;
            logger.Info("time for evaluee data (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            DateTime beforePairings = DateTime.Now;
            List<EvaluationPairing> pairings = DA.getEvalueePairings(evaluationId, evalueeId, getSelfEvaluations);

            List<string> assessorsWhoGraded = DA.getAssessorsWhoGraded(evaluationId, evalueeId);
            logger.Info("time for pairings (ms): " + (int)(DateTime.Now - beforePairings).TotalMilliseconds);
            DateTime beforeCriteria = DateTime.Now;
            EvaluationCriteria_Multilanguage criteriaMultilanguage = getEvaluationCriteriaConfiguration(evaluationId);
            logger.Info("time for criteria (ms): " + (int)(DateTime.Now - beforeCriteria).TotalMilliseconds);
            int gradesDone = 0;
            if (!ValueOperations.isNullOrEmpty(pairings))
            {
                foreach (EvaluationPairing ep in pairings)
                {
                    EvaluationTermsAndNotes theTerms = new EvaluationTermsAndNotes();
                    DateTime beforeTerms = DateTime.Now;
                    if (assessorsWhoGraded.Contains(ep.AssessorId))
                        theTerms.Terms = DA.getEvaluationTerms(evaluationId, evalueeId, ep.AssessorId, criteriaMultilanguage);
                    else
                        theTerms.Terms = new EvaluationTerms();
                    logger.Info("time for terms (ms): " + (int)(DateTime.Now - beforeTerms).TotalMilliseconds);
                    DateTime beforeEmployees = DateTime.Now;
                    if (!assessors.ContainsKey(ep.AssessorId))
                        assessors[ep.AssessorId] = DA.getEmployee(ep.AssessorId, getPhoto);
                    theTerms.Assessor = assessors[ep.AssessorId];
                    if (theTerms.Assessor != null)
                    {
                        theTerms.Evaluee = theEvaluee;
                        logger.Info("time for employees (ms): " + (int)(DateTime.Now - beforeEmployees).TotalMilliseconds);
                        DateTime beforeSelfEvaluations = DateTime.Now;
                        if (!assessorsCanSelfEvaluate.ContainsKey(ep.AssessorId))
                            assessorsCanSelfEvaluate[ep.AssessorId] = canSelfEvaluate(evaluationId, ep.AssessorId);
                        theTerms.AssessorCanSelfEvaluate = assessorsCanSelfEvaluate[ep.AssessorId];
                        theTerms.EvalueeCanSelfEvaluate = evalueeCanSelfEvaluate;
                        theTerms.Pairing = ep;
                        theTerms.Pairing.AutoOrManual = theTerms.Pairing.AutoOrManual.Equals("Auto") ? "auto" : "man";
                        logger.Info("time for self evaluations (ms): " + (int)(DateTime.Now - beforeSelfEvaluations).TotalMilliseconds);
                        DateTime beforeAssessorStatistics = DateTime.Now;
                        if (!assessorStatisticsStore.ContainsKey(ep.AssessorId))
                            assessorStatisticsStore[ep.AssessorId] = getAssessorStatistics(evaluationId, ep.AssessorId);
                        theTerms.AssessorStatistics = assessorStatisticsStore[ep.AssessorId];
                        if (theTerms.Terms.Count > 0)
                        {
                            theTerms.AverageGrade = DA.computeAverage(theTerms.Terms);
                            if (theTerms.AverageGrade > 0)
                                gradesDone++;
                        }
                        ret.grades.Add(theTerms);
                        logger.Info("time for assessor statistics (ms): " + (int)(DateTime.Now - beforeAssessorStatistics).TotalMilliseconds);
                    }
                }
                if (ret.grades.Count > 0)
                    ret.grades = ret.grades.OrderBy(x => x.Assessor.Surname).ThenBy(x => x.Assessor.Name).ToList();
                if (theEvaluee.GradeStats != null && theEvaluee.GradeStats.averageGrade > 0)
                    ret.averageGrade = theEvaluee.GradeStats.averageGrade;
                else
                {
                    if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                        ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                }
                ret.gradesPlanned = (theEvaluee.GradeStats != null && theEvaluee.GradeStats.gradesPlanned > 0 ? theEvaluee.GradeStats.gradesPlanned : pairings.Count);
                ret.gradesDone = (theEvaluee.GradeStats != null && theEvaluee.GradeStats.gradesDone > 0 ? theEvaluee.GradeStats.gradesDone : gradesDone);
                ret.AutoOrManual = ret.grades.Count(x => x.Pairing.AutoOrManual.Equals("man") || x.Pairing.AutoOrManual.Equals("Manual")) > 0 ? "man" : "auto";
                ret.hasChildRows = ret.gradesPlanned > 0;
            }
            else
                logger.Info("no pairings");

            DateTime beforePastAverage = DateTime.Now;
            EvaluationReceived history = DA.getLastEvalueeHistoryRecord(evalueeId, evaluationId);
            if (history != null)
                ret.pastAverage = history.Result;
            logger.Info("time for past average (ms): " + (int)(DateTime.Now - beforePastAverage).TotalMilliseconds);
            DateTime beforePromotionStatus = DateTime.Now;
            ret.employeePromotionStatus = DA.getEmployeePromotionStatus(evaluationId, evalueeId);
            ret.canBePromoted = ret.employeePromotionStatus.HasValue && (ret.employeePromotionStatus.Value == PromotionStatus.Proposed || ret.employeePromotionStatus.Value == PromotionStatus.Rejected);

            ret.numberOfReminders = DA.getEvalueeReminders(evaluationId, evalueeId, true).Count;

            logger.Info("time for promotion status (ms): " + (int)(DateTime.Now - beforePromotionStatus).TotalMilliseconds);
            logger.Info("time for whole procedure (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            return ret;
        }


        public EvaluationGrades getEvalueeGradesForTable(Evaluation theEvaluation, Employee theEvaluee, bool getSelfEvaluations,
                                                            bool getPhoto, List<string> downgraded)
        {
            bool isDowngraded = false;
            if (downgraded.Any() && downgraded.Contains(theEvaluee.EmployeeNumber))
            {
                isDowngraded = true;

            }
            theEvaluee.isDowngraded = isDowngraded;
            string evalueeId = theEvaluee.EmployeeNumber;
            int evaluationId = theEvaluation.EvaluationId;
            DateTime beforeStart = DateTime.Now;
            logger.Info("get evaluee grades of user " + evalueeId + " evaluation " + evaluationId + " selfEvaluation=" + getSelfEvaluations);
            EvaluationGrades ret = new EvaluationGrades();
            ret.evaluation = theEvaluation;
            ret.employeeId = evalueeId;
            ret.employeeData = theEvaluee;
            bool evalueeCanSelfEvaluate = canSelfEvaluate(evaluationId, evalueeId);
            ret.canSelfEvaluate = evalueeCanSelfEvaluate;
            logger.Info("time for evaluee data (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            DateTime beforePairings = DateTime.Now;
            List<EvaluationPairing> pairings = DA.getEvalueePairings(evaluationId, evalueeId, getSelfEvaluations);
            //List<string> assessorsWhoGraded = DA.getAssessorsWhoGraded(evaluationId, evalueeId);
            logger.Info("time for pairings (ms): " + (int)(DateTime.Now - beforePairings).TotalMilliseconds);
            DateTime beforeCriteria = DateTime.Now;
            //EvaluationCriteria_Multilanguage criteriaMultilanguage = getEvaluationCriteriaConfiguration(evaluationId);
            logger.Info("time for criteria (ms): " + (int)(DateTime.Now - beforeCriteria).TotalMilliseconds);
            int gradesDone = 0;
            if (theEvaluee.GradeStats != null && theEvaluee.GradeStats.averageGrade > 0)
                ret.averageGrade = theEvaluee.GradeStats.averageGrade;
            else
            {
                if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                    ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
            }
            ret.gradesPlanned = (theEvaluee.GradeStats != null && theEvaluee.GradeStats.gradesPlanned > 0 ? theEvaluee.GradeStats.gradesPlanned : pairings.Count);
            ret.gradesDone = (theEvaluee.GradeStats != null && theEvaluee.GradeStats.gradesDone > 0 ? theEvaluee.GradeStats.gradesDone : gradesDone);
            ret.AutoOrManual = ret.grades.Count(x => x.Pairing.AutoOrManual.Equals("man") || x.Pairing.AutoOrManual.Equals("Manual")) > 0 ? "man" : "auto";
            ret.hasChildRows = ret.gradesPlanned > 0;

            DateTime beforePastAverage = DateTime.Now;
            EvaluationReceived history = DA.getLastEvalueeHistoryRecord(evalueeId, evaluationId);
            if (history != null)
                ret.pastAverage = history.Result;
            logger.Info("time for past average (ms): " + (int)(DateTime.Now - beforePastAverage).TotalMilliseconds);
            DateTime beforePromotionStatus = DateTime.Now;
            ret.employeePromotionStatus = DA.getEmployeePromotionStatus(evaluationId, evalueeId);
            ret.canBePromoted = ret.employeePromotionStatus.HasValue && (ret.employeePromotionStatus.Value == PromotionStatus.Proposed || ret.employeePromotionStatus.Value == PromotionStatus.Rejected);

            ret.numberOfReminders = DA.getEvalueeReminders(evaluationId, evalueeId, true).Count;
            ret.hasLogAccess = DA.GetAcesses(evaluationId, evalueeId);
            logger.Info("time for promotion status (ms): " + (int)(DateTime.Now - beforePromotionStatus).TotalMilliseconds);
            logger.Info("time for whole procedure (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            return ret;
        }

        /// <summary>
        /// Get the data to show in the grid of evaluees, filtered 
        /// as specified in the Filters parameter
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public EvaluationData getEvalueeGrid(FilterModels filters)
        {
            return new EvaluationData();
        }

        /// <summary>
        /// Get Evaluee history without 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public EvaluationsReceived getEvalueeHistoryWithoutJoin(string employeeId)
        {
            return DA.getEvalueeHistory(employeeId);
        }

        /// <summary>
        /// Get the history of evaluations of a specific evaluee, with the 
        /// data, average vote etc.
        /// </summary>
        /// <param name="evalueeId"></param>
        /// <returns></returns>
        public EvaluationsReceived getEvalueeHistory(string evalueeId)
        {
            logger.Info("get evaluee history of user " + evalueeId);
            Evaluation currentEvaluation = getCurrentEvaluation();
            EvaluationsReceived history = DA.getEvalueeHistory(evalueeId);
            Contract eContract = DA.getCurrentEmployeeContract(evalueeId);
            EvaluationGrades grades = getEvalueeGrades(currentEvaluation.EvaluationId, evalueeId, false, false);
            if (grades.averageGrade > 0)
            {
                EvaluationReceived record = new EvaluationReceived()
                {
                    ContractDuration = (int)(eContract.contractEndDate - eContract.contractStartDate).TotalDays,
                    ContractEndDate = eContract.contractEndDate,
                    ContractStartDate = eContract.contractStartDate,
                    ContractType = eContract.contractType,
                    EmployeeNumber = evalueeId,
                    EvaluationDate = currentEvaluation.EndDate,
                    EvaluationId = currentEvaluation.EvaluationId,
                    NextEvaluation = currentEvaluation.EndDate,
                    NumberOfAssessors = grades.gradesDone,
                    Result = grades.averageGrade
                };
                history.Add(record);
            }
            return history;
        }

        /// <summary>
        /// Get the history of evaluations of a specific evaluee, with the 
        /// data, average vote etc.
        /// </summary>
        /// <param name="evalueeId"></param>
        /// <returns></returns>
        public EvaluationsReceived getEvalueeStaffDateHistory(string evalueeId)
        {
            logger.Info("get evaluee staff date history of user " + evalueeId);
            Evaluation currentEvaluation = getCurrentEvaluation();
            EvaluationsReceived history = DA.getEvalueeStaffDateHistory(evalueeId);
            Contract eContract = DA.getCurrentEmployeeContract(evalueeId);
            EvaluationGrades grades = getEvalueeGrades(currentEvaluation.EvaluationId, evalueeId, false, false);
            if (grades.averageGrade > 0)
            {
                EvaluationReceived record = new EvaluationReceived()
                {
                    ContractDuration = (int)(eContract.contractEndDate - eContract.contractStartDate).TotalDays,
                    ContractEndDate = eContract.contractEndDate,
                    ContractStartDate = eContract.contractStartDate,
                    ContractType = eContract.contractType,
                    EmployeeNumber = evalueeId,
                    EvaluationDate = currentEvaluation.StaffDate,
                    EvaluationId = currentEvaluation.EvaluationId,
                    NextEvaluation = currentEvaluation.StaffDate,
                    NumberOfAssessors = grades.gradesDone,
                    Result = grades.averageGrade
                };
                history.Add(record);
            }
            return history;
        }

        public EvaluationsReceived getEvalueeStaffDateHistorySimplified(Employee employee)
        {
            logger.Info("get evaluee staff date history of user " + employee.EmployeeNumber);

            EvaluationsReceived history = DA.getEvalueeStaffDateHistory(employee.EmployeeNumber);
            Contract eContract = DA.getCurrentEmployeeContract(employee.EmployeeNumber);
            var currentEvaluation = getCurrentEvaluation();
            EvaluationGrades grades = getEvalueeGradesHistorySimplified(currentEvaluation, employee, false, false);
            if (grades.averageGrade > 0)
            {
                EvaluationReceived record = new EvaluationReceived()
                {
                    ContractDuration = (int)(eContract.contractEndDate - eContract.contractStartDate).TotalDays,
                    ContractEndDate = eContract.contractEndDate,
                    ContractStartDate = eContract.contractStartDate,
                    ContractType = eContract.contractType,
                    EmployeeNumber = employee.EmployeeNumber,
                    EvaluationDate = currentEvaluation.StaffDate,
                    EvaluationId = currentEvaluation.EvaluationId,
                    NextEvaluation = currentEvaluation.StaffDate,
                    NumberOfAssessors = grades.gradesDone,
                    Result = grades.averageGrade
                };
                history.Add(record);
            }
            return history;
        }

        public AssessorStatistics getEvalueeStatistics(int evaluationId, string evalueeId)
        {
            logger.Info("getEvalueeStatistics of user " + evalueeId);
            AssessorStatistics ret = new AssessorStatistics();

            List<EvaluationPairing> pairings = DA.getEvalueePairings(evaluationId, evalueeId, false);
            ret.evaluationsPlanned = pairings.Count;
            ret.evaluationsDone = DA.countEvaluationsObtained(evaluationId, evalueeId);
            return ret;
        }

        public EvaluationReceived getLastEvalueeHistoryRecord(string evalueeId)
        {
            EvaluationReceived ret = null;
            int currentEvalId = getCurrentEvaluationId();

            EvaluationGrades grades = getEvalueeGrades(currentEvalId, evalueeId, false, false);
            if (grades.averageGrade > 0)
            {
                Contract eContract = DA.getCurrentEmployeeContract(evalueeId);
                Evaluation currEval = getCurrentEvaluation();
                EvaluationReceived record = new EvaluationReceived()
                {
                    ContractDuration = (int)(eContract.contractEndDate - eContract.contractStartDate).TotalDays,
                    ContractEndDate = eContract.contractEndDate,
                    ContractStartDate = eContract.contractStartDate,
                    ContractType = eContract.contractType,
                    EmployeeNumber = evalueeId,
                    EvaluationDate = currEval.StaffDate,
                    EvaluationId = currEval.EvaluationId,
                    NextEvaluation = currEval.StaffDate,
                    NumberOfAssessors = grades.gradesDone,
                    Result = grades.averageGrade
                };
                ret = record;
            }

            if (ret == null)
            {
                ret = DA.getLastEvalueeHistoryRecord(evalueeId, getCurrentEvaluationId(), true);
            }

            return ret;
        }

     

        /// <summary>
        /// Gets the privacy configuration
        /// </summary>
        /// <returns></returns>
        public PrivacyConfiguration getPrivacyConfiguration()
        {
            logger.Info("getPrivacyConfiguration");
            return DA.getPrivacyConfiguration();
        }

        /// <summary>
        /// Gets the type 3 notes of a promotion. EmployeeID is the person receiving the note, CommentatorID is 
        /// the one writing the notes
        /// </summary>
        /// <param name="evaluationId"></param>
        /// <param name="employeeId"></param>
        /// <param name="notes"></param>
        public string getPromotionNotes(int evaluationId, string employeeId, string commentatorId)
        {
            logger.Info("get promotion notes of user " + employeeId + " evaluation " + evaluationId + " commentator " + commentatorId);
            return DA.getPromotionNotes(evaluationId, employeeId, commentatorId);
        }

        public PromotionRulesConfiguration getPromotionRulesConfiguration()
        {
            logger.Info("getPromotionRulesConfiguration");
            return DA.getPromotionRulesConfiguration();
        }

        public EmployeeNames getProposedAssessorNames(int evaluationId, string evalueeId)
        {
            logger.Info("get proposed assessor names of user " + evalueeId + " evaluation " + evaluationId);
            EmployeeNames ret = new EmployeeNames();
            List<string> assessors = DA.getAllAssessors(evaluationId);
            if (!ValueOperations.isNullOrEmpty(assessors))
            {
                ret = DA.getEmployeeNames(assessors);
            }

            List<EvaluationPairing> pairings = DA.getEvalueePairings(evaluationId, evalueeId, false);
            if (!ret.IsEmpty() && !ValueOperations.isNullOrEmpty(pairings))
            {
                List<string> acceptedPairings = pairings.Where(x => x.Status == PairingStatus.Accepted).Select(x => x.AssessorId).ToList();
                if (!ValueOperations.isNullOrEmpty(acceptedPairings))
                {
                    var withoutAccepted = ret.Where(x => !acceptedPairings.Contains(x.employeeId)).ToList();
                    ret = new EmployeeNames();
                    ret.AddRange(withoutAccepted);
                }
            }
            if (!ret.IsEmpty() && ret[evalueeId] != null)
                ret.Remove(ret[evalueeId]);

            return ret;
        }


        public EmployeeNames getProposedEvalueeNames(int evaluationId, string assessorId)
        {
            logger.Info("get proposed evaluee names of user " + assessorId + " evaluation " + evaluationId);
            EmployeeNames ret = new EmployeeNames();
            List<string> evaluees = getAllEvalueesIdList(evaluationId);
            if (!ValueOperations.isNullOrEmpty(evaluees))
            {
                ret = DA.getEmployeeNames(evaluees);
            }

            List<EvaluationPairing> pairings = DA.getAssessorPairings(evaluationId, assessorId, false);
            if (!ret.IsEmpty() && !ValueOperations.isNullOrEmpty(pairings))
            {
                List<string> acceptedPairings = pairings.Where(x => x.Status == PairingStatus.Accepted).Select(x => x.EvalueeId).ToList();
                if (!ValueOperations.isNullOrEmpty(acceptedPairings))
                {
                    var withoutAccepted = ret.Where(x => !acceptedPairings.Contains(x.employeeId)).ToList();
                    ret = new EmployeeNames();
                    ret.AddRange(withoutAccepted);
                }
            }
            if (!ret.IsEmpty() && ret[assessorId] != null)
                ret.Remove(ret[assessorId]);

            return ret;
        }

        /// <summary>
        /// Returns the list of assessor ID entries proposed for the whole evaluation
        /// </summary>
        /// <param name="evaluationId"></param>
        /// <returns></returns>
        public ProposedAssessors getProposedAssessors(int evaluationId, bool getPhoto)
        {
            logger.Info("get proposed assessors for evaluation " + evaluationId);
            ProposedAssessors ret = new ProposedAssessors();
            PromotionRulesConfiguration promoRules = DA.getPromotionRulesConfiguration();
            List<string> assessors = DA.getPromotableEvaluees(evaluationId);
            Dictionary<string, Employee> assessorsDict = new Dictionary<string, Employee>();
            Dictionary<string, bool> assessorsCanSelfEvaluate = new Dictionary<string, bool>();
            Dictionary<string, AssessorStatistics> assessorStatisticsStore = new Dictionary<string, AssessorStatistics>();
            if (!ValueOperations.isNullOrEmpty(assessors))
            {
                foreach (string s in assessors)
                {
                    Employee emp = DA.getEmployee(s, getPhoto);
                    EvaluationGrades g = getEvalueeGrades(evaluationId, s, false, assessorsDict, assessorsCanSelfEvaluate, assessorStatisticsStore, getPhoto);
                    /*
                    ret.Add(new ProposedAssessor()
                    {
                        employeeData = emp,
                        averageGrade = g.averageGrade,
                        gradesPlanned = g.gradesPlanned,
                        gradesDone = g.gradesDone,
                        promotionStatus = PromotionStatus.Proposed,
                        pastAverage = g.pastAverage
                    });
                    */
                    double overallAverage = DA.getHistoricOverallAverageGrade(emp.EmployeeNumber);

                    if ((DateTime.Now - emp.HiringDate).TotalDays >= promoRules.minimumSeniority * 30 &&
                            overallAverage >= promoRules.minimumAverageRate)
                    {
                        ret.Add(new ProposedAssessor()
                        {
                            employeeData = emp,
                            averageGrade = g.averageGrade,
                            gradesPlanned = g.gradesPlanned,
                            gradesDone = g.gradesDone,
                            promotionStatus = PromotionStatus.Proposed,
                            pastAverage = g.pastAverage
                        });
                    }
                }
            }

            return ret;
        }

        public Reminders getReminders(string employeeId)
        {
            logger.Info("get reminders for user " + employeeId);
            return DA.getReminders(getCurrentEvaluationId(), employeeId);
        }

        public RemindersConfiguration getRemindersConfiguration()
        {
            logger.Info("get reminders configuration");
            return DA.getRemindersConfiguration();
        }

        public StaffMembersConfiguration getStaffMembersConfiguration()
        {
            logger.Info("get staff members configuration");
            return DA.getStaffMembersConfiguration();
        }

        public EvaluationConnectionFrequencies getTeammates(Evaluation ev)
        {
            logger.Info("getting teammate list for evaluation " + ev.EvaluationId);
            EvaluationConnectionFrequencies ret = new EvaluationConnectionFrequencies();
            EvaluationConnectionFrequencies theFrequencies = new EvaluationConnectionFrequencies();
            EvaluationConnections theConnections = DA.getEvaluationConnections(ev.StartDate, ev.EndDate);
            DateTime currentDate = ev.StartDate.Date;
            while (currentDate < ev.EndDate)
            {
                IEnumerable<EvaluationConnection> todaySpecial = (from x in theConnections where x.ConnectionDate == currentDate select x).ToList();
                var OdpOfTheDay = (from x in todaySpecial select x.ODP).Distinct().ToList();
                if (OdpOfTheDay.Any())
                {
                    foreach (string odp in OdpOfTheDay)
                    {
                        if (!string.IsNullOrEmpty(odp))
                        {
                            var team = (from x in todaySpecial orderby x.EmployeeCode where x.ODP == odp select x.EmployeeCode).ToList();
                            if (team.Any())
                            {
                                string[] teamArray = team.Distinct().ToArray();
                                if (teamArray.Length > 1)
                                {
                                    for (int i = 0; i < teamArray.Length - 1; i++)
                                    {
                                        for (int j = i + 1; j < teamArray.Length; j++)
                                        {
                                            if (!teamArray[i].Equals(teamArray[j]))
                                            {
                                                var freq = theFrequencies.findByCodes(teamArray[i], teamArray[j]);
                                                if (freq != null)
                                                    freq.frequency++;
                                                else
                                                    theFrequencies.Add(new EvaluationConnectionFrequency() { employee1 = teamArray[i], employee2 = teamArray[j] });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                var tasksOfTheDay = (from x in todaySpecial select x.Task).Distinct().ToList();
                if (tasksOfTheDay.Any())
                {
                    foreach (string task in tasksOfTheDay)
                    {
                        if (!string.IsNullOrEmpty(task))
                        {
                            var team = (from x in todaySpecial orderby x.EmployeeCode where (x.Task == task && string.IsNullOrEmpty(x.ODP)) select x.EmployeeCode).ToList();
                            if (team.Any())
                            {
                                string[] teamArray = team.Distinct().ToArray();
                                if (teamArray.Length > 1)
                                {
                                    for (int i = 0; i < teamArray.Length - 1; i++)
                                    {
                                        for (int j = i + 1; j < teamArray.Length; j++)
                                        {
                                            if (!teamArray[i].Equals(teamArray[j]))
                                            {
                                                var freq = theFrequencies.findByCodes(teamArray[i], teamArray[j]);
                                                if (freq != null)
                                                    freq.frequency++;
                                                else
                                                    theFrequencies.Add(new EvaluationConnectionFrequency() { employee1 = teamArray[i], employee2 = teamArray[j] });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                currentDate = currentDate.AddDays(1);
            }

            ret.AddRange((from x in theFrequencies where x.frequency >= 5 select x).ToList());

            return ret;
        }

        /// <summary>
        /// Get all the type 2 notes configured in the system
        /// </summary>
        /// <returns></returns>
        public Type2NoteDefinitions getType2NoteDefinitions()
        {
            logger.Info("get type 2 note definitions");
            return DA.getType2NoteDefinitions();
        }

        public UserUISettings getUserUISettings(string employeeId)
        {
            return DA.getUserUISettings(employeeId);
        }

        public void importEmployees()
        {
            int employeesReceived, employeesInserted, insertionErrors, connectionsReceived, contractsReceived;
            DA.updateEmployeesFromNavision(out employeesReceived, out employeesInserted, out insertionErrors);
        }

        public void insertEmployee(Employee emp)
        {
            DA.insertEmployee(emp);
        }

        public int insertEvaluation(DateTime staffDate, DateTime endDate, Departments departmentsToEvaluate)
        {
            logger.Info("insert evaluation staffDate=" + staffDate.ToString("s") + " endDate=" + endDate.ToString("s"));
            int ret = -1;
            DA.BeginTransaction();
            try
            {
                Evaluation ultimateEvaluation = DA.getUltimateEvaluation();

                if (staffDate < ultimateEvaluation.StaffDate)
                    throw new ValidationException("IE001", "The new evaluation\'s staff date must go after the staff dates of existing evaluations");
                if (endDate < ultimateEvaluation.EndDate)
                    throw new ValidationException("IE002", "The new evaluation\'s end date must go after the end dates of existing evaluations");

                int evaluationId = DA.insertEvaluation(ultimateEvaluation.EndDate.AddDays(1), endDate, staffDate);
                DA.insertDepartmentsToEvaluate(evaluationId, departmentsToEvaluate);
                ret = evaluationId;
                DA.CommitTransaction();
            }
            catch (ValidationException vex)
            {
                DA.RollbackTransaction();
                throw vex;
            }
            catch (Exception ex)
            {
                DA.RollbackTransaction();
            }
            return ret;
        }

        /// <summary>
        /// Manually pair an assessor and an evaluee
        /// </summary>
        /// <param name="assessorId"></param>
        /// <param name="evalueeId"></param>
        public void insertEvaluationPairing(int evaluationId, string assessorId, string evalueeId)
        {
            logger.Info("insert evaluation " + evaluationId + " pairing: " + assessorId + " evaluates " + evalueeId);
            DA.insertEvaluationPairing(evaluationId, assessorId, evalueeId);

            updateGradeStatistics(evaluationId, evalueeId);
            updateAssessorGradeStatistics(evaluationId, assessorId);
        }

        public void insertEvaluee(int evaluationId, string evalueeId, bool proposeForPromotion = false)
        {
            DA.insertEvalueeProposedPromotion(evaluationId, evalueeId, proposeForPromotion, true);
        }

        /// <summary>
        /// Allow an employee to self-evaluate
        /// </summary>
        /// <param name="assessorId"></param>
        /// <param name="evalueeId"></param>
        public void insertSelfEvaluation(int evaluationId, string employeeId)
        {
            logger.Info("insert self evaluation for " + employeeId + " in evaluation " + evaluationId);
            DA.insertSelfEvaluationPairing(evaluationId, employeeId);
        }

        public void insertType2Note(string noteText)
        {
            logger.Info("insert type 2 note " + noteText);
            string noteCode = DA.getNewIdFromSequence("Type2Notes");
            DA.insertType2Note(noteCode, noteText);
        }

        public void logError(Exception ex)
        {
            logger.Error(ex);
        }

        public void logInfo(string s)
        {
            logger.Info(s);
        }

        public string optionalEncryption_test(string s)
        {
            return Encryption.AesEncrypt(s);
        }

        public string optionalDecryption_test(string s)
        {
            return Encryption.AesDecrypt(s);
        }

        /// <summary>
        /// Promotes an evaluee to assessor for the evaluation specified. 
        /// Important note: only the evaluees that are already allowed to become 
        /// assessors will be promoted
        /// </summary>
        /// <param name="evalueeId"></param>
        public void promoteToAssessor(int evaluationId, List<string> evalueeIdList)
        {
            logger.Info("promoting users to assessors in evaluation " + evaluationId);
            if (!ValueOperations.isNullOrEmpty(evalueeIdList))
            {
                DA.updateEvalueePromotion(evaluationId, evalueeIdList);
                DA.updateEmployeeAssessorStatus(evalueeIdList, true);
            }
            var assDowngrade = GetDowngradedAssessors(evaluationId);
            List<string> removeFromDownGrade = new List<string>();
            if (assDowngrade.Any())
            {
                assDowngrade.ForEach(a =>
                {
                    if (evalueeIdList.Contains(a))
                    {
                        removeFromDownGrade.Add(a);
                    }
                });
                if (removeFromDownGrade.Any())
                {
                    DA.RemoveFromDowngrade(removeFromDownGrade);
                }
            }
        }

        public void restoreAssessors()
        {
            int evaluationId = getCurrentEvaluationId();
            List<string> assessorsIdList = DA.getAllAssessors(evaluationId);
            DA.updateEvalueePromotion(evaluationId, assessorsIdList);
        }


        public void sendErrorLog()
        {
            List<ErrorLog> errorsToSend = new List<ErrorLog>();
            if (!ValueOperations.isNullOrEmpty(errorsToSend))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Errors occurred during the last import: <br>").AppendLine();
                foreach (ErrorLog el in errorsToSend)
                {
                    sb.Append(el.logDescription).Append("<br>").AppendLine();

                }
                sb.AppendLine().Append("Please verify the imported data ").Append("<br>").AppendLine();
                sb.Append("<br>").AppendLine().AppendLine("Best regards").Append("<br>");
                sb.Append("The evaluations portal");
                SendMessageAsync("rubino@lenis.tech", "Errors occurred during import", sb.ToString(), sb.ToString().Replace("<br>", ""));

            }
        }

        /// <summary>
        /// Sends reminders urging to perform the evaluation
        /// </summary>
        /// <param name="evaluationReminders">the key is the user doing the evaluation, 
        /// the value is the user being evaluated</param>
        public void sendReminders(Reminders theReminders)
        {
            logger.Info("sending reminders");
            if (theReminders != null && !theReminders.IsEmpty())
            {
                int evaluationId = DA.getCurrentEvaluationId();
                Dictionary<string, List<string>> reminderLists = new Dictionary<string, List<string>>();
                foreach (Reminder r in theReminders)
                {
                    DA.insertReminder(evaluationId, r.assessorId, r.evalueeId, "StandardReminder");
                    if (reminderLists.ContainsKey(r.assessorId))
                        reminderLists[r.assessorId].Add(r.evalueeId);
                    else
                        reminderLists[r.assessorId] = new List<string>() { r.evalueeId };
                }

                if (reminderLists.Count > 0)
                {
                    var theEnum = reminderLists.GetEnumerator();
                    while (theEnum.MoveNext())
                    {
                        EmployeeNames evalueeNames = DA.getEmployeeNames(theEnum.Current.Value);
                        Employee theAssessor = DA.getEmployee(theEnum.Current.Key, false);
                        if (theAssessor != null && evalueeNames != null && !string.IsNullOrWhiteSpace(theAssessor.Email))
                        {
                            string emailBody = buildReminderBody(applicationConfiguration.reminderEmailBody,
                                                                theAssessor.getFullName(),
                                                                evalueeNames.toNameLines());
                            SendMessageAsync(applicationConfiguration.reminderEmailSubject, emailBody, emailBody, theAssessor.Email, theAssessor.EmployeeNumber);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sends assessment requests urging to perform the evaluation
        /// </summary>
        /// <param name="evaluationReminders">the key is the user doing the evaluation, 
        /// the value is the user being evaluated</param>
        public void sendAssessmentRequests()
        {
            logger.Info("sending assessment requests");
            int evaluationId = DA.getCurrentEvaluationId();
            List<string> assessorsToNotify = DA.getAssessmentRequestList(evaluationId);
            if (!ValueOperations.isNullOrEmpty(assessorsToNotify))
            {
                string subject, body;
                DA.getNotificationTemplate("AssessmentRequest", out subject, out body);
                Employees theAssessors = DA.getEmployees(assessorsToNotify, false);
                foreach (Employee sat in theAssessors)
                {
                    if (sat != null && !string.IsNullOrWhiteSpace(sat.Email))
                    {
                        string emailBody = buildReminderBody(body,
                                                            sat.getFullName(),
                                                            string.Empty);
                        DA.insertReminder(evaluationId, sat.EmployeeNumber, sat.EmployeeNumber, "AssessmentRequest");
                        SendMessageAsync(subject, emailBody, emailBody, sat.Email, sat.EmployeeNumber);
                    }
                }

            }
        }

        public void insertEvaluationHistoryRecord(EvaluationReceived record)
        {
            DA.insertEvaluationHistoryRecord(record);
        }

        public void switchCurrentEvaluation_Automatic()
        {
            logger.Info("automatic evaluation switch in progress");
            Evaluations allEval = DA.getAllEvaluations();
            int currentEvalId = getCurrentEvaluationId();
            Evaluation currentEval = allEval[currentEvalId.ToString()];
            if (currentEval.EndDate < DateTime.Now && currentEval.Status == EvaluationStatus.Underway)
            {
                logger.Info("Evaluation " + currentEvalId + " has expired");

                //When evaluation expire start update of employees year average and update total
                logger.Info("Updating Year Average value and Total of evaluation");
                updateEvaluationsSummaries(getAllEmployees(false), currentEval.EndDate);
                logger.Info("End of update Year Average and Total of evaluatation employee");

                //if the end date has elapsed and the evaluation is still underway, 
                //close it and open the next one
                Evaluation nextEvaluation = null;
                IEnumerator<Evaluation> theEnum = allEval.GetEnumerator();
                while (theEnum.MoveNext())
                {
                    if (theEnum.Current.EvaluationId == currentEval.EvaluationId)
                    {
                        //got the current. Now I have to check whether there is an imminent one
                        if (theEnum.MoveNext())
                        {
                            nextEvaluation = theEnum.Current;
                            break;
                        }
                    }
                }

                int nextEvaluationId;
                DateTime nextEvaluationDate = DateTime.MaxValue;
                if (nextEvaluation == null)
                {
                    //create a new evaluation
                    nextEvaluationId = DA.insertEvaluation(DateTime.Now, DateTime.Now.AddMonths(1), DateTime.Now.AddMonths(1));
                    nextEvaluationDate = DateTime.Now.AddMonths(1);
                    nextEvaluation = new Evaluation()
                    {
                        EndDate = nextEvaluationDate,
                        StaffDate = nextEvaluationDate,
                        EvaluationId = nextEvaluationId,
                        StartDate = DateTime.Now,
                        Status = EvaluationStatus.Imminent,
                        departments = currentEval.departments
                    };
                }
                else
                {
                    //start the next evaluation
                    nextEvaluationId = nextEvaluation.EvaluationId;
                    nextEvaluationDate = nextEvaluation.StaffDate;
                    DA.updateEvaluationStartDate(nextEvaluationId, DateTime.Now);
                }

                logger.Info("Next evaluation: " + nextEvaluationId);

                PromotionRulesConfiguration promoRules = DA.getPromotionRulesConfiguration();

                Dictionary<string, EvaluationGrades> allGrades = new Dictionary<string, EvaluationGrades>();
                Employees allEmp = getAllEmployees(false);
                List<string> downgraded = DA.GetDowngradedAssessors(currentEval.EvaluationId);
                List<string> assessorsIdList = DA.getAllAssessors(currentEval.EvaluationId);
                if (!ValueOperations.isNullOrEmpty(downgraded))
                {
                    foreach (string sdd in downgraded)
                        assessorsIdList.Remove(sdd);
                }

                Employees evaluees = new Employees();
                List<string> evalueesIdList = new List<string>();
                foreach (Employee e in allEmp)
                {
                    if (!("debug".Equals(e.EmployeeNumber) ||
                         "demo".Equals(e.EmployeeNumber) ||
                         "lenis".Equals(e.EmployeeNumber)))
                    {
                        Contract eContract = DA.getCurrentEmployeeContract(e.EmployeeNumber);
                        EvaluationGrades grades = getEvalueeGrades(currentEvalId, e.EmployeeNumber, false, false);
                        allGrades.Add(e.EmployeeNumber, grades);
                        EvaluationReceived record = new EvaluationReceived()
                        {
                            ContractDuration = (int)(eContract.contractEndDate - eContract.contractStartDate).TotalDays,
                            ContractEndDate = eContract.contractEndDate,
                            ContractStartDate = eContract.contractStartDate,
                            ContractType = eContract.contractType,
                            EmployeeNumber = e.EmployeeNumber,
                            EvaluationDate = currentEval.StaffDate,
                            EvaluationId = currentEval.EvaluationId,
                            NextEvaluation = nextEvaluationDate,
                            NumberOfAssessors = grades.gradesDone,
                            Result = grades.averageGrade
                        };
                        DA.insertEvaluationHistoryRecord(record);
                    }

                    string nextDateResult;
                    DateTime? nextDate = null;
                    try
                    {
                        nextDate = computeNextEvaluationDate(e, out nextDateResult);
                        if (nextDate == null)
                            logger.Info("next date is null");
                        if (nextDate.HasValue && nextDate.Value > DateTime.MinValue && nextDate.Value <= nextEvaluationDate &&
                            e.EmployeeState.Equals("Attivo"))
                        {
                            //the employee must be evaluated
                            evaluees.Add(e);
                            evalueesIdList.Add(e.EmployeeNumber);
                            logger.Info("Employee " + e.EmployeeNumber + " scheduled for evaluation");
                            DA.insertEvaluationSwitchLog(nextEvaluationId, e.EmployeeNumber, nextDateResult, true);
                        }
                        else
                            DA.insertEvaluationSwitchLog(nextEvaluationId, e.EmployeeNumber, nextDateResult, false);
                    }
                    catch (Exception ndex)
                    {
                        logger.Info("Error while computing next evaluation date for employee");
                        logger.Error(ndex);
                    }
                }
                logger.Info(evaluees.Count + " employees scheduled for evaluation");

                if (evaluees.Any())
                {
                    //list the directors and department heads
                    Employees directors = new Employees();
                    Dictionary<string, Employee> departmentHeads = new Dictionary<string, Employee>();
                    try
                    {
                        directors.AddRange((from x in allEmp where x.IsDirector && x.EmployeeState.Equals("Attivo") select x).ToList());
                        directors.removeEmployees(downgraded);
                    }
                    catch (Exception ex1)
                    {
                        logger.Error(ex1);
                        directors = new Employees();
                    }

                    try
                    {
                        var dh = (from x in allEmp where x.IsHeadOfDepartment && x.EmployeeState.Equals("Attivo") select x).ToList();
                        if (dh.Any())
                        {
                            foreach (Employee ed in dh)
                            {
                                if (!string.IsNullOrWhiteSpace(ed.DepartmentCode) && !departmentHeads.ContainsKey(ed.DepartmentCode) &&
                                    !downgraded.Contains(ed.EmployeeNumber))
                                    departmentHeads.Add(ed.DepartmentCode, ed);

                            }
                        }
                    }
                    catch (Exception ex2)
                    {
                        logger.Error(ex2);
                        departmentHeads = new Dictionary<string, Employee>();
                    }

                    logger.Info(directors.Count + " directors found");
                    logger.Info(departmentHeads.Count + " department heads found");

                    int pairingCount = 0;
                    int manualPairingCount = 0;
                    int selfPairingCount = 0;
                    int assessorsProposed = 0;

                    foreach (Employee currentEvaluee in evaluees)
                    {
                        //directors evaluate all employees
                        foreach (Employee dir in directors)
                        {
                            DA.insertEvaluationPairing(nextEvaluationId, dir.EmployeeNumber, currentEvaluee.EmployeeNumber, false);
                            pairingCount++;
                        }

                        //department heads evaluate all department staff
                        if (!string.IsNullOrWhiteSpace(currentEvaluee.DepartmentCode) && departmentHeads.ContainsKey(currentEvaluee.DepartmentCode))
                        {
                            if (departmentHeads[currentEvaluee.DepartmentCode].EmployeeNumber != currentEvaluee.EmployeeNumber)
                            {
                                DA.insertEvaluationPairing(nextEvaluationId, departmentHeads[currentEvaluee.DepartmentCode].EmployeeNumber, currentEvaluee.EmployeeNumber, false);
                                pairingCount++;
                            }
                        }

                        //manually set pairings are maintained
                        List<EvaluationPairing> oldPairings = DA.getManualEvalueePairingsHistory(currentEvaluee.EmployeeNumber, currentEval.EvaluationId);
                        if (!ValueOperations.isNullOrEmpty(oldPairings))
                        {
                            foreach (EvaluationPairing ep in oldPairings)
                            {
                                if (!downgraded.Contains(ep.AssessorId))
                                {
                                    DA.insertEvaluationPairing(nextEvaluationId, ep.AssessorId, ep.EvalueeId, true);
                                    pairingCount++;
                                    manualPairingCount++;
                                }
                            }
                        }

                        //self-evaluation is maintained
                        bool wasSelfEvaluating = DA.getWasSelfEvaluating(nextEvaluationId, currentEvaluee.EmployeeNumber);
                        if (wasSelfEvaluating)
                        {
                            DA.insertEvaluationPairing(nextEvaluationId, currentEvaluee.EmployeeNumber, currentEvaluee.EmployeeNumber, false);
                            pairingCount++;
                            selfPairingCount++;
                        }

                        //if the evaluee meets the requirements, propose the promotion to assessor
                        bool proposeForPromotion = false;
                        double overallAverage = DA.getHistoricOverallAverageGrade(currentEvaluee.EmployeeNumber);
                        if ((DateTime.Now - currentEvaluee.HiringDate).TotalDays >= promoRules.minimumSeniority * 30 &&
                            overallAverage >= promoRules.minimumAverageRate)
                        {
                            proposeForPromotion = true;
                            assessorsProposed++;
                        }

                        DA.insertEvalueeProposedPromotion(nextEvaluationId, currentEvaluee.EmployeeNumber, proposeForPromotion);
                    }

                    if (!ValueOperations.isNullOrEmpty(assessorsIdList))
                    {
                        DA.updateEvalueePromotion(nextEvaluationId, assessorsIdList);
                        DA.updateEmployeeAssessorStatus(assessorsIdList, true);
                    }
                    if (directors != null && !directors.IsEmpty())
                    {
                        DA.updateEvalueePromotion(nextEvaluationId, directors.Select(x => x.EmployeeNumber).ToList());
                        DA.updateEmployeeAssessorStatus(directors.Select(x => x.EmployeeNumber).ToList(), true);
                    }
                    if (departmentHeads != null && departmentHeads.Count > 0)
                    {
                        DA.updateEvalueePromotion(nextEvaluationId, departmentHeads.Keys.ToList());
                        DA.updateEmployeeAssessorStatus(departmentHeads.Keys.ToList(), true);
                    }

                    //find which employees have worked in the same team for at least 5 days
                    EvaluationConnectionFrequencies teamedEmployees = getTeammates(currentEval);
                    logger.Info("Teams found:" + teamedEmployees.Count);
                    //teammates evaluate each other
                    if (teamedEmployees.Any())
                    {
                        foreach (EvaluationConnectionFrequency ecf in teamedEmployees)
                        {
                            if (assessorsIdList.Contains(ecf.employee1) && evalueesIdList.Contains(ecf.employee2))
                            {
                                DA.insertEvaluationPairing(nextEvaluationId, ecf.employee1, ecf.employee2, false);
                                pairingCount++;
                            }
                            if (assessorsIdList.Contains(ecf.employee2) && evalueesIdList.Contains(ecf.employee1))
                            {
                                DA.insertEvaluationPairing(nextEvaluationId, ecf.employee2, ecf.employee1, false);
                                pairingCount++;
                            }
                        }
                    }
                    logger.Info(pairingCount + " pairings inserted");
                    logger.Info(manualPairingCount + " manual pairings inserted");
                    logger.Info(selfPairingCount + " self pairings inserted");
                    logger.Info(assessorsProposed + " assessors proposed for promotion");
                }

                //The criteria of the evaluation which just expired are reiterated in the next evaluation
                logger.Info("Updating evaluation criteria");
                EvaluationCriteria_Multilanguage oldCriteria = getEvaluationCriteriaConfiguration(currentEvalId, true);
                updateEvaluationCriteriaConfiguration(oldCriteria, nextEvaluationId);

                //Close the current evaluation and start the next one
                logger.Info("Updating the evaluation status");
                DA.updateEvaluationStatus(currentEvalId, EvaluationStatus.Expired);
                DA.updateEvaluationStatus(nextEvaluationId, EvaluationStatus.Underway);

                //updating grade statistics
                logger.Info("Updating grade statistics");
                updateAllGradeStatistics(nextEvaluationId);
                updateAllAssessorGradeStatistics(nextEvaluationId);
            }

            logger.Info("Procedure complete");
        }

        public void terminateDeletedEmployees()
        {
            DA.terminateDeletedEmployees();
        }

        /// <summary>
        /// Caches data from the Navision database. Meant to be scheduled 
        /// on a daily task
        /// </summary>
        public void syncWithNavision_Automatic()
        {
            try
            {
                logger.Info("automatic sync with navision started");
                Employees oldEmp = getAllEmployees(false);
                int employeesReceived, employeesInserted, insertionErrors, connectionsReceived, contractsReceived;
                DA.updateEmployeesFromNavision(out employeesReceived, out employeesInserted, out insertionErrors);
                DA.updateEvaluationConnectionsFromNavision(out connectionsReceived);
                DA.updateContractsFromNavision(out contractsReceived);
                DA.terminateDeletedEmployees();
                logger.Info("Employees received from Navision: " + employeesReceived);
                logger.Info("Employees inserted: " + employeesInserted);
                logger.Info("Insertion errors: " + insertionErrors);
                logger.Info("Connections between coworkers received and inserted: " + connectionsReceived);
                logger.Info("Contracts received and inserted: " + contractsReceived);
                //keep the assessor status for the employees which have been promoted
                List<string> assessorsAlreadyThere = DA.getPromotedAssessors();
                DA.updateEmployeeAssessorStatus(assessorsAlreadyThere, true);
                List<string> currentAssessors = new List<string>();

                int currentEvalId = getCurrentEvaluationId();
                Evaluation currentEval = DA.getEvaluation(currentEvalId);

                List<string> downgraded = DA.GetDowngradedAssessors(currentEval.EvaluationId);
                Employees allEmp = getAllEmployees(false);
                Employees newEvaluees = new Employees();
                List<string> evalueesAlreadyThere = DA.getPromotionEvaluees(currentEvalId);
                List<string> allEvalueesIdList = DA.getPromotionEvaluees(currentEvalId);
                foreach (Employee emp in allEmp)
                {
                    bool recomputeDistanceFromWork = false;
                    Employee oldEmployee = oldEmp[emp.EmployeeNumber];
                    if (oldEmployee == null)
                        recomputeDistanceFromWork = true;
                    else
                    {
                        if (oldEmployee.Home_Address1 != emp.Home_Address1 ||
                            oldEmployee.Home_City != emp.Home_City ||
                            oldEmployee.WorkplaceAddress1 != emp.WorkplaceAddress1 ||
                            oldEmployee.WorkplaceCity != emp.WorkplaceCity)
                            recomputeDistanceFromWork = true;
                    }


                    if (emp.WorkplaceAlgorithm != 2 && emp.EmployeeState.Equals("Attivo") &&
                        recomputeDistanceFromWork)
                    {
                      //  int distance = computeDistanceFromWork(emp);
                     //   DA.updateDistanceFromWork(emp.EmployeeNumber, distance);
                    }
                    if (emp.IsAssessor && emp.EmployeeState.Equals("Attivo") && !downgraded.Contains(emp.EmployeeNumber))
                    {
                        currentAssessors.Add(emp.EmployeeNumber);
                        DA.updateEvalueePromotion(currentEvalId, new List<string>() { emp.EmployeeNumber });
                        /*
                        if (!DA.updateEvalueePromotion(currentEvalId, new List<string>() { emp.EmployeeNumber }))
                        {
                            DA.insertEvalueeProposedPromotion(currentEvalId, emp.EmployeeNumber, true);
                            DA.updateEvalueePromotion(currentEvalId, new List<string>() { emp.EmployeeNumber });
                        }
                        */
                    }
                    /*
                    else
                    {
                        if (!evalueesAlreadyThere.Contains(emp.EmployeeNumber))
                            DA.insertEvalueeProposedPromotion(currentEvalId, emp.EmployeeNumber, false);
                    }
                    */
                    if (!evalueesAlreadyThere.Contains(emp.EmployeeNumber))
                    {
                        string nextDateResult;
                        DateTime? nextDate = computeNextEvaluationDate(emp, out nextDateResult);
                        if (nextDate.HasValue && nextDate > DateTime.MinValue && nextDate.Value <= currentEval.StaffDate &&
                            emp.EmployeeState.Equals("Attivo"))
                        {
                            //the employee must be evaluated
                            newEvaluees.Add(emp);
                            allEvalueesIdList.Add(emp.EmployeeNumber);
                            logger.Info("Employee " + emp.EmployeeNumber + " scheduled for evaluation");
                            DA.insertEvaluationSwitchLog(currentEvalId, emp.EmployeeNumber, nextDateResult, true);
                        }
                        else
                            DA.insertEvaluationSwitchLog(currentEvalId, emp.EmployeeNumber, nextDateResult, false);
                    }
                }
                if (newEvaluees.Any())
                {
                    //list the directors and department heads
                    Employees directors = new Employees();
                    Dictionary<string, Employee> departmentHeads = new Dictionary<string, Employee>();
                    try
                    {
                        directors.AddRange((from x in allEmp where x.IsDirector && x.EmployeeState.Equals("Attivo") select x).ToList());
                        directors.removeEmployees(downgraded);
                    }
                    catch (Exception ex1)
                    {
                        logger.Error(ex1);
                        directors = new Employees();
                    }

                    try
                    {
                        var dh = (from x in allEmp where x.IsHeadOfDepartment && x.EmployeeState.Equals("Attivo") select x).ToList();
                        if (dh.Any())
                        {
                            foreach (Employee ed in dh)
                            {
                                if (!string.IsNullOrWhiteSpace(ed.DepartmentCode) && !departmentHeads.ContainsKey(ed.DepartmentCode) &&
                                    !downgraded.Contains(ed.EmployeeNumber))
                                    departmentHeads.Add(ed.DepartmentCode, ed);

                            }
                        }
                    }
                    catch (Exception ex2)
                    {
                        logger.Error(ex2);
                        departmentHeads = new Dictionary<string, Employee>();
                    }

                    PromotionRulesConfiguration promoRules = DA.getPromotionRulesConfiguration();

                    foreach (Employee currentEvaluee in newEvaluees)
                    {
                        //directors evaluate all employees
                        if (!directors.IsEmpty())
                        {
                            foreach (Employee dir in directors)
                                DA.insertEvaluationPairing(currentEvalId, dir.EmployeeNumber, currentEvaluee.EmployeeNumber, false);
                        }

                        //department heads evaluate all department staff
                        if (!string.IsNullOrEmpty(currentEvaluee.DepartmentCode) && departmentHeads.ContainsKey(currentEvaluee.DepartmentCode))
                        {
                            if (departmentHeads[currentEvaluee.DepartmentCode].EmployeeNumber != currentEvaluee.EmployeeNumber)
                                DA.insertEvaluationPairing(currentEvalId, departmentHeads[currentEvaluee.DepartmentCode].EmployeeNumber, currentEvaluee.EmployeeNumber, false);
                        }

                        //if the evaluee meets the requirements, propose the promotion to assessor
                        EvaluationGrades grades = getEvalueeGrades(currentEvalId, currentEvaluee.EmployeeNumber, false, false);
                        double overallAverage = DA.getHistoricOverallAverageGrade(currentEvaluee.EmployeeNumber);
                        if ((DateTime.Now - currentEvaluee.HiringDate).TotalDays >= promoRules.minimumSeniority * 30 &&
                            overallAverage >= promoRules.minimumAverageRate)
                            DA.insertEvalueeProposedPromotion(currentEvalId, currentEvaluee.EmployeeNumber, true);
                        else
                            DA.insertEvalueeProposedPromotion(currentEvalId, currentEvaluee.EmployeeNumber, false); //insert as regular evaluee
                    }

                    DA.updateEvalueePromotion(currentEvalId, directors.Select(x => x.EmployeeNumber).ToList());
                    DA.updateEmployeeAssessorStatus(directors.Select(x => x.EmployeeNumber).ToList(), true);
                    DA.updateEvalueePromotion(currentEvalId, departmentHeads.Keys.ToList());
                    DA.updateEmployeeAssessorStatus(departmentHeads.Keys.ToList(), true);

                    //find which employees have worked in the same team for at least 5 days
                    Evaluation previousEvaluation;
                    Evaluation nextEvaluation;
                    getAdjacentEvaluations(currentEvalId, out previousEvaluation, out nextEvaluation);
                    EvaluationConnectionFrequencies teamedEmployees = new EvaluationConnectionFrequencies();
                    if (previousEvaluation != null)
                        teamedEmployees = getTeammates(previousEvaluation);
                    else
                        teamedEmployees = getTeammates(currentEval);

                    //teammates evaluate each other
                    if (teamedEmployees.Any())
                    {
                        foreach (EvaluationConnectionFrequency ecf in teamedEmployees)
                        {
                            if (currentAssessors.Contains(ecf.employee1) && allEvalueesIdList.Contains(ecf.employee2))
                                DA.insertEvaluationPairing(currentEvalId, ecf.employee1, ecf.employee2, false);
                            if (currentAssessors.Contains(ecf.employee2) && allEvalueesIdList.Contains(ecf.employee1))
                                DA.insertEvaluationPairing(currentEvalId, ecf.employee2, ecf.employee1, false);
                        }
                    }
                }

                //updating grade statistics
                updateAllGradeStatistics(currentEvalId);
                updateAllAssessorGradeStatistics(currentEvalId);

                sendErrorLog();

                logger.Info("automatic sync with navision completed");

                // TODO: Scommentare per la produzione
                logger.Info("starting average calculation of employee");
                updateEvaluationsSummaries(newEvaluees, DateTime.Now);

            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        /// <summary>
        /// Caches data from the Navision database. Meant to be done only once
        /// </summary>
        public void syncWithNavision_HistoryOnly()
        {
            try
            {
                DA.updateEvalueeHistoryFromNavision();

                List<DateTime> pastEvaluations = DA.getEvaluationDateHistory();
                DateTime epoch = new DateTime(1970, 1, 1);
                bool currentEvaluationFound = false;
                if (!ValueOperations.isNullOrEmpty(pastEvaluations))
                {
                    foreach (DateTime dt in pastEvaluations)
                    {
                        if (dt > epoch)
                        {
                            int evalID = DA.insertEvaluation(epoch, dt, dt);
                            if (!currentEvaluationFound && dt > DateTime.Now)
                            {
                                currentEvaluationFound = true;
                                DA.updateEvaluationStatus(evalID, EvaluationStatus.Underway);
                            }
                            else
                            {
                                if (dt < DateTime.Now)
                                    DA.updateEvaluationStatus(evalID, EvaluationStatus.Closed);
                            }
                            epoch = dt;
                            DA.updateEvaluationCriteriaFromNavision(evalID, dt);
                            DA.updateEvaluationIdInHistory(dt, evalID);
                        }
                    }
                }

                DA.updateEvaluationDetailsFromNavision();
                Evaluations evals = getAllEvaluations();
                foreach (Evaluation ev in evals)
                {
                    updateEvalueesFromDetails(ev.EvaluationId);
                    updateAssessorsFromDetails(ev.EvaluationId);
                    updatePairingsFromDetails(ev.EvaluationId);
                }

                DA.updateTermParentCodes();
                syncWithNavision_Automatic();
            }
            catch (Exception nex)
            {
                logger.Error(nex);
            }
        }

        public void syncWithNavision_HistoryPartial(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                DA.updateEvalueeHistoryFromNavision_Partial(dateFrom, dateTo);

                List<DateTime> pastEvaluations = DA.getEvaluationDateHistory_Partial(dateFrom, dateTo);
                DateTime epoch = new DateTime(1970, 1, 1);
                bool currentEvaluationFound = false;
                Evaluations evals = new Evaluations();
                if (!ValueOperations.isNullOrEmpty(pastEvaluations))
                {
                    foreach (DateTime dt in pastEvaluations)
                    {
                        if (dt > epoch)
                        {
                            int evalID = DA.insertEvaluation(epoch, dt, dt);
                            if (!currentEvaluationFound && dt > DateTime.Now)
                            {
                                currentEvaluationFound = true;
                                DA.updateEvaluationStatus(evalID, EvaluationStatus.Underway);
                            }
                            else
                            {
                                if (dt < DateTime.Now)
                                    DA.updateEvaluationStatus(evalID, EvaluationStatus.Closed);
                            }
                            epoch = dt;
                            DA.updateEvaluationCriteriaFromNavision(evalID, dt);
                            DA.updateEvaluationIdInHistory(dt, evalID);
                            evals.Add(DA.getEvaluation(evalID));
                        }
                    }
                }

                DA.updateEvaluationDetailsFromNavision_Partial(dateFrom, dateTo);
                foreach (Evaluation ev in evals)
                {
                    updateEvalueesFromDetails(ev.EvaluationId);
                    updateAssessorsFromDetails(ev.EvaluationId);
                    updatePairingsFromDetails(ev.EvaluationId);
                }

                DA.updateTermParentCodes(evals.getIdList());
                //syncWithNavision_Automatic();
            }
            catch (Exception nex)
            {
                logger.Error(nex);
            }
        }


        /// <summary>
        /// Update the visibility of the columns in the assessor grid
        /// </summary>
        /// <param name="theColumns"></param>
        public void updateAssessorColumns(GridColumns theColumns, string userId)
        {
            logger.Info("updating assessor columns for user " + userId);
            DA.updateGridColumns("Assessors", theColumns, userId);
        }

        public void updateAssessorsFromDetails(int evaluationId)
        {
            Evaluation ev = DA.getEvaluation(evaluationId);
            List<string> assessorIdList = DA.getAssessorsFromDetails(ev.EndDate);
            if (!ValueOperations.isNullOrEmpty(assessorIdList))
            {
                foreach (string s in assessorIdList)
                {
                    DA.insertEvalueeProposedPromotion(evaluationId, s, false);
                }
                DA.updateEvalueePromotion(evaluationId, assessorIdList);
            }
        }

        public void updateDefaultMessagesConfiguration(DefaultMessagesConfiguration conf)
        {
            logger.Info("updating default messages configuration");
            DA.updateDefaultMessagesConfiguration(conf);
        }

        public void updateDepartmentEvaluees(int currentEvalId)
        {
            try
            {
                logger.Info("updating department evaluees");
                List<string> currentAssessors = new List<string>();

                Evaluation currentEval = DA.getEvaluation(currentEvalId);
                if (currentEval.Status == EvaluationStatus.Underway)
                {
                    Employees allEmp = getAllEmployees(false);
                    Employees newEvaluees = new Employees();
                    //Employees evalueesToRemove = new Employees();
                    List<string> evalueesAlreadyThere = DA.getPromotionEvaluees(currentEvalId);
                    List<string> allEvalueeIdList = DA.getPromotionEvaluees(currentEvalId);
                    foreach (Employee emp in allEmp)
                    {
                        if (emp.IsAssessor)
                            currentAssessors.Add(emp.EmployeeNumber);
                        /*
                        if (emp.IsAssessor)
                        {
                            currentAssessors.Add(emp.EmployeeNumber);
                            if (!DA.updateEvalueePromotion(currentEvalId, new List<string>() { emp.EmployeeNumber }))
                            {
                                DA.insertEvalueeProposedPromotion(currentEvalId, emp.EmployeeNumber, true);
                                DA.updateEvalueePromotion(currentEvalId, new List<string>() { emp.EmployeeNumber });
                            }
                        }
                        else
                        {
                            if (!evalueesAlreadyThere.Contains(emp.EmployeeNumber))
                                DA.insertEvalueeProposedPromotion(currentEvalId, emp.EmployeeNumber, false);
                        }
                        */
                        if (!evalueesAlreadyThere.Contains(emp.EmployeeNumber))
                        {
                            string nextDateResult;
                            DateTime? nextDate = computeNextEvaluationDate(emp, out nextDateResult);
                            if (nextDate.HasValue && nextDate > DateTime.MinValue && nextDate.Value <= currentEval.StaffDate &&
                                emp.EmployeeState.Equals("Attivo"))
                            {
                                //the employee must be evaluated
                                newEvaluees.Add(emp);
                                allEvalueeIdList.Add(emp.EmployeeNumber);
                                logger.Info("Employee " + emp.EmployeeNumber + " scheduled for evaluation");
                                DA.insertEvaluationSwitchLog(currentEvalId, emp.EmployeeNumber, nextDateResult, true);
                            }
                            else
                                DA.insertEvaluationSwitchLog(currentEvalId, emp.EmployeeNumber, nextDateResult, false);
                        }
                        /*
                        else
                        {
                            string nextDateResult;
                            DateTime? nextDate = computeNextEvaluationDate(emp, out nextDateResult);
                            if (!nextDate.HasValue || nextDate <= DateTime.MinValue || nextDate.Value > currentEval.StaffDate)
                            {
                                //the employee must be removed
                                evalueesToRemove.Add(emp);
                                allEvalueeIdList.Remove(emp.EmployeeNumber);
                                logger.Info("Employee " + emp.EmployeeNumber + " removed from evaluation");
                            }
                        }
                        */
                    }
                    if (newEvaluees.Any())
                    {
                        //list the directors and department heads
                        Employees directors = new Employees();
                        Dictionary<string, Employee> departmentHeads = new Dictionary<string, Employee>();
                        List<string> downgraded = DA.GetDowngradedAssessors(currentEval.EvaluationId);
                        try
                        {
                            directors.AddRange((from x in allEmp where x.IsDirector && x.EmployeeState.Equals("Attivo") select x).ToList());
                            directors.removeEmployees(downgraded);
                        }
                        catch (Exception ex1)
                        {
                            logger.Error(ex1);
                            directors = new Employees();
                        }

                        try
                        {
                            var dh = (from x in allEmp where x.IsHeadOfDepartment && x.EmployeeState.Equals("Attivo") select x).ToList();
                            if (dh.Any())
                            {
                                foreach (Employee ed in dh)
                                {
                                    if (!string.IsNullOrWhiteSpace(ed.DepartmentCode) && !departmentHeads.ContainsKey(ed.DepartmentCode) &&
                                        !downgraded.Contains(ed.EmployeeNumber))
                                        departmentHeads.Add(ed.DepartmentCode, ed);

                                }
                            }
                        }
                        catch (Exception ex2)
                        {
                            logger.Error(ex2);
                            departmentHeads = new Dictionary<string, Employee>();
                            logger.Info("setting no heads and continuing");
                        }

                        Evaluation previousEvaluation;
                        Evaluation nextEvaluation;
                        getAdjacentEvaluations(currentEvalId, out previousEvaluation, out nextEvaluation);

                        PromotionRulesConfiguration promoRules = DA.getPromotionRulesConfiguration();
                        logger.Info("inserting new evaluees");
                        foreach (Employee currentEvaluee in newEvaluees)
                        {
                            //directors evaluate all employees
                            foreach (Employee dir in directors)
                                DA.insertEvaluationPairing(currentEvalId, dir.EmployeeNumber, currentEvaluee.EmployeeNumber, false);

                            //department heads evaluate all department staff
                            if (departmentHeads.ContainsKey(currentEvaluee.DepartmentCode))
                            {
                                if (departmentHeads[currentEvaluee.DepartmentCode].EmployeeNumber != currentEvaluee.EmployeeNumber)
                                    DA.insertEvaluationPairing(currentEvalId, departmentHeads[currentEvaluee.DepartmentCode].EmployeeNumber, currentEvaluee.EmployeeNumber, false);
                            }

                            //manually set pairings are maintained
                            List<EvaluationPairing> oldPairings = DA.getManualEvalueePairingsHistory(currentEvaluee.EmployeeNumber, previousEvaluation.EvaluationId);
                            if (!ValueOperations.isNullOrEmpty(oldPairings))
                            {
                                foreach (EvaluationPairing ep in oldPairings)
                                {
                                    if (!downgraded.Contains(ep.AssessorId))
                                    {
                                        DA.insertEvaluationPairing(currentEvalId, ep.AssessorId, ep.EvalueeId, true);
                                    }
                                }
                            }

                            //self-evaluation is maintained
                            bool wasSelfEvaluating = DA.getWasSelfEvaluating(currentEvalId, currentEvaluee.EmployeeNumber);
                            if (wasSelfEvaluating)
                            {
                                DA.insertEvaluationPairing(currentEvalId, currentEvaluee.EmployeeNumber, currentEvaluee.EmployeeNumber, false);
                            }

                            //if the evaluee meets the requirements, propose the promotion to assessor
                            bool proposeForPromotion = false;
                            EvaluationGrades grades = getEvalueeGrades(currentEvalId, currentEvaluee.EmployeeNumber, false, false);
                            double overallAverage = DA.getHistoricOverallAverageGrade(currentEvaluee.EmployeeNumber);
                            if ((DateTime.Now - currentEvaluee.HiringDate).TotalDays >= promoRules.minimumSeniority * 30 &&
                                overallAverage >= promoRules.minimumAverageRate)
                                proposeForPromotion = true;

                            DA.insertEvalueeProposedPromotion(currentEvalId, currentEvaluee.EmployeeNumber, proposeForPromotion); //insert as regular evaluee
                        }

                        //find which employees have worked in the same team for at least 5 days
                        EvaluationConnectionFrequencies teamedEmployees = new EvaluationConnectionFrequencies();
                        if (previousEvaluation != null)
                            teamedEmployees = getTeammates(previousEvaluation);
                        else
                            teamedEmployees = getTeammates(currentEval);

                        //teammates evaluate each other
                        if (teamedEmployees.Any())
                        {
                            foreach (EvaluationConnectionFrequency ecf in teamedEmployees)
                            {
                                if (currentAssessors.Contains(ecf.employee1) && allEvalueeIdList.Contains(ecf.employee2))
                                    DA.insertEvaluationPairing(currentEvalId, ecf.employee1, ecf.employee2, false);
                                if (currentAssessors.Contains(ecf.employee2) && allEvalueeIdList.Contains(ecf.employee1))
                                    DA.insertEvaluationPairing(currentEvalId, ecf.employee2, ecf.employee1, false);
                            }
                        }
                    }
                    /*
                    if (evalueesToRemove.Any())
                    {
                        foreach (Employee nemp in evalueesToRemove)
                        {
                            logger.Info("Deleting evaluee " + nemp.EmployeeNumber + " from evaluation " + currentEvalId);
                            DA.deleteEvaluationPairingsForEvaluee(currentEvalId, nemp.EmployeeNumber);
                            DA.deleteEvalueePromotion(currentEvalId, nemp.EmployeeNumber);
                        }
                    }
                    */
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }


        public void reinstateManualPairings_fix(int currentEvalId)
        {
            try
            {
                List<string> currentAssessors = new List<string>();

                Evaluation currentEval = DA.getEvaluation(currentEvalId);
                if (currentEval.Status == EvaluationStatus.Underway)
                {
                    Employees allEmp = getAllEmployees(false);
                    Employees newEvaluees = new Employees();
                    //Employees evalueesToRemove = new Employees();
                    List<string> evalueesAlreadyThere = DA.getPromotionEvaluees(currentEvalId);
                    List<string> allEvalueeIdList = DA.getPromotionEvaluees(currentEvalId);
                    foreach (Employee emp in allEmp)
                    {
                        if (emp.IsAssessor)
                            currentAssessors.Add(emp.EmployeeNumber);

                        if (evalueesAlreadyThere.Contains(emp.EmployeeNumber))
                        {
                            if (emp.EmployeeState.Equals("Attivo"))
                            {
                                //the employee must be evaluated
                                newEvaluees.Add(emp);
                                allEvalueeIdList.Add(emp.EmployeeNumber);
                            }
                        }
                    }
                    if (newEvaluees.Any())
                    {
                        List<string> downgraded = DA.GetDowngradedAssessors(currentEval.EvaluationId);

                        Evaluation previousEvaluation;
                        Evaluation nextEvaluation;
                        getAdjacentEvaluations(currentEvalId, out previousEvaluation, out nextEvaluation);

                        foreach (Employee currentEvaluee in newEvaluees)
                        {
                            //manually set pairings are maintained
                            List<EvaluationPairing> oldPairings = DA.getManualEvalueePairingsHistory(currentEvaluee.EmployeeNumber, previousEvaluation.EvaluationId);
                            if (!ValueOperations.isNullOrEmpty(oldPairings))
                            {
                                foreach (EvaluationPairing ep in oldPairings)
                                {
                                    if (!downgraded.Contains(ep.AssessorId))
                                    {
                                        DA.insertEvaluationPairing(currentEvalId, ep.AssessorId, ep.EvalueeId, true);
                                    }
                                }
                            }

                            //self-evaluation is maintained
                            bool wasSelfEvaluating = DA.getWasSelfEvaluating(currentEvalId, currentEvaluee.EmployeeNumber);
                            if (wasSelfEvaluating)
                            {
                                DA.insertEvaluationPairing(currentEvalId, currentEvaluee.EmployeeNumber, currentEvaluee.EmployeeNumber, false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void updateEvaluationDates(int evaluationId, DateTime staffDate, DateTime endDate)
        {
            DA.updateEvaluation(evaluationId, staffDate, endDate);
        }

        public void updateEvaluation(int evaluationId, DateTime staffDate, DateTime endDate, Departments departmentsToEvaluate)
        {
            logger.Info("updating evaluation " + evaluationId + " new staffDate=" + staffDate.ToString("s") + " endDate=" + endDate.ToString("s"));

            Evaluation previousEvaluation;
            Evaluation nextEvaluation;
            getAdjacentEvaluations(evaluationId, out previousEvaluation, out nextEvaluation);

            if ((previousEvaluation != null && staffDate < previousEvaluation.StaffDate) ||
                (nextEvaluation != null && staffDate > nextEvaluation.StaffDate))
                throw new ValidationException("IE001", "The new evaluation\'s staff date must go after the staff date of the previous evaluation and before the staff date of the next evaluation");
            if ((previousEvaluation != null && endDate < previousEvaluation.EndDate) ||
                (nextEvaluation != null && endDate > nextEvaluation.EndDate))
                throw new ValidationException("IE002", "The new evaluation\'s end date must go after the end date of the previous evaluation and before the end date of the next evaluation");


            DA.BeginTransaction();
            try
            {
                DA.deleteDepartmentsToEvaluate(evaluationId);
                DA.updateEvaluation(evaluationId, staffDate, endDate);
                DA.insertDepartmentsToEvaluate(evaluationId, departmentsToEvaluate);
                DA.CommitTransaction();
            }
            catch (ValidationException vex)
            {
                logger.Error(vex);
                DA.RollbackTransaction();
                throw vex;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                DA.RollbackTransaction();
            }

            updateDepartmentEvaluees(evaluationId);

            //updating grade statistics
            logger.Info("updating all grade statistics");
            updateAllGradeStatistics(evaluationId);
            updateAllAssessorGradeStatistics(evaluationId);
        }

        public void updateEvaluationCriteriaConfiguration(EvaluationCriteria_Multilanguage theCriteria, int evaluationId)
        {
            logger.Info("update evaluation criteria for " + evaluationId);
            if (theCriteria != null && theCriteria.Any())
            {
                foreach (EvaluationCriterion_Multilanguage crit in theCriteria)
                {
                    if (string.IsNullOrEmpty(crit.CriterionCode))
                        crit.CriterionCode = DA.getNewIdFromSequence("CriterionCodes");
                    if (crit.Subcriteria != null && crit.Subcriteria.Any())
                    {
                        foreach (EvaluationCriterion_Multilanguage subcrit in crit.Subcriteria)
                        {
                            if (string.IsNullOrEmpty(subcrit.CriterionCode))
                                subcrit.CriterionCode = DA.getNewIdFromSequence("CriterionCodes");
                        }
                    }
                }
            }
            DA.updateEvaluationCriteriaConfiguration(evaluationId, theCriteria);
        }

        public void updateEvaluationCriteriaFromNavision(int evalID, DateTime dt)
        {
            DA.updateEvaluationCriteriaFromNavision(evalID, dt);
        }

        public void updateEvaluationIdInHistoryForAll()
        {
            List<DateTime> pastEvaluations = DA.getEvaluationDateHistory();
            DateTime epoch = new DateTime(1970, 1, 1);
            bool currentEvaluationFound = false;
            if (!ValueOperations.isNullOrEmpty(pastEvaluations))
            {
                foreach (DateTime dt in pastEvaluations)
                {
                    if (dt > epoch)
                    {
                        Evaluation foundEval = DA.getEvaluationByDate(dt);

                        epoch = dt;
                        if (foundEval != null)
                            DA.updateEvaluationIdInHistory(dt, foundEval.EvaluationId);
                    }
                }
            }
        }

        /// <summary>
        /// Set the notes for an assessor/evaluee pairing
        /// </summary>
        /// <param name="pairingID"></param>
        /// <param name="notes"></param>
        public void updateEvaluationPairingNotes(int pairingID, string noteCode, string notes)
        {
            logger.Info("update evaluation pairing notes for " + pairingID + " code " + noteCode + " text=" + notes);
            DA.updateEvaluationPairingNotes(pairingID, noteCode, notes);
            var defaultNote = getType2NoteDefinitions().First();
            if (noteCode != defaultNote.noteCode)
            {
                var evP = DA.getEvaluationPairing(pairingID);
                if (evP != null)
                {
                    var gradeStat = DA.getGradeStatistics(evP.EvaluationId, evP.EvalueeId);
                    var gradeAssessorStat = DA.getAssessorGradeStatistics(evP.EvaluationId, evP.AssessorId);
                    int gradesDone = gradeStat.gradesDone + 1;
                    int gradesAssessorDone = gradeAssessorStat.gradesDone + 1;
                    //Aumenta il numero di valutazioni ricevute per il valutato e di valutazioni fatte per il valutatore, ma non cambia la media
                    DA.updateGradeStatistics(evP.EvaluationId, evP.EvalueeId, gradeStat.averageGrade, gradeStat.gradesPlanned, gradesDone);
                    DA.updateAssessorGradeStatistics(evP.EvaluationId, evP.AssessorId, gradeAssessorStat.averageGrade, gradeAssessorStat.gradesPlanned, gradesAssessorDone);
                }
            }
        }

        /// <summary>
        /// Change the status of assessor/evaluee pairings, by specifying if each pairing
        /// is rejected, accepted or just proposed
        /// </summary>
        /// <param name="pairings"></param>
        public void updateEvaluationPairingStatus(Dictionary<int, PairingStatus> pairings)
        {
            logger.Info("update evaluation pairing status");
            DA.updateEvaluationPairingStatus(pairings);
        }

        public void updateEvaluationRulesConfiguration(EvaluationRulesConfiguration conf)
        {
            logger.Info("update evaluation rules configuration");
            DA.updateEvaluationRulesConfiguration(conf);
        }

        public void updateEvaluationStaffDate(int evaluationId, DateTime staffDate)
        {
            logger.Info("update evaluation " + evaluationId + " new staffDate=" + staffDate.ToString("s"));
            DA.updateEvaluationStaffDate(evaluationId, staffDate);
        }

        /// <summary>
        /// Updates the evaluation notes for the current user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="notes"></param>
        public void updateEvaluationType1Notes(int evaluationId, string userId, string note)
        {
            logger.Info("update evaluation type 1 notes evaluation=" + evaluationId + " note=" + note + " user=" + userId);
            DA.updateEvaluationType1Notes(evaluationId, note);
        }

        /// <summary>
        /// Change the visibility of the evaluee grid's columns
        /// </summary>
        /// <param name="theColumns"></param>
        public void updateEvalueeColumns(GridColumns theColumns, string userId)
        {
            logger.Info("update evaluee columns for user " + userId);
            DA.updateGridColumns("Evaluees", theColumns, userId);
        }

        public void insertEvalueeColumns(GridColumns theColumns, string userId)
        {
            logger.Info("insert evaluee columns for user " + userId);
            DA.insertGridColumns("Evaluees", theColumns, userId);
        }

        public void updateEvalueesFromDetails(int evaluationId)
        {
            Evaluation ev = DA.getEvaluation(evaluationId);
            List<string> evalueeIdList = DA.getEvalueesFromDetails(ev.EndDate);
            if (!ValueOperations.isNullOrEmpty(evalueeIdList))
            {
                foreach (string s in evalueeIdList)
                {
                    DA.insertEvalueeProposedPromotion(evaluationId, s, false);
                }
            }
        }

        public void updateFilter(FilterModel theFilter)
        {
            DA.updateFilter(theFilter);
        }

        public void updateGrade(int evaluationId, string assessorId, string evalueeId, string termCode, decimal grade)
        {
            logger.Info("updating grade evaluationId=" + evaluationId + " assessorId=" + assessorId +
                        " evalueeId=" + evalueeId + " termCode=" + termCode + " grade=" + grade);
            bool updated = true;
            try
            {
                DA.updateGrade(evaluationId, assessorId, evalueeId, termCode, grade);
                updateGradeStatistics(evaluationId, evalueeId);
                updateAssessorGradeStatistics(evaluationId, assessorId);
            }
            catch (Exception ex)
            {
                updated = false;
                logger.Error(ex);
            }
            /*
            if(!updated)
                throw new ValidationException("IE003", "Could not insert the grade");
            */
        }

        public void updateCurrentEvaluationGradeStatistics()
        {
            updateAllGradeStatistics(getCurrentEvaluationId());
            updateAllAssessorGradeStatistics(getCurrentEvaluationId());
        }

        public void updateAllGradeStatistics(int evaluationId)
        {
            List<string> allidList = getAllEvalueesIdList(evaluationId);
            foreach (string asi in allidList)
                updateGradeStatistics(evaluationId, asi);
        }

        public void updateAllAssessorGradeStatistics(int evaluationId)
        {
            Employees allEmployees = getAllAssessors(evaluationId);
            foreach (Employee emp in allEmployees)
                updateAssessorGradeStatistics(evaluationId, emp.EmployeeNumber);
        }

        public void updateGradeStatistics(int evaluationId, string evalueeId)
        {
            EvaluationGrades ret = new EvaluationGrades();
            EvaluationCriteria_Multilanguage theCriteria = DA.getEvaluationCriteriaConfiguration(evaluationId);
            List<EvaluationPairing> pairings = DA.getEvalueePairings(evaluationId, evalueeId, false);
            List<string> assessorsWhoGraded = DA.getAssessorsWhoGraded(evaluationId, evalueeId);
            int gradesDone = 0;
            if (!ValueOperations.isNullOrEmpty(pairings))
            {
                foreach (EvaluationPairing ep in pairings)
                {
                    if (ep.NoteCode != null)
                        gradesDone++;
                    else
                    {
                        EvaluationTermsAndNotes theTerms = new EvaluationTermsAndNotes();
                        if (assessorsWhoGraded.Contains(ep.AssessorId))
                            theTerms.Terms = DA.getEvaluationTerms(evaluationId, evalueeId, ep.AssessorId, theCriteria);
                        else
                            theTerms.Terms = new EvaluationTerms();

                        if (theTerms.Terms.Count > 0)
                        {
                            theTerms.AverageGrade = DA.computeAverage(theTerms.Terms);
                            if (theTerms.AverageGrade > 0)
                                gradesDone++;
                        }

                        ret.grades.Add(theTerms);
                    }
                }
                if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                    ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                ret.gradesPlanned = pairings.Count;
                ret.gradesDone = gradesDone;

                DA.updateGradeStatistics(evaluationId, evalueeId, ret.averageGrade, ret.gradesPlanned, ret.gradesDone);
            }
            else
            {
                /*
                foreach (string ep in assessorsWhoGraded)
                {
                    EvaluationTermsAndNotes theTerms = new EvaluationTermsAndNotes();
                    theTerms.Terms = DA.getEvaluationTerms(evaluationId, evalueeId, ep, theCriteria);

                    if (theTerms.Terms.Count > 0)
                    {
                        theTerms.AverageGrade = computeAverage(theTerms.Terms);
                        if (theTerms.AverageGrade > 0)
                            gradesDone++;
                    }
                    ret.grades.Add(theTerms);
                }
                if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                    ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                ret.gradesPlanned = assessorsWhoGraded.Count;
                ret.gradesDone = gradesDone;

                DA.updateGradeStatistics(evaluationId, evalueeId, ret.averageGrade, ret.gradesPlanned, ret.gradesDone);
                */
                DA.updateGradeStatistics(evaluationId, evalueeId, 0, 0, 0);
                logger.Info("no pairings");
            }
        }

        public void updateOnlyDistanceFromWork_Fix()
        {
            Employees allEmp = getAllEmployees(false);
            foreach (Employee emp in allEmp)
            {
               // int distance = computeDistanceFromWork(emp);
              //  DA.updateDistanceFromWork(emp.EmployeeNumber, distance);
            }
        }


        public void updateAssessorGradeStatistics(int evaluationId, string assessorId)
        {
            EvaluationGrades ret = new EvaluationGrades();
            EvaluationCriteria_Multilanguage theCriteria = DA.getEvaluationCriteriaConfiguration(evaluationId);
            List<EvaluationPairing> pairings = DA.getAssessorPairings(evaluationId, assessorId, false);
            List<string> gradedEvaluees = DA.getGradedEvaluees(evaluationId, assessorId);
            int gradesDone = 0;
            if (!ValueOperations.isNullOrEmpty(pairings))
            {
                foreach (EvaluationPairing ep in pairings)
                {
                    if (ep.NoteCode != null)
                        gradesDone++;
                    else
                    {
                        EvaluationTermsAndNotes theTerms = new EvaluationTermsAndNotes();
                        DateTime beforeTerms = DateTime.Now;
                        if (gradedEvaluees.Contains(ep.EvalueeId))
                            theTerms.Terms = DA.getEvaluationTerms(evaluationId, ep.EvalueeId, ep.AssessorId, theCriteria);
                        else
                            theTerms.Terms = new EvaluationTerms();

                        if (theTerms.Terms.Count > 0)
                        {
                            theTerms.AverageGrade = DA.computeAverage(theTerms.Terms);
                            if (theTerms.AverageGrade > 0)
                                gradesDone++;
                        }

                        ret.grades.Add(theTerms);
                    }
                }

                if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                    ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);

                ret.gradesPlanned = pairings.Count;
                ret.gradesDone = gradesDone;

                DA.updateAssessorGradeStatistics(evaluationId, assessorId, ret.averageGrade, ret.gradesPlanned, ret.gradesDone);
            }
            else
            {
                DA.updateAssessorGradeStatistics(evaluationId, assessorId, 0, 0, 0);
                logger.Info("no pairings");
            }
        }

        public void updatePairingsFromDetails(int evaluationId)
        {
            Evaluation ev = DA.getEvaluation(evaluationId);
            List<KeyValuePair<string, string>> pairingList = DA.getPairingsFromDetails(ev.EndDate);
            if (!ValueOperations.isNullOrEmpty(pairingList))
            {
                foreach (KeyValuePair<string, string> kp in pairingList)
                {
                    DA.insertEvaluationPairing(evaluationId, kp.Key, kp.Value, false);
                }
            }
        }

        public void updatePeopleFromDetails_Automatic()
        {
            Evaluations evals = DA.getAllEvaluations();
            foreach (Evaluation ev in evals)
            {
                if (ev.EvaluationId != 152)
                {
                    updateEvalueesFromDetails(ev.EvaluationId);
                    updateAssessorsFromDetails(ev.EvaluationId);
                    updatePairingsFromDetails(ev.EvaluationId);
                }
            }
        }

        public void updatePeopleFromDetails_Single()
        {
            updateEvalueesFromDetails(152);
            updateAssessorsFromDetails(152);
            updatePairingsFromDetails(152);
        }

        /// <summary>
        /// Sets the privacy configuration
        /// </summary>
        /// <param name="conf"></param>
        public void updatePrivacyConfiguration(PrivacyConfiguration conf)
        {
            logger.Info("update privacy configuration");
            DA.updatePrivacyConfiguration(conf);
        }

        /// <summary>
        /// Updates the type 3 notes of a promotion (to be used especially when a promotion 
        /// is rejected). EmployeeID is the person receiving the note, CommentatorID is 
        /// the one writing the notes
        /// </summary>
        /// <param name="evaluationId"></param>
        /// <param name="employeeId"></param>
        /// <param name="notes"></param>
        public void updatePromotionNotes(int evaluationId, string employeeId, string commentatorId, string notes)
        {
            logger.Info("update promotion notes for evaluation " + evaluationId + " employee=" + employeeId + " commentator=" + commentatorId);
            DA.updatePromotionNotes(evaluationId, employeeId, commentatorId, notes);
        }

        public void updatePromotionRulesConfiguration(PromotionRulesConfiguration conf)
        {
            logger.Info("update promotion rules configuration");
            DA.updatePromotionRulesConfiguration(conf);
        }

        public void updatePromotionStatus(int evaluationId, List<string> employeeIdList, PromotionStatus status)
        {
            logger.Info("update promotion status for evaluation " + evaluationId + " status=" + status.ToString());
            DA.updatePromotionStatus(evaluationId, employeeIdList, status);
        }

        public void updateRemindersConfiguration(RemindersConfiguration conf)
        {
            logger.Info("update reminders configuration");
            DA.updateRemindersConfiguration(conf);
        }

        public void updateReminderSent(int reminderId)
        {
            logger.Info("update reminder sent " + reminderId);
            DA.updateReminderSent(reminderId);
        }

        /// <summary>
        /// Update to true to allow self evaluation
        /// </summary>
        /// <param name="allow"></param>
        public void updateSelfEvaluation(int evaluationId, bool allow)
        {
            logger.Info("update evaluation-wide self-grading on " + evaluationId);
            DA.updateSelfEvaluation(evaluationId, allow);
        }

        public void updateStaffMembersConfiguration(StaffMembersConfiguration conf)
        {
            logger.Info("update staff members configuration");
            DA.updateStaffMembersConfiguration(conf);
        }

        public void updateTermsFromDetails()
        {
            EvaluationDetails allDet = DA.getEvaluationDetails();
            foreach (EvaluationDetail det in allDet)
            {
                Evaluation eval = DA.getEvaluationByDate(det.EvaluationDate);
                if (eval != null)
                    DA.updateGrade(eval.EvaluationId, det.AssessorCode, det.UserCode, det.EvaluationTermCode, det.Grade);
            }
        }

        public void updateType2Note(string noteCode, string noteText)
        {
            logger.Info("update type 2 note " + noteCode);
            DA.updateType2Note(noteCode, noteText);
            logger.Info("note updated");
        }
        /*
         public List<EvaluationGrades> getAllEvalueeGrades(int evaluationId, FilterModel filter, int offset, int pageSize, 
                                                            EvalueeTableOrdering ordering, bool ascendingOrder)
        {
            logger.Info("getAllEvalueeGrades of evaluation " + evaluationId);
            List<EvaluationGrades> ret = new List<EvaluationGrades>();

            List<string> theEmployeeIdList = DA.getAllEvaluees_ordered(evaluationId, offset, pageSize, ordering, ascendingOrder);

            Employees theEmployees = DA.getEmployees(theEmployeeIdList);
            Evaluation theEvaluation = getEvaluation(evaluationId);
            //List<string> theEmployeeIdList = getAllEvalueesIdList(evaluationId);
            if (theEmployees!=null && !theEmployees.IsEmpty())
            {
                Dictionary<string, Employee> assessors = new Dictionary<string, Employee>();
                Dictionary<string, bool> assessorsCanSelfEvaluate = new Dictionary<string, bool>();
                Dictionary<string, AssessorStatistics> assessorStatisticsStore = new Dictionary<string, AssessorStatistics>();

                foreach (Employee emp in theEmployees)
                {
                    ret.Add(getEvalueeGrades(theEvaluation, emp, false, assessors, assessorsCanSelfEvaluate, assessorStatisticsStore));
                }
            }
         */
        public void updateUserUISettings(string employeeId, UserUISettings theSettings)
        {
            DA.updateUserUISettings(employeeId, theSettings);
        }

        private void getAdjacentEvaluations(int currentEvalId, out Evaluation previousEval, out Evaluation nextEval)
        {
            Evaluations allEval = DA.getAllEvaluations();
            Evaluation currentEval = allEval[currentEvalId.ToString()];
            previousEval = null;
            nextEval = null;

            IEnumerator<Evaluation> theEnum = allEval.GetEnumerator();
            while (theEnum.MoveNext())
            {
                if (theEnum.Current.EvaluationId == currentEval.EvaluationId)
                {
                    //got the current. Now I have to check whether there is an imminent one
                    if (theEnum.MoveNext())
                    {
                        nextEval = theEnum.Current;
                        break;
                    }
                }
                else
                    previousEval = theEnum.Current;
            }
        }

        /// <summary>
        /// Gets the details of every evaluation term happened in history (the term name, 
        /// the assessor's name, the date etc.)
        /// </summary>
        /// <returns></returns>
        private EvaluationDetails getEvaluationDetails()
        {
            logger.Info("get evaluation details");
            return DA.getEvaluationDetails();
        }
        /// <summary>
        /// Returns the list of assessor ID entries proposed for the specified evaluee
        /// </summary>
        /// <param name="evaluationId"></param>
        /// <param name="evalueeId"></param>
        /// <returns></returns>
        /*
        public ProposedAssessors getProposedAssessors(int evaluationId, string evalueeId, bool getPhoto)
        {
            logger.Info("get proposed assessors of user " + evalueeId + " evaluation " + evaluationId);
            ProposedAssessors ret = new ProposedAssessors();
            List<string> assessors = DA.getPromotionEmployees(evaluationId, "Assessor", PromotionStatus.Accepted);
            Dictionary<string, Employee> assessorsDict = new Dictionary<string, Employee>();
            Dictionary<string, bool> assessorsCanSelfEvaluate = new Dictionary<string, bool>();
            Dictionary<string, AssessorStatistics> assessorStatisticsStore = new Dictionary<string, AssessorStatistics>();
            if (!ValueOperations.isNullOrEmpty(assessors))
            {
                foreach (string s in assessors)
                {
                    Employee emp = DA.getEmployee(s, getPhoto);
                    EvaluationGrades g = getEvalueeGrades(evaluationId, s, false, assessorsDict, assessorsCanSelfEvaluate, assessorStatisticsStore, getPhoto);
                    ret.Add(new ProposedAssessor()
                    {
                        employeeData = emp,
                        averageGrade = g.averageGrade,
                        gradesPlanned = g.gradesPlanned,
                        gradesDone = g.gradesDone,
                        promotionStatus = PromotionStatus.Proposed,
                        pastAverage = g.pastAverage
                    });
                }
            }

            List<EvaluationPairing> pairings = DA.getEvalueePairings(evaluationId, evalueeId, false);
            if (!ret.IsEmpty() && !ValueOperations.isNullOrEmpty(pairings))
            {
                List<string> acceptedPairings = pairings.Where(x => x.Status == PairingStatus.Accepted).Select(x => x.AssessorId).ToList();
                if (!ValueOperations.isNullOrEmpty(acceptedPairings))
                {
                    var withoutAccepted = ret.Where(x => !acceptedPairings.Contains(x.employeeData.EmployeeNumber)).ToList();
                    ret = new ProposedAssessors();
                    ret.AddRange(withoutAccepted);
                }
            }
            if (!ret.IsEmpty() && ret[evalueeId] != null)
                ret.Remove(ret[evalueeId]);

            return ret;
        }
        */
        /*
        public void updateEvaluationTermsConfiguration(EvaluationTerms conf)
        {
            DA.updateEvaluationTermsConfiguration(conf);
        }
        */

        /// <summary>
        /// Method to downgrading the assessors
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="assessorsToDownIdList"></param>
        public void DowngradeAssessors(string userId, List<string> assessorsToDownIdList)
        {
            logger.Info("Downgrading assessors ");
            if (!ValueOperations.isNullOrEmpty(assessorsToDownIdList))
            {
                var currentId = getCurrentEvaluationId();
                DA.DowngradingAssessors(userId, assessorsToDownIdList);
                //Cancello gli accoppiamenti per la valutazione corrente

                assessorsToDownIdList.ForEach(a =>
                {
                    logger.Info("Delete pairings start ");
                    var assPairings = DA.getAssessorPairings(currentId, a, false);
                    assPairings.ForEach(aP =>
                    {
                        deleteEvaluationPairing(currentId, aP.AssessorId, aP.EvalueeId);

                    });
                    var autoPairings = DA.getAssessorPairings(currentId, a, true);
                    if (autoPairings.Any())
                    {
                        autoPairings.ForEach(aT =>
                        {
                            deleteEvaluationPairing(currentId, aT.AssessorId, aT.EvalueeId);
                        });
                    }
                    logger.Info("Delete pairings end ");
                    logger.Info("Start delete evaluation terms");
                    DA.deleteEvaluationTerms(currentId, a);
                    logger.Info("End delete evaluation terms");


                });




            }
        }

        /// <summary>
        /// Method to get downgraded assessors
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="assessorsToDownIdList"></param>
        public List<string> GetDowngradedAssessors(int evaluationId)
        {
            logger.Info("Get Downgraded assessors ");
            return DA.GetDowngradedAssessors(evaluationId);
        }


        /// <summary>
        /// Method to remove downgraded assessors
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="assessorsToDownIdList"></param>
        public void RemoveFromDowngrade(List<string> idsToRemove)
        {
            logger.Info("Remove Downgraded assessors ");
            DA.RemoveFromDowngrade(idsToRemove);
        }

        public EvaluationPairing getAssessorPairing(int evaluationId, string assessorId, string evalueeId)
        {
            return DA.getAssessorPairing(evaluationId, assessorId, evalueeId);
        }


        //public LoginMessage getLoginMessage(string language)
        //{
        //    LoginMessage result = new LoginMessage();
        //    logger.Info($"get login message: language={language}");

        //    result = DA.getLoginMessage(language);

        //    return result;
        //}

        //public LoginMessage_Multilanguage getLoginMessageMultilanguage()
        //{
        //    LoginMessage_Multilanguage result = new LoginMessage_Multilanguage();
        //    logger.Info($"get login message multilanguage");

        //    result = DA.getLoginMessageMultilanguage();

        //    return result;
        //}

        //public bool saveMessages(LoginMessage_Multilanguage[] messages)
        //{
        //    bool result = true;
        //    logger.Info($"save messages");

        //    result = DA.saveMessages(messages);

        //    return result;
        //}

        #region monthly message
        public MonthlyLoginMessage getMonthlyLoginMessage(string code, string language)
        {
            MonthlyLoginMessage result = new MonthlyLoginMessage();
            logger.Info($"get monthly login message: code={code} - language={language}");

            result = DA.getMonthlyLoginMessage(code, language);

            return result;
        }

        public MonthlyLoginMessages getMonthlyLoginMessages(string language)
        {
            MonthlyLoginMessages result = new MonthlyLoginMessages();
            logger.Info($"get monthly login messages: language={language}");

            result = DA.getMonthlyLoginMessages(language);

            return result;
        }

        public MonthlyLoginMessage_Multilanguage getMonthlyLoginMessageMultilanguage(string code)
        {
            MonthlyLoginMessage_Multilanguage result = new MonthlyLoginMessage_Multilanguage();
            logger.Info($"get monthly login message multilanguage: code={code}");

            result = DA.getMonthlyLoginMessageMultilanguage(code);

            return result;
        }

        public MonthlyLoginMessage_Multilanguage getMonthlyLoginMessageMultilanguage(int id)
        {
            MonthlyLoginMessage_Multilanguage result = new MonthlyLoginMessage_Multilanguage();
            logger.Info($"get monthly login message multilanguage: id={id}");

            result = DA.getMonthlyLoginMessageMultilanguage(id);

            return result;
        }

        public bool saveMonthlyMessages(MonthlyLoginMessage_Multilanguage[] messages)
        {
            bool result = true;
            logger.Info($"save messages");

            result = DA.saveMonthlyMessages(messages);

            return result;
        }
        #endregion

        public List<LogoutVideo_Multilanguage> getLogoutVideos()
        {
            List<LogoutVideo_Multilanguage> result = new List<LogoutVideo_Multilanguage>();
            logger.Info($"get logout videos");

            result = DA.getLogoutVideos();

            return result;
        }

        public LogoutVideo_Multilanguage getLogoutVideo(string language)
        {
            LogoutVideo_Multilanguage result = null;
            logger.Info($"get logout video - lang: {language}");

            result = DA.getLogoutVideo(language);

            return result;
        }

        public bool saveVideos(LogoutVideo_Multilanguage[] videos)
        {
            bool result = true;
            logger.Info($"save videos");

            result = DA.saveVideos(videos);

            return result;
        }

        public string GetNavision(string userId)
        {
            string result = null;
            logger.Info($"get navision - userId: {userId}");

            result = DA.getNavision(userId);

            return result;
        }

        public bool removeCriteriaVideo(string id, string lang, int evaluationId)
        {
            bool result = true;
            logger.Info($"remove criteria video");

            result = DA.removeCriteriaVideo(id, lang, evaluationId);

            return result;
        }


        public string FindAverageDifferences()
        {
            string result = null;


            result = DA.FindAverageDifferences();

            return result;
        }

        public double getAverage(string employeenumber, string evaluationId)
        {
            return DA.getAverage(employeenumber, evaluationId);
        }

        public void updateHistoryRecord(string employeeNumber, int evaluationId, decimal result)
        {
            DA.updateHistoryRecord(employeeNumber, evaluationId, result);
        }

        public void updateHistoryRecordAndAssessor(string employeeNumber, int evaluationId, decimal result, int numberOfAssessors)
        {
            DA.updateHistoryRecordAndAssessor(employeeNumber, evaluationId, result, numberOfAssessors);
        }



        #endregion

        #region Custom Filters

        public CustomFilterHeaders getCustomFilterList(string employeeId)
        {
            return DA.getCustomFilterList(employeeId);
        }

        public CustomFilter getCustomFilter(string employeeId, int customFilterId)
        {
            return DA.getCustomFilter(customFilterId);
        }

        public int saveCustomFilter(string employeeId, CustomFilter theFilter)
        {
            int ret = theFilter.customFilterHeader.customFilterId;
            if (theFilter.customFilterHeader.customFilterId == 0)
            {
                int newFilterId = DA.insertCustomFilter(employeeId, theFilter.customFilterHeader.customFilterName);
                theFilter.customFilterHeader.customFilterId = newFilterId;
                theFilter.customFilterModel.FilterId = newFilterId;
                ret = newFilterId;
            }
            DA.updateCustomFilter(theFilter);
            return ret;
        }

        public void deleteCustomFilter(int customFilterId)
        {
            DA.deleteCustomFilter(customFilterId);
        }

        #endregion

        #region Utility

        #region Sendmail


        #region E-MAIL


        public MailMessage BuildEmailBody(string from, string[] to, string bcc, string cc, string subject, string body, MailPriority Priority,
                  bool IsBodyHtml, string[] AttachmentPath)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from);
            foreach (string sender in to)
                mailMessage.To.Add(new MailAddress(sender));
            if (!string.IsNullOrEmpty(bcc))
            {
                mailMessage.Bcc.Add(new MailAddress(bcc));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                mailMessage.CC.Add(new MailAddress(cc));
            }
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            //mailMessage.ReplyToList.Add("rubino@lenis.tech");


            //thomas_mail_prova_IsBodyHtml
            //   IsBodyHtml = true;

            mailMessage.IsBodyHtml = IsBodyHtml;
            if (AttachmentPath != null && AttachmentPath.Length > 0)
                foreach (string Attachment in @AttachmentPath)
                    if (System.IO.File.Exists(Attachment))
                        mailMessage.Attachments.Add(new Attachment(@Attachment));
            mailMessage.Priority = Priority;

            //thomas_mail_prova_IsBodyHtml
            //   IsBodyHtml = false;

            return mailMessage;
        }

        public bool checkEmailServer()
        {
            IPHostEntry hostEntry = Dns.GetHostEntry(applicationConfiguration.EmailSmtpHost);
            IPEndPoint endPoint = new IPEndPoint(hostEntry.AddressList[0], int.Parse(applicationConfiguration.EmailSmtpPort));
            using (Socket tcpSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
            {
                //try to connect and test the rsponse for code 220 = success
                tcpSocket.Connect(endPoint);
                if (!CheckResponse(tcpSocket, 220))
                {
                    return false;
                }

                // send HELO and test the response for code 250 = proper response
                SendData(tcpSocket, string.Format("HELO {0}\r\n", Dns.GetHostName()));
                if (!CheckResponse(tcpSocket, 250))
                {
                    return false;
                }

                // if we got here it's that we can connect to the smtp server
                return true;
            }
        }

        public string SendMessage2(string pMessageSubject, string pMessageBody, string pAlternativeText, string pRecipientAddress, string[] pAttachmentsPath = null)
        {
            string ret = "Exception";
            string[] to = pRecipientAddress.Split(new char[] { ';', ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (to == null || to.Length == 0 || string.IsNullOrWhiteSpace(to[0]))
                return "No destination";
            string from = applicationConfiguration.EmailSmtpSenderAddress;

            //case per destinazione mail 
            string destinazioneCopiaMail = "";

            // Create string for HTML
            String apMessageForHtml = "";
            apMessageForHtml = apMessageForHtml + pMessageBody;
            // HTML generation
            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(apMessageForHtml, null, MediaTypeNames.Text.Html);


            // Create Alternative plain text part
            string alternativeText = pAlternativeText.Replace("<br>", "\n");
            AlternateView avText = AlternateView.CreateAlternateViewFromString(alternativeText, null, MediaTypeNames.Text.Plain);

            // EMAIL building and sending
            MailMessage m = new MailMessage();
            m = BuildEmailBody(from, to, destinazioneCopiaMail, "", pMessageSubject, apMessageForHtml, MailPriority.Normal, true, pAttachmentsPath);
            m.Headers.Add("Message-ID", "<" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "@phonzie.eu>");
            m.AlternateViews.Add(avText);
            m.AlternateViews.Add(avHtml);

            if (m != null)
            {
                ret = SendEmail(m);
            }

            return ret;

        }

        public string SendMessageAsync(string subject, string htmlBody, string plaintextBody, string addresses, string username = null)
        {
            string messageHtml = htmlBody;
            string messagePlaintext = plaintextBody.Replace("<br>", "\n");

            string[] theAddresses = addresses.Split(',', ';', ' ');
            foreach (string adds in theAddresses)
            {
                if (!string.IsNullOrEmpty(adds))
                {
                    CP_SystemNotification noti = new CP_SystemNotification()
                    {
                        dateInserted = DateTime.Now,
                        htmlBody = messageHtml,
                        messageSubject = subject,
                        name = "AsyncEmail",
                        notificationEvent = "AsyncEmail",
                        notificationMethod = "EMAIL",
                        notificationModule = "AsyncEmail",
                        plaintextBody = messagePlaintext,
                        recipientAddress = adds.Trim(),
                        cpUsername = username,
                        recipientName = "",
                        result = "READY",
                        templateId = "AsyncEmail"
                    };
                    DA.insertSystemNotification(noti);
                }
            }
            return String.Empty;
        }
        private bool CheckResponse(Socket socket, int expectedCode)
        {
            while (socket.Available == 0)
            {
                System.Threading.Thread.Sleep(100);
            }
            byte[] responseArray = new byte[1024];
            socket.Receive(responseArray, 0, socket.Available, SocketFlags.None);
            string responseData = Encoding.ASCII.GetString(responseArray);
            int responseCode = Convert.ToInt32(responseData.Substring(0, 3));
            if (responseCode == expectedCode)
            {
                return true;
            }
            return false;
        }

        private void SendData(Socket socket, string data)
        {
            byte[] dataArray = Encoding.ASCII.GetBytes(data);
            socket.Send(dataArray, 0, dataArray.Length, SocketFlags.None);
        }
        private string SendEmail(MailMessage Mail)
        {
            string ret = string.Empty;
            SmtpClient client;
            SmtpClient alternateClient;
            bool retryWithAlternateServer = false;
            string userName;
            string password;
            /*
             * first, try sending with the "official" phonzie server.
             * If it fails (by catching an exception) retry with the other (katamail) server
             */
            try
            {
                userName = applicationConfiguration.EmailSmtpUsername;
                password = applicationConfiguration.EmailSmtpPassword;

                client = new SmtpClient(applicationConfiguration.EmailSmtpHost, int.Parse(applicationConfiguration.EmailSmtpPort));
                client.EnableSsl = bool.Parse(applicationConfiguration.EmailSmtpUseSSL);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                if (!string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(userName))
                    client.Credentials = new NetworkCredential(userName, password);
                else
                    client.Credentials = new System.Net.NetworkCredential();

                client.Send(Mail);
            }
            catch (SmtpException sex)
            {
                retryWithAlternateServer = true;
                logger.Error(sex, "SQL Exception in SendEmail");
                ret = sex.Message;
            }
            catch (Exception exx)
            {
                logger.Error(exx, "Exception in SendEmail");
                ret = exx.Message;
            }

            if (retryWithAlternateServer)
            {
                try
                {
                    userName = applicationConfiguration.EmailSmtpAlternativeUsername;
                    password = applicationConfiguration.EmailSmtpAlternativePassword;

                    alternateClient = new SmtpClient(applicationConfiguration.EmailSmtpAlternativeHost, int.Parse(applicationConfiguration.EmailSmtpAlternativePort));
                    alternateClient.EnableSsl = bool.Parse(applicationConfiguration.EmailSmtpAlternativeUseSSL);
                    alternateClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    alternateClient.UseDefaultCredentials = false;
                    if (!string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(userName))
                        alternateClient.Credentials = new NetworkCredential(userName, password);
                    else
                        alternateClient.Credentials = new System.Net.NetworkCredential();

                    alternateClient.Send(Mail);

                    MailMessage adminWarningMessage = BuildEmailBody(applicationConfiguration.EmailSmtpAlternativeSenderAddress, new string[] { "rubino@phonzie.eu", "bacchi@phonzie.eu", "chiaretti@phonzie.eu", "quindor@katamail.com" }, "", "",
                        "Errore server e-mail", "Il server e-mail non sta funzionando correttamente. I messaggi sono attualmente inviati tramite il server alternativo. Per favore date un'occhiata appena possibile.",
                        MailPriority.Normal, false, null);
                    alternateClient.Send(adminWarningMessage);
                    adminWarningMessage.Dispose();
                    alternateClient = null;
                }
                catch (Exception exx)
                {
                    logger.Error(exx, "Exception in SendEmail");
                    ret = exx.Message;
                }
            }
            Mail.Dispose();
            client = null;
            return ret;
        }

        #endregion


        public void sendMailMerge()
        {
            List<CP_SystemNotification> mailMergeList = DA.lockSystemNotificationsToSend();
            foreach (CP_SystemNotification en in mailMergeList)
            {
                try
                {
                    string ret = null;
                    if (en.notificationMethod == "EMAIL")
                        ret = SendMessage2(en.messageSubject, en.htmlBody, en.plaintextBody, en.recipientAddress);
                    if (string.IsNullOrEmpty(ret))
                        DA.updateSystemNotificationResult(en.notificationId, "SENT", "");
                    else
                        DA.updateSystemNotificationResult(en.notificationId, "FAILED", ret);
                }
                catch (Exception e)
                {
                    DA.updateSystemNotificationResult(en.notificationId, "FAILED", e.Message);
                }
            }
        }

        private string buildSystemNotificationBody(string body, Dictionary<string, string> codenames)
        {
            string ret = body;
            Dictionary<string, string>.Enumerator dictEnum = codenames.GetEnumerator();
            while (dictEnum.MoveNext())
                ret = ret.Replace(dictEnum.Current.Key, dictEnum.Current.Value);
            return ret;
        }
        #endregion

        #region Encryption
        private static string SignToken(byte[] token)
        {
            byte[] key = Encoding.UTF8.GetBytes(ApplicationProperty.Settings.EncryptionKey);
            byte[] salt = new byte[8];
            RNGCryptoServiceProvider.Create().GetBytes(salt);
            byte[] message = new byte[salt.Length + token.Length];
            Array.Copy(salt, message, salt.Length);
            Array.Copy(token, 0, message, salt.Length, token.Length);
            byte[] hash;
            using (HMACMD5 hmac = new HMACMD5(key))
            {
                hash = hmac.ComputeHash(message);
            }

            byte[] signedToken = new byte[salt.Length + hash.Length + token.Length];
            Array.Copy(salt, 0, signedToken, 0, salt.Length);
            Array.Copy(hash, 0, signedToken, salt.Length, hash.Length);
            Array.Copy(token, 0, signedToken, salt.Length + hash.Length, token.Length);

            return Convert.ToBase64String(signedToken);
        }

        private static byte[] VerifyToken(byte[] signedToken)
        {
            byte[] key = Encoding.UTF8.GetBytes(ApplicationProperty.Settings.EncryptionKey);

            byte[] salt = new byte[8];
            Array.Copy(signedToken, 0, salt, 0, salt.Length);

            byte[] hash = new byte[16];
            Array.Copy(signedToken, salt.Length, hash, 0, hash.Length);

            byte[] token = new byte[signedToken.Length - salt.Length - hash.Length];
            Array.Copy(signedToken, salt.Length + hash.Length, token, 0, token.Length);

            byte[] message = new byte[salt.Length + token.Length];
            Array.Copy(salt, 0, message, 0, salt.Length);
            Array.Copy(token, 0, message, salt.Length, token.Length);

            byte[] hash2;
            using (HMACMD5 hmac = new HMACMD5(key))
            {
                hash2 = hmac.ComputeHash(message);
            }

            if (StructuralComparisons.StructuralEqualityComparer.Equals(hash, hash2))
            {
                return token;
            }

            return null;
        }
        #endregion



        public enum ImportStatus
        {
            OK,
            ERROR
        }

        private enum UserStatus
        {
            Disabled = 0,
            Enabled = 1,
            Deleted = 2
        }
        public class ImportResult
        {
            public string Message { get; set; }
            public Dictionary<string, List<int>> NotifyList { get; set; }
            public ImportStatus Status { get; set; }
        }


        public string GetTrend(decimal averageGrade, decimal pastAverageGrade, decimal tolerance)
        {
            try
            {
                if (averageGrade != 0)
                {

                    var tl = (pastAverageGrade * tolerance) / 100;
                    var pastAverageGradeUp = pastAverageGrade + tl;
                    var pastAverageGradedown = pastAverageGrade - tl;
                    if (averageGrade > pastAverageGradeUp)
                    {
                        return "up";
                    }
                    else if (averageGrade < pastAverageGradedown)
                    {
                        //evaluate.AverageRatingTrend =
                        return "down";
                    }
                    else
                    {
                        return "equal";
                    }
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                logger.Error(DateTime.Now + " : errore durante GetTrend: -->" + e.Message + "-->" + e.StackTrace);
                throw;
            }
        }



        public List<int> getAssessmentRequestList(int evaluationId)
        {

            logger.Info("sending assessment requests");
            int employeeWithouthEmail = 0;
            var assessorsToNotify = DA.getAssessmentRequestList(evaluationId);
            int totalEmployee = assessorsToNotify.Count();
            Employees theAssessors = DA.getEmployees(assessorsToNotify, false);
            foreach (Employee sat in theAssessors)
            {
                if (sat.Email == null)
                {
                    employeeWithouthEmail += 1;
                }
            }
            List<int> result = new List<int>();
            result.Add(totalEmployee - employeeWithouthEmail);
            result.Add(employeeWithouthEmail);
            return result;

        }

        public double getVoiceYearAverage(string userId, string yearRef, string criterionCode)
        {

            logger.Info("get year average");

            double average = DA.getVoiceYearAverage(userId, yearRef, criterionCode);

            return average;

        }

        public double getTotalYearAverage(string userId, string yearRef)
        {

            logger.Info("get Total Year Average");

            double average = DA.getTotalYearAverage(userId, yearRef);

            return average;

        }
        #endregion
    }
}


