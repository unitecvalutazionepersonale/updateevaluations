﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UnitecCommons
    {
        public static class Enums
        {
            public static string GetDescription(Enum value)
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                DescriptionAttribute[] attributes =
                    (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute),
                    false);

                if (attributes != null &&
                    attributes.Length > 0)
                    return attributes[0].Description;
                else
                    return value.ToString();
            }

            public static T GetEnumFromDescriptionOrName<T>(string description)
            {
                T ret;
                bool found = false;
                try
                {
                    ret = GetEnumFromDescription<T>(description);
                    found = true;
                    return ret;
                }
                catch (ArgumentException)
                {
                    found = false;
                }
                if (!found)
                {
                    try
                    {
                        ret = GetEnumFromName<T>(description);
                        found = true;
                        return ret;
                    }
                    catch (Exception)
                    {
                        found = false;
                    }
                }
                return default(T);
            }

            public static T GetEnumFromDescription<T>(string description)
            {
                Type type = typeof(T);
                if (description != null)
                {

                    DescriptionAttribute attribute;
                    if (!type.IsEnum) throw new InvalidOperationException();
                    foreach (FieldInfo field in type.GetFields())
                    {
                        if (!field.IsSpecialName)
                        {
                            attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                            if (attribute != null)
                            {
                                if (attribute.Description.Equals(description, StringComparison.CurrentCultureIgnoreCase))
                                    return (T)field.GetValue(null);
                            }
                            else
                            {
                                if (field.Name.Equals(description, StringComparison.CurrentCultureIgnoreCase))
                                    return (T)field.GetValue(null);
                            }
                        }
                    }
                }
                throw new ArgumentException("Not found " + description + " in descriptions of enum " + type.ToString());
            }

            public static T GetEnumFromName<T>(string name)
            {
                Type type = typeof(T);
                if (!type.IsEnum) throw new InvalidOperationException();
                FieldInfo fi = type.GetField(name);
                if (fi != null)
                    return (T)fi.GetValue(null);
                throw new ArgumentException("Not found " + name + " in names of enum " + type.ToString());
            }
        }

        public enum TwoFactorAuthenticationType
        {
            None = 0,
            GoogleAuth = 1,
        }
    }


