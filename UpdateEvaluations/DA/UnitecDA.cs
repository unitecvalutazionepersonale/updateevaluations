﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Data.SqlClient;
using System.Threading;
using UpdateEvaluations.BE;
using UnitecCommons;

namespace UpdateEvaluations.DA
{

    internal class UnitecDA
    {
        #region Global members

        UnitecConfiguration applicationConfiguration;

        SqlConnection connection;

        string connectionString;

        LogFactory logFactory;


        //private static Logger logger = LogManager.GetCurrentClassLogger();
        Logger logger;
        string navisionConnectionString = "Data Source=SRVVALUTAZIONI;Initial Catalog=VALUTAZIONI;Persist Security Info=True;User ID=lenis;Password=LNS639kwq!;Connection Timeout=1200";
        SqlTransaction transaction;
        public UnitecDA(string connectionString, UnitecConfiguration applicationConfiguration)
        {
            this.connectionString = connectionString;
            connection = new SqlConnection(this.connectionString);
            logFactory = new LogFactory();
            //logFactory.Configuration = new NLog.Config.XmlLoggingConfiguration(@"D:\Cityphone\UnitecValutazionePersonale\UnitecCore\NLog.config", true, logFactory);
            logger = logFactory.GetCurrentClassLogger();
            this.applicationConfiguration = applicationConfiguration;

        }


        public void BeginTransaction()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
                connection.Open();
            transaction = connection.BeginTransaction();
        }

        public void CommitTransaction()
        {
            transaction.Commit();
            if (connection.State == System.Data.ConnectionState.Open)
                connection.Close();
        }

        public string getConnectionString()
        {
            return connectionString;
        }


        public void RollbackTransaction()
        {
            transaction.Rollback();
            if (connection.State == System.Data.ConnectionState.Open)
                connection.Close();
        }

        int ExecuteCount(string query, SqlParameter[] parameters)
        {
            Int32 ret = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, connection, transaction))
                {
                    //cmd.Parameters.Clear();
                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            cmd.Parameters.Add(param);
                        }
                    }
                    ret = (Int32)cmd.ExecuteScalar();
                }
            }
            catch (Exception xx)
            {
                ret = 0;
            }

            return ret;
        }

        int ExecuteNonQuery(string query, params SqlParameter[] parameters)
        {
            return ExecuteNonQuery(query, true, parameters);
        }

        int ExecuteNonQuery(string query, bool writeLog, params SqlParameter[] parameters)
        {
            if (writeLog)
                logger.Info("Execute Non-Query:" + query);
            using (SqlCommand cmd = new SqlCommand(query, connection, transaction))
            {
                cmd.CommandTimeout = 10800;
                foreach (SqlParameter param in parameters)
                    cmd.Parameters.Add(param);

                int ret = cmd.ExecuteNonQuery();
                if (writeLog)
                    logger.Info("Results:" + ret);
                return ret;
            }
        }

        SqlDataReader ExecuteReader(string query, params SqlParameter[] parameters)
        {
            return ExecuteReader(query, true, parameters);
        }

        SqlDataReader ExecuteReader(string query, bool writeLog, params SqlParameter[] parameters)
        {
            if (writeLog)
                logger.Info("Execute Reader:" + query);
            using (SqlCommand cmd = new SqlCommand(query, connection, transaction))
            {
                cmd.CommandTimeout = 10800;
                if (parameters != null)
                    foreach (SqlParameter param in parameters)
                        cmd.Parameters.Add(param);
                SqlDataReader reader = cmd.ExecuteReader();
                if (writeLog)
                    logger.Info("Has Results:" + reader.HasRows);

                return reader;
            }
        }

        SqlDataReader ExecuteReaderNoLog(string query, params SqlParameter[] parameters)
        {
            return ExecuteReader(query, false, parameters);
        }

        string ExecuteScalar(string query, params SqlParameter[] parameters)
        {
            string ret = "";
            try
            {

                logger.Info("Execute Scalar:" + query);
                using (SqlCommand cmd = new SqlCommand(query, connection, transaction))
                {
                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            cmd.Parameters.Add(param);
                        }
                    }
                    object result = cmd.ExecuteScalar();

                    ret = result != null ? result.ToString() : null;
                    logger.Info("Result:" + ret);

                }
            }
            catch (Exception xx)
            {
                ret = "";
            }

            return ret;
        }

        int ExecuteStoredProcedure(string query, bool writeLog, params SqlParameter[] parameters)
        {
            if (writeLog)
                logger.Info("Execute Stored Procedure:" + query);
            using (SqlCommand cmd = new SqlCommand(query, connection, transaction))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                foreach (SqlParameter param in parameters)
                    cmd.Parameters.Add(param);

                int ret = cmd.ExecuteNonQuery();
                if (writeLog)
                    logger.Info("Results:" + ret);
                return ret;
            }
        }

        SqlDataReader ExecuteStoredProcedureReader(string query, bool writeLog, params SqlParameter[] parameters)
        {
            if (writeLog)
                logger.Info("Execute Stored Procedure Reader:" + query);
            using (SqlCommand cmd = new SqlCommand(query, connection, transaction))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.CommandTimeout = 600;
                if (parameters != null)
                    foreach (SqlParameter param in parameters)
                        cmd.Parameters.Add(param);
                SqlDataReader reader = cmd.ExecuteReader();
                if (writeLog)
                    logger.Info("Has Results:" + reader.HasRows);

                return reader;
            }
        }


        double ExecuteSum(string query, SqlParameter[] parameters)
        {
            double ret = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand(query, connection, transaction))
                {
                    //cmd.Parameters.Clear();
                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                        {
                            cmd.Parameters.Add(param);
                        }
                    }
                    ret = (double)cmd.ExecuteScalar();
                }
            }
            catch (Exception xx)
            {
                ret = 0;
            }

            return ret;
        }
        #endregion

        #region Common functions

        public SqlParameter createImageParameter(string parameterName, Byte[] parameterValue)
        {
            SqlParameter ret = new SqlParameter(parameterName, ValueOperations.bytesToDB(parameterValue));
            ret.SqlDbType = System.Data.SqlDbType.Image;
            return ret;
        }

        public SqlParameter createParameter(string parameterName, string parameterValue)
        {
            SqlParameter ret = new SqlParameter(parameterName, ValueOperations.stringToDB(parameterValue));
            return ret;
        }

        public SqlParameter createParameter(string parameterName, DateTime parameterValue)
        {
            SqlParameter ret = new SqlParameter(parameterName, ValueOperations.dateToDB(parameterValue));
            return ret;
        }

        public SqlParameter createParameter(string parameterName, DateTime? parameterValue)
        {
            if (parameterValue.HasValue)
                return new SqlParameter(parameterName, ValueOperations.dateToDB(parameterValue.Value));
            else
                return new SqlParameter(parameterName, DBNull.Value);
        }

        public SqlParameter createParameter(string parameterName, double? parameterValue)
        {
            if (parameterValue.HasValue)
                return new SqlParameter(parameterName, ValueOperations.doubleToDB(parameterValue.Value));
            else
                return new SqlParameter(parameterName, DBNull.Value);
        }

        public SqlParameter createParameter(string parameterName, int? parameterValue)
        {
            if (parameterValue.HasValue)
                return new SqlParameter(parameterName, ValueOperations.integerToDB(parameterValue.Value));
            else
                return new SqlParameter(parameterName, DBNull.Value);
        }

        public SqlParameter createParameter(string parameterName, double parameterValue)
        {
            SqlParameter ret = new SqlParameter(parameterName, ValueOperations.doubleToDB(parameterValue));
            return ret;
        }

        public SqlParameter createParameter(string parameterName, int parameterValue)
        {
            SqlParameter ret = new SqlParameter(parameterName, ValueOperations.integerToDB(parameterValue));
            return ret;
        }

        public SqlParameter createParameter(string parameterName, decimal parameterValue)
        {
            SqlParameter ret = new SqlParameter(parameterName, ValueOperations.decimalToDB(parameterValue));
            return ret;
        }

        public SqlParameter createParameter(string parameterName, bool parameterValue)
        {
            SqlParameter ret = new SqlParameter(parameterName, ValueOperations.boolToDB(parameterValue));
            return ret;
        }

        public SqlParameter createParameter(string parameterName, long parameterValue)
        {
            SqlParameter ret = new SqlParameter(parameterName, ValueOperations.longToDB(parameterValue));
            return ret;
        }

        public SqlParameter createParameter(string parameterName, Byte[] parameterValue)
        {
            if (parameterValue == null)
                return new SqlParameter(parameterName, System.Data.SqlTypes.SqlBinary.Null);
            else
                return new SqlParameter(parameterName, ValueOperations.bytesToDB(parameterValue));
        }



        /// <summary>
        /// Convert an array of bytes to a string of hex digits
        /// </summary>
        /// <param name="bytes">array of bytes</param>
        /// <returns>String of hex digits</returns>
        public string HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();
        }
        public bool insertLog(string logType, string description, string username, double amount, string paymentPreference, int applicationId = 0)
        {
            return insertLog(logType, description, username, amount, paymentPreference, null, applicationId);
        }

        public bool insertLog(string logType, string description, string username, double amount, string paymentPreference, string relatedObject,
                            int applicationId = 0)
        {
            bool ret = false;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                //Insert the parking into the DB
                string query = "INSERT INTO Logs (LogType, Description, Date, UserName, Amount, PaymentPreference, RelatedObject, ApplicationId) " +
                    " VALUES (@LogType, @Description, GETDATE(), @UserName, @Amount, @PaymentPreference, @RelatedObject, @ApplicationId)";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@LogType",logType),
                    createParameter("@Description",description),
                    createParameter("@UserName",username),
                    createParameter("@Amount",amount),
                    createParameter("@PaymentPreference", paymentPreference),
                    createParameter("@RelatedObject", relatedObject),
                    createParameter("@ApplicationId", applicationId)
                };

                ret = ExecuteNonQuery(query, pars) > 0;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
            return ret;
        }

        public bool insertLoginHistory(string username, int applicationId = 0)
        {
            bool ret = false;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                //Insert the parking into the DB
                string query = "INSERT INTO LoginHistory (Username,ApplicationId,LoginDate) " +
                    " VALUES (@Username, @ApplicationId, GETDATE())";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@Username",username),
                    createParameter("@ApplicationId",applicationId)
                };

                ret = ExecuteNonQuery(query, pars) > 0;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
            return ret;
        }

        private string amountToString(double pNumVal)
        {
            string ritString = "";
            const string apNumberOfDecimals = "0.00";
            try
            {
                ritString = pNumVal.ToString(apNumberOfDecimals);
            }
            catch
            {
                ritString = "0";
            }
            return ritString;
        }

        private string anonymizeEmployeeId(string original)
        {
            string ret = original;
            int rr;
            if (int.TryParse(original, out rr))
            {
                ret = "E" + ((rr + 1439) * 2) + "M";
            }
            return ret;
        }

        private DateTime normalizeAndDecrypt(object dbvalue)
        {
            DateTime ret = DateTime.MinValue;
            DateTime epoch = new DateTime(1901, 1, 1);
            if (DBNull.Value != dbvalue)
            {
                string s = ValueOperations.DBToString(dbvalue);
                if (!string.IsNullOrEmpty(s))
                {
                    s = optionalDecryption(s);
                    DateTime temp;
                    if (DateTime.TryParse(s, out temp))
                        ret = temp;
                    if (ret < epoch)
                        ret = DateTime.MinValue;
                }
            }
            return ret;
        }

        private string optionalDecryption(string s)
        {
            string ret = s;
            if (!string.IsNullOrWhiteSpace(s) && applicationConfiguration.useDbEncryption)
            {
                ret = Encryption.AesDecrypt(s);
            }

            return ret;
        }

        private string optionalEncryption(string s)
        {
            string ret = s;
            if (!string.IsNullOrWhiteSpace(s) && applicationConfiguration.useDbEncryption)
            {
                ret = Encryption.AesEncrypt(s);
            }

            return ret;
        }

        private string optionalEncryption(DateTime dt)
        {
            string ret = null;
            if (dt > DateTime.MinValue)
            {
                ret = dt.ToString("s");
                if (applicationConfiguration.useDbEncryption)
                {
                    ret = Encryption.AesEncrypt(ret);
                }
            }

            return ret;
        }
        #endregion

        #region Employees Accesses
        //todo: get dati popolamento tabella di log

        public int GetCurrentAccessId(string employeeNumber, int evaluationId)
        {
            int result = 0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                var sql = @"select top(1) AccessId 
					from [dbo].EmployeeAccesses 
                    where EmployeeNumber = @empId and EvaluationId = @evalId
                    and LogoutDate is null
                    order by LoginDate desc";

                var res = ExecuteScalar(sql,
                    new[] {
                        createParameter("@empId", employeeNumber),
                        createParameter("@evalId", evaluationId),
                    });

                if (res != null)
                    result = Convert.ToInt32(res);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        public int GetLastClosedAccessId(string employeeNumber, int evaluationId)
        {
            int result = 0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                var sql = @"select top(1) AccessId [dbo].EmployeeAccesses 
                    where EmployeeNumber = @empId and EvaluationId = @evalId
                    and LogoutDate is not null
                    order by LoginDate desc";

                var res = ExecuteScalar(sql,
                    new[] {
                        createParameter("@empId", employeeNumber),
                        createParameter("@evalId", evaluationId),
                    });

                if (res != null)
                    result = Convert.ToInt32(res);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        public DateTime GetLoginDate(int accessId)
        {
            DateTime result = DateTime.Now;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                var sql = @"select LoginDate from [dbo].EmployeeAccesses 
                    where AccessId = @accessId";

                var res = ExecuteScalar(sql,
                    new[] {
                        createParameter("@accessId", accessId)
                    });

                if (!string.IsNullOrEmpty(res))
                    result = Convert.ToDateTime(res);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        public int InsertAccess(string employeeNumber, int evaluationId)
        {
            int result = 0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                var sql = @"insert into [dbo].EmployeeAccesses (LoginDate, EmployeeNumber, EvaluationId) 
                    values
                    (getdate(), @empId, @evalId);
                    select SCOPE_IDENTITY();";

                var res = ExecuteScalar(sql,
                    new[] {
                        createParameter("@empId", employeeNumber),
                        createParameter("@evalId", evaluationId)
                    });

                if (res != null)
                    result = Convert.ToInt32(res);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        public bool InsertOrUpdateNotesAccess(int accessId, string notes, string pointImprovement, string recognizeYourself)
        {
            bool result = false;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                var sql = @"update [dbo].EmployeeAccesses set Notes = @notes, PointImprovement = @pointImprovement, RecognizeYourself = @recognizeYourself
                    where AccessId = @accessId";

                var res = ExecuteNonQuery(sql,
                    new[] {
                        createParameter("@accessId", accessId),
                        createParameter("@notes", notes),
                        createParameter("@pointImprovement", pointImprovement),
                        createParameter("@recognizeYourself", recognizeYourself),
                    });

                result = res > 0;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        public bool InsertLogoutAccess(int accessId, DateTime logoutDate, long loginDuration)
        {
            bool result = false;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                var sql = @"update [dbo].EmployeeAccesses set LogoutDate = @logoutDate, LoginDuration = @loginDuration
                    where AccessId = @accessId";

                var res = ExecuteNonQuery(sql,
                    new[] {
                        createParameter("@accessId", accessId),
                        createParameter("@logoutDate", logoutDate),
                        createParameter("@loginDuration", loginDuration)
                    });

                result = res > 0;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        public bool InsertOrUpdateCriterionAccessDetail(int accessId, string criterionCode,
            int? videoViewDuration, decimal grade, string trend)
        {
            bool result = false;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                //mi tocca fare una pre query altrimenti inserirebbe dei record in dettaglio che non deve inserire.
                //caso di refresh home, se non c'è nessun record null legato all'accessId e criterionCode inserisce un nuovo record quando non dovrebbe.
                var preQuery = "select 1 from [dbo].EmployeeAccessDetails where AccessId = @accessId and CriterionCode = @criterionCode";

                var resPre = ExecuteScalar(preQuery,
                    new[] {
                        createParameter("@accessId", accessId),
                        createParameter("@criterionCode", criterionCode)
                    });

                var sql = string.Empty;

                if (resPre != null)
                {
                    //solo nel caso in cui la durata di visualizzazione sia settata
                    if (videoViewDuration.HasValue)
                    {
                        sql = @"if exists (select 1 from [dbo].EmployeeAccessDetails where AccessId = @accessId and CriterionCode = @criterionCode and VideoViewDuration is null)
                            update [dbo].EmployeeAccessDetails set VideoViewDuration = @videoViewDuration, VideoViewDate = getdate() where AccessId = @accessId and CriterionCode = @criterionCode and VideoViewDuration is null
                        else
                            insert into [dbo].EmployeeAccessDetails (CriterionCode, VideoViewDuration, VideoViewDate, Grade, Trend, AccessId)
                            values
                            (@criterionCode, @videoViewDuration, getdate(), @grade, @trend, @accessId)";
                    }
                }
                else
                {
                    sql = @"insert into [dbo].EmployeeAccessDetails (CriterionCode, VideoViewDuration, Grade, Trend, AccessId)
                        values
                        (@criterionCode, @videoViewDuration, @grade, @trend, @accessId)";
                }

                if (!string.IsNullOrEmpty(sql))
                {
                    var res = ExecuteNonQuery(sql,
                        new[] {
                        createParameter("@accessId", accessId),
                        createParameter("@criterionCode", criterionCode),
                        createParameter("@videoViewDuration", videoViewDuration),
                        createParameter("@grade", grade),
                        createParameter("@trend", trend),
                        });

                    result = res > 0;
                }
                else
                {
                    result = true;
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        public bool InsertNotesCriterionAccessDetail(int accessId, string criterionCode,
            string note)
        {
            bool result = false;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                //mi tocca fare una pre query altrimenti inserirebbe dei record in dettaglio che non deve inserire.
                //caso di refresh home, se non c'è nessun record null legato all'accessId e criterionCode inserisce un nuovo record quando non dovrebbe.
                var preQuery = "select 1 from [dbo].EmployeeAccessDetails where AccessId = @accessId and CriterionCode = @criterionCode";

                var resPre = ExecuteScalar(preQuery,
                    new[] {
                        createParameter("@accessId", accessId),
                        createParameter("@criterionCode", criterionCode)
                    });

                var sql = string.Empty;

                if (resPre != null)
                {
                    //dovrebbe fare solo update perché il voto e il trend l'ha già inserito
                    sql = @"if exists (select 1 from [dbo].EmployeeAccessDetails where AccessId = @accessId and CriterionCode = @criterionCode and Note is null)
                        update [dbo].EmployeeAccessDetails set Note = @note where AccessId = @accessId and CriterionCode = @criterionCode and Note is null
                    else
                        insert into [dbo].EmployeeAccessDetails (CriterionCode, Note, AccessId)
                        values
                        (@criterionCode, @note, @accessId)";
                }
                else
                {
                    sql = @"insert into [dbo].EmployeeAccessDetails (CriterionCode, Note, AccessId)
                        values
                        (@criterionCode, @note, @accessId)";
                }

                if (!string.IsNullOrEmpty(sql))
                {
                    var res = ExecuteNonQuery(sql,
                        new[] {
                        createParameter("@accessId", accessId),
                        createParameter("@criterionCode", criterionCode),
                        createParameter("@note", note)
                        });

                    result = res > 0;
                }
                else
                {
                    result = true;
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }
        #endregion

        #region Employees Privileges

        public List<int> GetPrivilegeIdList(int roleId)
        {
            List<int> ret = new List<int>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "SELECT PrivilegeId FROM RolesToPrivileges WHERE RoleId=@RoleId";
                SqlParameter[] pars = new SqlParameter[] {
                        createParameter("@RoleId", roleId)
                    };

                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                    ret.Add(ValueOperations.DBToInteger(reader["PrivilegeId"]));

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        /// <summary>
        /// Lista dei privileggi cui sono associati dei WorkplaceAlgorithm valorizzati (non null)
        /// </summary>
        /// <returns></returns>
        public List<Privileges> GetAllPrivilegesWithWorkplaceAlgorithm()
        {
            List<Privileges> ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "SELECT * FROM Privileges WHERE WorkplaceAlgorithm IS NOT NULL";

                SqlDataReader reader = ExecuteReader(query);
                if (!reader.HasRows)
                {
                    reader.Close();
                    return ret;
                }

                ret = new List<Privileges>();

                while (reader.Read())
                    ret.Add(new Privileges()
                    {
                        PrivilegeId = ValueOperations.DBToInteger(reader["PrivilegeId"]),
                        WorkplaceAlgorithm = ValueOperations.DBToInteger(reader["WorkplaceAlgorithm"]),
                        PrivilegeName = ValueOperations.DBToString(reader["PrivilegeName"]),
                        PrivilegeNotes = ValueOperations.DBToString(reader["PrivilegeNotes"])
                    });

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        #endregion

        #region Evaluations

        #region user utils
        public string getNavision(string userId)
        {
            var ret = string.Empty;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                var sql = @"select NavisionEmployeeNumber
                from [dbo].Employees
                where EmployeeNumber = @id";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@id", userId)
                };
                string res = ExecuteScalar(sql, pars);

                if (res == null || res is DBNull)
                    ret = "navision_" + userId;
                else
                    ret = res.ToString();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        #endregion

        #region criteria
        public bool removeCriteriaVideo(string id, string lang, int evaluationId)
        {
            bool result = true;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"update [dbo].EvaluationCriteriaLanguages set VideoPath = null, VideoEnable = 0 where CriterionCode = @id and LanguageCode = @lang and EvaluationId = @evid";

                var parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@id", id));
                parameters.Add(new SqlParameter("@lang", lang));
                parameters.Add(new SqlParameter("@evid", evaluationId));

                ExecuteNonQuery(query, parameters.ToArray());
            }
            catch
            {
                result = false;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }
        #endregion

        #region videos
        /// <summary>
        /// Get video by a specific language
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public LogoutVideo_Multilanguage getLogoutVideo(string language)
        {
            LogoutVideo_Multilanguage result = new LogoutVideo_Multilanguage();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select lvl.LogoutVideoId, lvl.LanguageCode, lvl.Path, lvl.Enable
                    from [dbo].LogoutVideoLanguages lvl
                    where lvl.LanguageCode = @lang";
                SqlDataReader reader = ExecuteReader(query,
                    new SqlParameter("@lang", language));

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result = new LogoutVideo_Multilanguage
                        {
                            Id = Convert.ToInt32(reader["LogoutVideoId"]),
                            LanguageCode = reader["LanguageCode"].ToString(),
                            Path = reader["Path"] is DBNull
                                ? default(string)
                                : reader["Path"].ToString(),
                            Enable = Convert.ToBoolean(reader["Enable"])
                        };
                    }
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        /// <summary>
        /// Get all videos
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public List<LogoutVideo_Multilanguage> getLogoutVideos(bool? isEnable = null)
        {
            List<LogoutVideo_Multilanguage> result = new List<LogoutVideo_Multilanguage>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                var query = new StringBuilder(@"select lvl.LogoutVideoId, lvl.LanguageCode, lvl.Path, lvl.Enable
                    from [dbo].LogoutVideoLanguages lvl");

                if (isEnable.HasValue)
                    query.Append(" where lvl.Enable = 1 and lvl.Path is not null ");

                SqlDataReader reader = ExecuteReader(query.ToString());

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.Add(new LogoutVideo_Multilanguage
                        {
                            Id = Convert.ToInt32(reader["LogoutVideoId"]),
                            LanguageCode = reader["LanguageCode"].ToString(),
                            Path = reader["Path"] is DBNull
                                ? default(string)
                                : reader["Path"].ToString(),
                            Enable = Convert.ToBoolean(reader["Enable"])
                        });
                    }
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        public bool saveVideos(LogoutVideo_Multilanguage[] videos)
        {
            bool result = true;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            BeginTransaction();

            try
            {
                string query = @"if not exists (select 1 from [dbo].LogoutVideoLanguages where LogoutVideoId = @id)
                                    insert into [dbo].LogoutVideoLanguages (LanguageCode, Path, Enable) values (@lang, @path, @enable)
                                else
                                    update [dbo].LogoutVideoLanguages set Path = @path, Enable = @enable where LogoutVideoId = @id";
                foreach (var v in videos)
                {
                    var parameters = new List<SqlParameter>();

                    parameters.Add(new SqlParameter("@id", v.Id));
                    parameters.Add(new SqlParameter("@lang", v.LanguageCode));
                    if (v.Path == null || v.VideoDeleted)
                    {
                        parameters.Add(new SqlParameter("@path", DBNull.Value));
                    }
                    else
                        parameters.Add(new SqlParameter("@path", v.Path));

                    parameters.Add(new SqlParameter("@enable", v.Enable));

                    var n = ExecuteNonQuery(query, parameters.ToArray());

                    if (n == 0)
                        throw new Exception("Record da aggiornare non trovato o non inserito");
                }

                CommitTransaction();
            }
            catch
            {
                RollbackTransaction();

                result = false;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }
        #endregion

        #region messages
        ///// <summary>
        ///// Get message by code and language
        ///// </summary>
        ///// <param name="code"></param>
        ///// <param name="language"></param>
        ///// <returns></returns>
        //public LoginMessage getLoginMessage(string language)
        //{
        //	LoginMessage result = null;

        //	bool wasOpen = false;
        //	if (connection.State == System.Data.ConnectionState.Open)
        //		wasOpen = true;
        //	else
        //		connection.Open();

        //	try
        //	{
        //		string query = @"select lml.LanguageCode, lml.Message
        //                  from [dbo].LoginMessageLanguages lml
        //                  where lml.LanguageCode = @lang";
        //		SqlDataReader reader = ExecuteReader(query,
        //			new SqlParameter("@lang", language));

        //		if (reader.HasRows)
        //		{
        //			while (reader.Read())
        //			{
        //				result = new LoginMessage
        //				{
        //					LanguageCode = reader["LanguageCode"].ToString(),
        //					Message = reader["message"].ToString()
        //				};
        //			}
        //		}
        //		reader.Close();
        //	}
        //	finally
        //	{
        //		if (!wasOpen)
        //			connection.Close();
        //	}

        //	return result;
        //}

        ///// <summary>
        ///// Get a message in all languages
        ///// </summary>
        ///// <param name="code"></param>
        ///// <returns></returns>
        //public LoginMessage_Multilanguage getLoginMessageMultilanguage()
        //{
        //	LoginMessage_Multilanguage result = new LoginMessage_Multilanguage();

        //	bool wasOpen = false;
        //	if (connection.State == System.Data.ConnectionState.Open)
        //		wasOpen = true;
        //	else
        //		connection.Open();

        //	try
        //	{
        //		string query = @"select lml.Message, lml.LanguageCode
        //                  from [dbo].LoginMessageLanguages lml";
        //		SqlDataReader reader = ExecuteReader(query);

        //		if (reader.HasRows)
        //		{
        //			result = new LoginMessage_Multilanguage();

        //			while (reader.Read())
        //			{
        //				result.Messages.Add(reader["languageCode"].ToString(), reader["message"].ToString());
        //			}
        //		}
        //		reader.Close();
        //	}
        //	finally
        //	{
        //		if (!wasOpen)
        //			connection.Close();
        //	}

        //	return result;
        //}

        ///// <summary>
        ///// save messages
        ///// </summary>
        ///// <param name="messages"></param>
        ///// <returns></returns>
        //public bool saveMessages(LoginMessage_Multilanguage[] messages)
        //{
        //	bool result = true;

        //	bool wasOpen = false;
        //	if (connection.State == System.Data.ConnectionState.Open)
        //		wasOpen = true;
        //	else
        //		connection.Open();

        //	BeginTransaction();

        //	try
        //	{
        //		string query = @"if not exists (select 1 from [dbo].LoginMessageLanguages where LanguageCode = @lang)
        //                                  insert into [dbo].LoginMessageLanguages (LanguageCode, Message) values (@lang, @message)
        //                              else
        //                                  update [dbo].LoginMessageLanguages set Message = @message where LanguageCode = @lang";
        //		foreach (var m in messages)
        //		{
        //			foreach (var mm in m.Messages)
        //			{
        //				var parameters = new List<SqlParameter>();

        //				parameters.Add(new SqlParameter("@lang", mm.Key));
        //				parameters.Add(new SqlParameter("@message", mm.Value));

        //				var n = ExecuteNonQuery(query, parameters.ToArray());

        //				if (n == 0)
        //					throw new Exception("Record da aggiornare non trovato o non inserito");
        //			}

        //			//faccio ripostare tra un messaggio e l'altro
        //			Thread.Sleep(400);
        //		}

        //		CommitTransaction();
        //	}
        //	catch
        //	{
        //		RollbackTransaction();
        //		result = false;
        //	}
        //	finally
        //	{
        //		if (!wasOpen)
        //			connection.Close();
        //	}

        //	return result;
        //}

        #region monthly messages
        public bool saveMonthlyMessages(MonthlyLoginMessage_Multilanguage[] messages)
        {
            bool result = true;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            BeginTransaction();

            try
            {
                string query = @"if not exists (select 1 from [dbo].MonthlyLoginMessageLanguages where LanguageCode = @lang and MonthlyLoginMessageId = @id)
                                    insert into [dbo].MonthlyLoginMessageLanguages (MonthlyLoginMessageId, LanguageCode, Message) values (@id, @lang, @message)
                                else
                                    update [dbo].MonthlyLoginMessageLanguages set Message = @message where LanguageCode = @lang and MonthlyLoginMessageId = @id";
                foreach (var m in messages)
                {
                    foreach (var mm in m.Messages)
                    {
                        var parameters = new List<SqlParameter>();

                        parameters.Add(new SqlParameter("@id", m.Id));
                        parameters.Add(new SqlParameter("@lang", mm.Key));
                        parameters.Add(new SqlParameter("@message", mm.Value));

                        var n = ExecuteNonQuery(query, parameters.ToArray());

                        if (n == 0)
                            throw new Exception("Record da aggiornare non trovato o non inserito");
                    }

                    //faccio ripostare tra un messaggio e l'altro
                    Thread.Sleep(400);
                }

                CommitTransaction();
            }
            catch
            {
                RollbackTransaction();
                result = false;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        /// <summary>
        /// Get message by code and language
        /// </summary>
        /// <param name="code"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public MonthlyLoginMessage getMonthlyLoginMessage(string code, string language)
        {
            MonthlyLoginMessage result = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select mlm.Code, mlm.MonthlyLoginMessageId, mlml.Message, mlml.LanguageCode
                    from [dbo].MonthlyLoginMessage mlm
                    left join [dbo].MonthlyLoginMessageLanguages mlml on mlm.MonthlyLoginMessageId = mlml.MonthlyLoginMessageId and mlml.LanguageCode = @lang
                    where mlm.Code = @code and mlm.[enable] = 1";
                SqlDataReader reader = ExecuteReader(query,
                    new SqlParameter("@code", code),
                    new SqlParameter("@lang", language));

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result = new MonthlyLoginMessage
                        {
                            Code = reader["code"].ToString(),
                            Id = Convert.ToInt32(reader["monthlyLoginMessageId"]),
                            Message = reader["message"].ToString()
                        };
                    }
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        /// <summary>
        /// Get all messages by a specific language
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public MonthlyLoginMessages getMonthlyLoginMessages(string language)
        {
            MonthlyLoginMessages result = new MonthlyLoginMessages();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select mlm.Code, mlm.MonthlyLoginMessageId, mlml.Message, mlml.LanguageCode
                    from [dbo].MonthlyLoginMessage mlm
                    left join [dbo].MonthlyLoginMessageLanguages mlml on mlm.MonthlyLoginMessageId = mlml.MonthlyLoginMessageId and mlml.LanguageCode = @lang
					where mlm.[enable] = 1
                    order by mlm.Code";
                SqlDataReader reader = ExecuteReader(query,
                    new SqlParameter("@lang", language));

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.Add(new MonthlyLoginMessage
                        {
                            Code = reader["code"].ToString(),
                            Id = Convert.ToInt32(reader["monthlyLoginMessageId"]),
                            Message = reader["message"].ToString()
                        });
                    }
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        /// <summary>
        /// Get a message in all languages
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MonthlyLoginMessage_Multilanguage getMonthlyLoginMessageMultilanguage(string code)
        {
            MonthlyLoginMessage_Multilanguage result = new MonthlyLoginMessage_Multilanguage();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select mlm.Code, mlm.MonthlyLoginMessageId, mlml.Message, mlml.LanguageCode
                    from [dbo].MonthlyLoginMessage mlm
                    left join [dbo].MonthlyLoginMessageLanguages mlml on mlm.MonthlyLoginMessageId = mlml.MonthlyLoginMessageId
                    where mlm.Code = @code and mlm.[enable] = 1";
                SqlDataReader reader = ExecuteReader(query,
                    new SqlParameter("@code", code));

                if (reader.HasRows)
                {
                    result = new MonthlyLoginMessage_Multilanguage();

                    while (reader.Read())
                    {
                        if (string.IsNullOrEmpty(result.Code))
                            result.Code = reader["code"].ToString();

                        if (result.Id == default(int))
                            result.Id = Convert.ToInt32(reader["monthlyLoginMessageId"]);

                        result.Messages.Add(reader["languageCode"].ToString(), reader["message"].ToString());
                    }
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }

        /// <summary>
        /// Get a message in all languages
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public MonthlyLoginMessage_Multilanguage getMonthlyLoginMessageMultilanguage(int id)
        {
            MonthlyLoginMessage_Multilanguage result = new MonthlyLoginMessage_Multilanguage();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select mlm.Code, mlm.MonthlyLoginMessageId, mlml.Message, mlml.LanguageCode
                    from [dbo].MonthlyLoginMessage mlm
                    left join [dbo].MonthlyLoginMessageLanguages mlml on mlm.MonthlyLoginMessageId = mlml.MonthlyLoginMessageId
                    where mlm.MonthlyLoginMessageId = @id and mlm.[enable] = 1";
                SqlDataReader reader = ExecuteReader(query,
                    new SqlParameter("@id", id));

                if (reader.HasRows)
                {
                    result = new MonthlyLoginMessage_Multilanguage();

                    while (reader.Read())
                    {
                        if (string.IsNullOrEmpty(result.Code))
                            result.Code = reader["code"].ToString();

                        if (result.Id == default(int))
                            result.Id = Convert.ToInt32(reader["monthlyLoginMessageId"]);

                        result.Messages.Add(reader["languageCode"].ToString(), reader["message"].ToString());
                    }
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return result;
        }
        #endregion

        #endregion

        #region evaluation trends
        private EvaluationTrend getEvaluationTrend(SqlDataReader reader, int evaluationId, decimal tolerance)
        {
            var o = new EvaluationTrend();
            o.Grade = Convert.ToDecimal(reader["average"]);
            o.CriterionCode = reader["termcode"].ToString();
            //o.Trend = GetTrend(Convert.ToString(reader["EmployeeNumber"]), evaluationId, tolerance);
            //todo: to set VideoDateView

            return o;
        }

        public ICollection<EvaluationTrend> getEvaluationTerm(string employeeNumber, string lang, string[] criterionCode, decimal tolerance, int? evaluationId = null)
        {
            var wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            if (!evaluationId.HasValue)
                evaluationId = getLastEvaluationId(employeeNumber);

            var sql = $@"select e.EvaluationStatus, e.StaffDate, ec.CriterionCode, 
                isnull(ecl.CriterionDescription, isnull(ecl2.CriterionDescription, ec.CriterionDescription)) as CriterionDescription, 
                ecl.VideoPath, ecl.VideoEnable,
                emp.EmployeeNumber, avg(et.Grade) as Grade,
                t.videoViewDate
                from [dbo].Evaluations e
                inner join [dbo].EvaluationCriteria ec on e.EvaluationId = ec.EvaluationId
                left join [dbo].EvaluationCriteriaLanguages ecl on ec.EvaluationId = ecl.EvaluationId and ec.CriterionCode = ecl.CriterionCode and ecl.LanguageCode = @lang
                left join [dbo].EvaluationCriteriaLanguages ecl2 on ec.CriterionCode = ecl2.CriterionCode and ecl2.LanguageCode = @lang and ecl2.CriterionDescription is not null
                inner join [dbo].EvaluationTerms et on et.TermCode = ec.CriterionCode and e.EvaluationId = et.EvaluationId
                inner join [dbo].Employees emp on emp.EmployeeNumber = et.EvalueeId
                left join (
                    select max(ead.VideoViewDate) as VideoViewDate, ea.EvaluationId, ead.CriterionCode, ea.EmployeeNumber
                    from  [dbo].EmployeeAccesses ea
                    inner join [dbo].EmployeeAccessDetails ead on ea.AccessId = ead.AccessId
                    where ea.EvaluationId = @evalId
                    and ea.EmployeeNumber = @empNum
		            and ead.VideoViewDate is not null
                    group by ea.EvaluationId, ead.CriterionCode, ea.EmployeeNumber
                ) as t on t.EvaluationId = e.EvaluationId and t.CriterionCode = ec.CriterionCode and t.EmployeeNumber = emp.EmployeeNumber
                where emp.EmployeeNumber = @empNum and e.EvaluationId = @evalId and ec.CriterionCode in ({string.Join(",", criterionCode.Select(x => $"'{x}'"))})
                group by e.EvaluationStatus, e.StaffDate, ec.CriterionCode, 
                isnull(ecl.CriterionDescription, isnull(ecl2.CriterionDescription, ec.CriterionDescription)), 
                ecl.VideoPath, ecl.VideoEnable,
                emp.EmployeeNumber, t.videoViewDate";

            try
            {
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@empNum", employeeNumber),
                    createParameter("@evalId", evaluationId),
                    createParameter("@lang", lang)
                };

                var result = new List<EvaluationTrend>();

                SqlDataReader reader = ExecuteReader(sql, pars);
                while (reader.Read())
                {
                    var o = getEvaluationTrend(reader, evaluationId.Value, tolerance);
                    result.Add(o);
                }
                reader.Close();

                foreach (var o in result)
                {
                    o.Trend = GetTrend(o.EmployeeNumber, evaluationId.Value, tolerance);
                }

                return result;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return null;
        }

        public ICollection<EvaluationTrend> getWorstEvaluationTerm(string employeeNumber, string lang, decimal tolerance, int? evaluationId = null, int? topRecord = 3)
        {
            var wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            if (!evaluationId.HasValue)
                evaluationId = getLastEvaluationId(employeeNumber);
            var eval = getEvaluation(evaluationId.Value);
            var accesId = GetCurrentAccessId(employeeNumber, evaluationId.Value);

            var getWorsTerms = $@"select distinct {(topRecord.HasValue ? $"top({topRecord.Value})" : "")} (termcode), avg(grade) as average 
                               from evaluationterms where evaluationId = @evaluationId and evalueeId = @employeeNumber and grade>0  group by (termcode)order by average";

            try
            {
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@employeeNumber", employeeNumber),
                    createParameter("@evaluationId", evaluationId),

                };

                var result = new List<EvaluationTrend>();

                SqlDataReader reader = ExecuteReader(getWorsTerms, pars);
                while (reader.Read())
                {
                    var o = getEvaluationTrend(reader, evaluationId.Value, tolerance);
                    result.Add(o);
                }
                reader.Close();

                //TODO - 08/01/2020 - da questo punto il codice potrebbe essere sostituito dal nuovo metodo VoiceConfiguration
                var criterion = getEvaluationCriteriaConfiguration(evaluationId.Value, true);

                foreach (var o in result)
                {
                    var criteria = criterion.SingleOrDefault(c => c.CriterionCode.Equals(o.CriterionCode));
                    string description = "";
                    bool enable = false;
                    string videoPath = "";
                    bool isFind = false;
                    if (criteria == null)
                    {
                        criterion.ToList().ForEach(c =>
                        {
                            if (c.Subcriteria.Any())
                            {
                                if (!isFind)
                                {
                                    criteria = c.Subcriteria.SingleOrDefault(cr => cr.CriterionCode.Equals(o.CriterionCode));
                                }
                                if (criteria != null)
                                {
                                    isFind = true;
                                }
                            }
                        });
                    }
                    if (criteria != null)
                    {
                        o.Status = eval.Status;
                        o.StaffDate = eval.StaffDate;
                        criteria.CriterionDescription.TryGetValue(lang, out description);
                        o.CriterionDescription = description;
                        criteria.VideoPaths.TryGetValue(lang, out videoPath);
                        o.VideoPath = videoPath;
                        criteria.VideoEnables.TryGetValue(lang, out enable);
                        o.VideoEnable = enable;
                        o.EmployeeNumber = employeeNumber;
                        o.VideoDateView = getVideoViewDate(accesId, criteria.CriterionCode);
                    }
                    o.Trend = GetTrend(o.EmployeeNumber, evaluationId.Value, tolerance);
                }

                return result;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return null;
        }

        public ICollection<EvaluationTrend> getWorstMacroEvaluationTerm(string employeeNumber, string lang, decimal tolerance, decimal toleranceAbsolute, int? evaluationId = null, int? topRecord = 1)
        {
            var wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            var getWorsTerms = $@"select distinct {(topRecord.HasValue ? $"top({topRecord.Value})" : "")} (termcode), avg(grade) as average 
                               from evaluationterms where evaluationId = @evaluationId and evalueeId = @employeeNumber and grade>0 and parentTermCode is null group by (termcode)order by average";

            try
            {
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@employeeNumber", employeeNumber),
                    createParameter("@evaluationId", evaluationId),

                };

                var result = new List<EvaluationTrend>();

                SqlDataReader reader = ExecuteReader(getWorsTerms, pars);
                while (reader.Read())
                {
                    var o = getEvaluationTrend(reader, evaluationId.Value, tolerance);
                    result.Add(o);
                }
                reader.Close();

                return this.VoiceConfiguration(result, employeeNumber, lang, toleranceAbsolute, evaluationId);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return null;
        }

        public ICollection<EvaluationTrend> getWorstSubEvaluationTerm(string employeeNumber, string lang, decimal tolerance, decimal toleranceAbsolute, int? evaluationId = null, int? topRecord = 3)
        {
            var wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            var getWorsTerms = $@"select distinct {(topRecord.HasValue ? $"top({topRecord.Value})" : "")} (termcode), avg(grade) as average 
                               from evaluationterms where evaluationId = @evaluationId and evalueeId = @employeeNumber and grade>0 and parentTermCode is not null  group by (termcode)order by average";

            try
            {
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@employeeNumber", employeeNumber),
                    createParameter("@evaluationId", evaluationId),

                };

                var result = new List<EvaluationTrend>();

                SqlDataReader reader = ExecuteReader(getWorsTerms, pars);
                while (reader.Read())
                {
                    var o = getEvaluationTrend(reader, evaluationId.Value, tolerance);
                    result.Add(o);
                }
                reader.Close();

                return this.VoiceConfiguration(result, employeeNumber, lang, toleranceAbsolute, evaluationId);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return null;
        }

        private List<EvaluationTrend> VoiceConfiguration(List<EvaluationTrend> listEvaluationTrend, string employeeNumber, string lang, decimal toleranceAbsolute, int? evaluationId)
        {
            List<EvaluationTrend> result = listEvaluationTrend;

            if (!evaluationId.HasValue)
                evaluationId = getLastEvaluationId(employeeNumber);
            var eval = getEvaluation(evaluationId.Value);
            var accesId = GetCurrentAccessId(employeeNumber, evaluationId.Value);

            var criterion = getEvaluationCriteriaConfiguration(evaluationId.Value, true);

            EvaluationGrades currEvaluationGrades = getEvalueeGradesSimplified(getEvaluation(evaluationId.Value), getEmployee(employeeNumber, false), false, new Dictionary<string, Employee>(), new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), false);

            EvaluationGrades pastEvaluationGrades = null;
            var pastEvaluation = getLastEvalueeHistoryRecord(employeeNumber, evaluationId.Value);
            if (pastEvaluation != null)
                pastEvaluationGrades = getEvalueeGradesSimplified(getEvaluation(pastEvaluation.EvaluationId), getEmployee(employeeNumber, false), false, new Dictionary<string, Employee>(), new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), false);

            foreach (var o in result)
            {
                var criteria = criterion.SingleOrDefault(c => c.CriterionCode.Equals(o.CriterionCode));
                string description = "";
                bool enable = false;
                string videoPath = "";
                bool isFind = false;
                if (criteria == null)
                {
                    criterion.ToList().ForEach(c =>
                    {
                        if (c.Subcriteria.Any())
                        {
                            if (!isFind)
                            {
                                criteria = c.Subcriteria.SingleOrDefault(cr => cr.CriterionCode.Equals(o.CriterionCode));
                            }
                            if (criteria != null)
                            {
                                isFind = true;
                            }
                        }
                    });
                }
                if (criteria != null)
                {
                    o.Status = eval.Status;
                    o.StaffDate = eval.StaffDate;
                    criteria.CriterionDescription.TryGetValue(lang, out description);
                    o.CriterionDescription = description;
                    criteria.VideoPaths.TryGetValue(lang, out videoPath);
                    o.VideoPath = videoPath;
                    criteria.VideoEnables.TryGetValue(lang, out enable);
                    o.VideoEnable = enable;
                    o.EmployeeNumber = employeeNumber;
                    o.VideoDateView = getVideoViewDate(accesId, criteria.CriterionCode);
                }
                //o.Trend = this.GetTrendAbsolute(o.EmployeeNumber, evaluationId.Value, toleranceAbsolute);
                if (pastEvaluationGrades == null)
                    o.Trend = "equal";
                else
                    o.Trend = this.GetTrendAbsolutePerVoce(o.EmployeeNumber, evaluationId.Value, o.CriterionCode, toleranceAbsolute, currEvaluationGrades.grades, pastEvaluationGrades.grades);

                o.CriterionDescription = (!string.IsNullOrEmpty(o.CriterionCode) && o.CriterionCode.EndsWith("000") ? ValueOperations.setOnlyFirtUpper(o.CriterionDescription) : o.CriterionDescription);
                o.CriterionDescription = (!string.IsNullOrEmpty(o.CriterionDescription) ? ValueOperations.deleteAsterisks(o.CriterionDescription) : o.CriterionDescription);
            }

            return result;
        }

        public ICollection<EvaluationTrend> getWorstEvaluationTermSameGrade(string employeeNumber, string lang, decimal grade, decimal tolerance,
            int? evaluationId = null, int? topRecord = 3, string[] criteriaToExclude = null)
        {
            var wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            if (!evaluationId.HasValue)
                evaluationId = getLastEvaluationId(employeeNumber);

            var sql = new StringBuilder($@"select {(topRecord.HasValue ? $"top({topRecord.Value})" : "")} e.EvaluationStatus, e.StaffDate, ec.CriterionCode, 
                isnull(ecl.CriterionDescription, isnull(ecl2.CriterionDescription, ec.CriterionDescription)) as CriterionDescription, 
                isnull(ecl.VideoPath, ecl3.VideoPath) as VideoPath, isnull(ecl.VideoEnable, ecl3.VideoEnable) as VideoEnable,
                emp.EmployeeNumber, avg(et.Grade) as Grade,
                t.videoViewDate
                from [dbo].Evaluations e
                inner join [dbo].EvaluationCriteria ec on e.EvaluationId = ec.EvaluationId
                left join [dbo].EvaluationCriteriaLanguages ecl on ec.EvaluationId = ecl.EvaluationId and ec.CriterionCode = ecl.CriterionCode and ecl.LanguageCode = @lang
                left join [dbo].EvaluationCriteriaLanguages ecl2 on ec.CriterionCode = ecl2.CriterionCode and ecl2.LanguageCode = @lang and ecl2.CriterionDescription is not null
                left join [dbo].EvaluationCriteriaLanguages ecl3 on ec.CriterionCode = ecl3.CriterionCode and ecl3.LanguageCode = @lang and ecl3.VideoPath is not null and ecl3.VideoEnable = 1
                inner join [dbo].EvaluationTerms et on et.TermCode = ec.CriterionCode and e.EvaluationId = et.EvaluationId
                inner join [dbo].Employees emp on emp.EmployeeNumber = et.EvalueeId
                left join (
                    select max(ead.VideoViewDate) as VideoViewDate, ea.EvaluationId, ead.CriterionCode, ea.EmployeeNumber
                    from  [dbo].EmployeeAccesses ea
                    inner join [dbo].EmployeeAccessDetails ead on ea.AccessId = ead.AccessId
                    where ea.EvaluationId = @evalId
                    and ea.EmployeeNumber = @empNum
		            and ead.VideoViewDate is not null
                    group by ea.EvaluationId, ead.CriterionCode, ea.EmployeeNumber
                ) as t on t.EvaluationId = e.EvaluationId and t.CriterionCode = ec.CriterionCode and t.EmployeeNumber = emp.EmployeeNumber
                where emp.EmployeeNumber = @empNum and e.EvaluationId = @evalId");

            if (criteriaToExclude != null)
                sql.Append($" and ec.CriterionCode not in ({(string.Join(",", criteriaToExclude.Select(x => "'" + x + "'")))})");

            sql.Append(@" group by e.EvaluationStatus, e.StaffDate, ec.CriterionCode, 
                isnull(ecl.CriterionDescription, isnull(ecl2.CriterionDescription, ec.CriterionDescription)), 
                isnull(ecl.VideoPath, ecl3.VideoPath), isnull(ecl.VideoEnable, ecl3.VideoEnable),
                emp.EmployeeNumber,
                t.videoViewDate
                having avg(et.Grade) = @grade
                order by avg(et.Grade) asc");

            try
            {
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@empNum", employeeNumber),
                    createParameter("@evalId", evaluationId),
                    createParameter("@lang", lang),
                    createParameter("@grade", grade)
                };

                var result = new List<EvaluationTrend>();

                SqlDataReader reader = ExecuteReader(sql.ToString(), pars);
                while (reader.Read())
                {
                    var o = getEvaluationTrend(reader, evaluationId.Value, tolerance);

                    result.Add(o);
                }
                reader.Close();

                foreach (var o in result)
                {
                    o.Trend = GetTrend(o.EmployeeNumber, evaluationId.Value, tolerance);
                }

                return result;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return null;
        }

        public ICollection<EvaluationTrend> getBestEvaluationTerm(string employeeNumber, string lang, decimal tolerance, int? evaluationId = null, int? topRecord = 2)
        {
            var wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            if (!evaluationId.HasValue)
                evaluationId = getLastEvaluationId(employeeNumber);
            var eval = getEvaluation(evaluationId.Value);
            var accesId = GetCurrentAccessId(employeeNumber, evaluationId.Value);


            var getWorsTerms = $@"select distinct {(topRecord.HasValue ? $"top({topRecord.Value})" : "")} (termcode), avg(grade) as average 
                               from evaluationterms where evaluationId = @evaluationId and evalueeId = @employeeNumber  and grade>0  group by (termcode)order by average desc";

            try
            {
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@employeeNumber", employeeNumber),
                    createParameter("@evaluationId", evaluationId),

                };

                var result = new List<EvaluationTrend>();

                SqlDataReader reader = ExecuteReader(getWorsTerms, pars);
                while (reader.Read())
                {
                    var o = getEvaluationTrend(reader, evaluationId.Value, tolerance);
                    result.Add(o);
                }
                reader.Close();

                var criterion = getEvaluationCriteriaConfiguration(evaluationId.Value, true);

                foreach (var o in result)
                {
                    var criteria = criterion.SingleOrDefault(c => c.CriterionCode.Equals(o.CriterionCode));
                    string description = "";
                    bool enable = false;
                    string videoPath = "";
                    bool isFind = false;
                    if (criteria == null)
                    {
                        criterion.ToList().ForEach(c =>
                        {
                            if (c.Subcriteria.Any())
                            {
                                if (!isFind)
                                {
                                    criteria = c.Subcriteria.SingleOrDefault(cr => cr.CriterionCode.Equals(o.CriterionCode));
                                }
                                if (criteria != null)
                                {
                                    isFind = true;
                                }
                            }
                        });
                    }
                    if (criteria != null)
                    {
                        o.Status = eval.Status;
                        o.StaffDate = eval.StaffDate;
                        criteria.CriterionDescription.TryGetValue(lang, out description);
                        o.CriterionDescription = description;
                        criteria.VideoPaths.TryGetValue(lang, out videoPath);
                        o.VideoPath = videoPath;
                        criteria.VideoEnables.TryGetValue(lang, out enable);
                        o.VideoEnable = enable;
                        o.EmployeeNumber = employeeNumber;
                        o.VideoDateView = getVideoViewDate(accesId, criteria.CriterionCode);
                    }
                    o.Trend = GetTrend(o.EmployeeNumber, evaluationId.Value, tolerance);
                }

                return result;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return null;
        }

        public ICollection<EvaluationTrend> getBestEvaluationTermOverGradeLimit(string employeeNumber, string lang, decimal gradeLimit, decimal tolerance, decimal toleranceAbsolute, int? evaluationId = null, int? topRecord = 2)
        {
            var wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            if (!evaluationId.HasValue)
                evaluationId = getLastEvaluationId(employeeNumber);
            var eval = getEvaluation(evaluationId.Value);
            var accesId = GetCurrentAccessId(employeeNumber, evaluationId.Value);


            var getWorsTerms = $@"select distinct {(topRecord.HasValue ? $"top({topRecord.Value})" : "")} (termcode), avg(grade) as average 
                               from evaluationterms where evaluationId = @evaluationId and evalueeId = @employeeNumber  and grade>0  group by (termcode)order by average desc";

            try
            {
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@employeeNumber", employeeNumber),
                    createParameter("@evaluationId", evaluationId),

                };

                var result = new List<EvaluationTrend>();

                SqlDataReader reader = ExecuteReader(getWorsTerms, pars);
                while (reader.Read())
                {
                    var o = getEvaluationTrend(reader, evaluationId.Value, tolerance);
                    result.Add(o);
                }
                reader.Close();

                var criterion = getEvaluationCriteriaConfiguration(evaluationId.Value, true);

                foreach (var o in result)
                {
                    var criteria = criterion.SingleOrDefault(c => c.CriterionCode.Equals(o.CriterionCode));
                    string description = "";
                    bool enable = false;
                    string videoPath = "";
                    bool isFind = false;
                    if (criteria == null)
                    {
                        criterion.ToList().ForEach(c =>
                        {
                            if (c.Subcriteria.Any())
                            {
                                if (!isFind)
                                {
                                    criteria = c.Subcriteria.SingleOrDefault(cr => cr.CriterionCode.Equals(o.CriterionCode));
                                }
                                if (criteria != null)
                                {
                                    isFind = true;
                                }
                            }
                        });
                    }
                    if (criteria != null)
                    {
                        o.Status = eval.Status;
                        o.StaffDate = eval.StaffDate;
                        criteria.CriterionDescription.TryGetValue(lang, out description);
                        o.CriterionDescription = description;
                        criteria.VideoPaths.TryGetValue(lang, out videoPath);
                        o.VideoPath = videoPath;
                        criteria.VideoEnables.TryGetValue(lang, out enable);
                        o.VideoEnable = enable;
                        o.EmployeeNumber = employeeNumber;
                        o.VideoDateView = getVideoViewDate(accesId, criteria.CriterionCode);
                    }
                    //o.Trend = this.GetTrendAbsolutePerVoce(o.EmployeeNumber, evaluationId.Value, o.CriterionCode, toleranceAbsolute);

                    o.CriterionDescription = (!string.IsNullOrEmpty(o.CriterionCode) && o.CriterionCode.EndsWith("000") ? ValueOperations.setOnlyFirtUpper(o.CriterionDescription) : o.CriterionDescription);
                    o.CriterionDescription = (!string.IsNullOrEmpty(o.CriterionDescription) ? ValueOperations.deleteAsterisks(o.CriterionDescription) : o.CriterionDescription);
                }

                return result;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return null;
        }
        #endregion

        #region access log
        public EmployeeAccessDetailsDataSheet getEmployeesAccessDetailsDataSheet(int accessId, string lang)
        {
            EmployeeAccessDetailsDataSheet ret = null;

            var sql = @"select emp.EmployeeNumber, emp.Name, emp.Surname, emp.BirthDate, emp.BirthPlace, emp.Nationality, emp.Home_Address1, emp.BloodType, 
				emp.HiringDate, emp.TerminationDate, emp.ContractType, emp.Company, emp.Workplace, emp.CurrentJob, emp.SpecialCategory, emp.SpecialCategoryType, emp.TestResult, 
				emp.DistanceFromWork, emp.Education, emp.ExtensionNotes as Motivations,
				t.LoginDate, t.LogoutDate, t.LoginDuration, t.AccessId, t.Notes, t.PointImprovement, t.RecognizeYourself,
				t.VideoViewDuration, 
				e.Evaluationid, e.StaffDate, empp.EmployeeNumber as hasPhoto,
				ec.CriterionCode, 
				isnull(ecl.CriterionDescription, isnull(ecl2.CriterionDescription, ec.CriterionDescription)) as CriterionDescription
				from [dbo].Evaluations e	
				inner join [dbo].EvaluationTerms et on e.EvaluationId = et.EvaluationId
				inner join [dbo].Employees emp on emp.EmployeeNumber = et.EvalueeId
				left join [dbo].EmployeePhotos empp on empp.EmployeeNumber = emp.EmployeeNumber
				inner join (
					select ea.LoginDate, ea.LogoutDate, ea.LoginDuration, ea.EvaluationId, ea.EmployeeNumber, ead.VideoViewDuration, ead.CriterionCode,
					ea.AccessId, ea.Notes, ea.PointImprovement, ea.RecognizeYourself
					from  [dbo].EmployeeAccesses ea
					inner join [dbo].EmployeeAccessDetails ead on ea.AccessId = ead.AccessId
					where ea.AccessId = @accessId
				) as t on t.EvaluationId = e.EvaluationId and t.EmployeeNumber = emp.EmployeeNumber
				inner join [dbo].EvaluationCriteria ec on e.EvaluationId = ec.EvaluationId and ec.CriterionCode = t.CriterionCode
				left join [dbo].EvaluationCriteriaLanguages ecl on ec.EvaluationId = ecl.EvaluationId and ec.CriterionCode = ecl.CriterionCode and ecl.LanguageCode = @lang
				left join [dbo].EvaluationCriteriaLanguages ecl2 on ec.CriterionCode = ecl2.CriterionCode and ecl2.LanguageCode = @lang and ecl2.CriterionDescription is not null
				where t.LoginDate is not null and t.AccessId = @accessId
				group by t.AccessId, emp.EmployeeNumber, emp.[Name], emp.Surname, emp.DepartmentDescription, 
				emp.ContractType, 
				emp.Name, emp.Surname, emp.BirthDate, emp.BirthPlace, emp.Nationality, emp.Home_Address1, emp.BloodType, 
				emp.HiringDate, emp.TerminationDate, emp.ContractType, emp.Company, emp.Workplace, emp.CurrentJob, emp.SpecialCategory, emp.SpecialCategoryType, emp.TestResult, 
				emp.DistanceFromWork, emp.Education, emp.ExtensionNotes,
				t.LogoutDate, t.LoginDate, t.LoginDuration, e.Evaluationid, e.StaffDate, empp.EmployeeNumber,
				t.VideoViewDuration, t.Notes, t.PointImprovement, t.RecognizeYourself,
				ec.CriterionCode,
				isnull(ecl.CriterionDescription, isnull(ecl2.CriterionDescription, ec.CriterionDescription))";

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                SqlDataReader reader = ExecuteReader(sql, new[] {
                    new SqlParameter("@accessId", accessId),
                    new SqlParameter("@lang", lang)
                });

                var videoCount = 0;
                var videoDuration = 0;

                while (reader.Read())
                {
                    var code = reader["CriterionCode"].ToString();

                    if (ret == null)
                    {
                        ret = new EmployeeAccessDetailsDataSheet
                        {
                            EmployeeNumber = reader["EmployeeNumber"].ToString(),
                            AccessId = Convert.ToInt32(reader["AccessId"]),
                            LoginDate = Convert.ToDateTime(reader["LoginDate"]),
                            StaffDate = Convert.ToDateTime(reader["StaffDate"]),
                            VideoViewDuration = (reader["VideoViewDuration"] is DBNull ? default(int?) : Convert.ToInt32(reader["VideoViewDuration"])),
                            Notes = ValueOperations.DBToString(reader["Notes"]),
                            PointImprovement = ValueOperations.DBToString(reader["PointImprovement"]),
                            RecognizeYourself = ValueOperations.DBToString(reader["RecognizeYourself"]),
                            HasPhoto = !(reader["hasPhoto"] is DBNull),
                            BirthDate = Convert.ToDateTime(optionalDecryption(reader["BirthDate"].ToString())),
                            BirthPlace = optionalDecryption(reader["BirthPlace"].ToString()),
                            BloodType = (reader["BloodType"] is DBNull ? default(string) : reader["BloodType"].ToString()),
                            Company = (reader["Company"] is DBNull ? default(string) : reader["Company"].ToString()),
                            Site = (reader["Workplace"] is DBNull ? default(string) : reader["Workplace"].ToString()),
                            CompanyDistance = (reader["DistanceFromWork"] is DBNull ? default(int?) : Convert.ToInt32(reader["DistanceFromWork"])),
                            ContractType = (reader["ContractType"] is DBNull ? default(string) : reader["ContractType"].ToString()),
                            Domicile = (reader["Home_Address1"] is DBNull ? default(string) : optionalDecryption(reader["Home_Address1"].ToString())),
                            HiringDate = (reader["HiringDate"] is DBNull) ? default(DateTime?) : Convert.ToDateTime(optionalDecryption(reader["HiringDate"].ToString())),
                            TerminationDate = (reader["TerminationDate"] is DBNull) ? default(DateTime?) : Convert.ToDateTime(optionalDecryption(reader["TerminationDate"].ToString())),
                            Job = (reader["CurrentJob"] is DBNull ? default(string) : optionalDecryption(reader["CurrentJob"].ToString())),
                            LogoutDate = (reader["LogoutDate"] is DBNull) ? default(DateTime?) : Convert.ToDateTime(reader["LogoutDate"]),
                            Motivations = (reader["Motivations"] is DBNull ? default(string) : reader["Motivations"].ToString()),
                            Name = optionalDecryption(reader["Name"].ToString()) + " " + optionalDecryption(reader["Surname"].ToString()),
                            Nationality = (reader["Nationality"] is DBNull ? default(string) : reader["Nationality"].ToString()),
                            ProtectedCategory = (reader["SpecialCategory"] is DBNull ? default(string) : optionalDecryption(reader["SpecialCategory"].ToString())),
                            ProtectedCategoryType = (reader["SpecialCategoryType"] is DBNull ? default(string) : optionalDecryption(reader["SpecialCategoryType"].ToString())),
                            Qualification = (reader["Education"] is DBNull ? default(string) : reader["Education"].ToString()),
                            SessionDuration = (reader["LoginDuration"] is DBNull ? default(int?) : Convert.ToInt32(reader["LoginDuration"])),
                            TestResults = (reader["TestResult"] is DBNull ? default(string) : reader["TestResult"].ToString())
                        };
                    }

                    var dur = (reader["VideoViewDuration"] is DBNull ? default(int?) : Convert.ToInt32(reader["VideoViewDuration"]));

                    if (dur.HasValue)
                    {
                        videoDuration += dur.Value;
                        videoCount++;
                    }
                }
                reader.Close();

                ret.VideoViewsCount = videoCount;
                ret.VideoViewDuration = videoDuration;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public ICollection<EmployeeAccessDetailsData> getEmployeesAccessDetailsData(int? evaluationId, string employeeNumber, string language)
        {
            var ret = new List<EmployeeAccessDetailsData>();

            var pars = new List<SqlParameter>
            {
                new SqlParameter("@empId", employeeNumber),
                new SqlParameter("@lang", language)
            };

            var sql = new StringBuilder(@"select emp.EmployeeNumber, isnull(ecl.CriterionDescription, isnull(ecl2.CriterionDescription, ec.CriterionDescription)) as CriterionDescription, ec.CriterionCode, t.LoginDate, 
                t.VideoViewDuration, e.Evaluationid, t.Grade, t.Trend, t.AccessId, e.StaffDate, t.Notes, t.RecognizeYourself, t.PointImprovement, empp.EmployeeNumber as hasPhoto
                from [dbo].Evaluations e
                inner join [dbo].EvaluationCriteria ec on e.EvaluationId = ec.EvaluationId
                left join [dbo].EvaluationCriteriaLanguages ecl on ec.EvaluationId = ecl.EvaluationId and ec.CriterionCode = ecl.CriterionCode and ecl.LanguageCode = @lang
                left join [dbo].EvaluationCriteriaLanguages ecl2 on ec.CriterionCode = ecl2.CriterionCode and ecl2.LanguageCode = @lang and ecl2.CriterionDescription is not null
                inner join [dbo].EvaluationTerms et on et.TermCode = ec.CriterionCode and e.EvaluationId = et.EvaluationId
                inner join [dbo].Employees emp on emp.EmployeeNumber = et.EvalueeId
                left join [dbo].EmployeePhotos empp on empp.EmployeeNumber = emp.EmployeeNumber
                left join (
	                select ea.LoginDate, ea.EvaluationId, ea.EmployeeNumber, ead.VideoViewDuration, ead.Grade, ead.Trend, ead.CriterionCode, ea.AccessId, ea.Notes, ea.RecognizeYourself, ea.PointImprovement
	                from  [dbo].EmployeeAccesses ea
	                inner join [dbo].EmployeeAccessDetails ead on ea.AccessId = ead.AccessId
	                where ea.EmployeeNumber = @empId ");

            if (evaluationId.HasValue)
            {
                sql.Append(" and ea.EvaluationId = @evalId ");
                pars.Add(new SqlParameter("@evalId", evaluationId.Value));
            }

            sql.Append(@") as t on t.EvaluationId = e.EvaluationId and t.EmployeeNumber = emp.EmployeeNumber and t.CriterionCode = ec.CriterionCode
                where emp.EmployeeNumber = @empId");

            if (evaluationId.HasValue)
            {
                sql.Append(" and e.EvaluationId = @evalId ");
            }

            sql.Append(@" and t.LoginDate is not null
                group by t.AccessId, ec.CriterionCode, emp.EmployeeNumber, emp.[Name], emp.Surname, emp.DepartmentDescription, 
                emp.ContractType, t.LoginDate, t.VideoViewDuration, e.Evaluationid, t.Grade, t.Trend, 
                isnull(ecl.CriterionDescription, isnull(ecl2.CriterionDescription, ec.CriterionDescription)), e.StaffDate, t.Notes, t.RecognizeYourself, t.PointImprovement, empp.EmployeeNumber
                order by e.StaffDate desc, t.LoginDate desc, t.Grade asc, t.Trend asc, ec.CriterionCode asc");

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                SqlDataReader reader = ExecuteReader(sql.ToString(), new[] {
                    new SqlParameter("@evalId", evaluationId),
                    new SqlParameter("@empId", employeeNumber),
                    new SqlParameter("@lang", language)
                });

                while (reader.Read())
                {
                    ret.Add(new EmployeeAccessDetailsData
                    {
                        AccessId = Convert.ToInt32(reader["AccessId"]),
                        CriterionDescription = reader["CriterionDescription"].ToString(),
                        Grade = Convert.ToDecimal(reader["Grade"]),
                        LoginDate = Convert.ToDateTime(reader["LoginDate"]),
                        Trend = reader["Trend"].ToString(),
                        StaffDate = Convert.ToDateTime(reader["StaffDate"]),
                        VideoViewDuration = (reader["VideoViewDuration"] is DBNull ? default(int?) : Convert.ToInt32(reader["VideoViewDuration"])),
                        Notes = ValueOperations.DBToString(reader["Notes"]),
                        HasPhoto = !(reader["hasPhoto"] is DBNull),
                        EvaluationId = evaluationId.Value,
                        CriterionCode = reader["CriterionCode"].ToString()
                    });
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public ICollection<EmployeeAccessesData> getEmployeesAccessesData(int? evaluationId, string[] employeeNumbers)
        {
            var ret = new List<EmployeeAccessesData>();

            var sql = new StringBuilder(@"select emp.EmployeeNumber, emp.[Name], emp.Surname, emp.DepartmentDescription, 
                emp.ContractType, t.LoginDate, t.LoginDuration, empp.EmployeeNumber as hasPhoto, e.StaffDate
                from [dbo].Evaluations e
                inner join [dbo].EvaluationCriteria ec on e.EvaluationId = ec.EvaluationId
                inner join [dbo].EvaluationTerms et on et.TermCode = ec.CriterionCode and e.EvaluationId = et.EvaluationId
                inner join [dbo].Employees emp on emp.EmployeeNumber = et.EvalueeId
                left join [dbo].EmployeePhotos empp on empp.EmployeeNumber = emp.EmployeeNumber
                left join [dbo].EmployeeAccesses t on (
	                select top(1) EmployeeAccesses.AccessId ");

            sql.Append(@" from  [dbo].EmployeeAccesses 
					inner join [dbo].EmployeeAccessDetails on EmployeeAccesses.AccessId = EmployeeAccessDetails.AccessId
					where EmployeeAccesses.LoginDate is not null 
					and EmployeeAccesses.EvaluationId = e.EvaluationId 
					and EmployeeAccesses.EmployeeNumber = emp.EmployeeNumber 
					order by EmployeeAccesses.LoginDate desc
                ) = t.AccessId
                where ");


            var paras = new List<SqlParameter>();

            var addEval = false;
            if (evaluationId.HasValue)
            {
                addEval = true;

                sql.Append(" e.EvaluationId = @evalId ");

                paras.Add(createParameter("@evalId", evaluationId.Value));
            }

            if (employeeNumbers?.Length > 0)
            {
                if (addEval)
                    sql.Append(" and ");

                sql.Append($" emp.EmployeeNumber in ({string.Join(",", employeeNumbers.Select(x => "'" + x + "'"))}) ");
            }

            sql.Append(" and e.EvaluationStatus in ('Closed', 'Expired') ");

            sql.Append(@"
                group by emp.EmployeeNumber, emp.[Name], emp.Surname, emp.DepartmentDescription, 
                emp.ContractType, t.LoginDate, t.LoginDuration, empp.EmployeeNumber, e.StaffDate ");

            if (addEval)
                sql.Append(", e.EvaluationId ");

            sql.Append(" order by t.LoginDate desc, t.LoginDuration desc, emp.EmployeeNumber asc");

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                SqlDataReader reader = ExecuteReader(sql.ToString(), paras.ToArray());
                while (reader.Read())
                {
                    ret.Add(new EmployeeAccessesData
                    {
                        EmployeeNumber = reader["EmployeeNumber"].ToString(),
                        EmployeeName = (optionalDecryption(ValueOperations.DBToString(reader["Surname"])) + " " + optionalDecryption(ValueOperations.DBToString(reader["Name"]))).ToUpper(),
                        Department = ValueOperations.DBToString(reader["DepartmentDescription"]),
                        Contract = ValueOperations.DBToString(reader["ContractType"]),
                        LastAccess = (reader["LoginDate"] is DBNull
                            ? default(DateTime?)
                            : Convert.ToDateTime(reader["LoginDate"])),
                        LastAccessDuration = (reader["LoginDuration"] is DBNull
                            ? default(long?)
                            : Convert.ToInt64(reader["LoginDuration"])),
                        HasPhoto = !(reader["hasPhoto"] is DBNull),
                        StaffDate = Convert.ToDateTime(reader["StaffDate"]),
                        EvaluationId = evaluationId.Value
                    });
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public DateTime getVideoViewDate(int accessId, string criterionCode)
        {
            DateTime ret = DateTime.MinValue;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string sql = @" SELECT VideoViewDate FROM EmployeeAccessDetails where accessId=@accessId and CriterionCode=@code ";

                var pars = new List<SqlParameter>
                {
                    new SqlParameter("@accessId", accessId),
                    new SqlParameter("@code", criterionCode)
                };
                SqlDataReader reader = ExecuteReader(sql, pars.ToArray());
                while (reader.Read())
                {
                    ret = reader["VideoViewDate"] != DBNull.Value ? ValueOperations.DBToDate(reader["VideoViewDate"]) : DateTime.MinValue;
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public List<EmployeeFilteringData> getEvalueeFilteringAccessesDataSimplified(int evaluationId)
        {
            List<EmployeeFilteringData> ret = new List<EmployeeFilteringData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select emp.EmployeeNumber, emp.[Name], emp.Surname
                from [dbo].Evaluations e                
                inner join [dbo].EvaluationCriteria ec on e.EvaluationId = ec.EvaluationId                
                inner join [dbo].EvaluationTerms et on et.TermCode = ec.CriterionCode and e.EvaluationId = et.EvaluationId                
                inner join [dbo].Employees emp on emp.EmployeeNumber = et.EvalueeId   
                where  e.EvaluationId = @EvaluationId 
                group by emp.EmployeeNumber, emp.[Name], emp.Surname";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                {
                    ret.Add(new EmployeeFilteringData()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"]))

                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret = ret.OrderBy(x => x.surname).ToList();

            return ret;
        }

        public List<EmployeeFilteringData> getEvalueeFilteringAccessesDataSimplified()
        {
            List<EmployeeFilteringData> ret = new List<EmployeeFilteringData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select emp.EmployeeNumber, emp.[Name], emp.Surname
                from [dbo].Employees emp    
                group by emp.EmployeeNumber, emp.[Name], emp.Surname";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(new EmployeeFilteringData()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"]))

                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret = ret.OrderBy(x => x.surname).ToList();

            return ret;
        }

        #endregion

        public int countEvaluationsDone(int evaluationId, string assessorId)
        {
            int ret = 0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select COUNT( DISTINCT EvalueeId) AS Numero from EvaluationTerms 
                                 where EvaluationId=@EvaluationId AND AssessorId=@AssessorId 
                                    and EvalueeId IS NOT NULL AND Grade>0";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@AssessorId", assessorId)
                    };
                string res = ExecuteScalar(query, pars);
                ret = int.Parse(res);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public int countEvaluationsObtained(int evaluationId, string evalueeId)
        {
            int ret = 0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select COUNT( DISTINCT AssessorId) AS Numero from EvaluationTerms 
                                 where EvaluationId=@EvaluationId AND EvalueeId=@EvalueeId 
                                    and AssessorId IS NOT NULL AND Grade>0";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@EvalueeId", evalueeId)
                    };
                string res = ExecuteScalar(query, pars);
                ret = int.Parse(res);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public void deleteDepartmentsToEvaluate(int evaluationId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"DELETE FROM DepartmentsToEvaluate WHERE EvaluationId=@EvaluationId";

                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId)
                    };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void deleteEvaluation(int evaluationId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"DELETE FROM Evaluations WHERE EvaluationId=@EvaluationId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }



        public void deleteEvaluationPairing(int evaluationId, string assessorId, string evalueeId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"DELETE FROM EvaluationPairings WHERE 
                                EvaluationId=@EvaluationId AND AssessorId=@AssessorId AND
                                EvalueeId=@EvalueeId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@AssessorId",assessorId),
                    createParameter("@EvalueeId",evalueeId)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void deleteEvaluationPairingsForEvaluee(int evaluationId, string evalueeId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"DELETE FROM EvaluationPairings WHERE 
                                EvaluationId=@EvaluationId AND
                                EvalueeId=@EvalueeId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@EvalueeId",evalueeId)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void deleteEvalueePromotion(int evaluationId, string evalueeId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"DELETE FROM EvaluationPromotions WHERE EvaluationId=@EvaluationId AND EmployeeType='Evaluee' AND EmployeeId=@EmployeeId AND (ManualInsertion IS NULL OR ManualInsertion='False')";
                SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@EvaluationId",evaluationId),
                            createParameter("@EmployeeId",evalueeId)
                        };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }
        public void deleteType2Note(string noteCode)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "DELETE FROM Type2NoteDefinitions WHERE NoteCode=@NoteCode";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@NoteCode",noteCode)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public List<string> getAllAssessors(int evaluationId)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"select EmployeeNumber from Employees WHERE dbo.IsAssessorInEvaluation (EmployeeNumber,@EvaluationId) ='True' AND EmployeeState='Attivo'";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                    ret.Add(reader["EmployeeNumber"].ToString());
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getAllAssessors_ordered(int evaluationId, int offset, int pageSize, AssessorTableOrdering ordering, bool ascendingOrder)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT em.EmployeeNumber FROM EvaluationPromotions ep, Employees em 
                                WHERE ep.EmployeeId=em.EmployeeNumber AND ep.EvaluationId=@EvaluationId 
                                AND dbo.IsAssessorInEvaluation (em.EmployeeNumber,@EvaluationId)='True'
                                ORDER BY em." + ordering.ToString() + " " + (ascendingOrder ? "ASC" : "DESC");
                query += " OFFSET " + offset + " ROWS FETCH NEXT " + pageSize + " ROWS ONLY";


                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                    ret.Add(reader["EmployeeNumber"].ToString());
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Dictionary<string, string> getAllContractTypes()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from ContractTypes";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(reader["ContractType"].ToString(), reader["ContractDescription"].ToString());
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Departments getAllDepartments()
        {
            Departments ret = new Departments();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select distinct DepartmentCode, DepartmentDescription from Employees";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(new Department()
                    {
                        departmentCode = reader["DepartmentCode"].ToString(),
                        departmentDescription = reader["DepartmentDescription"].ToString(),
                    });
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Employees getAllEmployees(bool getPhoto)
        {
            Employees ret = new Employees();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                if (getPhoto)
                {
                    string query = "select emp.*, rol.RoleType, pho.Photo, sum.yearAverage from Employees emp left join EmployeeRoles rol on emp.EmployeeNumber=rol.EmployeeId left join EmployeePhotos pho on emp.EmployeeNumber=pho.EmployeeNumber LEFT JOIN EvaluationsSummary sum ON sum.EmployeeNumber = emp.EmployeeNumber";
                    SqlDataReader reader = ExecuteReader(query);
                    while (reader.Read())
                        ret.Add(toEmployeeWithPhoto(reader));
                    reader.Close();
                }
                else
                {
                    string query = "select emp.*, rol.RoleType, sum.yearAverage from Employees emp left join EmployeeRoles rol on emp.EmployeeNumber=rol.EmployeeId LEFT JOIN EvaluationsSummary sum ON sum.EmployeeNumber = emp.EmployeeNumber";
                    SqlDataReader reader = ExecuteReader(query);
                    while (reader.Read())
                        ret.Add(toEmployee(reader));
                    reader.Close();
                }

                foreach (Employee emp in ret)
                    emp.ContractData = getCurrentEmployeeContract(emp.EmployeeNumber);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Evaluations getAllEvaluations()
        {
            Evaluations ret = new Evaluations();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from Evaluations WHERE EvaluationStatus IS NOT NULL ORDER BY EndDate";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(toEvaluation(reader));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getAllEvaluees_ordered(int evaluationId, int offset, int pageSize, EvalueeTableOrdering ordering, bool ascendingOrder)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                /*string query = @"SELECT em.EmployeeNumber FROM EvaluationPromotions ep, Employees em 
                                WHERE ep.EmployeeId=em.EmployeeNumber AND ep.EvaluationId=@EvaluationId 
                                AND (ep.EmployeeType='Evaluee' OR 
                                    (ep.EmployeeType='Assessor' AND ep.PromotionStatus IN ('Proposed','Rejected')))
                                ORDER BY em." + ordering.ToString() + " " + (ascendingOrder ? "ASC" : "DESC");
                 */
                string query = @"SELECT em.EmployeeNumber FROM EvaluationPromotions ep, Employees em 
                                WHERE ep.EmployeeId=em.EmployeeNumber AND ep.EvaluationId=@EvaluationId 
                                ORDER BY em." + ordering.ToString() + " " + (ascendingOrder ? "ASC" : "DESC");
                query += " OFFSET " + offset + " ROWS FETCH NEXT " + pageSize + " ROWS ONLY";


                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                    ret.Add(reader["EmployeeNumber"].ToString());
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getAllNonEvalueesIdList(int evaluationId)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select EmployeeNumber FROM Employees EXCEPT SELECT EmployeeId from EvaluationPromotions WHERE EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId)
                };

                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                    ret.Add(reader["EmployeeNumber"].ToString());
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getAllSites()
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select DISTINCT Workplace FROM Employees WHERE Workplace IS NOT NULL ORDER BY Workplace";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                    ret.Add(ValueOperations.DBToString(reader["Workplace"]));

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Dictionary<int, string> getAllWorkplaceAlgorithms()
        {
            Dictionary<int, string> ret = new Dictionary<int, string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from WorkplaceAlgorithms";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToInteger(reader["AlgorithmCode"]), reader["AlgorithmDescription"].ToString());
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<EmployeeFilteringData> getAssessorFilteringData(int evaluationId)
        {
            List<EmployeeFilteringData> ret = new List<EmployeeFilteringData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT        dbo.Employees.EmployeeNumber, dbo.Employees.Name, dbo.Employees.Surname, dbo.Employees.Workplace, dbo.Employees.DepartmentCode, dbo.Employees.DepartmentDescription, 
                                                        dbo.Employees.IsDirector,dbo.Employees.IsHeadOfDepartment, dbo.Employees.WorkplaceAlgorithm, dbo.Employees.ExternalEmployee, dbo.Employees.Commuter, dbo.Employees.ExtensionNotes, GradeStats.AverageGrade, 
                                                         GradeStats.MostRecentGrade, LastReminders.NumReminder, dbo.Employees.CurrentJob, dbo.EmployeeRoles.RoleType, ManualPairings.AutoOrManual, dbo.Employees.ContractType, dbo.Employees.Company, 
                                                         dbo.Employees.HiringDate,  dbo.Employees.openResearch, dbo.Employees.TerminationDate, NumberOfEvaluees
                                FROM            dbo.Employees INNER JOIN
                                                             (SELECT em.EmployeeNumber 
                                                              FROM  dbo.Employees em 
                                                              WHERE dbo.IsAssessorInEvaluation (em.EmployeeNumber,@EvaluationId)='True' AND em.EmployeeState='Attivo') AS DesignatedAssessors ON 
                                                         dbo.Employees.EmployeeNumber = DesignatedAssessors.EmployeeNumber LEFT OUTER JOIN
                                                             (SELECT        dbo.EmployeeContracts.EmployeeId, dbo.EmployeeContracts.ContractType, dbo.EmployeeContracts.Company, dbo.EmployeeContracts.ContractStartDate, dbo.EmployeeContracts.ContractEndDate, 
                                                                                         dbo.EmployeeContracts.Extensions, dbo.EmployeeContracts.ExtensionNotes
                                                               FROM            dbo.EmployeeContracts INNER JOIN
                                                                                             (SELECT        EmployeeId, MAX(ContractStartDate) AS maxDate
                                                                                               FROM            dbo.EmployeeContracts AS EmployeeContracts_1
                                                                                               GROUP BY EmployeeId) AS lastContracts ON dbo.EmployeeContracts.EmployeeId = lastContracts.EmployeeId AND dbo.EmployeeContracts.ContractStartDate = lastContracts.maxDate) AS CurrentContracts ON 
                                                         dbo.Employees.EmployeeNumber = CurrentContracts.EmployeeId LEFT OUTER JOIN
                                                             (SELECT DISTINCT AutoOrManual, AssessorId
                                                               FROM            dbo.EvaluationPairings
                                                               WHERE        (EvaluationId = @EvaluationId) AND (AutoOrManual = 'Manual')) AS ManualPairings ON dbo.Employees.EmployeeNumber = ManualPairings.AssessorId LEFT OUTER JOIN
                                                         dbo.EmployeeRoles ON dbo.Employees.EmployeeNumber = dbo.EmployeeRoles.EmployeeId LEFT OUTER JOIN
                                                             (SELECT        AssessorId, COUNT(ReminderId) AS NumReminder
                                                               FROM            dbo.Reminders AS Reminders_1
                                                               WHERE        (EvaluationId = @EvaluationId)
                                                               GROUP BY AssessorId) AS LastReminders ON dbo.Employees.EmployeeNumber = LastReminders.AssessorId LEFT OUTER JOIN
                                                             (SELECT        AssessorId, AVG(Grade) AS AverageGrade, COUNT(DISTINCT EvalueeId) AS NumberOfEvaluees, MAX(LastModified) AS MostRecentGrade
                                                               FROM            dbo.EvaluationTerms AS EvaluationTerms_1
                                                               WHERE        (Grade > 0) AND (AssessorId IS NOT NULL) AND (EvaluationId = @EvaluationId)
                                                               GROUP BY AssessorId) AS GradeStats ON dbo.Employees.EmployeeNumber = GradeStats.AssessorId";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                {
                    ret.Add(new EmployeeFilteringData()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                        workplace = ValueOperations.DBToString(reader["Workplace"]),
                        departmentCode = ValueOperations.DBToString(reader["DepartmentCode"]),
                        departmentDescription = ValueOperations.DBToString(reader["DepartmentDescription"]),
                        averageGrade = ValueOperations.DBToDecimal(reader["AverageGrade"]),
                        gradesDone = ValueOperations.DBToInteger(reader["NumberOfEvaluees"]),
                        mostRecentGrade = ValueOperations.DBToDate(reader["MostRecentGrade"]),
                        reminders = ValueOperations.DBToInteger(reader["NumReminder"]),
                        company = ValueOperations.DBToString(reader["Company"]),
                        contractEndDate = normalizeAndDecrypt(reader["TerminationDate"]),
                        contractExpiresInDays = (int)(normalizeAndDecrypt(reader["TerminationDate"]) - DateTime.Now).TotalDays,
                        contractStartDate = normalizeAndDecrypt(reader["HiringDate"]),
                        contractType = ValueOperations.DBToString(reader["ContractType"]),
                        currentJob = optionalDecryption(ValueOperations.DBToString(reader["CurrentJob"])),
                        hasManualPairing = "Manual".Equals(ValueOperations.DBToString(reader["AutoOrManual"])),
                        role = ValueOperations.DBToBool(reader["IsDirector"]) ? "Director" : (ValueOperations.DBToBool(reader["IsHeadOfDepartment"]) ? "HeadOfDepartment" : "User"),
                        externalEmployee = ValueOperations.DBToBool(reader["ExternalEmployee"]),
                        commuter = ValueOperations.DBToBool(reader["Commuter"]),
                        extensionNotes = ValueOperations.DBToString(reader["ExtensionNotes"]),
                        workplaceAlgorithm = ValueOperations.DBToInteger(reader["WorkplaceAlgorithm"]),
                        OpenResearch = ValueOperations.DBToInteger(reader["openResearch"])
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret = ret.OrderBy(x => x.surname).ToList();

            return ret;
        }

        public List<EmployeeFilteringData> getAssessorFilteringDataSimplified(int evaluationId)
        {
            List<EmployeeFilteringData> ret = new List<EmployeeFilteringData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT       dbo.Employees.EmployeeNumber, dbo.Employees.Name, dbo.Employees.Surname   FROM     dbo.Employees
								
								 INNER JOIN
                                                             (SELECT em.EmployeeNumber 
                                                              FROM  dbo.Employees em 
                                                              WHERE dbo.IsAssessorInEvaluation (em.EmployeeNumber,@EvaluationId)='True' AND em.EmployeeState='Attivo') AS DesignatedAssessors
                                                         ON   dbo.Employees.EmployeeNumber = DesignatedAssessors.EmployeeNumber
		                    LEFT OUTER JOIN
                                                             (SELECT DISTINCT AutoOrManual, AssessorId
                                                               FROM            dbo.EvaluationPairings
                                                               WHERE        (EvaluationId = @EvaluationId) AND (AutoOrManual = 'Manual')) AS ManualPairings ON dbo.Employees.EmployeeNumber = ManualPairings.AssessorId
                                                             
";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                {
                    ret.Add(new EmployeeFilteringData()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"]))
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret = ret.OrderBy(x => x.surname).ToList();

            return ret;
        }



        public List<EmployeeFilteringData> getAssessorFilteringData_old(int evaluationId)
        {
            List<EmployeeFilteringData> ret = new List<EmployeeFilteringData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT        dbo.Employees.EmployeeNumber, dbo.Employees.Name, dbo.Employees.Surname, dbo.Employees.Workplace, dbo.Employees.DepartmentCode, dbo.Employees.DepartmentDescription, 
                                                        dbo.Employees.IsDirector,dbo.Employees.IsHeadOfDepartment, dbo.Employees.WorkplaceAlgorithm, dbo.Employees.ExternalEmployee, dbo.Employees.Commuter, dbo.Employees.ExtensionNotes, GradeStats.AverageGrade, 
                                                         GradeStats.MostRecentGrade, LastReminders.NumReminder, dbo.Employees.CurrentJob, dbo.EmployeeRoles.RoleType, ManualPairings.AutoOrManual, CurrentContracts.ContractType, CurrentContracts.Company, 
                                                         CurrentContracts.ContractStartDate, CurrentContracts.ContractEndDate, NumberOfEvaluees
                                FROM            dbo.Employees INNER JOIN
                                                             (SELECT em.EmployeeNumber 
                                                              FROM  dbo.Employees em 
                                                              WHERE dbo.IsAssessorInEvaluation (em.EmployeeNumber,@EvaluationId)='True' AND em.EmployeeState='Attivo') AS DesignatedAssessors ON 
                                                         dbo.Employees.EmployeeNumber = DesignatedAssessors.EmployeeNumber LEFT OUTER JOIN
                                                             (SELECT        dbo.EmployeeContracts.EmployeeId, dbo.EmployeeContracts.ContractType, dbo.EmployeeContracts.Company, dbo.EmployeeContracts.ContractStartDate, dbo.EmployeeContracts.ContractEndDate, 
                                                                                         dbo.EmployeeContracts.Extensions, dbo.EmployeeContracts.ExtensionNotes
                                                               FROM            dbo.EmployeeContracts INNER JOIN
                                                                                             (SELECT        EmployeeId, MAX(ContractStartDate) AS maxDate
                                                                                               FROM            dbo.EmployeeContracts AS EmployeeContracts_1
                                                                                               GROUP BY EmployeeId) AS lastContracts ON dbo.EmployeeContracts.EmployeeId = lastContracts.EmployeeId AND dbo.EmployeeContracts.ContractStartDate = lastContracts.maxDate) AS CurrentContracts ON 
                                                         dbo.Employees.EmployeeNumber = CurrentContracts.EmployeeId LEFT OUTER JOIN
                                                             (SELECT DISTINCT AutoOrManual, AssessorId
                                                               FROM            dbo.EvaluationPairings
                                                               WHERE        (EvaluationId = @EvaluationId) AND (AutoOrManual = 'Manual')) AS ManualPairings ON dbo.Employees.EmployeeNumber = ManualPairings.AssessorId LEFT OUTER JOIN
                                                         dbo.EmployeeRoles ON dbo.Employees.EmployeeNumber = dbo.EmployeeRoles.EmployeeId LEFT OUTER JOIN
                                                             (SELECT        AssessorId, COUNT(ReminderId) AS NumReminder
                                                               FROM            dbo.Reminders AS Reminders_1
                                                               WHERE        (EvaluationId = @EvaluationId)
                                                               GROUP BY AssessorId) AS LastReminders ON dbo.Employees.EmployeeNumber = LastReminders.AssessorId LEFT OUTER JOIN
                                                             (SELECT        AssessorId, AVG(Grade) AS AverageGrade, COUNT(DISTINCT EvalueeId) AS NumberOfEvaluees, MAX(LastModified) AS MostRecentGrade
                                                               FROM            dbo.EvaluationTerms AS EvaluationTerms_1
                                                               WHERE        (Grade > 0) AND (AssessorId IS NOT NULL) AND (EvaluationId = @EvaluationId)
                                                               GROUP BY AssessorId) AS GradeStats ON dbo.Employees.EmployeeNumber = GradeStats.AssessorId";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                {
                    ret.Add(new EmployeeFilteringData()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                        workplace = ValueOperations.DBToString(reader["Workplace"]),
                        departmentCode = ValueOperations.DBToString(reader["DepartmentCode"]),
                        departmentDescription = ValueOperations.DBToString(reader["DepartmentDescription"]),
                        averageGrade = ValueOperations.DBToDecimal(reader["AverageGrade"]),
                        gradesDone = ValueOperations.DBToInteger(reader["NumberOfEvaluees"]),
                        mostRecentGrade = ValueOperations.DBToDate(reader["MostRecentGrade"]),
                        reminders = ValueOperations.DBToInteger(reader["NumReminder"]),
                        company = ValueOperations.DBToString(reader["Company"]),
                        contractEndDate = ValueOperations.DBToDate(reader["ContractEndDate"]),
                        contractExpiresInDays = (int)(ValueOperations.DBToDate(reader["ContractEndDate"]) - DateTime.Now).TotalDays,
                        contractStartDate = ValueOperations.DBToDate(reader["ContractStartDate"]),
                        contractType = ValueOperations.DBToString(reader["ContractType"]),
                        currentJob = optionalDecryption(ValueOperations.DBToString(reader["CurrentJob"])),
                        hasManualPairing = "Manual".Equals(ValueOperations.DBToString(reader["AutoOrManual"])),
                        role = ValueOperations.DBToBool(reader["IsDirector"]) ? "Director" : (ValueOperations.DBToBool(reader["IsHeadOfDepartment"]) ? "HeadOfDepartment" : "User"),
                        externalEmployee = ValueOperations.DBToBool(reader["ExternalEmployee"]),
                        commuter = ValueOperations.DBToBool(reader["Commuter"]),
                        extensionNotes = ValueOperations.DBToString(reader["ExtensionNotes"]),
                        workplaceAlgorithm = ValueOperations.DBToInteger(reader["WorkplaceAlgorithm"]),
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret = ret.OrderBy(x => x.surname).ToList();

            return ret;
        }

        public List<EvaluationPairing> getAssessorPairings(int evaluationId, string assessorId, bool getSelfEvaluations)
        {
            List<EvaluationPairing> ret = new List<EvaluationPairing>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from EvaluationPairings where EvaluationId=@EvaluationId AND 
                                AssessorId=@AssessorId AND IsSelfEvaluation=@IsSelfEvaluation";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@AssessorId",assessorId),
                        createParameter("@IsSelfEvaluation",getSelfEvaluations)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(toEvaluationPairing(reader));
                }

                reader.Close();

                if (!ValueOperations.isNullOrEmpty(ret))
                {
                    logger.Info($"Count n. {ret.Count}");

                    foreach (EvaluationPairing ep in ret)
                    {
                        //query = @"select COUNT(*) from Reminders WITH (NOLOCK) WHERE EvaluationId=@EvaluationId AND 
                        //                          EvalueeId=@EvalueeId";
                        //pars = new SqlParameter[]{
                        //	createParameter("@EvaluationId",evaluationId),
                        //	createParameter("@EvalueeId",ep.EvalueeId)
                        //};
                        //string res = ExecuteScalar(query, pars);
                        int resCount = 0;
                        //if (int.TryParse(res, out resCount))
                        ep.RemindersSent = resCount;
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }



        public int getCountAssessorPairings(int evaluationId, string assessorId, bool getSelfEvaluations)
        {
            int ret = 0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from EvaluationPairings where EvaluationId=@EvaluationId AND 
                                AssessorId=@AssessorId AND IsSelfEvaluation=@IsSelfEvaluation";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@AssessorId",assessorId),
                        createParameter("@IsSelfEvaluation",getSelfEvaluations)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                ret = ExecuteCount(query, pars);
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public EvaluationPairing getAssessorPairing(int evaluationId, string assessorId, string evalueeId)
        {
            EvaluationPairing ret = new EvaluationPairing();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from EvaluationPairings where EvaluationId=@EvaluationId AND 
                                AssessorId=@AssessorId AND  EvalueeId=@EvalueeId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@AssessorId",assessorId),
                        createParameter("@EvalueeId", evalueeId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret = toEvaluationPairing(reader);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getAssessorsFromDetails(DateTime endDate)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select DISTINCT AssessorCode FROM EvaluationDetails WHERE EvaluationDate=@EvaluationDate";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationDate", endDate));
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToString(reader["AssessorCode"]));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getAssessorsWhoGraded(int evaluationId, string evalueeId)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select DISTINCT AssessorId from EvaluationTerms where EvaluationId=@EvaluationId AND 
                                EvalueeId=@EvalueeId AND Grade>0";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@EvalueeId", evalueeId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToString(reader["AssessorId"]));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Contract getCurrentEmployeeContract(string employeeId)
        {
            Contract ret = new Contract();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * FROM EmployeeContracts WHERE EmployeeId=@EmployeeId ORDER BY ContractStartDate DESC";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EmployeeId", employeeId));
                if (reader.Read())
                {
                    ret = new Contract()
                    {
                        company = ValueOperations.DBToString(reader["Company"]),
                        contractEndDate = ValueOperations.DBToNormalizedDate(reader["ContractEndDate"]),
                        contractStartDate = ValueOperations.DBToNormalizedDate(reader["ContractStartDate"]),
                        contractType = ValueOperations.DBToString(reader["ContractType"]),
                        employeeId = ValueOperations.DBToString(reader["EmployeeId"]),
                        extensionNotes = ValueOperations.DBToString(reader["ExtensionNotes"]),
                        extensions = ValueOperations.DBToString(reader["Extensions"]),
                        contractExpiresInDays = (int)(ValueOperations.DBToDate(reader["ContractEndDate"]) - DateTime.Now).TotalDays
                    };
                    if (ret.contractExpiresInDays < 0)
                        ret.contractExpiresInDays = -1;
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public int getCurrentEvaluationId()
        {
            int ret = -1;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select EvaluationId from Evaluations WHERE EvaluationStatus IN ('Underway','Expired') ORDER BY StartDate DESC";

                SqlDataReader reader = ExecuteReader(query);
                if (reader.Read())
                {
                    ret = ValueOperations.DBToInteger(reader["EvaluationId"]);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public DefaultMessagesConfiguration getDefaultMessagesConfiguration(string username)
        {
            DefaultMessagesConfiguration ret = new DefaultMessagesConfiguration();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from PrivacyConfiguration where Username=@Username";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@Username",username)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret.conf1 = reader["PrivacyConfiguration"].ToString();
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Departments getDepartmentsToEvaluate(int evaluationId)
        {
            Departments ret = new Departments();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from DepartmentsToEvaluate 
                                    WHERE EvaluationId=@EvaluationId ORDER BY DepartmentCode";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(new Department()
                    {
                        departmentCode = reader["DepartmentCode"].ToString(),
                        departmentDescription = reader["DepartmentDescription"].ToString()
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Employee getEmployeeSimplified(string employeeId)
        {
            Employee ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "select emp.EmployeeNumber, emp.Name, emp.Surname from Employees emp WHERE emp.EmployeeNumber=@EmployeeNumber";
                SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@EmployeeNumber",employeeId)
                        };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                    ret = toEmployeeSimplified(reader);
                reader.Close();

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public byte[] getPhoto(string employeeId)
        {
            byte[] ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "select EmployeePhotos.Photo from EmployeePhotos WHERE EmployeePhotos.EmployeeNumber=@EmployeeNumber";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EmployeeNumber",employeeId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                    ret = ValueOperations.DBToByteArray(reader["Photo"]);
            }

            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Employee getEmployee(string employeeId, bool getPhoto)
        {
            Employee ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                if (getPhoto)
                {
                    string query = "SELECT emp.*, rol.RoleType, pho.Photo, sum.yearAverage";
                    query = query + " FROM Employees emp";
                    query = query + " LEFT JOIN EmployeeRoles rol ON emp.EmployeeNumber=rol.EmployeeId";
                    query = query + " LEFT JOIN EmployeePhotos pho ON emp.EmployeeNumber=pho.EmployeeNumber";
                    query = query + " LEFT JOIN EvaluationsSummary sum ON sum.EmployeeNumber = emp.EmployeeNumber";
                    query = query + " WHERE emp.EmployeeNumber=@EmployeeNumber";

                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EmployeeNumber",employeeId)
                    };
                    SqlDataReader reader = ExecuteReader(query, pars);
                    if (reader.Read())
                        ret = toEmployeeWithPhoto(reader);
                    reader.Close();
                }
                else
                {
                    string query = "SELECT emp.*, rol.RoleType, sum.yearAverage";
                    query = query + " FROM Employees emp";
                    query = query + " LEFT JOIN EmployeeRoles rol ON emp.EmployeeNumber=rol.EmployeeId";
                    query = query + " LEFT JOIN EvaluationsSummary sum ON sum.EmployeeNumber = emp.EmployeeNumber";
                    query = query + " WHERE emp.EmployeeNumber=@EmployeeNumber";

                    SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@EmployeeNumber",employeeId)
                        };
                    SqlDataReader reader = ExecuteReader(query, pars);
                    if (reader.Read())
                        ret = toEmployee(reader);
                    reader.Close();
                }
                if (ret != null)
                {
                    ret.ContractData = getCurrentEmployeeContract(ret.EmployeeNumber);
                    //TODO ret.PrivilegeIdList = GetPrivilegeIdList(ret.RoleId);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Employee getEmployee(string employeeId, string password, bool getPhoto)
        {
            Employee ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                if (getPhoto)
                {
                    //string query = "select emp.*, rol.RoleType, pho.Photo from Employees emp left join EmployeeRoles rol on emp.EmployeeNumber=rol.EmployeeId left join EmployeePhotos pho on emp.EmployeeNumber=pho.EmployeeNumber WHERE emp.EmployeeNumber=@EmployeeNumber AND emp.Password=@Password";
                    string query = "SELECT emp.*, rol.RoleType, pho.Photo, sum.yearAverage";
                    query = query + " FROM Employees emp";
                    query = query + " LEFT JOIN EmployeeRoles rol ON emp.EmployeeNumber=rol.EmployeeId";
                    query = query + " LEFT JOIN EmployeePhotos pho ON emp.EmployeeNumber=pho.EmployeeNumber";
                    query = query + " LEFT JOIN EvaluationsSummary sum ON sum.EmployeeNumber = emp.EmployeeNumber";
                    query = query + " WHERE emp.EmployeeNumber=@EmployeeNumber AND emp.Password=@Password";

                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EmployeeNumber",employeeId),
                        createParameter("@Password",optionalEncryption(password))
                    };
                    SqlDataReader reader = ExecuteReader(query, pars);
                    if (reader.Read())
                        ret = toEmployeeWithPhoto(reader);
                    reader.Close();
                }
                else
                {
                    //string query = "select emp.*, rol.RoleType from Employees emp left join EmployeeRoles rol on emp.EmployeeNumber=rol.EmployeeId WHERE emp.EmployeeNumber=@EmployeeNumber AND emp.Password=@Password";
                    string query = "SELECT emp.*, rol.RoleType, sum.yearAverage";
                    query = query + " FROM Employees emp";
                    query = query + " LEFT JOIN EmployeeRoles rol ON emp.EmployeeNumber=rol.EmployeeId";
                    query = query + " LEFT JOIN EvaluationsSummary sum ON sum.EmployeeNumber = emp.EmployeeNumber";
                    query = query + " WHERE emp.EmployeeNumber=@EmployeeNumber AND emp.Password=@Password";

                    SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@EmployeeNumber",employeeId),
                            createParameter("@Password",optionalEncryption(password)),
                        };
                    SqlDataReader reader = ExecuteReader(query, pars);
                    if (reader.Read())
                        ret = toEmployee(reader);
                    reader.Close();
                }
                if (ret != null)
                {
                    ret.ContractData = getCurrentEmployeeContract(ret.EmployeeNumber);
                    ret.PrivilegeIdList = GetPrivilegeIdList(ret.RoleId);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<EmployeeAssessmentData> getEmployeeAssessmentData(List<string> employeeIdList)
        {
            List<EmployeeAssessmentData> ret = new List<EmployeeAssessmentData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select Employees.EmployeeNumber, Employees.Name, Employees.Surname, Photo, Employees.CurrentJob, Employees.HiringDate, 
                                    Employees.TerminationDate, Employees.ContractType, Employees.Company, Employees.DepartmentCode, Employees.DepartmentDescription 
                                    from Employees left join EmployeePhotos 
                                    on Employees.EmployeeNumber=EmployeePhotos.EmployeeNumber 
                                    WHERE Employees.EmployeeNumber IN (" + ValueOperations.arrayToDbInClause(employeeIdList) + ")";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                    ret.Add(new EmployeeAssessmentData()
                    {
                        EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        Name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        Surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                        Photo = ValueOperations.DBToByteArray(reader["Photo"]),
                        CurrentJob = optionalDecryption(ValueOperations.DBToString(reader["CurrentJob"])),
                        HiringDate = normalizeAndDecrypt(reader["HiringDate"]),
                        TerminationDate = normalizeAndDecrypt(reader["TerminationDate"]),
                        ContractType = ValueOperations.DBToString(reader["ContractType"]),
                        Company = ValueOperations.DBToString(reader["Company"]),
                        DepartmentCode = ValueOperations.DBToString(reader["DepartmentCode"]),
                        DepartmentDescription = ValueOperations.DBToString(reader["DepartmentDescription"]),
                    });
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<EmployeeAssessmentData> getEmployeeAssessmentData_old(List<string> employeeIdList)
        {
            List<EmployeeAssessmentData> ret = new List<EmployeeAssessmentData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select Employees.EmployeeNumber, Name, Surname, Photo, CurrentJob, HiringDate, 
                                TerminationDate, ContractType, Company, DepartmentCode, DepartmentDescription 
                                from Employees left join EmployeePhotos 
                                on Employees.EmployeeNumber=EmployeePhotos.EmployeeNumber 
                                WHERE Employees.EmployeeNumber IN (" + ValueOperations.arrayToDbInClause(employeeIdList) + ")";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                    ret.Add(new EmployeeAssessmentData()
                    {
                        EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        Name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        Surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                        Photo = ValueOperations.DBToByteArray(reader["Photo"]),
                        CurrentJob = optionalDecryption(ValueOperations.DBToString(reader["CurrentJob"])),
                        HiringDate = normalizeAndDecrypt(reader["HiringDate"]),
                        TerminationDate = normalizeAndDecrypt(reader["TerminationDate"]),
                        ContractType = ValueOperations.DBToString(reader["ContractType"]),
                        Company = ValueOperations.DBToString(reader["Company"]),
                        DepartmentCode = ValueOperations.DBToString(reader["DepartmentCode"]),
                        DepartmentDescription = ValueOperations.DBToString(reader["DepartmentDescription"]),
                    });
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Contracts getEmployeeContractHistory(string employeeId)
        {
            Contracts ret = new Contracts();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * FROM EmployeeContracts WHERE EmployeeId=@EmployeeId ORDER BY ContractStartDate DESC";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EmployeeId", employeeId));
                while (reader.Read())
                {
                    ret.Add(new Contract()
                    {
                        company = ValueOperations.DBToString(reader["Company"]),
                        contractEndDate = ValueOperations.DBToDate(reader["ContractEndDate"]),
                        contractStartDate = ValueOperations.DBToDate(reader["ContractStartDate"]),
                        contractType = ValueOperations.DBToString(reader["ContractType"]),
                        employeeId = ValueOperations.DBToString(reader["EmployeeId"]),
                        extensionNotes = ValueOperations.DBToString(reader["ExtensionNotes"]),
                        extensions = ValueOperations.DBToString(reader["Extensions"])
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Employee getEmployeeFromSID(byte[] objectSID)
        {
            Employee ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT        emp.*, rol.RoleType, sum.yearAverage
                                FROM            dbo.EmployeeObjectSID AS os INNER JOIN
                                                dbo.Employees AS emp ON os.EmployeeNumber = emp.EmployeeNumber LEFT OUTER JOIN
                                                dbo.EmployeeRoles AS rol ON emp.EmployeeNumber = rol.EmployeeId LEFT JOIN 
                                                dbo.EvaluationsSummary sum ON sum.EmployeeNumber = emp.EmployeeNumber
                                WHERE           os.ObjectSID=@ObjectSID";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@ObjectSID",objectSID)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                    ret = toEmployee(reader);
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
            if (ret != null)
            {
                ret.ContractData = getCurrentEmployeeContract(ret.EmployeeNumber);
                ret.PrivilegeIdList = GetPrivilegeIdList(ret.RoleId);
            }
            return ret;
        }

        public EmployeeNames getEmployeeNames(List<string> employeeIdList)
        {
            EmployeeNames ret = new EmployeeNames();
            EmployeeNames temp = new EmployeeNames();
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select EmployeeNumber, Name, Surname, DepartmentCode  
                                FROM Employees WHERE EmployeeNumber IN (" +
                                ValueOperations.arrayToDbInClause(employeeIdList) + ")";

                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    temp.Add(new EmployeeName()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                        departmentCode = ValueOperations.DBToString(reader["DepartmentCode"])
                    });
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret.AddRange(temp.OrderBy(x => x.surname));

            return ret;
        }

        public EmployeeNames getAllEmployeeNames(List<int> accessibleWorkplacesAlgorithmList)
        {
            EmployeeNames ret = new EmployeeNames();
            EmployeeNames temp = new EmployeeNames();
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select EmployeeNumber, Name, Surname, DepartmentCode";
                query = query + " FROM Employees WHERE EmployeeState='Attivo' ";

                /*** Filtro per i Workplaces relativi ai privilegi dell'utente ************/
                if (accessibleWorkplacesAlgorithmList != null && accessibleWorkplacesAlgorithmList.Count > 0)
                {
                    query = query + " AND WorkplaceAlgorithm IN (";
                    foreach (int k in accessibleWorkplacesAlgorithmList)
                        query = query + k + ",";

                    query = query.TrimEnd(',') + ")";
                }
                /**************************************************************************/

                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    temp.Add(new EmployeeName()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                        departmentCode = ValueOperations.DBToString(reader["DepartmentCode"])
                    });
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret.AddRange(temp.OrderBy(x => x.surname));

            return ret;
        }

        public List<EmployeePersonalInfo> getEmployeePersonalInfo()
        {
            List<EmployeePersonalInfo> ret = new List<EmployeePersonalInfo>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select EmployeeNumber, Name, Surname from Employees";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                    ret.Add(new EmployeePersonalInfo()
                    {
                        EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        Name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        Surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"]))
                    });
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret = ret.OrderBy(x => x.Surname).ToList();

            return ret;
        }


        public void terminateDeletedEmployees()
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select EmployeeNumber, TerminationDate from employees where ISNUMERIC(EmployeeNumber)=1 
                                    and not exists 
                                        (select numdipendente from ANAGRAFICA_DIPENDENTI 
                                            where ISNUMERIC(NumDipendente)=1 AND 
                                            convert(int,NumDipendente)=convert(int,EmployeeNumber))";
                SqlDataReader reader = ExecuteReader(query);

                List<string> morituri = new List<string>();
                while (reader.Read())
                {
                    DateTime terminationDate = normalizeAndDecrypt(reader["TerminationDate"]);
                    if (terminationDate <= ValueOperations.getEpoch())
                    {
                        morituri.Add(ValueOperations.DBToString(reader["EmployeeNumber"]));
                    }
                }
                reader.Close();

                if (!ValueOperations.isNullOrEmpty(morituri))
                {
                    foreach (string sm in morituri)
                    {
                        query = "UPDATE Employees SET TerminationDate=@TerminationDate, EmployeeState='Terminato' WHERE EmployeeNumber=@EmployeeNumber";
                        ExecuteNonQuery(query, createParameter("@TerminationDate", optionalEncryption(DateTime.Now)),
                                                createParameter("@EmployeeNumber", sm));
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public PromotionStatus? getEmployeePromotionStatus(int evaluationId, string employeeId)
        {
            PromotionStatus? ret = PromotionStatus.Accepted;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from EvaluationPromotions WHERE EmployeeId=@EmployeeId AND 
                                    EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EmployeeId",employeeId),
                    createParameter("@EvaluationId",evaluationId)
                };

                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = null;
                    if (!string.IsNullOrEmpty(reader["PromotionStatus"].ToString()))
                        ret = Enums.GetEnumFromName<PromotionStatus>(reader["PromotionStatus"].ToString());

                    if ("Assessor".Equals(reader["EmployeeType"].ToString()) &&
                        ret.HasValue && ret.Value == PromotionStatus.Accepted)
                        ret = PromotionStatus.Accepted;
                    else if ("Assessor".Equals(reader["EmployeeType"].ToString()) &&
                        ret.HasValue && ret.Value == PromotionStatus.Rejected)
                        ret = PromotionStatus.Rejected;
                    else if (("Evaluee".Equals(reader["EmployeeType"].ToString()) && ValueOperations.DBToBool(reader["CanBePromoted"])) ||
                        (ret.HasValue && ret.Value == PromotionStatus.Proposed))
                        ret = PromotionStatus.Proposed;
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Employees getEmployees(List<string> employeeIdList, bool getPhoto)
        {
            Employees ret = new Employees();

            if (!ValueOperations.isNullOrEmpty(employeeIdList))
            {
                bool wasOpen = false;
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                try
                {
                    if (getPhoto)
                    {
                        string query = @"select emp.*, rol.RoleType, pho.Photo, sum.yearAverage from Employees emp 
                                    left join EmployeeRoles rol on emp.EmployeeNumber=rol.EmployeeId 
                                    left join EmployeePhotos pho on emp.EmployeeNumber=pho.EmployeeNumber
                                    LEFT JOIN EvaluationsSummary sum ON sum.EmployeeNumber = emp.EmployeeNumber
                                    WHERE emp.EmployeeNumber IN (" + ValueOperations.arrayToDbInClause(employeeIdList) + ")";
                        SqlDataReader reader = ExecuteReader(query);
                        while (reader.Read())
                        {
                            Employee emp = toEmployeeWithPhoto(reader);
                            if (emp != null)
                                ret.Add(emp);
                        }
                        reader.Close();
                    }
                    else
                    {
                        string query = @"select emp.*, rol.RoleType, sum.yearAverage from Employees emp 
                                    left join EmployeeRoles rol on emp.EmployeeNumber=rol.EmployeeId 
                                    LEFT JOIN dbo.EvaluationsSummary sum ON  sum.EmployeeNumber = emp.EmployeeNumber
                                    WHERE emp.EmployeeNumber IN (" + ValueOperations.arrayToDbInClause(employeeIdList) + ")";
                        SqlDataReader reader = ExecuteReader(query);
                        while (reader.Read())
                        {
                            Employee emp = toEmployee(reader);
                            if (emp != null)
                                ret.Add(emp);
                        }
                        reader.Close();
                    }


                    foreach (Employee emp3 in ret)
                        emp3.ContractData = getCurrentEmployeeContract(emp3.EmployeeNumber);

                    var downgraded = GetDowngradedAssessorsList();
                    foreach (Employee emp3 in ret)
                    {
                        if (downgraded.Any() && downgraded.Contains(emp3.EmployeeNumber))
                        {
                            emp3.isDowngraded = true;
                        }
                    }
                }

                finally
                {
                    if (!wasOpen)
                        connection.Close();
                }


            }
            return ret;
        }


        public List<ErrorLog> getErrorLogsToSend()
        {
            List<ErrorLog> ret = new List<ErrorLog>();
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"SELECT * FROM ErrorLog WHERE Status='READY'";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(new ErrorLog()
                    {
                        logDescription = reader["LogDescription"].ToString(),
                        logMessage = reader["LogMessage"].ToString(),
                        logType = reader["LogType"].ToString(),
                    });
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Evaluation getEvaluation(int evaluationId)
        {
            Evaluation ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from Evaluations where EvaluationStatus IS NOT NULL AND EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = toEvaluation(reader);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        /// <summary>
        /// Delete evaluations Summary
        /// </summary>
        /// <param name="employeeNumber"></param>
        public void deleteEvaluationSummary(string employeeNumber)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            string sql = "DELETE FROM EvaluationsSummary WHERE EmployeeNumber = @EmployeeNumber";

            SqlParameter[] args = new SqlParameter[]
            {
                createParameter("@EmployeeNumber", employeeNumber)
            };

            try
            {
                ExecuteNonQuery(sql, args);
            }
            catch
            {
                if (!wasOpen)
                    connection.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

        }

        /// <summary>
        /// Update record into table EvaluationSummary
        /// </summary>
        /// <param name="evaluationSummary"></param>
        public void updateEvaluationsSummary(EvaluationSummary evaluationSummary)
        {
            updateEvaluationsSummary(evaluationSummary.EmployeeNumber, evaluationSummary.TotalAverage, evaluationSummary.YearAverage);
        }


        /// <summary>
        /// Update record into table EvaluationSummary
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="totalAverage"></param>
        /// <param name="yearAverage"></param>
        public void updateEvaluationsSummary(string employeeNumber, decimal totalAverage, decimal yearAverage)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            string sql =
                "UPDATE EvaluationsSummary SET TotalAverage=@TotalAverage, YearAverage=@YearAverage, ModifyDate=@ModifyDate WHERE EmployeeNumber = @EmployeeNumber";

            SqlParameter[] args = new SqlParameter[]
            {
                createParameter("@TotalAverage", totalAverage),
                createParameter("@YearAverage", yearAverage),
                createParameter("@ModifyDate", DateTime.Now),
                createParameter("@EmployeeNumber", employeeNumber)

            };


            try
            {
                ExecuteNonQuery(sql, args);
            }
            catch
            {
                if (!wasOpen)
                    connection.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }


        }
        /// <summary>
        /// Insert new record into table EvaluationSummary
        /// </summary>
        /// <param name="evaluationSummary"></param>
        public void insertEvaluationSummary(EvaluationSummary evaluationSummary)
        {
            insertEvaluationSummary(evaluationSummary.EmployeeNumber, evaluationSummary.TotalAverage,
                evaluationSummary.YearAverage);
        }


        /// <summary>
        /// Insert new record into table EvaluationSummary
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <param name="totalAverage"></param>
        /// <param name="yearAverage"></param>
        public void insertEvaluationSummary(string employeeNumber, decimal totalAverage, decimal yearAverage)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            string query = @"INSERT INTO EvaluationsSummary (EmployeeNumber, TotalAverage, YearAverage, ModifyDate)
                                 VALUES (@EmployeeNumber, @TotalAverage, @YearAverage, @ModifyDate)";

            SqlParameter[] args = new SqlParameter[]
            {
                createParameter("@EmployeeNumber", employeeNumber),
                createParameter("@TotalAverage", totalAverage),
                createParameter("@yearAverage", yearAverage),
                createParameter("@ModifyDate", DateTime.Now)
            };

            try
            {
                ExecuteNonQuery(query, args);
            }
            catch
            {
                if (!wasOpen)
                    connection.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

        }


        /// <summary>
        /// Get Evaluation Summary by EmployeeNumber
        /// </summary>
        /// <param name="employeeNumber"></param>
        /// <returns></returns>
        public EvaluationSummary getEvaluationsSummary(string employeeNumber)
        {

            EvaluationSummary evaluationSummary = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            string query = "SELECT * FROM EvaluationsSummary where EmployeeNumber = @EmployeeNumber";
            SqlParameter[] args = new SqlParameter[]
            {
                createParameter("@EmployeeNumber", employeeNumber)
            };


            try
            {
                var reader = ExecuteReader(query, args);
                if (reader.Read())
                    evaluationSummary = toEvaluationSummary(reader);
                reader.Close();

            }
            catch (Exception ex)
            {
                if (!wasOpen)
                    connection.Close();

            }

            return evaluationSummary;

        }


        /// <summary>
        /// Utility extension for receive EvaluationSummary
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static EvaluationSummary toEvaluationSummary(SqlDataReader reader)
        {
            return new EvaluationSummary()
            {
                EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                TotalAverage = ValueOperations.DBToDecimal(reader["TotalAverage"]),
                YearAverage = ValueOperations.DBToDecimal(reader["YearAverage"]),
                ModifyDate = ValueOperations.DBToDate(reader["ModifyDate"])

            };

        }
        public Evaluation getEvaluationByDate(DateTime endDate)
        {
            Evaluation ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from Evaluations where EvaluationStatus IS NOT NULL AND EndDate=@EndDate";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EndDate",endDate)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = toEvaluation(reader);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public EvaluationConnections getEvaluationConnections(DateTime dateFrom, DateTime dateTo)
        {
            EvaluationConnections ret = new EvaluationConnections();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from EvaluationConnections where ConnectionDate>@DateFrom AND ConnectionDate<@DateTo";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@DateFrom",dateFrom),
                        createParameter("@DateTo",dateTo),
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(new EvaluationConnection()
                    {
                        Task = ValueOperations.DBToString(reader["Task"]),
                        EmployeeCode = ValueOperations.DBToString(reader["EmployeeCode"]),
                        ODP = ValueOperations.DBToString(reader["ODP"]),
                        Commission = ValueOperations.DBToString(reader["Commission"]),
                        ConnectionDate = DateTime.Parse(ValueOperations.DBToString(reader["ConnectionDate"])),
                        ConnectionType = ValueOperations.DBToString(reader["ConnectionType"])
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
            return ret;
        }

        public EvaluationCriteria_Multilanguage getEvaluationCriteriaConfiguration(int evaluationId, bool callSlow = false)
        {
            EvaluationCriteria_Multilanguage ret = new EvaluationCriteria_Multilanguage();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select *, macroAreaName FROM EvaluationCriteria LEFT JOIN MacroAreas ON EvaluationCriteria.macroareaid=MacroAreas.macroareaid WHERE EvaluationId=@EvaluationId ORDER BY ParentCode";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                {
                    EvaluationCriterion_Multilanguage crit = new EvaluationCriterion_Multilanguage()
                    {
                        CriterionCode = ValueOperations.safeToString(ValueOperations.DBToString(reader["CriterionCode"])).Trim(),
                        CriterionWeight = ValueOperations.DBToInteger(reader["CriterionWeight"]),
                        Ordering = ValueOperations.DBToInteger(reader["Ordering"]),
                        ValidFrom = ValueOperations.DBToDate(reader["ValidFrom"]),
                        ValidTo = ValueOperations.DBToDate(reader["ValidTo"]),
                        Expanded = ValueOperations.DBToBool(reader["Expanded"]),
                        macroAreaName = ValueOperations.safeToString(reader["macroAreaName"]),
                        macroAreaId = ValueOperations.DBToInteger(reader["macroAreaid"]),
                        macroAreaOrder = ValueOperations.DBToInteger(reader["macroAreaOrder"])


                    };

                    string parentCode = ValueOperations.DBToString(reader["ParentCode"]);
                    if (string.IsNullOrEmpty(parentCode) && ret[crit.CriterionCode] == null)
                        ret.Add(crit);
                    else
                    {
                        if (ret[parentCode] != null)
                            ret[parentCode].Subcriteria.Add(crit);
                    }
                }

                reader.Close();

                //return the criteria in order
                if (!ret.IsEmpty())
                {
                    List<EvaluationCriterion_Multilanguage> ordered = ret.OrderBy(x => x.Ordering).ToList();
                    ret.Clear();
                    ret.AddRange(ordered);

                    foreach (EvaluationCriterion_Multilanguage theCrit in ret)
                    {
                        if (!theCrit.Subcriteria.IsEmpty())
                        {
                            ordered = theCrit.Subcriteria.OrderBy(x => x.Ordering).ToList();
                            theCrit.Subcriteria.Clear();
                            theCrit.Subcriteria.AddRange(ordered);
                        }
                    }
                }


                if (!ret.IsEmpty() && callSlow)
                {
                    query = VerySlowMethod(evaluationId, ret, ref reader);
                }
                else
                {
                    query = FasterMethod(evaluationId, ret, ref reader);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        private string FasterMethod(int evaluationId, EvaluationCriteria_Multilanguage ret, ref SqlDataReader reader)
        {

            var query = "select * FROM EvaluationCriteriaLanguages WHERE EvaluationId=@EvaluationId AND CriterionCode=@CriterionCode ORDER BY LanguageCode";

            foreach (EvaluationCriterion_Multilanguage theCrit in ret)
            {
                reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId), createParameter("@CriterionCode", theCrit.CriterionCode));
                while (reader.Read())
                {
                    try
                    {
                        theCrit.CriterionDescription.Add(ValueOperations.safeToString(ValueOperations.DBToString(reader["LanguageCode"])).Trim(),
                                                        ValueOperations.safeToString(ValueOperations.DBToString(reader["CriterionDescription"])).Trim());
                    }
                    catch (Exception lex) { }
                }

                reader.Close();

                if (!theCrit.Subcriteria.IsEmpty())
                {
                    foreach (EvaluationCriterion_Multilanguage subCrit in theCrit.Subcriteria)
                    {
                        reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId), createParameter("@CriterionCode", subCrit.CriterionCode));
                        while (reader.Read())
                        {
                            try
                            {
                                subCrit.CriterionDescription.Add(ValueOperations.safeToString(ValueOperations.DBToString(reader["LanguageCode"])).Trim(),
                                                                ValueOperations.safeToString(ValueOperations.DBToString(reader["CriterionDescription"])).Trim());
                            }
                            catch (Exception lex) { }
                        }

                        reader.Close();
                    }
                }
            }


            return query;
        }

        private string VerySlowMethod(int evaluationId, EvaluationCriteria_Multilanguage ret, ref SqlDataReader reader)
        {
            string query = @"select distinct ecl.LanguageCode, 
                        isnull(ecl.CriterionDescription, ecl2.CriterionDescription) as CriterionDescription, 
                        isnull(ecl.VideoPath, ecl3.VideoPath) as VideoPath, 
                        isnull(ecl.VideoEnable, ecl3.VideoEnable) as VideoEnable
                        FROM EvaluationCriteriaLanguages ecl
                        left join [dbo].EvaluationCriteriaLanguages ecl2 on ecl.CriterionCode = ecl2.CriterionCode and ecl2.LanguageCode = ecl.LanguageCode and ecl2.CriterionDescription is not null
                        left join [dbo].EvaluationCriteriaLanguages ecl3 on ecl.CriterionCode = ecl3.CriterionCode and ecl3.LanguageCode = ecl.LanguageCode and ecl3.VideoPath is not null and ecl3.VideoEnable = 1
                        WHERE ecl.EvaluationId=@EvaluationId AND ecl.CriterionCode=@CriterionCode ORDER BY ecl.LanguageCode";
            foreach (EvaluationCriterion_Multilanguage theCrit in ret)
            {
                reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId), createParameter("@CriterionCode", theCrit.CriterionCode));
                while (reader.Read())
                {
                    try
                    {
                        var lang = ValueOperations.safeToString(ValueOperations.DBToString(reader["LanguageCode"])).Trim();
                        theCrit.VideoPaths.Add(lang, ValueOperations.DBToString(reader["VideoPath"]));
                        theCrit.VideoEnables.Add(lang, ValueOperations.DBToBool(reader["VideoEnable"]));

                        theCrit.CriterionDescription.Add(lang,
                                                        ValueOperations.safeToString(ValueOperations.DBToString(reader["CriterionDescription"])).Trim());
                    }
                    catch (Exception lex) { }
                }

                reader.Close();

                if (!theCrit.Subcriteria.IsEmpty())
                {
                    foreach (EvaluationCriterion_Multilanguage subCrit in theCrit.Subcriteria)
                    {
                        reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId), createParameter("@CriterionCode", subCrit.CriterionCode));
                        while (reader.Read())
                        {
                            try
                            {
                                var lang = ValueOperations.safeToString(ValueOperations.DBToString(reader["LanguageCode"])).Trim();
                                subCrit.VideoPaths.Add(lang, ValueOperations.DBToString(reader["VideoPath"]));
                                subCrit.VideoEnables.Add(lang, ValueOperations.DBToBool(reader["VideoEnable"]));

                                subCrit.CriterionDescription.Add(lang,
                                                                ValueOperations.safeToString(ValueOperations.DBToString(reader["CriterionDescription"])).Trim());
                            }
                            catch (Exception lex) { }
                        }

                        reader.Close();
                    }
                }
            }

            return query;
        }

        public List<DateTime> getEvaluationDateHistory()
        {
            List<DateTime> ret = new List<DateTime>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "SELECT DISTINCT EvaluationDate FROM EvaluationsReceived ORDER BY EvaluationDate";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToDate(reader["EvaluationDate"]));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<DateTime> getEvaluationDateHistory_Partial(DateTime dateFrom, DateTime dateTo)
        {
            List<DateTime> ret = new List<DateTime>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "SELECT DISTINCT EvaluationDate FROM EvaluationsReceived WHERE EvaluationDate BETWEEN @DateFrom AND @DateTo ORDER BY EvaluationDate";
                SqlDataReader reader = ExecuteReader(query, createParameter("@DateFrom", dateFrom), createParameter("@DateTo", dateTo));
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToDate(reader["EvaluationDate"]));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public EvaluationDetails getEvaluationDetails()
        {
            EvaluationDetails ret = new EvaluationDetails();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * FROM EvaluationDetails";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(new EvaluationDetail()
                    {
                        AssessorCode = ValueOperations.DBToString(reader["AssessorCode"]),
                        EvaluationTermCode = ValueOperations.DBToString(reader["EvaluationTermCode"]),
                        UserCode = ValueOperations.DBToString(reader["UserCode"]),
                        //                        AssessorSurname = ValueOperations.DBToString(reader["AssessorSurname"]),
                        EvaluationDate = ValueOperations.DBToDate(reader["EvaluationDate"]),
                        EvaluationTermDescription = ValueOperations.DBToString(reader["EvaluationTermDescription"]),
                        //                        AssessorName = ValueOperations.DBToString(reader["AssessorName"]),
                        //                        UserFullName = ValueOperations.DBToString(reader["UserFullName"]),
                        Grade = ValueOperations.DBToDecimal(reader["Grade"])
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public EvaluationRulesConfiguration getEvaluationRulesConfiguration()
        {
            EvaluationRulesConfiguration ret = new EvaluationRulesConfiguration();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from EvaluationRulesConfiguration";
                SqlDataReader reader = ExecuteReader(query);
                if (reader.Read())
                {
                    ret.minimumAssignableRate = ValueOperations.DBToDouble(reader["MinimumAssignableRate"]);
                    ret.maximumAssignableRate = ValueOperations.DBToDouble(reader["MaximumAssignableRate"]);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public EvaluationStatus getEvaluationStatus(int evaluationId)
        {
            EvaluationStatus ret = EvaluationStatus.Imminent;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select EvaluationStatus from Evaluations where EvaluationStatus IS NOT NULL AND EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = Enums.GetEnumFromName<EvaluationStatus>(reader["EvaluationStatus"].ToString());
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public EvaluationTerms getEvaluationTerms(int evaluationId, string evalueeId, string assessorId, EvaluationCriteria_Multilanguage allCriteria)
        {
            EvaluationTerms ret = new EvaluationTerms();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                EvaluationCriteria_Multilanguage theCriteria = allCriteria;

                string query = @"select et.*, ec.CriterionWeight from EvaluationTerms et, EvaluationCriteria ec 
                                where et.TermCode=ec.CriterionCode AND ec.EvaluationId=et.EvaluationId 
                                AND et.EvaluationId=@EvaluationId AND
                                EvalueeId=@EvalueeId ";

                if (!string.IsNullOrEmpty(assessorId))
                    query += " AND AssessorId=@AssessorId ";

                query += " ORDER BY ParentTermCode, TermCode";

                ICollection<SqlParameter> pars = new List<SqlParameter>{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@EvalueeId",evalueeId),
                    };

                if (!string.IsNullOrEmpty(assessorId))
                    pars.Add(createParameter("@AssessorId", assessorId));

                SqlDataReader reader = ExecuteReader(query, pars.ToArray());
                while (reader.Read())
                {
                    EvaluationTerm eterm = new EvaluationTerm()
                    {
                        TermId = ValueOperations.DBToInteger(reader["TermId"]),
                        TermCode = ValueOperations.DBToString(reader["TermCode"]).Trim(),
                        TermDescription = ValueOperations.safeToString(ValueOperations.DBToString(reader["TermDescription"])).Trim(),
                        AssessorId = ValueOperations.DBToString(reader["AssessorId"]),
                        EvaluationId = ValueOperations.DBToInteger(reader["EvaluationId"]),
                        EvalueeId = ValueOperations.DBToString(reader["EvalueeId"]),
                        Grade = ValueOperations.DBToDecimal(reader["Grade"]),
                        LastModified = ValueOperations.DBToDate(reader["LastModified"]),
                        CriterionWeight = ValueOperations.DBToDouble(reader["CriterionWeight"])
                    };

                    string parentCode = ValueOperations.DBToString(reader["ParentTermCode"]);
                    if (string.IsNullOrEmpty(parentCode))
                        ret.Add(eterm);
                    else
                    {
                        if (ret[parentCode] == null)
                        {
                            string ppCode;
                            int weight = 0;
                            EvaluationCriterion_Multilanguage theCrit = theCriteria.findByCode(parentCode, out ppCode);
                            if (theCrit != null)
                                weight = theCrit.CriterionWeight;
                            ret.Add(new EvaluationTerm() { TermCode = parentCode, CriterionWeight = weight });
                        }
                        ret[parentCode].SubTerms.Add(eterm);
                    }
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public EvaluationType1Notes getEvaluationType1Notes(int evaluationId, string userId)
        {
            EvaluationType1Notes ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from EvaluationType1Notes where EvaluationId=@EvaluationId AND UserId=@UserId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@UserId",userId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = new EvaluationType1Notes()
                    {
                        evaluationId = ValueOperations.DBToInteger(reader["EvaluationId"]),
                        userId = ValueOperations.DBToString(reader["UserId"]),
                        note = ValueOperations.DBToString(reader["Note"])
                    };
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<EmployeeFilteringData> getEvalueeFilteringData(int evaluationId)
        {
            List<EmployeeFilteringData> ret = new List<EmployeeFilteringData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT        dbo.Employees.EmployeeNumber, dbo.Employees.Name, dbo.Employees.Surname, dbo.Employees.Workplace, dbo.Employees.DepartmentCode, dbo.Employees.DepartmentDescription, 
                                                    dbo.Employees.IsDirector,dbo.Employees.IsHeadOfDepartment, dbo.Employees.WorkplaceAlgorithm, dbo.Employees.ExternalEmployee, dbo.Employees.Commuter, dbo.Employees.ExtensionNotes, GradeStats.AverageGrade, 
                                                     GradeStats.NumberOfAssessors, GradeStats.MostRecentGrade, LastReminders.NumReminder, dbo.Employees.CurrentJob, dbo.EmployeeRoles.RoleType, EvalPromotions.EmployeeType, EvalPromotions.PromotionStatus, 
                                                     ManualPairings.AutoOrManual, dbo.Employees.ContractType, dbo.Employees.Company, dbo.Employees.HiringDate, dbo.Employees.TerminationDate, dbo.Employees.OpenResearch, EvalPromotions.CanBePromoted,dbo.evaluationsSummary.yearAverage
                                    FROM            dbo.Employees LEFT JOIN dbo.evaluationsSummary on dbo.evaluationsSummary.EmployeeNumber=dbo.Employees.EmployeeNumber
                                                                
                                                               INNER JOIN
                                                                 (SELECT        em.EmployeeNumber
                                                                   FROM            dbo.EvaluationPromotions AS ep INNER JOIN
                                                                                             dbo.Employees AS em ON ep.EmployeeId = em.EmployeeNumber
                                                                   WHERE        ep.EvaluationId = @EvaluationId) AS DesignatedEvaluees ON 
                                                             dbo.Employees.EmployeeNumber = DesignatedEvaluees.EmployeeNumber LEFT OUTER JOIN
                                                                 (SELECT        dbo.EmployeeContracts.EmployeeId, dbo.EmployeeContracts.ContractType, dbo.EmployeeContracts.Company, dbo.EmployeeContracts.ContractStartDate, dbo.EmployeeContracts.ContractEndDate, 
                                                                                             dbo.EmployeeContracts.Extensions, dbo.EmployeeContracts.ExtensionNotes
                                                                   FROM            dbo.EmployeeContracts INNER JOIN
                                                                                                 (SELECT        EmployeeId, MAX(ContractStartDate) AS maxDate
                                                                                                   FROM            dbo.EmployeeContracts AS EmployeeContracts_1
                                                                                                   GROUP BY EmployeeId) AS lastContracts ON dbo.EmployeeContracts.EmployeeId = lastContracts.EmployeeId AND dbo.EmployeeContracts.ContractStartDate = lastContracts.maxDate) AS CurrentContracts ON 
                                                             dbo.Employees.EmployeeNumber = CurrentContracts.EmployeeId LEFT OUTER JOIN
                                                                 (SELECT DISTINCT AutoOrManual, EvalueeId
                                                                   FROM            dbo.EvaluationPairings
                                                                   WHERE        (EvaluationId = @EvaluationId) AND (AutoOrManual = 'Manual')) AS ManualPairings ON dbo.Employees.EmployeeNumber = ManualPairings.EvalueeId LEFT OUTER JOIN
                                                                 (SELECT        PromotionId, EvaluationId, EmployeeId, EmployeeType, CanBePromoted, PromotionStatus
                                                                   FROM            dbo.EvaluationPromotions
                                                                   WHERE        EvaluationId = @EvaluationId) AS EvalPromotions ON dbo.Employees.EmployeeNumber = EvalPromotions.EmployeeId LEFT OUTER JOIN
                                                             dbo.EmployeeRoles ON dbo.Employees.EmployeeNumber = dbo.EmployeeRoles.EmployeeId LEFT OUTER JOIN
                                                                 (SELECT        EvalueeId, COUNT(ReminderId) AS NumReminder
                                                                   FROM            dbo.Reminders AS Reminders_1
                                                                   WHERE        (EvaluationId = @EvaluationId)
                                                                   GROUP BY EvalueeId) AS LastReminders ON dbo.Employees.EmployeeNumber = LastReminders.EvalueeId LEFT OUTER JOIN
                                                                 (SELECT        EvalueeId, AVG(Grade) AS AverageGrade, COUNT(DISTINCT AssessorId) AS NumberOfAssessors, MAX(LastModified) AS MostRecentGrade
                                                                   FROM            dbo.EvaluationTerms AS EvaluationTerms_1
                                                                   WHERE        (Grade > 0) AND (EvalueeId IS NOT NULL) AND (EvaluationId = @EvaluationId)
                                                                   GROUP BY EvalueeId) AS GradeStats ON dbo.Employees.EmployeeNumber = GradeStats.EvalueeId";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                {
                    string employeeType = ValueOperations.DBToString(reader["EmployeeType"]);
                    string promotionStatus = ValueOperations.DBToString(reader["PromotionStatus"]);
                    bool canBePromoted = ValueOperations.DBToBool(reader["CanBePromoted"]);

                    ret.Add(new EmployeeFilteringData()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                        workplace = ValueOperations.DBToString(reader["Workplace"]),
                        departmentCode = ValueOperations.DBToString(reader["DepartmentCode"]),
                        departmentDescription = ValueOperations.DBToString(reader["DepartmentDescription"]),
                        averageGrade = ValueOperations.DBToDecimal(reader["AverageGrade"]),
                        gradesDone = ValueOperations.DBToInteger(reader["NumberOfAssessors"]),
                        mostRecentGrade = ValueOperations.DBToDate(reader["MostRecentGrade"]),
                        reminders = ValueOperations.DBToInteger(reader["NumReminder"]),
                        company = ValueOperations.DBToString(reader["Company"]),
                        contractEndDate = normalizeAndDecrypt(reader["TerminationDate"]),
                        contractExpiresInDays = (int)(normalizeAndDecrypt(reader["TerminationDate"]) - DateTime.Now).TotalDays,
                        contractStartDate = normalizeAndDecrypt(reader["HiringDate"]),
                        contractType = ValueOperations.DBToString(reader["ContractType"]),
                        currentJob = optionalDecryption(ValueOperations.DBToString(reader["CurrentJob"])),
                        hasManualPairing = "Manual".Equals(ValueOperations.DBToString(reader["AutoOrManual"])),
                        isProposedAssessor = ("Evaluee".Equals(employeeType) && canBePromoted) || ("Assessor".Equals(employeeType) && !"Accepted".Equals(promotionStatus)),
                        role = ValueOperations.DBToBool(reader["IsDirector"]) ? "Director" : (ValueOperations.DBToBool(reader["IsHeadOfDepartment"]) ? "HeadOfDepartment" : "User"),
                        externalEmployee = ValueOperations.DBToBool(reader["ExternalEmployee"]),
                        commuter = ValueOperations.DBToBool(reader["Commuter"]),
                        extensionNotes = ValueOperations.DBToString(reader["ExtensionNotes"]),
                        workplaceAlgorithm = ValueOperations.DBToInteger(reader["WorkplaceAlgorithm"]),
                        OpenResearch = ValueOperations.DBToInteger(reader["OpenResearch"]),
                        YearAverage = ValueOperations.DBToDecimal(reader["yearAverage"])

                    });
                }

                reader.Close();
                var downGraded = GetDowngradedAssessors(evaluationId);
                ret.ForEach(e =>
                {
                    bool isDowngraded = false;
                    if (downGraded.Any() && downGraded.Contains(e.employeeId))
                    {
                        isDowngraded = true;
                    }
                    e.isDowngraded = isDowngraded;

                });


                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret = ret.OrderBy(x => x.surname).ToList();
            return ret;
        }

        public bool GetAcesses(int evaluationId, string employeeNumber)
        {
            bool ret = false;
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"Select * from EmployeeAccesses where evaluationId=@evaluationId and EmployeeNumber=@employeeNumber";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@employeeNumber",employeeNumber),

                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.HasRows)
                {
                    ret = true;
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }


            return ret;
        }


        public List<EmployeeFilteringData> getEvalueeFilteringDataSimplified(int evaluationId, List<int> accessibleWorkplacesAlgorithmList = null)
        {
            List<EmployeeFilteringData> ret = new List<EmployeeFilteringData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT        dbo.Employees.EmployeeNumber, dbo.Employees.Name, dbo.Employees.Surname
                                FROM dbo.Employees INNER JOIN
                                      (SELECT        em.EmployeeNumber
                                        FROM            dbo.EvaluationPromotions AS ep INNER JOIN
                                        dbo.Employees AS em ON ep.EmployeeId = em.EmployeeNumber
                                        WHERE        ep.EvaluationId = @EvaluationId AND em.EmployeeState='Attivo') AS DesignatedEvaluees
                                ON dbo.Employees.EmployeeNumber = DesignatedEvaluees.EmployeeNumber 
                               LEFT OUTER JOIN (SELECT DISTINCT AutoOrManual, EvalueeId
                                                                   FROM            dbo.EvaluationPairings
                                                                   WHERE        (EvaluationId = @EvaluationId) AND (AutoOrManual = 'Manual')) AS ManualPairings ON dbo.Employees.EmployeeNumber = ManualPairings.EvalueeId
                               ";

                /*** Filtro per i Workplaces relativi ai privilegi dell'utente ************/
                if (accessibleWorkplacesAlgorithmList != null && accessibleWorkplacesAlgorithmList.Count > 0)
                {
                    query = query + " WHERE dbo.Employees.WorkplaceAlgorithm IN (";
                    foreach (int k in accessibleWorkplacesAlgorithmList)
                        query = query + k + ",";

                    query = query.TrimEnd(',') + ")";
                }
                /**************************************************************************/

                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                {
                    ret.Add(new EmployeeFilteringData()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"]))

                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret = ret.OrderBy(x => x.surname).ToList();

            return ret;
        }


        public List<EmployeeFilteringData> getEvalueeFilteringData_old(int evaluationId)
        {
            List<EmployeeFilteringData> ret = new List<EmployeeFilteringData>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT        dbo.Employees.EmployeeNumber, dbo.Employees.Name, dbo.Employees.Surname, dbo.Employees.Workplace, dbo.Employees.DepartmentCode, dbo.Employees.DepartmentDescription, 
                                                    dbo.Employees.IsDirector,dbo.Employees.IsHeadOfDepartment, dbo.Employees.WorkplaceAlgorithm, dbo.Employees.ExternalEmployee, dbo.Employees.Commuter, dbo.Employees.ExtensionNotes, GradeStats.AverageGrade, 
                                                     GradeStats.NumberOfAssessors, GradeStats.MostRecentGrade, LastReminders.NumReminder, dbo.Employees.CurrentJob, dbo.EmployeeRoles.RoleType, EvalPromotions.EmployeeType, EvalPromotions.PromotionStatus, 
                                                     ManualPairings.AutoOrManual, CurrentContracts.ContractType, CurrentContracts.Company, CurrentContracts.ContractStartDate, CurrentContracts.ContractEndDate, EvalPromotions.CanBePromoted
                                    FROM            dbo.Employees INNER JOIN
                                                                 (SELECT        em.EmployeeNumber
                                                                   FROM            dbo.EvaluationPromotions AS ep INNER JOIN
                                                                                             dbo.Employees AS em ON ep.EmployeeId = em.EmployeeNumber
                                                                   WHERE        ep.EvaluationId = @EvaluationId) AS DesignatedEvaluees ON 
                                                             dbo.Employees.EmployeeNumber = DesignatedEvaluees.EmployeeNumber LEFT OUTER JOIN
                                                                 (SELECT        dbo.EmployeeContracts.EmployeeId, dbo.EmployeeContracts.ContractType, dbo.EmployeeContracts.Company, dbo.EmployeeContracts.ContractStartDate, dbo.EmployeeContracts.ContractEndDate, 
                                                                                             dbo.EmployeeContracts.Extensions, dbo.EmployeeContracts.ExtensionNotes
                                                                   FROM            dbo.EmployeeContracts INNER JOIN
                                                                                                 (SELECT        EmployeeId, MAX(ContractStartDate) AS maxDate
                                                                                                   FROM            dbo.EmployeeContracts AS EmployeeContracts_1
                                                                                                   GROUP BY EmployeeId) AS lastContracts ON dbo.EmployeeContracts.EmployeeId = lastContracts.EmployeeId AND dbo.EmployeeContracts.ContractStartDate = lastContracts.maxDate) AS CurrentContracts ON 
                                                             dbo.Employees.EmployeeNumber = CurrentContracts.EmployeeId LEFT OUTER JOIN
                                                                 (SELECT DISTINCT AutoOrManual, EvalueeId
                                                                   FROM            dbo.EvaluationPairings
                                                                   WHERE        (EvaluationId = @EvaluationId) AND (AutoOrManual = 'Manual')) AS ManualPairings ON dbo.Employees.EmployeeNumber = ManualPairings.EvalueeId LEFT OUTER JOIN
                                                                 (SELECT        PromotionId, EvaluationId, EmployeeId, EmployeeType, CanBePromoted, PromotionStatus
                                                                   FROM            dbo.EvaluationPromotions
                                                                   WHERE        EvaluationId = @EvaluationId) AS EvalPromotions ON dbo.Employees.EmployeeNumber = EvalPromotions.EmployeeId LEFT OUTER JOIN
                                                             dbo.EmployeeRoles ON dbo.Employees.EmployeeNumber = dbo.EmployeeRoles.EmployeeId LEFT OUTER JOIN
                                                                 (SELECT        EvalueeId, COUNT(ReminderId) AS NumReminder
                                                                   FROM            dbo.Reminders AS Reminders_1
                                                                   WHERE        (EvaluationId = @EvaluationId)
                                                                   GROUP BY EvalueeId) AS LastReminders ON dbo.Employees.EmployeeNumber = LastReminders.EvalueeId LEFT OUTER JOIN
                                                                 (SELECT        EvalueeId, AVG(Grade) AS AverageGrade, COUNT(DISTINCT AssessorId) AS NumberOfAssessors, MAX(LastModified) AS MostRecentGrade
                                                                   FROM            dbo.EvaluationTerms AS EvaluationTerms_1
                                                                   WHERE        (Grade > 0) AND (EvalueeId IS NOT NULL) AND (EvaluationId = @EvaluationId)
                                                                   GROUP BY EvalueeId) AS GradeStats ON dbo.Employees.EmployeeNumber = GradeStats.EvalueeId";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                {
                    string employeeType = ValueOperations.DBToString(reader["EmployeeType"]);
                    string promotionStatus = ValueOperations.DBToString(reader["PromotionStatus"]);
                    bool canBePromoted = ValueOperations.DBToBool(reader["CanBePromoted"]);

                    ret.Add(new EmployeeFilteringData()
                    {
                        employeeId = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                        surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                        workplace = ValueOperations.DBToString(reader["Workplace"]),
                        departmentCode = ValueOperations.DBToString(reader["DepartmentCode"]),
                        departmentDescription = ValueOperations.DBToString(reader["DepartmentDescription"]),
                        averageGrade = ValueOperations.DBToDecimal(reader["AverageGrade"]),
                        gradesDone = ValueOperations.DBToInteger(reader["NumberOfAssessors"]),
                        mostRecentGrade = ValueOperations.DBToDate(reader["MostRecentGrade"]),
                        reminders = ValueOperations.DBToInteger(reader["NumReminder"]),
                        company = ValueOperations.DBToString(reader["Company"]),
                        contractEndDate = ValueOperations.DBToDate(reader["ContractEndDate"]),
                        contractExpiresInDays = (int)(ValueOperations.DBToDate(reader["ContractEndDate"]) - DateTime.Now).TotalDays,
                        contractStartDate = ValueOperations.DBToDate(reader["ContractStartDate"]),
                        contractType = ValueOperations.DBToString(reader["ContractType"]),
                        currentJob = optionalDecryption(ValueOperations.DBToString(reader["CurrentJob"])),
                        hasManualPairing = "Manual".Equals(ValueOperations.DBToString(reader["AutoOrManual"])),
                        isProposedAssessor = ("Evaluee".Equals(employeeType) && canBePromoted) || ("Assessor".Equals(employeeType) && !"Accepted".Equals(promotionStatus)),
                        role = ValueOperations.DBToBool(reader["IsDirector"]) ? "Director" : (ValueOperations.DBToBool(reader["IsHeadOfDepartment"]) ? "HeadOfDepartment" : "User"),
                        externalEmployee = ValueOperations.DBToBool(reader["ExternalEmployee"]),
                        commuter = ValueOperations.DBToBool(reader["Commuter"]),
                        extensionNotes = ValueOperations.DBToString(reader["ExtensionNotes"]),
                        workplaceAlgorithm = ValueOperations.DBToInteger(reader["WorkplaceAlgorithm"])
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            ret = ret.OrderBy(x => x.surname).ToList();

            return ret;
        }

        public EvaluationsReceived getEvalueeHistory(string evalueeId)
        {
            EvaluationsReceived ret = new EvaluationsReceived();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT er.*, ev.EndDate FROM EvaluationsReceived er, Evaluations ev 
                                    WHERE er.EvaluationId IS NOT NULL AND ev.EvaluationStatus is not null 
                                    AND ev.EvaluationId=er.EvaluationId AND (er.EmployeeNumber = @EmployeeNumber) 
                                    AND er.Result>0 
                                    ORDER BY EndDate";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EmployeeNumber", evalueeId));
                while (reader.Read())
                {
                    ret.Add(new EvaluationReceived()
                    {
                        ContractEndDate = ValueOperations.DBToNormalizedDate(reader["ContractEndDate"]),
                        ContractStartDate = ValueOperations.DBToNormalizedDate(reader["ContractStartDate"]),
                        EvaluationDate = ValueOperations.DBToNormalizedDate(reader["EndDate"]),
                        ContractDuration = ValueOperations.DBToInteger(reader["ContractDuration"]),
                        EvaluationNotes = ValueOperations.DBToString(reader["EvaluationNotes"]),
                        EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        NumberOfAssessors = ValueOperations.DBToInteger(reader["NumberOfAssessors"]),
                        NextEvaluation = ValueOperations.DBToNormalizedDate(reader["NextEvaluation"]),
                        Result = ValueOperations.DBToDecimal(reader["Result"]),
                        ContractType = ValueOperations.DBToString(reader["ContractType"]),
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        //public List<EvaluationPairing> getEvalueePairings(int evaluationId, string evalueeId, bool getSelfEvaluations)
        //{
        //	List<EvaluationPairing> ret = new List<EvaluationPairing>();

        //	bool wasOpen = false;
        //	if (connection.State == System.Data.ConnectionState.Open)
        //		wasOpen = true;
        //	else
        //		connection.Open();

        //	try
        //	{
        //		string query = @"SELECT        dbo.EvaluationPairings.*, RR.RemindersAlreadySent
        //                              FROM            dbo.EvaluationPairings LEFT OUTER JOIN
        //                                                           (SELECT        AssessorId, COUNT(*) AS RemindersAlreadySent
        //                                                             FROM            dbo.Reminders
        //                                                             WHERE        (EvaluationId = @EvaluationId)
        //                                                             GROUP BY AssessorId) AS RR ON dbo.EvaluationPairings.AssessorId = RR.AssessorId
        //                              WHERE        (dbo.EvaluationPairings.EvaluationId = @EvaluationId) AND 
        //                              dbo.EvaluationPairings.EvalueeId=@EvalueeId AND dbo.EvaluationPairings.IsSelfEvaluation=@IsSelfEvaluation";
        //		SqlParameter[] pars = new SqlParameter[]{
        //				createParameter("@EvaluationId",evaluationId),
        //				createParameter("@EvalueeId",evalueeId),
        //				createParameter("@IsSelfEvaluation",getSelfEvaluations)
        //			};
        //		SqlDataReader reader = ExecuteReader(query, pars);
        //		while (reader.Read())
        //		{
        //                  EvaluationPairing ep = toEvaluationPairing(reader);
        //                  ep.RemindersSent = ValueOperations.DBToInteger(reader["RemindersAlreadySent"]);
        //			ret.Add(ep);
        //		}

        //		reader.Close();
        //	}
        //	finally
        //	{
        //		if (!wasOpen)
        //			connection.Close();
        //	}

        //	return ret;
        //}

        public List<EvaluationPairing> getEvalueePairings(int evaluationId, string evalueeId, bool getSelfEvaluations)
        {
            List<EvaluationPairing> ret = new List<EvaluationPairing>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT dbo.EvaluationPairings.*
                                 FROM   dbo.EvaluationPairings   WHERE dbo.EvaluationPairings.EvaluationId = @EvaluationId AND 
                                        dbo.EvaluationPairings.EvalueeId=@EvalueeId AND dbo.EvaluationPairings.IsSelfEvaluation=@IsSelfEvaluation";

                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@EvalueeId",evalueeId),
                        createParameter("@IsSelfEvaluation",getSelfEvaluations)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    EvaluationPairing ep = toEvaluationPairing(reader);
                    ret.Add(ep);
                }

                reader.Close();
                if (!ValueOperations.isNullOrEmpty(ret))
                {
                    foreach (EvaluationPairing ep in ret)
                    {
                        query = @"select COUNT(*) from Reminders WHERE EvaluationId=@EvaluationId AND 
                                AssessorId=@AssessorId";
                        pars = new SqlParameter[]{
                            createParameter("@EvaluationId",evaluationId),
                            createParameter("@AssessorId",ep.AssessorId)
                        };
                        string res = ExecuteScalar(query, pars);
                        int resCount = 0;
                        if (int.TryParse(res, out resCount))
                            ep.RemindersSent = resCount;
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getAssessmentRequestList(int evaluationId)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT DISTINCT ep.AssessorId
                                FROM   dbo.EvaluationPairings ep 
                                    inner join Employees emp on ep.AssessorId=emp.EmployeeNumber 
                                WHERE  ep.EvaluationId=@EvaluationId and emp.EmployeeState<>'Terminato' and not exists 
	                                    (select AssessorId from Reminders 
                                        where TemplateId='AssessmentRequest' and EvaluationId=@EvaluationId 
			                                    and Reminders.AssessorId=ep.AssessorId)";

                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToString(reader["AssessorId"]));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }



        public List<EvaluationPairing> getManualEvalueePairingsHistory(string evalueeId, int currentEvaluationId)
        {
            List<EvaluationPairing> ret = new List<EvaluationPairing>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                /*
                 * Passo 1: seleziona l'ultima valutazione in cui era presente l'evaluee
                 */
                int lastEvaluation = -1;
                string query = @"select top 1 EvaluationId from EvaluationPromotions 
                                where EmployeeId = @EvalueeId AND EvaluationId<=@EvaluationId 
                                order by evaluationId desc";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvalueeId",evalueeId),
                    createParameter("@EvaluationId",currentEvaluationId)
                };

                SqlDataReader reader1 = ExecuteReader(query, pars);
                if (reader1.Read())
                {
                    lastEvaluation = ValueOperations.DBToInteger(reader1["EvaluationId"]);
                }
                reader1.Close();


                /*
                 * Passo 2: seleziona i valutatori di quel valutato nella valutazione di cui 
                 * al passo 1 che hanno legami impostati manualmente
                 */
                query = @"SELECT DISTINCT ep.AssessorId
                                FROM   dbo.EvaluationPairings ep 
                                    inner join Employees emp on ep.AssessorId=emp.EmployeeNumber 
                                WHERE  ep.AutoOrManual IN ('Manual','man') AND ep.EvaluationId=@EvaluationId AND
                                    ep.EvalueeId=@EvalueeId and emp.EmployeeState<>'Terminato'";

                pars = new SqlParameter[]{
                    createParameter("@EvalueeId",evalueeId),
                    createParameter("@EvaluationId",lastEvaluation)
                };

                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    EvaluationPairing ep = new EvaluationPairing()
                    {
                        AssessorId = ValueOperations.DBToString(reader["AssessorId"]),
                        EvalueeId = evalueeId,
                        AutoOrManual = "Manual",
                    };
                    ret.Add(ep);
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public bool getWasSelfEvaluating(int evaluationId, string evalueeId)
        {
            bool ret = false;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from EvaluationPairings where assessorid=evalueeid and evalueeid=@EvalueeId and evaluationid = 
	                                (select top 1 evaluationid from evaluationpromotions where employeeid=@EvalueeId and evaluationid<@EvaluationId order by evaluationid desc)";

                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvalueeId",evalueeId),
                        createParameter("@EvaluationId",evaluationId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                ret = reader.HasRows;
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<EvaluationPairing> getEvalueePairingsWithoutReminders(int evaluationId, string evalueeId, bool getSelfEvaluations)
        {
            List<EvaluationPairing> ret = new List<EvaluationPairing>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT dbo.EvaluationPairings.*
                                 FROM   dbo.EvaluationPairings   WHERE dbo.EvaluationPairings.EvaluationId = @EvaluationId AND 
                                        dbo.EvaluationPairings.EvalueeId=@EvalueeId AND dbo.EvaluationPairings.IsSelfEvaluation=@IsSelfEvaluation";

                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@EvalueeId",evalueeId),
                        createParameter("@IsSelfEvaluation",getSelfEvaluations)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    EvaluationPairing ep = toEvaluationPairing(reader);
                    ret.Add(ep);
                }

                reader.Close();

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public Reminders getEvalueeReminders(int evaluationId, string employeeId, bool includeSentReminders = false)
        {
            Reminders ret = new Reminders();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from Reminders WHERE EvaluationId=@EvaluationId AND EvalueeId=@EvalueeId";
                if (!includeSentReminders)
                    query += @" AND Status='READY'";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@EvalueeId",employeeId)
                };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(new Reminder()
                    {
                        assessorId = reader["AssessorId"].ToString(),
                        evaluationId = ValueOperations.DBToInteger(reader["EvaluationId"].ToString()),
                        evalueeId = reader["EvalueeId"].ToString(),
                        reminderDate = ValueOperations.DBToDate(reader["ReminderDate"]),
                        reminderId = ValueOperations.DBToInteger(reader["ReminderId"].ToString())
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getEvalueesFromDetails(DateTime endDate)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select DISTINCT UserCode FROM EvaluationDetails WHERE EvaluationDate=@EvaluationDate AND Grade>0";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationDate", endDate));
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToString(reader["UserCode"]));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getEvalueesFromHistory(DateTime startDate, DateTime endDate)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select EmployeeNumber FROM EvaluationsReceived WHERE NextEvaluation>= @StartDate and NextEvaluation<= @EndDate";
                SqlDataReader reader = ExecuteReader(query, createParameter("@StartDate", startDate), createParameter("@EndDate", endDate));
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToString(reader["EmployeeNumber"]));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public EvaluationsReceived getEvalueeStaffDateHistory(string evalueeId)
        {
            EvaluationsReceived ret = new EvaluationsReceived();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"SELECT er.*, ev.StaffDate FROM EvaluationsReceived er, Evaluations ev 
                                    WHERE er.EvaluationId IS NOT NULL AND ev.EvaluationStatus is not null 
                                    AND ev.EvaluationId=er.EvaluationId AND (er.EmployeeNumber = @EmployeeNumber) 
                                    AND er.Result>0 
                                    ORDER BY StaffDate";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EmployeeNumber", evalueeId));
                while (reader.Read())
                {
                    ret.Add(new EvaluationReceived()
                    {
                        ContractEndDate = ValueOperations.DBToNormalizedDate(reader["ContractEndDate"]),
                        ContractStartDate = ValueOperations.DBToNormalizedDate(reader["ContractStartDate"]),
                        EvaluationDate = ValueOperations.DBToNormalizedDate(reader["StaffDate"]),
                        ContractDuration = ValueOperations.DBToInteger(reader["ContractDuration"]),
                        EvaluationNotes = ValueOperations.DBToString(reader["EvaluationNotes"]),
                        EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        NumberOfAssessors = ValueOperations.DBToInteger(reader["NumberOfAssessors"]),
                        NextEvaluation = ValueOperations.DBToNormalizedDate(reader["NextEvaluation"]),
                        Result = ValueOperations.DBToDecimal(reader["Result"]),
                        ContractType = ValueOperations.DBToString(reader["ContractType"]),
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public FilterModel getFilters(string userId, string filterClass)
        {
            FilterModel ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * FROM Filters WHERE (FilterClass = @FilterClass) AND UserId=@UserId";
                SqlDataReader reader = ExecuteReader(query, createParameter("@FilterClass", filterClass),
                                                            createParameter("@UserId", userId));
                while (reader.Read())
                {
                    ret = new FilterModel()
                    {
                        FilterName = ValueOperations.DBToString(reader["FilterName"]),
                        IsAllEvaluation = ValueOperations.DBToBool(reader["IsAllEvaluation"]),
                        IsAssessorHidden = ValueOperations.DBToBool(reader["IsAssessorHidden"]),
                        IsAverageEvaluation = ValueOperations.DBToBool(reader["IsAverageEvaluation"]),
                        IsDepartment = ValueOperations.DBToBool(reader["IsDepartment"]),
                        IsEvaluated = ValueOperations.DBToBool(reader["IsEvaluated"]),
                        IsEvaluationDate = ValueOperations.DBToBool(reader["IsEvaluationDate"]),
                        IsEvaluationReceived = ValueOperations.DBToBool(reader["IsEvaluationReceived"]),
                        IsEvaluationReminded = ValueOperations.DBToBool(reader["IsEvaluationReminded"]),
                        IsEvaluationUnexpressed = ValueOperations.DBToBool(reader["IsEvaluationUnexpressed"]),
                        IsMaximumExcluded = ValueOperations.DBToBool(reader["IsMaximumExcluded"]),
                        IsMinimumExcluded = ValueOperations.DBToBool(reader["IsMinimumExcluded"]),
                        IsNames = ValueOperations.DBToBool(reader["IsNames"]),
                        IsNumberEvaluationReceived = ValueOperations.DBToBool(reader["IsNumberEvaluationReceived"]),
                        IsOnlySuggestedAssessors = ValueOperations.DBToBool(reader["IsOnlySuggestedAssessors"]),
                        IsSite = ValueOperations.DBToBool(reader["IsSite"]),
                        IsStaffMemberHidden = ValueOperations.DBToBool(reader["IsStaffMemberHidden"]),
                        MaximumAverageEvaluationNumber = ValueOperations.DBToDouble(reader["MaximumAverageEvaluationNumber"]),
                        MinimumAverageEvaluationNumber = ValueOperations.DBToDouble(reader["MinimumAverageEvaluationNumber"]),
                        MinimumEvaluationNumber = ValueOperations.DBToInteger(reader["MinimumEvaluationNumber"]),
                        MaximumEvaluationNumber = ValueOperations.DBToInteger(reader["MaximumEvaluationNumber"]),
                        NumberBeforeEvaluationDate = ValueOperations.DBToInteger(reader["NumberBeforeEvaluationDate"]),
                        FilterId = ValueOperations.DBToInteger(reader["FilterId"]),
                        Commuter = ValueOperations.DBToBool(reader["Commuter"]),
                        ExternalEmployee = ValueOperations.DBToBool(reader["ExternalEmployee"]),
                        IsCommuter = ValueOperations.DBToBool(reader["IsCommuter"]),
                        IsContractType = ValueOperations.DBToBool(reader["IsContractType"]),
                        IsExternalEmployee = ValueOperations.DBToBool(reader["IsExternalEmployee"]),
                        IsWorkplaceAlgorithm = ValueOperations.DBToBool(reader["IsWorkplaceAlgorithm"]),
                        IsExcludeCompiledEvaluations = ValueOperations.DBToBool(reader["IsExcludeCompiledEvaluations"]),
                        IsOpenResearch = ValueOperations.DBToBool(reader["IsOpenResearch"]),
                        IsColorEnable = ValueOperations.DBToBool(reader["IsColorEnable"]),
                        IsOrange = ValueOperations.DBToBool(reader["IsOrange"]),
                        IsPurple = ValueOperations.DBToBool(reader["IsPurple"]),
                        IsGreen = ValueOperations.DBToBool(reader["IsGreen"]),
                        IsRed = ValueOperations.DBToBool(reader["IsRed"]),
                        IsWhite = ValueOperations.DBToBool(reader["IsWhite"]),
                        DepartmentList = new List<string>(),
                        NamesList = new List<string>(),
                        SitesList = new List<string>(),
                        ContractType = new List<string>(),
                        WorkplaceAlgorithm = new List<int>()
                    };
                }

                reader.Close();

                if (ret != null)
                {
                    query = "select * FROM FilterDepartments WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.FilterId));
                    while (reader.Read())
                        ret.DepartmentList.Add(ValueOperations.DBToString(reader["FilterDepartment"]));

                    reader.Close();

                    query = "select * FROM FilterNames WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.FilterId));
                    while (reader.Read())
                        ret.NamesList.Add(ValueOperations.DBToString(reader["FilterName"]));

                    reader.Close();

                    query = "select * FROM FilterSites WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.FilterId));
                    while (reader.Read())
                        ret.SitesList.Add(ValueOperations.DBToString(reader["FilterSite"]));

                    reader.Close();

                    query = "select * FROM FilterAlgorithms WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.FilterId));
                    while (reader.Read())
                        ret.WorkplaceAlgorithm.Add(ValueOperations.DBToInteger(reader["FilterAlgorithm"]));

                    reader.Close();

                    query = "select * FROM FilterContractTypes WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.FilterId));
                    while (reader.Read())
                        ret.ContractType.Add(ValueOperations.DBToString(reader["FilterContractType"]));

                    reader.Close();
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public CustomFilter getCustomFilter(int customFilterId)
        {
            CustomFilter ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * FROM CustomFilters WHERE FilterId=@CustomFilterId";
                SqlDataReader reader = ExecuteReader(query, createParameter("@CustomFilterId", customFilterId));
                while (reader.Read())
                {
                    ret = new CustomFilter();
                    ret.customFilterHeader.customFilterId = customFilterId;
                    ret.customFilterHeader.customFilterName = ValueOperations.DBToString(reader["FilterName"]);
                    ret.customFilterModel = new FilterModel()
                    {
                        FilterName = ValueOperations.DBToString(reader["FilterName"]),
                        IsAllEvaluation = ValueOperations.DBToBool(reader["IsAllEvaluation"]),
                        IsAssessorHidden = ValueOperations.DBToBool(reader["IsAssessorHidden"]),
                        IsAverageEvaluation = ValueOperations.DBToBool(reader["IsAverageEvaluation"]),
                        IsDepartment = ValueOperations.DBToBool(reader["IsDepartment"]),
                        IsEvaluated = ValueOperations.DBToBool(reader["IsEvaluated"]),
                        IsEvaluationDate = ValueOperations.DBToBool(reader["IsEvaluationDate"]),
                        IsEvaluationReceived = ValueOperations.DBToBool(reader["IsEvaluationReceived"]),
                        IsEvaluationReminded = ValueOperations.DBToBool(reader["IsEvaluationReminded"]),
                        IsEvaluationUnexpressed = ValueOperations.DBToBool(reader["IsEvaluationUnexpressed"]),
                        IsMaximumExcluded = ValueOperations.DBToBool(reader["IsMaximumExcluded"]),
                        IsMinimumExcluded = ValueOperations.DBToBool(reader["IsMinimumExcluded"]),
                        IsNames = ValueOperations.DBToBool(reader["IsNames"]),
                        IsNumberEvaluationReceived = ValueOperations.DBToBool(reader["IsNumberEvaluationReceived"]),
                        IsOnlySuggestedAssessors = ValueOperations.DBToBool(reader["IsOnlySuggestedAssessors"]),
                        IsSite = ValueOperations.DBToBool(reader["IsSite"]),
                        IsStaffMemberHidden = ValueOperations.DBToBool(reader["IsStaffMemberHidden"]),
                        MaximumAverageEvaluationNumber = ValueOperations.DBToDouble(reader["MaximumAverageEvaluationNumber"]),
                        MinimumAverageEvaluationNumber = ValueOperations.DBToDouble(reader["MinimumAverageEvaluationNumber"]),
                        MinimumEvaluationNumber = ValueOperations.DBToInteger(reader["MinimumEvaluationNumber"]),
                        MaximumEvaluationNumber = ValueOperations.DBToInteger(reader["MaximumEvaluationNumber"]),
                        NumberBeforeEvaluationDate = ValueOperations.DBToInteger(reader["NumberBeforeEvaluationDate"]),
                        FilterId = ValueOperations.DBToInteger(reader["FilterId"]),
                        Commuter = ValueOperations.DBToBool(reader["Commuter"]),
                        ExternalEmployee = ValueOperations.DBToBool(reader["ExternalEmployee"]),
                        IsCommuter = ValueOperations.DBToBool(reader["IsCommuter"]),
                        IsContractType = ValueOperations.DBToBool(reader["IsContractType"]),
                        IsExternalEmployee = ValueOperations.DBToBool(reader["IsExternalEmployee"]),
                        IsWorkplaceAlgorithm = ValueOperations.DBToBool(reader["IsWorkplaceAlgorithm"]),
                        IsExcludeCompiledEvaluations = ValueOperations.DBToBool(reader["IsExcludeCompiledEvaluations"]),
                        IsOpenResearch = ValueOperations.DBToBool(reader["IsOpenResearch"]),
                        DepartmentList = new List<string>(),
                        NamesList = new List<string>(),
                        SitesList = new List<string>(),
                        ContractType = new List<string>(),
                        WorkplaceAlgorithm = new List<int>()
                    };
                }

                reader.Close();

                if (ret.customFilterModel != null)
                {
                    query = "select * FROM CustomFilterDepartments WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.customFilterModel.FilterId));
                    while (reader.Read())
                        ret.customFilterModel.DepartmentList.Add(ValueOperations.DBToString(reader["FilterDepartment"]));

                    reader.Close();

                    query = "select * FROM CustomFilterNames WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.customFilterModel.FilterId));
                    while (reader.Read())
                        ret.customFilterModel.NamesList.Add(ValueOperations.DBToString(reader["FilterName"]));

                    reader.Close();

                    query = "select * FROM CustomFilterSites WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.customFilterModel.FilterId));
                    while (reader.Read())
                        ret.customFilterModel.SitesList.Add(ValueOperations.DBToString(reader["FilterSite"]));

                    reader.Close();

                    query = "select * FROM CustomFilterAlgorithms WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.customFilterModel.FilterId));
                    while (reader.Read())
                        ret.customFilterModel.WorkplaceAlgorithm.Add(ValueOperations.DBToInteger(reader["FilterAlgorithm"]));

                    reader.Close();

                    query = "select * FROM CustomFilterContractTypes WHERE (FilterId = @FilterId)";
                    reader = ExecuteReader(query, createParameter("@FilterId", ret.customFilterModel.FilterId));
                    while (reader.Read())
                        ret.customFilterModel.ContractType.Add(ValueOperations.DBToString(reader["FilterContractType"]));

                    reader.Close();
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public CustomFilterHeaders getCustomFilterList(string employeeId)
        {
            CustomFilterHeaders ret = new CustomFilterHeaders();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * FROM CustomFilters WHERE UserId=@UserId";
                SqlDataReader reader = ExecuteReader(query, createParameter("@UserId", employeeId));
                while (reader.Read())
                {
                    ret.Add(new CustomFilterHeader()
                    {
                        customFilterId = ValueOperations.DBToInteger(reader["FilterId"]),
                        customFilterName = ValueOperations.DBToString(reader["FilterName"])
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public List<string> getGradedEvaluees(int evaluationId, string assessorId)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select DISTINCT EvalueeId from EvaluationTerms where EvaluationId=@EvaluationId AND 
                                AssessorId=@AssessorId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@AssessorId", assessorId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToString(reader["EvalueeId"]));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public GradeStatistics getGradeStatistics(int evaluationId, string employeeId)
        {
            GradeStatistics ret = new GradeStatistics();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * FROM GradeStatistics where EvaluationId=@EvaluationId AND 
                                EmployeeId=@EmployeeId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@EmployeeId", employeeId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret = new GradeStatistics()
                    {
                        averageGrade = ValueOperations.DBToDecimal(reader["AverageGrade"]),
                        gradesDone = ValueOperations.DBToInteger(reader["GradesReceived"]),
                        gradesPlanned = ValueOperations.DBToInteger(reader["GradesPlanned"]),
                    };
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public GradeStatistics getAssessorGradeStatistics(int evaluationId, string employeeId)
        {
            GradeStatistics ret = new GradeStatistics();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * FROM AssessorGradeStatistics where EvaluationId=@EvaluationId AND 
                                EmployeeId=@EmployeeId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@EmployeeId", employeeId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret = new GradeStatistics()
                    {
                        averageGrade = ValueOperations.DBToDecimal(reader["AverageGrade"]),
                        gradesDone = ValueOperations.DBToInteger(reader["GradesReceived"]),
                        gradesPlanned = ValueOperations.DBToInteger(reader["GradesPlanned"]),
                    };
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public GridColumns getGridColumns(string gridName, string userId)
        {
            GridColumns ret = new GridColumns();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from GridColumns where GridName=@GridName AND UserId=@UserId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@GridName",gridName),
                        createParameter("@UserId", userId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(new GridColumn()
                    {
                        columnName = ValueOperations.DBToString(reader["ColumnName"]),
                        visible = ValueOperations.DBToBool(reader["Visible"])
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public double getHistoricOverallAverageGrade(string employeeId)
        {
            double ret = 0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select AVG(Result) AS theAverage from EvaluationsReceived where EmployeeNumber=@EmployeeId AND Result>0";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EmployeeId",employeeId)
                    };
                string result = ExecuteScalar(query, pars);
                ret = ValueOperations.DBToDouble(result);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public int getLastEvaluationId(string employeeNumber)
        {
            var ret = default(int);

            var wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select top(1) e.EvaluationId
                    from [dbo].[Evaluations] e
                    inner join [dbo].[EvaluationsReceived] er on e.EvaluationId = er.EvaluationId
                    where er.EmployeeNumber = @empNum and e.StaffDate is not null and er.Result > 0
                    order by e.StaffDate desc";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@empNum",employeeNumber)
                    };
                string res = ExecuteScalar(query, pars);

                if (res != null)
                    ret = int.Parse(res);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public int? getBeforeRequestEvaluationId(string employeeNumber, int evaluationId)
        {
            var ret = default(int?);

            var wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                var sql = @"select top(1) e.EvaluationId
                from [dbo].[Evaluations] e
                inner join[dbo].[EvaluationsReceived] er on e.EvaluationId = er.EvaluationId
                where er.EmployeeNumber = @empNum and e.StaffDate is not null and er.Result > 0 and e.EvaluationId < @evalId
                order by e.StaffDate desc";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@empNum",employeeNumber),
                    createParameter("@evalId", evaluationId)
                };
                string res = ExecuteScalar(sql, pars);

                if (res != null)
                    ret = int.Parse(res);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public EvaluationReceived getLastEvalueeHistoryRecord(string evalueeId, int evaluationId, bool includeSpecifiedEvaluation = false)
        {
            EvaluationReceived ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select TOP (1) * FROM EvaluationsReceived WHERE (EmployeeNumber = @EmployeeNumber) AND Result>0 AND EvaluationId" + (includeSpecifiedEvaluation ? "<=" : "<") + "@EvaluationId order by EvaluationDate DESC";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EmployeeNumber", evalueeId), createParameter("@EvaluationId", evaluationId));
                if (reader.Read())
                {
                    ret = new EvaluationReceived()
                    {
                        ContractEndDate = ValueOperations.DBToDate(reader["ContractEndDate"]),
                        ContractStartDate = ValueOperations.DBToDate(reader["ContractStartDate"]),
                        EvaluationDate = ValueOperations.DBToDate(reader["EvaluationDate"]),
                        ContractDuration = ValueOperations.DBToInteger(reader["ContractDuration"]),
                        EvaluationNotes = ValueOperations.DBToString(reader["EvaluationNotes"]),
                        EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                        NumberOfAssessors = ValueOperations.DBToInteger(reader["NumberOfAssessors"]),
                        NextEvaluation = ValueOperations.DBToDate(reader["NextEvaluation"]),
                        Result = ValueOperations.DBToDecimal(reader["Result"]),
                        ContractType = ValueOperations.DBToString(reader["ContractType"]),
                        EvaluationId = ValueOperations.DBToInteger(reader["EvaluationId"]),
                    };
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public string getNewIdFromSequence(string sequenceName)
        {
            string ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                SqlParameter par1 = new SqlParameter("@SequenceName", sequenceName);
                par1.Direction = System.Data.ParameterDirection.Input;

                SqlParameter par2 = new SqlParameter("@GeneratedId", System.Data.SqlDbType.NVarChar);
                par2.Size = 100;
                par2.Direction = System.Data.ParameterDirection.Output; // This is important!

                ExecuteStoredProcedure("GetNewIdFromSequence", false, par1, par2);

                ret = ValueOperations.DBToString(par2.Value);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
            return ret;
        }

        public DateTime getNextEvaluationDate()
        {
            DateTime ret = DateTime.MinValue;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select MIN(ev.EndDate) AS NextDate from Evaluations ev
                                    WHERE ev.EvaluationStatus IS NOT NULL AND CONVERT(date, ev.EndDate)>=CONVERT(date, GETDATE())";
                SqlDataReader reader = ExecuteReader(query);
                if (reader.Read())
                {
                    ret = ValueOperations.DBToDate(reader["NextDate"]);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public DateTime getNextEvaluationDateForDepartment(string departmentCode)
        {
            DateTime ret = DateTime.MinValue;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select MIN(ev.EndDate) AS NextDate from Evaluations ev, DepartmentsToEvaluate dte 
                                    WHERE ev.EvaluationId=dte.EvaluationId AND dte.DepartmentCode=@DepartmentCode 
                                    AND ev.EndDate>GETDATE() AND ev.EvaluationStatus IS NOT NULL";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@DepartmentCode",departmentCode)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = ValueOperations.DBToDate(reader["NextDate"]);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public DateTime getNextEvaluationStaffDate()
        {
            DateTime ret = DateTime.MinValue;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select MIN(ev.StaffDate) AS NextDate from Evaluations ev
                                    WHERE ev.EvaluationStatus IS NOT NULL AND CONVERT(date, ev.StaffDate)>=CONVERT(date, GETDATE())";
                SqlDataReader reader = ExecuteReader(query);
                if (reader.Read())
                {
                    ret = ValueOperations.DBToDate(reader["NextDate"]);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public DateTime getNextEvaluationStaffDateForDepartment(string departmentCode)
        {
            DateTime ret = DateTime.MinValue;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select MIN(ev.StaffDate) AS NextDate from Evaluations ev, DepartmentsToEvaluate dte 
                                    WHERE ev.EvaluationId=dte.EvaluationId AND dte.DepartmentCode=@DepartmentCode 
                                    AND ev.StaffDate>GETDATE() AND ev.EvaluationStatus IS NOT NULL";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@DepartmentCode",departmentCode)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = ValueOperations.DBToDate(reader["NextDate"]);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<KeyValuePair<string, string>> getPairingsFromDetails(DateTime endDate)
        {
            List<KeyValuePair<string, string>> ret = new List<KeyValuePair<string, string>>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select DISTINCT AssessorCode, UserCode FROM EvaluationDetails WHERE EvaluationDate=@EvaluationDate";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationDate", endDate));
                while (reader.Read())
                {
                    ret.Add(new KeyValuePair<string, string>(ValueOperations.DBToString(reader["AssessorCode"]), ValueOperations.DBToString(reader["UserCode"])));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public PrivacyConfiguration getPrivacyConfiguration()
        {
            PrivacyConfiguration ret = new PrivacyConfiguration();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from PrivacyConfiguration";

                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.employees.Add(new EmployeeHeader()
                    {
                        employeeId = reader["UserId"].ToString(),
                        fullName = reader["FullName"].ToString()
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getPromotableEvaluees(int evaluationId)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select DISTINCT EmployeeId from EvaluationPromotions WHERE EvaluationId=@EvaluationId
                                AND ((EmployeeType='Evaluee' AND CanBePromoted='True') OR 
                                    (EmployeeType='Assessor' AND PromotionStatus IN ('Proposed','Rejected')))";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId)
                };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                    ret.Add(reader["EmployeeId"].ToString());
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getPromotedAssessors()
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select DISTINCT EmployeeId from EvaluationPromotions WHERE EmployeeType='Assessor' AND PromotionStatus='Accepted'";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                    ret.Add(reader["EmployeeId"].ToString());
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getPromotionEmployees(int evaluationId, string employeeType, PromotionStatus? status)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from EvaluationPromotions WHERE EmployeeType=@EmployeeType AND 
                                    EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EmployeeType",employeeType),
                    createParameter("@EvaluationId",evaluationId)
                };
                if (status.HasValue)
                {
                    query = @"select * from EvaluationPromotions WHERE EmployeeType=@EmployeeType AND 
                                EvaluationId=@EvaluationId AND PromotionStatus=@PromotionStatus";
                    pars = new SqlParameter[]{
                        createParameter("@EmployeeType",employeeType),
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@PromotionStatus",status.Value.ToString()),
                    };
                }
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                    ret.Add(reader["EmployeeId"].ToString());
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getPromotionEvaluees(int evaluationId)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select DISTINCT EmployeeId from EvaluationPromotions WHERE EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId)
                };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                    ret.Add(reader["EmployeeId"].ToString());
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getPromotionEvaluees_allHistory()
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select DISTINCT EmployeeId from EvaluationPromotions";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                    ret.Add(reader["EmployeeId"].ToString());
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public string getPromotionNotes(int evaluationId, string employeeId, string commentatorId)
        {
            string ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from PromotionNotes WHERE EvaluationId=@EvaluationId AND 
                                    CommentatorId=@CommentatorId AND EmployeeId=@EmployeeId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EmployeeId",employeeId),
                    createParameter("@CommentatorId",commentatorId),
                    createParameter("@EvaluationId",evaluationId)
                };

                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                    ret = reader["PromotionNotes"].ToString();
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public PromotionRulesConfiguration getPromotionRulesConfiguration()
        {
            PromotionRulesConfiguration ret = new PromotionRulesConfiguration();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from PromotionRulesConfiguration";
                SqlDataReader reader = ExecuteReader(query);
                if (reader.Read())
                {
                    ret.minimumAverageRate = ValueOperations.DBToDouble(reader["MinimumAverageRate"]);
                    ret.minimumSeniority = ValueOperations.DBToDouble(reader["MinimumSeniority"]);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> getProposedAssessors(int evaluationId, string evalueeId)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from EvaluationPairings where EvaluationId=@EvaluationId AND EvalueeId=@EvalueeId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@EvalueeId",evalueeId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(ValueOperations.DBToString(reader["AssessorId"]));
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Reminders getReminders(int evaluationId, string employeeId, bool includeSentReminders = false)
        {
            Reminders ret = new Reminders();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from Reminders WHERE EvaluationId=@EvaluationId AND AssessorId=@AssessorId";
                if (!includeSentReminders)
                    query += @" AND Status='READY' AND TemplateId='StandardReminder'";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@AssessorId",employeeId)
                };
                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(new Reminder()
                    {
                        assessorId = reader["AssessorId"].ToString(),
                        evaluationId = ValueOperations.DBToInteger(reader["EvaluationId"].ToString()),
                        evalueeId = reader["EvalueeId"].ToString(),
                        reminderDate = ValueOperations.DBToDate(reader["ReminderDate"]),
                        reminderId = ValueOperations.DBToInteger(reader["ReminderId"].ToString())
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public RemindersConfiguration getRemindersConfiguration()
        {
            RemindersConfiguration ret = new RemindersConfiguration();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from RemindersConfiguration";
                SqlDataReader reader = ExecuteReader(query);
                if (reader.Read())
                {
                    ret.numberDaysBeforeReminder = ValueOperations.DBToInteger(reader["NumberDaysBeforeReminder"]);
                    ret.numberEvaluationsBeforeReminder = ValueOperations.DBToInteger(reader["NumberEvaluationsBeforeReminder"]);
                    ret.isEnabled = ValueOperations.DBToBool(reader["IsEnabled"]);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public StaffMembersConfiguration getStaffMembersConfiguration()
        {
            StaffMembersConfiguration ret = new StaffMembersConfiguration();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from StaffMembersConfiguration";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.members.Add(new StaffMember()
                    {
                        employeeId = ValueOperations.DBToString(reader["UserId"]),
                        fullName = ValueOperations.DBToString(reader["FullName"])
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Type2NoteDefinitions getType2NoteDefinitions()
        {
            Type2NoteDefinitions ret = new Type2NoteDefinitions();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from Type2NoteDefinitions order by NoteCode";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(new Type2NoteDefinition()
                    {
                        noteCode = reader["NoteCode"].ToString(),
                        noteText = reader["NoteText"].ToString()
                    });
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Evaluation getUltimateEvaluation()
        {
            Evaluation ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from Evaluations WHERE EvaluationStatus IS NOT NULL order by StartDate DESC";
                SqlDataReader reader = ExecuteReader(query);
                if (reader.Read())
                {
                    ret = toEvaluation(reader);
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public UserUISettings getUserUISettings(string employeeId)
        {
            UserUISettings ret = new UserUISettings();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "select * from UserSettingsUI where UserID=@UserID";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@UserID",employeeId)
                    };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = new UserUISettings()
                    {
                        assessorsPageSize = ValueOperations.DBToInteger(reader["AssessorsPageSize"]),
                        evalueesPageSize = ValueOperations.DBToInteger(reader["EvalueesPageSize"])
                    };
                }

                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public void insertDepartmentsToEvaluate(int evaluationId, Departments dep)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"INSERT INTO DepartmentsToEvaluate (EvaluationId, DepartmentCode, DepartmentDescription)
                                 VALUES (@EvaluationId,@DepartmentCode,@DepartmentDescription)";

                SqlParameter[] pars;
                if (dep != null && dep.Any())
                {
                    foreach (Department d in dep)
                    {
                        pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@DepartmentCode",d.departmentCode),
                        createParameter("@DepartmentDescription",d.departmentDescription)
                    };
                        ExecuteNonQuery(query, pars);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void insertEmployee(Employee emp)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"INSERT INTO Employees(EmployeeNumber,Name,Surname,BirthDate,BloodType,BirthPlace,Nationality,
                        Citizenship,Home_Address1,Home_Address2,Home_CAP,Home_City,Home_Province,Home_State,
                        Education,CurrentJob,HiringDate,TerminationDate,TotalMonths,NumberOfExtensions,
                        ExtensionsLeft,ContractType,Company,Workplace,EmployeeState,SpecialCategory,SpecialCategoryType,
                        SpecialCategoryNotes,TestResult,RequiredMinDaysBusinessTrips,TotalBusinessTripDays,
                        WorkplaceAlgorithm,WorkplaceAddress1, WorkplaceAddress2,WorkplaceCountryCode,WorkplaceCAP,
                        WorkplaceCity,WorkplaceProvince,ProbationaryPeriodCompleted,Commuter,DepartmentCode,
                        DepartmentDescription,DoNotEvaluate,IsAssessor,IsHeadOfDepartment,IsDirector,Password,
                        TruancyRate,OpenResearch) VALUES(
                        @EmployeeNumber,@Name,@Surname,@BirthDate,@BloodType,@BirthPlace,@Nationality,
                        @Citizenship,@Home_Address1,@Home_Address2,@Home_CAP,@Home_City,@Home_Province,@Home_State,
                        @Education,@CurrentJob,@HiringDate,@TerminationDate,@TotalMonths,@NumberOfExtensions,
                        @ExtensionsLeft,@ContractType,@Company,@Workplace,@EmployeeState,@SpecialCategory,@SpecialCategoryType,
                        @SpecialCategoryNotes,@TestResult,@RequiredMinDaysBusinessTrips,@TotalBusinessTripDays,
                        @WorkplaceAlgorithm,@WorkplaceAddress1, @WorkplaceAddress2,@WorkplaceCountryCode,@WorkplaceCAP,
                        @WorkplaceCity,@WorkplaceProvince,@ProbationaryPeriodCompleted,@Commuter,@DepartmentCode,
                        @DepartmentDescription,@DoNotEvaluate,@IsAssessor,@IsHeadOfDepartment,@IsDirector,@Password,
                        @TruancyRate,@OpenResearch)";


                SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@EmployeeNumber",emp.EmployeeNumber),
                            createParameter("@Name",optionalEncryption( emp.Name)),
                            createParameter("@Surname",optionalEncryption(emp.Surname)),
                            createParameter("@BirthDate", optionalEncryption(emp.BirthDate)),
                            createParameter("@BloodType",emp.BloodType),
                            createParameter("@BirthPlace",optionalEncryption( emp.BirthPlace)),
                            createParameter("@Nationality",emp.Nationality),
                            createParameter("@Citizenship",emp.Citizenship),
                            createParameter("@Home_Address1",optionalEncryption(emp.Home_Address1)),
                            createParameter("@Home_Address2",emp.Home_Address2),
                            createParameter("@Home_CAP",optionalEncryption(emp.Home_CAP)),
                            createParameter("@Home_City",optionalEncryption(emp.Home_City)),
                            createParameter("@Home_Province",optionalEncryption(emp.Home_Province)),
                            createParameter("@Home_State",emp.Home_State),
                            createParameter("@Education",emp.Education),
                            createParameter("@CurrentJob",optionalEncryption(emp.CurrentJob)),
                            createParameter("@HiringDate", optionalEncryption( emp.HiringDate)),
                            createParameter("@TerminationDate",optionalEncryption( emp.TerminationDate)),
                            createParameter("@TotalMonths",emp.TotalMonths),
                            createParameter("@NumberOfExtensions",emp.NumberOfExtensions),
                            createParameter("@ExtensionsLeft",emp.ExtensionsLeft),
                            createParameter("@ContractType",emp.ContractType),
                            createParameter("@Company",emp.Company),
                            createParameter("@Workplace",emp.Workplace),
                            createParameter("@EmployeeState",emp.EmployeeState),
                            createParameter("@SpecialCategory",optionalEncryption( emp.SpecialCategory)),
                            createParameter("@SpecialCategoryType",optionalEncryption(emp.SpecialCategoryType)),
                            createParameter("@SpecialCategoryNotes",optionalEncryption(emp.SpecialCategoryNotes)),
                            createParameter("@TestResult",emp.TestResult),
                            createParameter("@RequiredMinDaysBusinessTrips",emp.RequiredMinDaysBusinessTrips),
                            createParameter("@TotalBusinessTripDays",emp.TotalBusinessTripDays),
                            createParameter("@WorkplaceAlgorithm",emp.WorkplaceAlgorithm),
                            createParameter("@WorkplaceAddress1",emp.WorkplaceAddress1),
                            createParameter("@WorkplaceAddress2",emp.WorkplaceAddress2),
                            createParameter("@WorkplaceCountryCode",emp.WorkplaceCountryCode),
                            createParameter("@WorkplaceCAP",emp.WorkplaceCAP),
                            createParameter("@WorkplaceCity",emp.WorkplaceCity),
                            createParameter("@WorkplaceProvince",emp.WorkplaceProvince),
                            createParameter("@ProbationaryPeriodCompleted",emp.ProbationaryPeriodCompleted),
                            createParameter("@Commuter",emp.Commuter),
                            createParameter("@DepartmentCode",emp.DepartmentCode),
                            createParameter("@DepartmentDescription",emp.DepartmentDescription),
                            createParameter("@DoNotEvaluate",emp.DoNotEvaluate),
                            createParameter("@IsAssessor",emp.IsAssessor),
                            createParameter("@IsHeadOfDepartment",emp.IsHeadOfDepartment),
                            createParameter("@IsDirector",emp.IsDirector),
                            createParameter("@Password",optionalEncryption( emp.Password)),
                            createParameter("@TruancyRate",emp.TruancyRate),
                            createParameter("@OpenResearch",emp.OpenResearch)
                        };

                bool inserted = false;
                string message = string.Empty;
                try
                {
                    ExecuteNonQuery(query, pars);
                    inserted = true;
                }
                catch (Exception sex)
                {
                    inserted = false;
                    message = sex.Message;
                }

                if (!inserted)
                {
                    insertErrorLog("EmployeeInsert", "Unable to insert employee " + emp.EmployeeNumber +
                                    " " + emp.Name + " " + emp.Surname +
                                    ". Possible duplicate entry", message);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void insertErrorLog(string logType, string logDescription, string message)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"INSERT INTO ErrorLog (LogType, LogDescription, LogDate, Status, LogMessage) 
                                    VALUES (@LogType, @LogDescription, GETDATE(), 'READY', @LogMessage)";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@LogType",logType),
                    createParameter("@LogDescription",logDescription),
                    createParameter("@LogMessage",message)
                };
                ExecuteNonQuery(query, pars);

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public int insertEvaluation(DateTime startDate, DateTime endDate, DateTime staffDate)
        {
            int ret = -1;
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"INSERT INTO Evaluations (StartDate, EndDate, EvaluationStatus, StaffDate)
                                 OUTPUT INSERTED.EvaluationId 
                                 VALUES (@StartDate, @EndDate, 'Imminent', @StaffDate)";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@StartDate",startDate),
                    createParameter("@EndDate",endDate),
                    createParameter("@StaffDate",staffDate)
                };
                ret = int.Parse(ExecuteScalar(query, pars));

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
            return ret;
        }

        public void updateHistoryRecord(string employeeNumber, int evaluationId, decimal result)
        {
            bool wasOpen = false;
            try
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();
                string query = "UPDATE EvaluationsReceived SET result=@result WHERE EmployeeNumber=@EmployeeNumber AND EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EmployeeNumber", employeeNumber),
                        createParameter("@EvaluationId", evaluationId),
                          createParameter("@result", result),
                    };
                ExecuteNonQuery(query, pars);


            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateHistoryRecordAndAssessor(string employeeNumber, int evaluationId, decimal result, int numberOfAssessors)
        {
            bool wasOpen = false;
            try
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();
                string query = "UPDATE EvaluationsReceived SET result=@result, NumberOfAssessors=@NumberOfAssessors WHERE EmployeeNumber=@EmployeeNumber AND EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EmployeeNumber", employeeNumber),
                        createParameter("@EvaluationId", evaluationId),
                          createParameter("@result", result),
                           createParameter("@NumberOfAssessors", numberOfAssessors),
                    };
                ExecuteNonQuery(query, pars);


            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void insertEvaluationHistoryRecord(EvaluationReceived er)
        {
            bool wasOpen = false;
            try
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();
                string query = "DELETE FROM EvaluationsReceived WHERE EmployeeNumber=@EmployeeNumber AND EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EmployeeNumber",er.EmployeeNumber),
                        createParameter("@EvaluationId",er.EvaluationId),
                    };
                ExecuteNonQuery(query, pars);

                query = @"INSERT INTO EvaluationsReceived (ContractDuration, ContractEndDate,
                            ContractStartDate, ContractType, EmployeeNumber, EvaluationDate,
                            EvaluationNotes, NextEvaluation, Result, NumberOfAssessors,
                            EvaluationId) VALUES (
                            @ContractDuration, @ContractEndDate,
                            @ContractStartDate, @ContractType, @EmployeeNumber, @EvaluationDate,
                            @EvaluationNotes, @NextEvaluation, @Result, @NumberOfAssessors,
                            @EvaluationId)";

                pars = new SqlParameter[]{
                        createParameter("@ContractDuration",er.ContractDuration),
                        createParameter("@ContractEndDate",er.ContractEndDate),
                        createParameter("@ContractStartDate",er.ContractStartDate),
                        createParameter("@ContractType",er.ContractType),
                        createParameter("@EmployeeNumber",er.EmployeeNumber),
                        createParameter("@EvaluationDate",er.EvaluationDate),
                        createParameter("@EvaluationNotes",er.EvaluationNotes),
                        createParameter("@NextEvaluation",er.NextEvaluation),
                        createParameter("@Result",er.Result),
                        createParameter("@NumberOfAssessors",er.NumberOfAssessors),
                        createParameter("@EvaluationId",er.EvaluationId),
                    };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void insertEvaluationPairing(int evaluationId, string assessorId, string evalueeId, bool manual = true)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE EvaluationPairings SET Status='Accepted' WHERE EvaluationId=@EvaluationId AND
                                    AssessorId=@AssessorId AND EvalueeId=@EvalueeId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@AssessorId",assessorId),
                    createParameter("@EvalueeId",evalueeId)
                };
                int res = ExecuteNonQuery(query, pars);

                if (res == 0)
                {
                    query = @"INSERT INTO EvaluationPairings (EvaluationId, AssessorId, EvalueeId, Status, AutoOrManual)
                                 VALUES (@EvaluationId, @AssessorId, @EvalueeId, 'Accepted', @AutoOrManual)";

                    pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@AssessorId",assessorId),
                        createParameter("@EvalueeId",evalueeId),
                        createParameter("@AutoOrManual", manual ? "Manual" : "Auto")
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void insertEvaluationSwitchLog(int evaluationId, string employeeId, string message, bool includedInEvaluation)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"INSERT INTO EvaluationSwitchLog (EvaluationId, DateInserted, EmployeeId, LogDescription, IncludedInEvaluation) 
                                    VALUES (@EvaluationId, GETDATE(), @EmployeeId, @LogDescription, @IncludedInEvaluation)";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@EmployeeId",employeeId),
                    createParameter("@LogDescription",message),
                    createParameter("@IncludedInEvaluation",includedInEvaluation)
                };
                ExecuteNonQuery(query, pars);

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void insertEvalueeProposedPromotion(int evaluationId, string evalueeId, bool canBePromoted, bool manualInsertion = false)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE EvaluationPromotions SET CanBePromoted=@CanBePromoted, ManualInsertion=@ManualInsertion
                                    WHERE EvaluationId=@EvaluationId AND EmployeeId=@EmployeeId";
                SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@EvaluationId",evaluationId),
                            createParameter("@CanBePromoted",canBePromoted),
                            createParameter("@EmployeeId",evalueeId),
                            createParameter("@ManualInsertion",manualInsertion)
                        };
                int res = ExecuteNonQuery(query, pars);
                if (res == 0)
                {
                    query = @"INSERT INTO EvaluationPromotions (EvaluationId, EmployeeId, EmployeeType, CanBePromoted, ManualInsertion)
                                VALUES (@EvaluationId, @EmployeeId, 'Evaluee', @CanBePromoted, @ManualInsertion)";
                    pars = new SqlParameter[]{
                            createParameter("@EvaluationId",evaluationId),
                            createParameter("@CanBePromoted",canBePromoted),
                            createParameter("@EmployeeId",evalueeId),
                            createParameter("@ManualInsertion",manualInsertion)
                        };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void insertFilter(string userId, string filterClass)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"INSERT INTO Filters (UserId, FilterClass)
                                 VALUES (@UserId, @FilterClass)";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@UserId",userId),
                    createParameter("@FilterClass",filterClass)
                };
                ExecuteNonQuery(query, pars);

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }


        public int insertCustomFilter(string userId, string filterName)
        {
            int ret = 0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"INSERT INTO CustomFilters (UserId, FilterName)
                                 VALUES (@UserId, @FilterName)
                                    SELECT SCOPE_IDENTITY()";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@UserId",userId),
                    createParameter("@FilterName",filterName)
                };
                ret = Convert.ToInt32(ExecuteScalar(query, pars));

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public void insertReminder(int evaluationId, string assessorId, string evalueeId, string templateId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"INSERT INTO Reminders (EvaluationId, AssessorId, EvalueeId, ReminderDate, Status, TemplateId) VALUES 
                                (@EvaluationId, @AssessorId, @EvalueeId, GETDATE(), 'READY', @TemplateId)";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@AssessorId",assessorId),
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@EvalueeId",evalueeId),
                    createParameter("@TemplateId",templateId)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void insertSelfEvaluationPairing(int evaluationId, string employeeId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE EvaluationPairings SET Status='Accepted', IsSelfEvaluation='True',AutoOrManual='Manual' WHERE EvaluationId=@EvaluationId AND
                                    AssessorId=@AssessorId AND EvalueeId=@EvalueeId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@AssessorId",employeeId),
                    createParameter("@EvalueeId",employeeId)
                };
                int res = ExecuteNonQuery(query, pars);

                if (res == 0)
                {
                    query = @"INSERT INTO EvaluationPairings (EvaluationId, AssessorId, EvalueeId, Status, IsSelfEvaluation,AutoOrManual)
                                 VALUES (@EvaluationId, @AssessorId, @EvalueeId, 'Accepted', 'True','Manual')";

                    pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@AssessorId",employeeId),
                        createParameter("@EvalueeId",employeeId)
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void insertType2Note(string noteCode, string noteText)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "INSERT INTO Type2NoteDefinitions (NoteCode, NoteText) VALUES (@NoteCode, @NoteText)";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@NoteCode",noteCode),
                    createParameter("@NoteText",noteText)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public bool isEvaluee(int evaluationId, string evalueeId)
        {
            bool ret = false;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select * from EvaluationPromotions WHERE EmployeeId=@EmployeeId AND 
                                    EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EmployeeId",evalueeId),
                    createParameter("@EvaluationId",evaluationId)
                };
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.HasRows)
                    ret = true;
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public Employee toEmployeeSimplified(SqlDataReader reader)
        {
            Employee ret = new Employee()
            {
                EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                Name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                Surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
            };

            return ret;
        }

        public Employee toEmployee(SqlDataReader reader)
        {
            Employee ret = new Employee()
            {
                EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                Name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                Surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                BirthDate = normalizeAndDecrypt(reader["BirthDate"]),
                BirthPlace = optionalDecryption(ValueOperations.DBToString(reader["BirthPlace"])),
                BloodType = ValueOperations.DBToString(reader["BloodType"]),
                Nationality = ValueOperations.DBToString(reader["Nationality"]),
                Citizenship = ValueOperations.DBToString(reader["Citizenship"]),
                Home_Address1 = optionalDecryption(ValueOperations.DBToString(reader["Home_Address1"])),
                Home_Address2 = optionalDecryption(ValueOperations.DBToString(reader["Home_Address2"])),
                Home_CAP = optionalDecryption(ValueOperations.DBToString(reader["Home_CAP"])),
                Home_City = optionalDecryption(ValueOperations.DBToString(reader["Home_City"])),
                Home_Province = optionalDecryption(ValueOperations.DBToString(reader["Home_Province"])),
                Home_State = ValueOperations.DBToString(reader["Home_State"]),
                Education = ValueOperations.DBToString(reader["Education"]),
                CurrentJob = optionalDecryption(ValueOperations.DBToString(reader["CurrentJob"])),
                HiringDate = normalizeAndDecrypt(reader["HiringDate"]),
                TerminationDate = normalizeAndDecrypt(reader["TerminationDate"]),
                TotalMonths = ValueOperations.DBToInteger(reader["TotalMonths"]),
                NumberOfExtensions = ValueOperations.DBToInteger(reader["NumberOfExtensions"]),
                ExtensionsLeft = ValueOperations.DBToInteger(reader["ExtensionsLeft"]),
                ContractType = ValueOperations.DBToString(reader["ContractType"]),
                Company = ValueOperations.DBToString(reader["Company"]),
                Workplace = ValueOperations.DBToString(reader["Workplace"]),
                EmployeeState = ValueOperations.DBToString(reader["EmployeeState"]),
                SpecialCategory = optionalDecryption(ValueOperations.DBToString(reader["SpecialCategory"])),
                SpecialCategoryType = optionalDecryption(ValueOperations.DBToString(reader["SpecialCategoryType"])),
                SpecialCategoryNotes = optionalDecryption(ValueOperations.DBToString(reader["SpecialCategoryNotes"])),
                TestResult = ValueOperations.DBToString(reader["TestResult"]),
                RequiredMinDaysBusinessTrips = ValueOperations.DBToInteger(reader["RequiredMinDaysBusinessTrips"]),
                TotalBusinessTripDays = ValueOperations.DBToInteger(reader["TotalBusinessTripDays"]),
                WorkplaceAlgorithm = ValueOperations.DBToInteger(reader["WorkplaceAlgorithm"]),
                WorkplaceAddress1 = ValueOperations.DBToString(reader["WorkplaceAddress1"]),
                WorkplaceAddress2 = ValueOperations.DBToString(reader["WorkplaceAddress2"]),
                WorkplaceCountryCode = ValueOperations.DBToString(reader["WorkplaceCountryCode"]),
                WorkplaceCAP = ValueOperations.DBToString(reader["WorkplaceCAP"]),
                WorkplaceCity = ValueOperations.DBToString(reader["WorkplaceCity"]),
                WorkplaceProvince = ValueOperations.DBToString(reader["WorkplaceProvince"]),
                ProbationaryPeriodCompleted = ValueOperations.DBToBool(reader["ProbationaryPeriodCompleted"]),
                Commuter = ValueOperations.DBToBool(reader["Commuter"]),
                DepartmentCode = ValueOperations.DBToString(reader["DepartmentCode"]),
                DepartmentDescription = ValueOperations.DBToString(reader["DepartmentDescription"]),
                DoNotEvaluate = ValueOperations.DBToBool(reader["DoNotEvaluate"]),
                DistanceFromWork = ValueOperations.DBToInteger(reader["DistanceFromWork"]),
                RoleType = string.IsNullOrEmpty(ValueOperations.DBToString(reader["RoleType"])) ? "User" : ValueOperations.DBToString(reader["RoleType"]),
                IsHeadOfDepartment = ValueOperations.DBToBool(reader["IsHeadOfDepartment"]),
                IsDirector = ValueOperations.DBToBool(reader["IsDirector"]),
                IsAssessor = ValueOperations.DBToBool(reader["IsAssessor"]),
                UserType = ValueOperations.DBToBool(reader["IsDirector"]) ? "Director" : (ValueOperations.DBToBool(reader["IsHeadOfDepartment"]) ? "HeadOfDepartment" : "User"),
                Email = optionalDecryption(ValueOperations.DBToString(reader["Email"])),
                ExternalEmployee = ValueOperations.DBToBool(reader["ExternalEmployee"]),
                ExtensionNotes = ValueOperations.DBToString(reader["ExtensionNotes"]),
                TruancyRate = ValueOperations.DBToString(reader["TruancyRate"]),
                OpenResearch = ValueOperations.DBToInteger(reader["OpenResearch"]),
                RoleId = ValueOperations.DBToInteger(reader["RoleId"]),

            };
            if (reader["yearAverage"] != null)
            {
                ret.yearAverage = ValueOperations.DBToDouble(reader["yearAverage"]);
            }
            return ret;
        }

        public Employee toEmployeeWithPhoto(SqlDataReader reader)
        {
            Employee ret = new Employee()
            {
                Photo = ValueOperations.DBToByteArray(reader["Photo"]),
                EmployeeNumber = ValueOperations.DBToString(reader["EmployeeNumber"]),
                Name = optionalDecryption(ValueOperations.DBToString(reader["Name"])),
                Surname = optionalDecryption(ValueOperations.DBToString(reader["Surname"])),
                BirthDate = normalizeAndDecrypt(reader["BirthDate"]),
                BirthPlace = optionalDecryption(ValueOperations.DBToString(reader["BirthPlace"])),
                BloodType = ValueOperations.DBToString(reader["BloodType"]),
                Nationality = ValueOperations.DBToString(reader["Nationality"]),
                Citizenship = ValueOperations.DBToString(reader["Citizenship"]),
                Home_Address1 = optionalDecryption(ValueOperations.DBToString(reader["Home_Address1"])),
                Home_Address2 = optionalDecryption(ValueOperations.DBToString(reader["Home_Address2"])),
                Home_CAP = optionalDecryption(ValueOperations.DBToString(reader["Home_CAP"])),
                Home_City = optionalDecryption(ValueOperations.DBToString(reader["Home_City"])),
                Home_Province = optionalDecryption(ValueOperations.DBToString(reader["Home_Province"])),
                Home_State = ValueOperations.DBToString(reader["Home_State"]),
                Education = ValueOperations.DBToString(reader["Education"]),
                CurrentJob = optionalDecryption(ValueOperations.DBToString(reader["CurrentJob"])),
                HiringDate = normalizeAndDecrypt(reader["HiringDate"]),
                TerminationDate = normalizeAndDecrypt(reader["TerminationDate"]),
                TotalMonths = ValueOperations.DBToInteger(reader["TotalMonths"]),
                NumberOfExtensions = ValueOperations.DBToInteger(reader["NumberOfExtensions"]),
                ExtensionsLeft = ValueOperations.DBToInteger(reader["ExtensionsLeft"]),
                ContractType = ValueOperations.DBToString(reader["ContractType"]),
                Company = ValueOperations.DBToString(reader["Company"]),
                Workplace = ValueOperations.DBToString(reader["Workplace"]),
                EmployeeState = ValueOperations.DBToString(reader["EmployeeState"]),
                SpecialCategory = optionalDecryption(ValueOperations.DBToString(reader["SpecialCategory"])),
                SpecialCategoryType = optionalDecryption(ValueOperations.DBToString(reader["SpecialCategoryType"])),
                SpecialCategoryNotes = optionalDecryption(ValueOperations.DBToString(reader["SpecialCategoryNotes"])),
                TestResult = ValueOperations.DBToString(reader["TestResult"]),
                RequiredMinDaysBusinessTrips = ValueOperations.DBToInteger(reader["RequiredMinDaysBusinessTrips"]),
                TotalBusinessTripDays = ValueOperations.DBToInteger(reader["TotalBusinessTripDays"]),
                WorkplaceAlgorithm = ValueOperations.DBToInteger(reader["WorkplaceAlgorithm"]),
                WorkplaceAddress1 = ValueOperations.DBToString(reader["WorkplaceAddress1"]),
                WorkplaceAddress2 = ValueOperations.DBToString(reader["WorkplaceAddress2"]),
                WorkplaceCountryCode = ValueOperations.DBToString(reader["WorkplaceCountryCode"]),
                WorkplaceCAP = ValueOperations.DBToString(reader["WorkplaceCAP"]),
                WorkplaceCity = ValueOperations.DBToString(reader["WorkplaceCity"]),
                WorkplaceProvince = ValueOperations.DBToString(reader["WorkplaceProvince"]),
                ProbationaryPeriodCompleted = ValueOperations.DBToBool(reader["ProbationaryPeriodCompleted"]),
                Commuter = ValueOperations.DBToBool(reader["Commuter"]),
                DepartmentCode = ValueOperations.DBToString(reader["DepartmentCode"]),
                DepartmentDescription = ValueOperations.DBToString(reader["DepartmentDescription"]),
                DoNotEvaluate = ValueOperations.DBToBool(reader["DoNotEvaluate"]),
                DistanceFromWork = ValueOperations.DBToInteger(reader["DistanceFromWork"]),
                RoleType = string.IsNullOrEmpty(ValueOperations.DBToString(reader["RoleType"])) ? "User" : ValueOperations.DBToString(reader["RoleType"]),
                IsHeadOfDepartment = ValueOperations.DBToBool(reader["IsHeadOfDepartment"]),
                IsDirector = ValueOperations.DBToBool(reader["IsDirector"]),
                IsAssessor = ValueOperations.DBToBool(reader["IsAssessor"]),
                UserType = ValueOperations.DBToBool(reader["IsDirector"]) ? "Director" : (ValueOperations.DBToBool(reader["IsHeadOfDepartment"]) ? "HeadOfDepartment" : "User"),
                Email = optionalDecryption(ValueOperations.DBToString(reader["Email"])),
                ExternalEmployee = ValueOperations.DBToBool(reader["ExternalEmployee"]),
                ExtensionNotes = ValueOperations.DBToString(reader["ExtensionNotes"]),
                TruancyRate = ValueOperations.DBToString(reader["TruancyRate"]),
                OpenResearch = ValueOperations.DBToInteger(reader["OpenResearch"]),
                RoleId = ValueOperations.DBToInteger(reader["RoleId"])

            };
            if (reader["yearAverage"] != null)
            {
                ret.yearAverage = ValueOperations.DBToDouble(reader["yearAverage"]);
            }
            return ret;
        }

        public EvaluationPairing toEvaluationPairing(SqlDataReader reader)
        {
            EvaluationPairing ret = new EvaluationPairing()
            {
                AssessorId = ValueOperations.DBToString(reader["AssessorId"]),
                EvaluationId = ValueOperations.DBToInteger(reader["EvaluationId"]),
                EvalueeId = ValueOperations.DBToString(reader["EvalueeId"]),
                NoteCode = ValueOperations.DBToString(reader["NoteCode"]),
                Notes = ValueOperations.DBToString(reader["Notes"]),
                AutoOrManual = ValueOperations.DBToString(reader["AutoOrManual"]),
                Status = Enums.GetEnumFromName<PairingStatus>(reader["Status"].ToString()),
                PairingId = ValueOperations.DBToInteger(reader["PairingId"]),
                RemindersSent = ValueOperations.DBToInteger(reader["RemindersSent"]),
            };
            return ret;
        }

        public void updateContractsFromNavision(out int contractsReceived)
        {
            bool wasOpen = false;
            SqlConnection navisionConn = new SqlConnection(navisionConnectionString);

            try
            {
                Contracts theContracts = null;
                navisionConn.Open();
                string navisionQuery = "select * from CONTRATTI_DIPENDENTI";
                SqlCommand theCommand = navisionConn.CreateCommand();
                theCommand.CommandText = navisionQuery;
                SqlDataReader navisionReader = theCommand.ExecuteReader();
                if (navisionReader.HasRows)
                    theContracts = new Contracts();
                while (navisionReader.Read())
                {
                    theContracts.Add(new Contract()
                    {
                        employeeId = (ValueOperations.DBToString(navisionReader["NumDipendente"])),
                        contractType = !string.IsNullOrEmpty(ValueOperations.DBToString(navisionReader["TipoContratto"])) && ValueOperations.DBToString(navisionReader["TipoContratto"]).Contains("Indet") ? "T.I." : "T.D.",
                        company = ValueOperations.DBToString(navisionReader["Azienda"]),
                        contractStartDate = ValueOperations.DBToDate(navisionReader["DataInizioContratto"]),
                        contractEndDate = ValueOperations.DBToDate(navisionReader["DataFineContratto"]),
                        extensions = ValueOperations.DBToString(navisionReader["Proroghe"]),
                        extensionNotes = ValueOperations.DBToString(navisionReader["NoteProroghe"]),
                    });
                }

                navisionReader.Close();

                contractsReceived = theContracts.Count;

                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                string query = "TRUNCATE TABLE EmployeeContracts";
                ExecuteNonQuery(query);

                query = @"INSERT INTO EmployeeContracts(Company, ContractEndDate, ContractStartDate, 
                        ContractType, EmployeeId, ExtensionNotes, Extensions) VALUES 
                        (@Company, @ContractEndDate, @ContractStartDate, 
                        @ContractType, @EmployeeId, @ExtensionNotes, @Extensions)";

                IEnumerator<Contract> theEnumerator = theContracts.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@Company",theEnumerator.Current.company),
                        createParameter("@ContractEndDate",theEnumerator.Current.contractEndDate),
                        createParameter("@ContractStartDate",theEnumerator.Current.contractStartDate),
                        createParameter("@ContractType",theEnumerator.Current.contractType),
                        createParameter("@EmployeeId",theEnumerator.Current.employeeId),
                        createParameter("@ExtensionNotes",theEnumerator.Current.extensionNotes),
                        createParameter("@Extensions",theEnumerator.Current.extensions)
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
                if (navisionConn.State == System.Data.ConnectionState.Open)
                    navisionConn.Close();
            }
        }

        public void updateDefaultMessagesConfiguration(DefaultMessagesConfiguration conf)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE PrivacyConfiguration SET PrivacyConfiguration=@PrivacyConfiguration";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@PrivacyConfiguration",conf.ToString())
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateDistanceFromWork(string employeeId, int distance)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"UPDATE Employees SET DistanceFromWork=@DistanceFromWork WHERE EmployeeNumber=@EmployeeId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@DistanceFromWork",distance),
                    createParameter("@EmployeeId",employeeId),
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEmployeeAssessorStatus(List<string> employeeIdList, bool isAssessor)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"UPDATE Employees SET IsAssessor=@IsAssessor WHERE EmployeeNumber IN (" +
                                        ValueOperations.arrayToDbInClause(employeeIdList) + ")";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@IsAssessor",isAssessor)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEmployeesFromNavision(out int employeesReceived,
                                                out int employeesInserted,
                                                out int insertionErrors)
        {
            bool wasOpen = false;
            SqlConnection navisionConn = new SqlConnection(navisionConnectionString);

            employeesReceived = 0;
            employeesInserted = 0;
            insertionErrors = 0;

            try
            {
                Employees theEmployees = null;
                navisionConn.Open();
                string navisionQuery = "select * from ANAGRAFICA_DIPENDENTI";
                SqlCommand theCommand = navisionConn.CreateCommand();
                theCommand.CommandText = navisionQuery;
                theCommand.CommandTimeout = 600;
                logger.Info("Selecting ANAGRAFICA_DIPENDENTI");
                SqlDataReader navisionReader = theCommand.ExecuteReader();
                logger.Info("ANAGRAFICA_DIPENDENTI opened");
                bool thereAreRows = false;
                if (navisionReader.HasRows)
                {
                    theEmployees = new Employees();
                    thereAreRows = true;
                }
                while (thereAreRows)
                {
                    try
                    {
                        thereAreRows = navisionReader.Read();
                        if (thereAreRows)
                        {
                            logger.Info("reading employee " + ValueOperations.DBToString(navisionReader["NumDipendente"]));
                            logger.Info("dates " + ValueOperations.DBToDate(navisionReader["DataNascita"]) + " -- " +
                                ValueOperations.DBToDate(navisionReader["DataAssunzione"]) + " -- " +
                                ValueOperations.DBToDate(navisionReader["DataTermine"]));
                            /*
                            DateTime birthdate, hiringdate, terminationdate;
                            if(!DateTime.TryParseExact(ValueOperations.DBToString(navisionReader["DataNascita"]), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out birthdate))
                            {
                                logger.Info("Unrecognized birth date:"+ ValueOperations.DBToString(navisionReader["DataNascita"]));
                            }
                            if(!DateTime.TryParseExact(ValueOperations.DBToString(navisionReader["DataAssunzione"]), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out hiringdate))
                            {
                                logger.Info("Unrecognized hiring date:"+ ValueOperations.DBToString(navisionReader["DataAssunzione"]));
                            }
                            if(!DateTime.TryParseExact(ValueOperations.DBToString(navisionReader["DataTermine"]), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out terminationdate))
                            {
                                logger.Info("Unrecognized termination date:"+ ValueOperations.DBToString(navisionReader["DataTermine"]));
                            }
                            */
                            theEmployees.Add(new Employee()
                            {
                                EmployeeNumber = (ValueOperations.DBToString(navisionReader["NumDipendente"])),
                                Name = optionalEncryption(ValueOperations.DBToString(navisionReader["Nome"])),
                                Surname = optionalEncryption(ValueOperations.DBToString(navisionReader["Cognome"])),
                                //                                Photo = ValueOperations.DBToByteArray(navisionReader["Foto"]),
                                BirthDate = ValueOperations.DBToDate(navisionReader["DataNascita"]),
                                BirthPlace = optionalEncryption(ValueOperations.DBToString(navisionReader["Luogo Nascita"])),
                                BloodType = ValueOperations.DBToString(navisionReader["GruppoSanguigno"]),
                                TruancyRate = ValueOperations.DBToString(navisionReader["% Assenteismo"]) + "%",
                                OpenResearch = ValueOperations.DBToInteger(navisionReader["Ricerca di prossima apertura"]),
                                Nationality = ValueOperations.DBToString(navisionReader["Nazionalità"]),
                                Citizenship = ValueOperations.DBToString(navisionReader["Cittadinanza"]),
                                Home_Address1 = optionalEncryption(ValueOperations.DBToString(navisionReader["Domicilio_Indirizzo 1"])),
                                Home_Address2 = optionalEncryption(ValueOperations.DBToString(navisionReader["Domicilio_Indirizzo 2"])),
                                Home_CAP = optionalEncryption(ValueOperations.DBToString(navisionReader["Domicilio_CAP"])),
                                Home_City = optionalEncryption(ValueOperations.DBToString(navisionReader["Domicilio_Citta"])),
                                Home_Province = optionalEncryption(ValueOperations.DBToString(navisionReader["Domicilio_Provincia"])),
                                Home_State = ValueOperations.DBToString(navisionReader["Domicilio_Nazione"]),
                                Education = ValueOperations.DBToString(navisionReader["TitoloStudio"]),
                                CurrentJob = optionalEncryption(ValueOperations.DBToString(navisionReader["MansioneAttuale"])),
                                HiringDate = ValueOperations.DBToDate(navisionReader["DataAssunzione"]),
                                TerminationDate = ValueOperations.DBToDate(navisionReader["DataTermine"]),
                                TotalMonths = ValueOperations.DBToInteger(navisionReader["Totale Mesi T.D."]),
                                NumberOfExtensions = ValueOperations.DBToInteger(navisionReader["Numero Proroghe"]),
                                ExtensionsLeft = ValueOperations.DBToInteger(navisionReader["Proroghe Residue"]),
                                ContractType = "Tempo Indeterminato".Equals(ValueOperations.DBToString(navisionReader["TipoContratto"]), StringComparison.CurrentCultureIgnoreCase) ? "T.I." : "T.D.",
                                Company = ValueOperations.DBToString(navisionReader["Azienda"]),
                                Workplace = ValueOperations.DBToString(navisionReader["Sede di Lavoro"]),
                                WorkplaceAlgorithm = ValueOperations.DBToInteger(navisionReader["Algoritmo_Sede_Lavoro"]),
                                WorkplaceAddress1 = ValueOperations.DBToString(navisionReader["Indirizzo_Sede_Lavoro_1"]),
                                WorkplaceAddress2 = ValueOperations.DBToString(navisionReader["Indirizzo_Sede_Lavoro_2"]),
                                WorkplaceCountryCode = ValueOperations.DBToString(navisionReader["Codice_Paese_Sede_Lavoro"]),
                                WorkplaceCAP = ValueOperations.DBToString(navisionReader["CAP_Sede_Lavoro"]),
                                WorkplaceCity = ValueOperations.DBToString(navisionReader["Città_Sede_Lavoro"]),
                                WorkplaceProvince = ValueOperations.DBToString(navisionReader["Provincia_Sede_Lavoro"]),
                                EmployeeState = ValueOperations.DBToString(navisionReader["StatoDipendente"]),
                                SpecialCategory = optionalEncryption(ValueOperations.DBToString(navisionReader["CategoriaProtetta"])),
                                SpecialCategoryType = optionalEncryption(ValueOperations.DBToString(navisionReader["TipoCategoriaProtetta"])),
                                SpecialCategoryNotes = optionalEncryption(ValueOperations.DBToString(navisionReader["NoteCategoriaProtetta"])),
                                TestResult = ValueOperations.DBToString(navisionReader["EsitoTest"]),
                                RequiredMinDaysBusinessTrips = ValueOperations.DBToInteger(navisionReader["Requisito gg minimo trasferta"]),
                                TotalBusinessTripDays = ValueOperations.DBToInteger(navisionReader["Totale gg Traferta"]),
                                ProbationaryPeriodCompleted = ValueOperations.DBToBool(navisionReader["Periodo Prova Superato"]),
                                Commuter = ValueOperations.DBToBool(navisionReader["Trasfertista"]),
                                DepartmentCode = ValueOperations.DBToString(navisionReader["Codice_Reparto"]),
                                DepartmentDescription = ValueOperations.DBToString(navisionReader["Descrizione_Reparto"]),
                                DoNotEvaluate = ValueOperations.DBToBool(navisionReader["FlagNonValutazione"]),
                                //                                ObjectSID = ValueOperations.DBToByteArray(navisionReader["ObjectSID"]),
                                IsAssessor = ValueOperations.DBToBool(navisionReader["Valutatore"]),
                                IsHeadOfDepartment = ValueOperations.DBToBool(navisionReader["CapoReparto"]),
                                IsDirector = ValueOperations.DBToBool(navisionReader["MembroDirezione"]),
                                ExternalEmployee = ValueOperations.DBToBool(navisionReader["Esterno"]),
                                ExtensionNotes = ValueOperations.DBToString(navisionReader["NoteProroghe"]),

                            });
                        }
                    }
                    catch (Exception nex)
                    {
                        logger.Info("Could not read employee, still going");
                        logger.Error(nex);
                    }
                }

                navisionReader.Close();

                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                employeesReceived = theEmployees.Count;

                string query;
                /*string query = "TRUNCATE TABLE Employees";
                ExecuteNonQuery(query);
                query = "TRUNCATE TABLE EmployeePhotos";
                ExecuteNonQuery(query);
                query = "TRUNCATE TABLE EmployeeObjectSID";
                ExecuteNonQuery(query);
                */

                IEnumerator<Employee> theEnumerator = theEmployees.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    bool updated = false;
                    bool inserted = false;
                    query = @"UPDATE [dbo].[Employees]
                               SET [Name] = @Name, 
                                  [Surname] = @Surname, 
                                  [BirthDate] = @BirthDate,
                                  [BloodType] = @BloodType,
                                  [BirthPlace] = @BirthPlace,
                                  [Nationality] = @Nationality,
                                  [Citizenship] = @Citizenship,
                                  [Home_Address1] = @Home_Address1,
                                  [Home_Address2] = @Home_Address2,
                                  [Home_CAP] = @Home_CAP, 
                                  [Home_City] = @Home_City,
                                  [Home_Province] = @Home_Province, 
                                  [Home_State] = @Home_State, 
                                  [Education] = @Education, 
                                  [CurrentJob] = @CurrentJob,
                                  [HiringDate] = @HiringDate,
                                  [TerminationDate] = @TerminationDate, 
                                  [TotalMonths] = @TotalMonths, 
                                  [NumberOfExtensions] = @NumberOfExtensions,
                                  [ExtensionsLeft] = @ExtensionsLeft, 
                                  [ContractType] = @ContractType, 
                                  [Company] = @Company, 
                                  [Workplace] = @Workplace,
                                  [EmployeeState] = @EmployeeState,
                                  [SpecialCategory] = @SpecialCategory,
                                  [SpecialCategoryType] = @SpecialCategoryType,
                                  [SpecialCategoryNotes] = @SpecialCategoryNotes,
                                  [TestResult] = @TestResult, 
                                  [RequiredMinDaysBusinessTrips] = @RequiredMinDaysBusinessTrips, 
                                  [TotalBusinessTripDays] = @TotalBusinessTripDays, 
                                  [WorkplaceAlgorithm] = @WorkplaceAlgorithm, 
                                  [WorkplaceAddress1] = @WorkplaceAddress1, 
                                  [WorkplaceAddress2] = @WorkplaceAddress2, 
                                  [WorkplaceCountryCode] = @WorkplaceCountryCode,
                                  [WorkplaceCAP] = @WorkplaceCAP, 
                                  [WorkplaceCity] = @WorkplaceCity,
                                  [WorkplaceProvince] = @WorkplaceProvince, 
                                  [ProbationaryPeriodCompleted] = @ProbationaryPeriodCompleted, 
                                  [Commuter] = @Commuter, 
                                  [DepartmentCode] = @DepartmentCode, 
                                  [DepartmentDescription] = @DepartmentDescription, 
                                  [DoNotEvaluate] = @DoNotEvaluate,
                                  IsHeadOfDepartment = @IsHeadOfDepartment,
                                  IsDirector = @IsDirector,
                                  ExternalEmployee = @ExternalEmployee,
                                  ExtensionNotes = @ExtensionNotes,
                                  TruancyRate = @TruancyRate,
                                  OpenResearch =@openResearch
                             WHERE [EmployeeNumber] = @EmployeeNumber";
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EmployeeNumber",theEnumerator.Current.EmployeeNumber),
                        createParameter("@Name",theEnumerator.Current.Name),
                        createParameter("@Surname",theEnumerator.Current.Surname),
                        createParameter("@BirthDate", optionalEncryption(theEnumerator.Current.BirthDate)),
                        createParameter("@BloodType",theEnumerator.Current.BloodType),
                        createParameter("@BirthPlace",theEnumerator.Current.BirthPlace),
                        createParameter("@Nationality",theEnumerator.Current.Nationality),
                        createParameter("@Citizenship",theEnumerator.Current.Citizenship),
                        createParameter("@Home_Address1",theEnumerator.Current.Home_Address1),
                        createParameter("@Home_Address2",theEnumerator.Current.Home_Address2),
                        createParameter("@Home_CAP",theEnumerator.Current.Home_CAP),
                        createParameter("@Home_City",theEnumerator.Current.Home_City),
                        createParameter("@Home_Province",theEnumerator.Current.Home_Province),
                        createParameter("@Home_State",theEnumerator.Current.Home_State),
                        createParameter("@Education",theEnumerator.Current.Education),
                        createParameter("@CurrentJob",theEnumerator.Current.CurrentJob),
                        createParameter("@HiringDate", optionalEncryption( theEnumerator.Current.HiringDate)),
                        createParameter("@TerminationDate",optionalEncryption( theEnumerator.Current.TerminationDate)),
                        createParameter("@TotalMonths",theEnumerator.Current.TotalMonths),
                        createParameter("@NumberOfExtensions",theEnumerator.Current.NumberOfExtensions),
                        createParameter("@ExtensionsLeft",theEnumerator.Current.ExtensionsLeft),
                        createParameter("@ContractType",theEnumerator.Current.ContractType),
                        createParameter("@Company",theEnumerator.Current.Company),
                        createParameter("@Workplace",theEnumerator.Current.Workplace),
                        createParameter("@EmployeeState",theEnumerator.Current.EmployeeState),
                        createParameter("@SpecialCategory",theEnumerator.Current.SpecialCategory),
                        createParameter("@SpecialCategoryType",theEnumerator.Current.SpecialCategoryType),
                        createParameter("@SpecialCategoryNotes",theEnumerator.Current.SpecialCategoryNotes),
                        createParameter("@TestResult",theEnumerator.Current.TestResult),
                        createParameter("@RequiredMinDaysBusinessTrips",theEnumerator.Current.RequiredMinDaysBusinessTrips),
                        createParameter("@TotalBusinessTripDays",theEnumerator.Current.TotalBusinessTripDays),
                        createParameter("@WorkplaceAlgorithm",theEnumerator.Current.WorkplaceAlgorithm),
                        createParameter("@WorkplaceAddress1",theEnumerator.Current.WorkplaceAddress1),
                        createParameter("@WorkplaceAddress2",theEnumerator.Current.WorkplaceAddress2),
                        createParameter("@WorkplaceCountryCode",theEnumerator.Current.WorkplaceCountryCode),
                        createParameter("@WorkplaceCAP",theEnumerator.Current.WorkplaceCAP),
                        createParameter("@WorkplaceCity",theEnumerator.Current.WorkplaceCity),
                        createParameter("@WorkplaceProvince",theEnumerator.Current.WorkplaceProvince),
                        createParameter("@ProbationaryPeriodCompleted",theEnumerator.Current.ProbationaryPeriodCompleted),
                        createParameter("@Commuter",theEnumerator.Current.Commuter),
                        createParameter("@DepartmentCode",theEnumerator.Current.DepartmentCode),
                        createParameter("@DepartmentDescription",theEnumerator.Current.DepartmentDescription),
                        createParameter("@DoNotEvaluate",theEnumerator.Current.DoNotEvaluate),
                        createParameter("@IsHeadOfDepartment",theEnumerator.Current.IsHeadOfDepartment),
                        createParameter("@IsDirector",theEnumerator.Current.IsDirector),
                        createParameter("@ExternalEmployee",theEnumerator.Current.ExternalEmployee),
                        createParameter("@ExtensionNotes",theEnumerator.Current.ExtensionNotes),
                        createParameter("@TruancyRate",theEnumerator.Current.TruancyRate),
                        createParameter("@openResearch",theEnumerator.Current.OpenResearch)
                    };

                    updated = ExecuteNonQuery(query, pars) > 0;

                    if (!updated)
                    {
                        query = @"INSERT INTO Employees(EmployeeNumber,Name,Surname,BirthDate,BloodType,BirthPlace,Nationality,
                        Citizenship,Home_Address1,Home_Address2,Home_CAP,Home_City,Home_Province,Home_State,
                        Education,CurrentJob,HiringDate,TerminationDate,TotalMonths,NumberOfExtensions,
                        ExtensionsLeft,ContractType,Company,Workplace,EmployeeState,SpecialCategory,SpecialCategoryType,
                        SpecialCategoryNotes,TestResult,RequiredMinDaysBusinessTrips,TotalBusinessTripDays,
                        WorkplaceAlgorithm,WorkplaceAddress1, WorkplaceAddress2,WorkplaceCountryCode,WorkplaceCAP,
                        WorkplaceCity,WorkplaceProvince,ProbationaryPeriodCompleted,Commuter,DepartmentCode,
                        DepartmentDescription,DoNotEvaluate,IsAssessor,IsHeadOfDepartment,IsDirector,
                        ExternalEmployee, ExtensionNotes,TruancyRate,OpenResearch) VALUES(
                        @EmployeeNumber,@Name,@Surname,@BirthDate,@BloodType,@BirthPlace,@Nationality,
                        @Citizenship,@Home_Address1,@Home_Address2,@Home_CAP,@Home_City,@Home_Province,@Home_State,
                        @Education,@CurrentJob,@HiringDate,@TerminationDate,@TotalMonths,@NumberOfExtensions,
                        @ExtensionsLeft,@ContractType,@Company,@Workplace,@EmployeeState,@SpecialCategory,@SpecialCategoryType,
                        @SpecialCategoryNotes,@TestResult,@RequiredMinDaysBusinessTrips,@TotalBusinessTripDays,
                        @WorkplaceAlgorithm,@WorkplaceAddress1, @WorkplaceAddress2,@WorkplaceCountryCode,@WorkplaceCAP,
                        @WorkplaceCity,@WorkplaceProvince,@ProbationaryPeriodCompleted,@Commuter,@DepartmentCode,
                        @DepartmentDescription,@DoNotEvaluate,@IsAssessor,@IsHeadOfDepartment,@IsDirector,
                        @ExternalEmployee, @ExtensionNotes,@TruancyRate,@openResearch)";


                        pars = new SqlParameter[]{
                            createParameter("@EmployeeNumber",theEnumerator.Current.EmployeeNumber),
                            createParameter("@Name",theEnumerator.Current.Name),
                            createParameter("@Surname",theEnumerator.Current.Surname),
                            createParameter("@BirthDate", optionalEncryption(theEnumerator.Current.BirthDate)),
                            createParameter("@BloodType",theEnumerator.Current.BloodType),
                            createParameter("@BirthPlace",theEnumerator.Current.BirthPlace),
                            createParameter("@Nationality",theEnumerator.Current.Nationality),
                            createParameter("@Citizenship",theEnumerator.Current.Citizenship),
                            createParameter("@Home_Address1",theEnumerator.Current.Home_Address1),
                            createParameter("@Home_Address2",theEnumerator.Current.Home_Address2),
                            createParameter("@Home_CAP",theEnumerator.Current.Home_CAP),
                            createParameter("@Home_City",theEnumerator.Current.Home_City),
                            createParameter("@Home_Province",theEnumerator.Current.Home_Province),
                            createParameter("@Home_State",theEnumerator.Current.Home_State),
                            createParameter("@Education",theEnumerator.Current.Education),
                            createParameter("@CurrentJob",theEnumerator.Current.CurrentJob),
                            createParameter("@HiringDate", optionalEncryption( theEnumerator.Current.HiringDate)),
                            createParameter("@TerminationDate",optionalEncryption( theEnumerator.Current.TerminationDate)),
                            createParameter("@TotalMonths",theEnumerator.Current.TotalMonths),
                            createParameter("@NumberOfExtensions",theEnumerator.Current.NumberOfExtensions),
                            createParameter("@ExtensionsLeft",theEnumerator.Current.ExtensionsLeft),
                            createParameter("@ContractType",theEnumerator.Current.ContractType),
                            createParameter("@Company",theEnumerator.Current.Company),
                            createParameter("@Workplace",theEnumerator.Current.Workplace),
                            createParameter("@EmployeeState",theEnumerator.Current.EmployeeState),
                            createParameter("@SpecialCategory",theEnumerator.Current.SpecialCategory),
                            createParameter("@SpecialCategoryType",theEnumerator.Current.SpecialCategoryType),
                            createParameter("@SpecialCategoryNotes",theEnumerator.Current.SpecialCategoryNotes),
                            createParameter("@TestResult",theEnumerator.Current.TestResult),
                            createParameter("@RequiredMinDaysBusinessTrips",theEnumerator.Current.RequiredMinDaysBusinessTrips),
                            createParameter("@TotalBusinessTripDays",theEnumerator.Current.TotalBusinessTripDays),
                            createParameter("@WorkplaceAlgorithm",theEnumerator.Current.WorkplaceAlgorithm),
                            createParameter("@WorkplaceAddress1",theEnumerator.Current.WorkplaceAddress1),
                            createParameter("@WorkplaceAddress2",theEnumerator.Current.WorkplaceAddress2),
                            createParameter("@WorkplaceCountryCode",theEnumerator.Current.WorkplaceCountryCode),
                            createParameter("@WorkplaceCAP",theEnumerator.Current.WorkplaceCAP),
                            createParameter("@WorkplaceCity",theEnumerator.Current.WorkplaceCity),
                            createParameter("@WorkplaceProvince",theEnumerator.Current.WorkplaceProvince),
                            createParameter("@ProbationaryPeriodCompleted",theEnumerator.Current.ProbationaryPeriodCompleted),
                            createParameter("@Commuter",theEnumerator.Current.Commuter),
                            createParameter("@DepartmentCode",theEnumerator.Current.DepartmentCode),
                            createParameter("@DepartmentDescription",theEnumerator.Current.DepartmentDescription),
                            createParameter("@DoNotEvaluate",theEnumerator.Current.DoNotEvaluate),
                            createParameter("@IsAssessor",theEnumerator.Current.IsAssessor),
                            createParameter("@IsHeadOfDepartment",theEnumerator.Current.IsHeadOfDepartment),
                            createParameter("@IsDirector",theEnumerator.Current.IsDirector),
                            createParameter("@ExternalEmployee",theEnumerator.Current.ExternalEmployee),
                            createParameter("@ExtensionNotes",theEnumerator.Current.ExtensionNotes),
                            createParameter("@TruancyRate",theEnumerator.Current.TruancyRate),
                            createParameter("@openResearch",theEnumerator.Current.OpenResearch)
                        };

                        string message = string.Empty;
                        try
                        {
                            ExecuteNonQuery(query, pars);
                            inserted = true;
                            employeesInserted++;
                        }
                        catch (Exception sex)
                        {
                            inserted = false;
                            message = sex.Message;
                            insertionErrors++;
                        }


                        if (!inserted)
                        {
                            insertErrorLog("EmployeeInsert", "Unable to insert employee " + theEnumerator.Current.EmployeeNumber +
                                            " " + theEnumerator.Current.Name + " " + theEnumerator.Current.Surname +
                                            ". Possible duplicate entry", message);
                        }
                    }

                    /*
                    if (updated || inserted)
                    {
                        int res = 0;
                        if (theEnumerator.Current.Photo != null)
                        {
                            query = @"UPDATE EmployeePhotos SET Photo=@Photo WHERE EmployeeNumber=@EmployeeNumber";
                            pars = new SqlParameter[]{
                                createParameter("@EmployeeNumber",theEnumerator.Current.EmployeeNumber),
                                createImageParameter("@Photo",theEnumerator.Current.Photo)
                            };
                            res = ExecuteNonQuery(query, pars);

                            if (res == 0)
                            {
                                query = @"INSERT INTO EmployeePhotos(EmployeeNumber,Photo) VALUES (@EmployeeNumber,@Photo)";
                                pars = new SqlParameter[]{
                                createParameter("@EmployeeNumber",theEnumerator.Current.EmployeeNumber),
                                createImageParameter("@Photo",theEnumerator.Current.Photo)
                            };
                                ExecuteNonQuery(query, pars);
                            }
                        }

                        if (theEnumerator.Current.ObjectSID != null)
                        {
                            query = @"UPDATE EmployeeObjectSID SET ObjectSID=@ObjectSID WHERE EmployeeNumber=@EmployeeNumber";
                            pars = new SqlParameter[]{
                            createParameter("@EmployeeNumber",theEnumerator.Current.EmployeeNumber),
                            createParameter("@ObjectSID",theEnumerator.Current.ObjectSID)
                        };
                            res = ExecuteNonQuery(query, pars);
                            if (res == 0)
                            {
                                query = @"INSERT INTO EmployeeObjectSID(EmployeeNumber,ObjectSID) VALUES (@EmployeeNumber,@ObjectSID)";
                                pars = new SqlParameter[]{
                                createParameter("@EmployeeNumber",theEnumerator.Current.EmployeeNumber),
                                createParameter("@ObjectSID",theEnumerator.Current.ObjectSID)
                            };
                                ExecuteNonQuery(query, pars);
                            }
                        }
                    }
                    */
                }

                query = "UPDATE EmployeeObjectSID SET LastModified = GETDATE(), ObjectSID = (SELECT TOP(1) ad.ObjectSID FROM UtentiADFoto ad WHERE ad.ID_Impiegato_Navision = EmployeeNumber)  WHERE LastModified IS NULL";
                ExecuteNonQuery(query);
                query = "UPDATE EmployeeObjectSID SET LastModified=GETDATE(), ObjectSID = (SELECT TOP(1) ad.ObjectSID FROM UtentiADFoto ad WHERE ad.ID_Impiegato_Navision=EmployeeNumber)  WHERE LastModified<(SELECT TOP(1) CONVERT(datetime, ad.DATA_ULTIMA_MODIFICA) FROM UtentiADFoto ad WHERE ad.ID_Impiegato_Navision=EmployeeNumber)";
                ExecuteNonQuery(query);
                query = "INSERT INTO EmployeeObjectSID(EmployeeNumber,ObjectSID,LastModified) (SELECT ad.ID_Impiegato_Navision,ad.ObjectSid,GETDATE() FROM UtentiADFoto ad WHERE NOT EXISTS (SELECT eos.EmployeeNumber FROM EmployeeObjectSID eos WHERE eos.EmployeeNumber=ad.ID_Impiegato_Navision ))";
                ExecuteNonQuery(query);
                query = "UPDATE EmployeePhotos SET LastModified=GETDATE(), Photo = (SELECT TOP(1) ad.Foto FROM UtentiADFoto ad WHERE ad.ID_Impiegato_Navision=EmployeeNumber) WHERE LastModified IS NULL";
                ExecuteNonQuery(query);
                query = "UPDATE EmployeePhotos SET LastModified=GETDATE(), Photo = (SELECT TOP(1) ad.Foto FROM UtentiADFoto ad WHERE ad.ID_Impiegato_Navision=EmployeeNumber) WHERE LastModified<(SELECT TOP(1) CONVERT(datetime, ad.DATA_ULTIMA_MODIFICA) FROM UtentiADFoto ad WHERE ad.ID_Impiegato_Navision=EmployeeNumber)";
                ExecuteNonQuery(query);
                query = "INSERT INTO EmployeePhotos(EmployeeNumber,Photo,LastModified) (SELECT ad.ID_Impiegato_Navision,ad.FOTO,GETDATE() FROM UtentiADFoto ad WHERE NOT EXISTS (SELECT eos.EmployeeNumber FROM EmployeePhotos eos WHERE eos.EmployeeNumber=ad.ID_Impiegato_Navision ))";
                ExecuteNonQuery(query);


                //query = "UPDATE Employees SET Email = (SELECT TOP(1) ad.Email FROM UtentiADFoto ad WHERE ad.ID_Impiegato_Navision=EmployeeNumber)";
                //ExecuteNonQuery(query);

                Dictionary<string, string> employeeEmails = new Dictionary<string, string>();
                query = "SELECT ID_Impiegato_Navision, Email FROM UtentiADFoto";
                SqlDataReader emailReader = ExecuteReader(query);
                while (emailReader.Read())
                {
                    if (!string.IsNullOrEmpty(ValueOperations.DBToString(emailReader["ID_Impiegato_Navision"])) &&
                        !string.IsNullOrWhiteSpace(optionalEncryption(ValueOperations.DBToString(emailReader["Email"]))))
                        employeeEmails[ValueOperations.DBToString(emailReader["ID_Impiegato_Navision"])] = optionalEncryption(ValueOperations.DBToString(emailReader["Email"]));
                }
                emailReader.Close();

                if (employeeEmails.Count > 0)
                {
                    var emailEnum = employeeEmails.GetEnumerator();
                    while (emailEnum.MoveNext())
                    {
                        query = "UPDATE Employees SET Email = @Email WHERE EmployeeNumber=@EmployeeNumber";
                        ExecuteNonQuery(query, createParameter("@Email", emailEnum.Current.Value),
                                            createParameter("@EmployeeNumber", emailEnum.Current.Key));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info("Error during employee insertion");
                logger.Error(ex);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
                if (navisionConn.State == System.Data.ConnectionState.Open)
                    navisionConn.Close();
            }
        }

        public void updateErrorLogSent()
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"UPDATE ErrorLog SET Status='SENT' WHERE Status='READY'";
                ExecuteNonQuery(query);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluation(int evaluationId, DateTime staffDate, DateTime endDate)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE Evaluations SET StaffDate=@StaffDate, EndDate=@EndDate WHERE EvaluationId=@EvaluationId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@StaffDate",staffDate),
                    createParameter("@EndDate",endDate),
                    createParameter("@EvaluationId",evaluationId)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationConnectionsFromNavision(out int connectionsReceived)
        {
            bool wasOpen = false;
            SqlConnection navisionConn = new SqlConnection(navisionConnectionString);

            try
            {
                EvaluationConnections theConnections = null;
                navisionConn.Open();
                SqlParameter par1 = new SqlParameter("@dataDa", new DateTime(2000, 1, 1));
                par1.Direction = System.Data.ParameterDirection.Input;
                SqlParameter par2 = new SqlParameter("@dataA", DateTime.Now);
                par2.Direction = System.Data.ParameterDirection.Input;

                SqlCommand theCommand = navisionConn.CreateCommand();
                theCommand.CommandType = System.Data.CommandType.StoredProcedure;
                theCommand.CommandText = "[VALUTAZIONI_LEGAMI_TASK_ODS]";
                theCommand.Parameters.Add(par1);
                theCommand.Parameters.Add(par2);
                SqlDataReader navisionReader = theCommand.ExecuteReader();
                if (navisionReader.HasRows)
                    theConnections = new EvaluationConnections();
                while (navisionReader.Read())
                {
                    theConnections.Add(new EvaluationConnection()
                    {
                        Commission = ValueOperations.DBToString(navisionReader["Commessa"]),
                        ConnectionType = ValueOperations.DBToString(navisionReader["Tipo"]),
                        EmployeeCode = (ValueOperations.DBToString(navisionReader["Codice Dipendente"])),
                        ODP = ValueOperations.DBToString(navisionReader["N° ODP"]),
                        Task = ValueOperations.DBToString(navisionReader["Task"]),
                        ConnectionDate = ValueOperations.DBToDate(navisionReader["Data"])
                    });
                }

                navisionReader.Close();

                connectionsReceived = theConnections.Count;
                logger.Info("connections to insert: " + connectionsReceived);

                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                string query = "TRUNCATE TABLE EvaluationConnections";
                ExecuteNonQuery(query);

                query = @"INSERT INTO EvaluationConnections (Commission, ConnectionType, EmployeeCode, ODP, Task, 
                            ConnectionDate) VALUES (@Commission, @ConnectionType, @EmployeeCode, @ODP, @Task, 
                            @ConnectionDate)";

                IEnumerator<EvaluationConnection> theEnumerator = theConnections.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    try
                    {
                        SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@Commission",theEnumerator.Current.Commission),
                            createParameter("@ConnectionDate",theEnumerator.Current.ConnectionDate),
                            createParameter("@ConnectionType",theEnumerator.Current.ConnectionType),
                            createParameter("@EmployeeCode",theEnumerator.Current.EmployeeCode),
                            createParameter("@ODP",theEnumerator.Current.ODP),
                            createParameter("@Task",theEnumerator.Current.Task)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                    catch (Exception exx)
                    {
                        logger.Info("Error while inserting connection");
                        logger.Error(exx);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
                if (navisionConn.State == System.Data.ConnectionState.Open)
                    navisionConn.Close();
            }
        }

        public void updateEvaluationCriteriaConfiguration(int evaluationId, EvaluationCriteria_Multilanguage theCriteria)
        {
            bool wasOpen = false;

            try
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                //var evals = getAllEvaluations();

                string query = "DELETE FROM EvaluationCriteria WHERE EvaluationId=@EvaluationId";
                ExecuteNonQuery(query, createParameter("@EvaluationId", evaluationId));

                query = "DELETE FROM EvaluationCriteriaLanguages WHERE EvaluationId=@EvaluationId";
                ExecuteNonQuery(query, createParameter("@EvaluationId", evaluationId));

                query = @"INSERT INTO EvaluationCriteria(CriterionCode,CriterionWeight,
                            ValidFrom,ValidTo,ParentCode,EvaluationId,Expanded,Ordering,macroareaid,macroareaOrder) VALUES(
                            @CriterionCode,@CriterionWeight,
                            @ValidFrom,@ValidTo,@ParentCode,@EvaluationId,@Expanded,@Ordering,@macroareaid,@macroareaOrder)";

                int count1 = 1;
                IEnumerator<EvaluationCriterion_Multilanguage> theEnumerator = theCriteria.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@CriterionCode",theEnumerator.Current.CriterionCode),
                        createParameter("@CriterionWeight",theEnumerator.Current.CriterionWeight),
                        createParameter("@Ordering",count1),
                        createParameter("@ValidFrom",theEnumerator.Current.ValidFrom),
                        createParameter("@ValidTo",theEnumerator.Current.ValidTo),
                        createParameter("@ParentCode",string.Empty),
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@Expanded", theEnumerator.Current.Expanded),
                        createParameter("@macroareaid", theEnumerator.Current.macroAreaId),
                        createParameter("@macroareaOrder", theEnumerator.Current.macroAreaOrder),
                    };
                    ExecuteNonQuery(query, pars);

                    int count2 = 1;
                    IEnumerator<EvaluationCriterion_Multilanguage> theEnumerator2 = theEnumerator.Current.Subcriteria.GetEnumerator();
                    while (theEnumerator2.MoveNext())
                    {
                        pars = new SqlParameter[]{
                            createParameter("@CriterionCode",theEnumerator2.Current.CriterionCode),
                            createParameter("@CriterionWeight",theEnumerator2.Current.CriterionWeight),
                            createParameter("@Ordering",count2),
                            createParameter("@ValidFrom",theEnumerator2.Current.ValidFrom),
                            createParameter("@ValidTo",theEnumerator2.Current.ValidTo),
                            createParameter("@ParentCode",theEnumerator.Current.CriterionCode),
                            createParameter("@EvaluationId", evaluationId),
                            createParameter("@Expanded", theEnumerator2.Current.Expanded),
                            createParameter("@macroareaid", theEnumerator2.Current.macroAreaId),
                            createParameter("@macroareaOrder", theEnumerator2.Current.macroAreaOrder),
                        };
                        ExecuteNonQuery(query, pars);
                        count2++;
                    }
                    count1++;
                }

                query = @"INSERT INTO EvaluationCriteriaLanguages(CriterionCode,LanguageCode,
                            CriterionDescription,EvaluationId, VideoPath, VideoEnable) VALUES(
                            @CriterionCode,@LanguageCode,
                            @CriterionDescription,@EvaluationId, @VideoPath, @VideoEnable)";

                theEnumerator = theCriteria.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    if (theEnumerator.Current.CriterionDescription != null && theEnumerator.Current.CriterionDescription.Keys.Any())
                    {
                        foreach (string sk in theEnumerator.Current.CriterionDescription.Keys)
                        {
                            SqlParameter[] pars = new SqlParameter[]{
                                createParameter("@CriterionCode",theEnumerator.Current.CriterionCode),
                                createParameter("@LanguageCode",sk),
                                createParameter("@CriterionDescription",theEnumerator.Current.CriterionDescription[sk]),
                                createParameter("@EvaluationId",evaluationId),
                                createParameter("@VideoPath", theEnumerator.Current.VideoPaths[sk] == "null"
                                    ? default(string)
                                    : theEnumerator.Current.VideoPaths[sk]),
                                createParameter("@VideoEnable", theEnumerator.Current.VideoEnables[sk])
                            };
                            ExecuteNonQuery(query, pars);

                            //setto il video per tutti i record esistenti
                            var sqlUpdate = @"update EvaluationCriteriaLanguages set VideoPath = @VideoPath, VideoEnable = @VideoEnable, CriterionDescription = @CriterionDescription
                                where CriterionCode = @CriterionCode and LanguageCode = @LanguageCode";
                            var parsU = new[]
                            {
                                createParameter("@CriterionCode",theEnumerator.Current.CriterionCode),
                                createParameter("@LanguageCode",sk),
                                createParameter("@VideoPath", theEnumerator.Current.VideoPaths[sk] == "null"
                                    ? default(string)
                                    : theEnumerator.Current.VideoPaths[sk]),
                                createParameter("@VideoEnable", theEnumerator.Current.VideoEnables[sk]),
                                createParameter("@CriterionDescription",theEnumerator.Current.CriterionDescription[sk])
                            };

                            ExecuteNonQuery(sqlUpdate, parsU);

                            //foreach (var ev in evals)
                            //{
                            //    //setto la lingua per tutti gli eval
                            //    var sqlMiss = @"if not exists (select 1 from EvaluationCriteriaLanguages where EvaluationId = @EvaluationId and CriterionCode = @CriterionCode and LanguageCode = @LanguageCode)
                            //        INSERT INTO EvaluationCriteriaLanguages(CriterionCode,LanguageCode,
                            //        CriterionDescription,EvaluationId, VideoPath, VideoEnable) VALUES(
                            //        @CriterionCode,@LanguageCode,
                            //        @CriterionDescription,@EvaluationId, @VideoPath, @VideoEnable)";

                            //    var parsM = new[]
                            //    {
                            //        createParameter("@CriterionCode",theEnumerator.Current.CriterionCode),
                            //        createParameter("@LanguageCode",sk),
                            //        createParameter("@CriterionDescription",theEnumerator.Current.CriterionDescription[sk]),
                            //        createParameter("@EvaluationId", ev.EvaluationId),
                            //        createParameter("@VideoPath", theEnumerator.Current.VideoPaths[sk]),
                            //        createParameter("@VideoEnable", theEnumerator.Current.VideoEnables[sk])
                            //    };

                            //    ExecuteNonQuery(sqlMiss, parsM);

                            //    Thread.Sleep(100);
                            //}
                        }
                    }

                    IEnumerator<EvaluationCriterion_Multilanguage> theEnumerator2 = theEnumerator.Current.Subcriteria.GetEnumerator();
                    while (theEnumerator2.MoveNext())
                    {
                        if (theEnumerator2.Current.CriterionDescription != null && theEnumerator2.Current.CriterionDescription.Keys.Any())
                        {
                            foreach (string sk in theEnumerator2.Current.CriterionDescription.Keys)
                            {
                                SqlParameter[] pars = new SqlParameter[]{
                                    createParameter("@CriterionCode",theEnumerator2.Current.CriterionCode),
                                    createParameter("@LanguageCode",sk),
                                    createParameter("@CriterionDescription",theEnumerator2.Current.CriterionDescription[sk]),
                                    createParameter("@EvaluationId",evaluationId),
                                    createParameter("@VideoPath", theEnumerator2.Current.VideoPaths[sk] == "null"
                                        ? default(string)
                                        : theEnumerator2.Current.VideoPaths[sk]),
                                    createParameter("@VideoEnable", theEnumerator2.Current.VideoEnables[sk])
                                };
                                ExecuteNonQuery(query, pars);

                                //setto il video per tutti i record esistenti
                                var sqlUpdate = @"update EvaluationCriteriaLanguages set VideoPath = @VideoPath, VideoEnable = @VideoEnable, CriterionDescription = @CriterionDescription
                                where CriterionCode = @CriterionCode and LanguageCode = @LanguageCode";
                                var parsU = new[]
                                {
                                    createParameter("@CriterionCode",theEnumerator2.Current.CriterionCode),
                                    createParameter("@LanguageCode",sk),
                                    createParameter("@VideoPath", theEnumerator2.Current.VideoPaths[sk] == "null"
                                        ? default(string)
                                        : theEnumerator2.Current.VideoPaths[sk]),
                                    createParameter("@VideoEnable", theEnumerator2.Current.VideoEnables[sk]),
                                    createParameter("@CriterionDescription",theEnumerator2.Current.CriterionDescription[sk])
                                };

                                ExecuteNonQuery(sqlUpdate, parsU);

                                //foreach (var ev in evals)
                                //{
                                //    //setto la lingua per tutti gli eval
                                //    var sqlMiss = @"if not exists (select 1 from EvaluationCriteriaLanguages where EvaluationId = @EvaluationId and CriterionCode = @CriterionCode and LanguageCode = @LanguageCode)
                                //        INSERT INTO EvaluationCriteriaLanguages (CriterionCode,LanguageCode,
                                //        CriterionDescription,EvaluationId, VideoPath, VideoEnable) VALUES(
                                //        @CriterionCode,@LanguageCode,
                                //        @CriterionDescription,@EvaluationId, @VideoPath, @VideoEnable)";

                                //    var parsM = new[]
                                //    {
                                //        createParameter("@CriterionCode",theEnumerator2.Current.CriterionCode),
                                //        createParameter("@LanguageCode",sk),
                                //        createParameter("@CriterionDescription",theEnumerator2.Current.CriterionDescription[sk]),
                                //        createParameter("@EvaluationId", ev.EvaluationId),
                                //        createParameter("@VideoPath", theEnumerator2.Current.VideoPaths[sk]),
                                //        createParameter("@VideoEnable", theEnumerator2.Current.VideoEnables[sk])
                                //    };

                                //    ExecuteNonQuery(sqlMiss, parsM);

                                //    Thread.Sleep(100);
                                //}
                            }
                        }
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationCriteriaFromNavision(int evaluationId, DateTime evaluationDate)
        {
            bool wasOpen = false;
            SqlConnection navisionConn = new SqlConnection(navisionConnectionString);

            try
            {
                EvaluationCriteria theCriteria = null;
                navisionConn.Open();
                string navisionQuery = @"select * FROM CRITERI_VALUTAZIONE WHERE DataInizioValidita<=@DataValutazione AND (DataFineValidita>=@DataValutazione OR DataFineValidita=(CONVERT(date,'1900-01-01'))) ORDER BY CodiceValutazionePadre";
                SqlCommand theCommand = navisionConn.CreateCommand();
                theCommand.CommandText = navisionQuery;
                theCommand.Parameters.Add(createParameter("@DataValutazione", evaluationDate));
                SqlDataReader navisionReader = theCommand.ExecuteReader();
                if (navisionReader.HasRows)
                    theCriteria = new EvaluationCriteria();
                while (navisionReader.Read())
                {
                    EvaluationCriterion crit = new EvaluationCriterion()
                    {
                        CriterionCode = ValueOperations.DBToString(navisionReader["CodiceVautazione"]).Trim(),
                        CriterionDescription = ValueOperations.DBToString(navisionReader["Descrizione"]).Trim(),
                        CriterionWeight = ValueOperations.DBToInteger(navisionReader["PESO"]),
                        ValidFrom = ValueOperations.DBToDate(navisionReader["DataInizioValidita"]),
                        ValidTo = ValueOperations.DBToDate(navisionReader["DataFineValidita"])
                    };

                    string parentCode = ValueOperations.DBToString(navisionReader["CodiceValutazionePadre"]);
                    if (string.IsNullOrEmpty(parentCode))
                        theCriteria.Add(crit);
                    else
                        theCriteria[parentCode].Subcriteria.Add(crit);
                }

                navisionReader.Close();

                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                //string query = "TRUNCATE TABLE EvaluationCriteria";
                //ExecuteNonQuery(query);

                string query = @"INSERT INTO EvaluationCriteria(CriterionCode,CriterionDescription,CriterionWeight,ValidFrom,ValidTo,ParentCode,EvaluationId) VALUES(
                        @CriterionCode,@CriterionDescription,@CriterionWeight,@ValidFrom,@ValidTo,@ParentCode,@EvaluationId)";

                IEnumerator<EvaluationCriterion> theEnumerator = theCriteria.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@CriterionCode",theEnumerator.Current.CriterionCode),
                        createParameter("@CriterionDescription",theEnumerator.Current.CriterionDescription),
                        createParameter("@CriterionWeight",theEnumerator.Current.CriterionWeight),
                        createParameter("@ValidFrom",theEnumerator.Current.ValidFrom),
                        createParameter("@ValidTo",theEnumerator.Current.ValidTo),
                        createParameter("@ParentCode",string.Empty),
                        createParameter("@EvaluationId",evaluationId)
                    };
                    ExecuteNonQuery(query, pars);

                    IEnumerator<EvaluationCriterion> theEnumerator2 = theEnumerator.Current.Subcriteria.GetEnumerator();
                    while (theEnumerator2.MoveNext())
                    {
                        pars = new SqlParameter[]{
                            createParameter("@CriterionCode",theEnumerator2.Current.CriterionCode),
                            createParameter("@CriterionDescription",theEnumerator2.Current.CriterionDescription),
                            createParameter("@CriterionWeight",theEnumerator2.Current.CriterionWeight),
                            createParameter("@ValidFrom",theEnumerator2.Current.ValidFrom),
                            createParameter("@ValidTo",theEnumerator2.Current.ValidTo),
                            createParameter("@ParentCode",theEnumerator.Current.CriterionCode),
                            createParameter("@EvaluationId",evaluationId)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }

                query = @"insert into EvaluationCriteriaLanguages(CriterionCode,CriterionDescription,EvaluationId,LanguageCode) 
                        VALUES (@CriterionCode,@CriterionDescription,@EvaluationId,@LanguageCode)";
                theEnumerator.Reset();
                while (theEnumerator.MoveNext())
                {
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@CriterionCode",theEnumerator.Current.CriterionCode),
                        createParameter("@CriterionDescription",theEnumerator.Current.CriterionDescription),
                        createParameter("@LanguageCode","it"),
                        createParameter("@EvaluationId",evaluationId)
                    };
                    ExecuteNonQuery(query, pars);

                    IEnumerator<EvaluationCriterion> theEnumerator2 = theEnumerator.Current.Subcriteria.GetEnumerator();
                    while (theEnumerator2.MoveNext())
                    {
                        pars = new SqlParameter[]{
                            createParameter("@CriterionCode",theEnumerator2.Current.CriterionCode),
                            createParameter("@CriterionDescription",theEnumerator2.Current.CriterionDescription),
                            createParameter("@LanguageCode","it"),
                            createParameter("@EvaluationId",evaluationId)
                        };
                        ExecuteNonQuery(query, pars);


                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
                if (navisionConn.State == System.Data.ConnectionState.Open)
                    navisionConn.Close();
            }
        }

        public void updateEvaluationDetailsFromNavision()
        {
            bool wasOpen = false;
            SqlConnection navisionConn = new SqlConnection(navisionConnectionString);

            try
            {
                EvaluationDetails theDetails = null;
                navisionConn.Open();
                string navisionQuery = "select *, convert (datetime, data_valutazione,103) as dataint from VALUTAZIONI_DETTAGLIO ORDER BY dataint DESC";
                SqlCommand theCommand = navisionConn.CreateCommand();
                theCommand.CommandText = navisionQuery;
                SqlDataReader navisionReader = theCommand.ExecuteReader();
                if (navisionReader.HasRows)
                    theDetails = new EvaluationDetails();
                while (navisionReader.Read())
                {
                    theDetails.Add(new EvaluationDetail()
                    {
                        AssessorCode = (ValueOperations.DBToString(navisionReader["Codice_Valutatore"])),
                        EvaluationTermCode = ValueOperations.DBToString(navisionReader["Cod_Parametro_Valutazione"]),
                        UserCode = (ValueOperations.DBToString(navisionReader["Cod_Utente"])),
                        AssessorSurname = ValueOperations.DBToString(navisionReader["Cognome_Valutatore"]),
                        EvaluationDate = DateTime.ParseExact(ValueOperations.DBToString(navisionReader["Data_Valutazione"]), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture),
                        EvaluationTermDescription = ValueOperations.DBToString(navisionReader["Descrizione_Parametro_Valutazione"]),
                        AssessorName = ValueOperations.DBToString(navisionReader["Nome_Valutatore"]),
                        UserFullName = ValueOperations.DBToString(navisionReader["Nominativo_Utente"]),
                        Grade = ValueOperations.DBToDecimal(navisionReader["Voto"])
                    });
                }

                navisionReader.Close();

                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                string query = "TRUNCATE TABLE EvaluationDetails";
                ExecuteNonQuery(query);
                query = "TRUNCATE TABLE EvaluationTerms";
                ExecuteNonQuery(query);

                SqlParameter[] pars;
                IEnumerator<EvaluationDetail> theEnumerator = theDetails.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    query = @"INSERT INTO EvaluationDetails (AssessorCode,EvaluationTermCode,UserCode,AssessorSurname,
                            EvaluationDate,EvaluationTermDescription,AssessorName,UserFullName,Grade) VALUES (
                            @AssessorCode,@EvaluationTermCode,@UserCode,@AssessorSurname,
                            @EvaluationDate,@EvaluationTermDescription,@AssessorName,@UserFullName,@Grade)";

                    pars = new SqlParameter[]{
                        createParameter("@AssessorCode",theEnumerator.Current.AssessorCode),
                        createParameter("@EvaluationTermCode",theEnumerator.Current.EvaluationTermCode),
                        createParameter("@UserCode",theEnumerator.Current.UserCode),
                        createParameter("@AssessorSurname",theEnumerator.Current.AssessorSurname),
                        createParameter("@EvaluationDate",theEnumerator.Current.EvaluationDate),
                        createParameter("@EvaluationTermDescription",theEnumerator.Current.EvaluationTermDescription),
                        createParameter("@AssessorName",theEnumerator.Current.AssessorName),
                        createParameter("@UserFullName",theEnumerator.Current.UserFullName),
                        createParameter("@Grade",theEnumerator.Current.Grade)
                    };
                    ExecuteNonQuery(query, pars);

                    Evaluation ev = getEvaluationByDate(theEnumerator.Current.EvaluationDate);

                    query = @"INSERT INTO EvaluationTerms (TermCode, TermDescription, EvaluationId,
                              AssessorId, EvalueeId, Grade, ParentTermCode, LastModified) VALUES 
                                (@TermCode, @TermDescription, @EvaluationId,
                              @AssessorId, @EvalueeId, @Grade, @ParentTermCode, GETDATE())";
                    pars = new SqlParameter[]{
                            createParameter("@Grade",theEnumerator.Current.Grade),
                            createParameter("@TermCode",theEnumerator.Current.EvaluationTermCode),
                            createParameter("@EvaluationId",ev.EvaluationId),
                            createParameter("@AssessorId",theEnumerator.Current.AssessorCode),
                            createParameter("@EvalueeId",theEnumerator.Current.UserCode),
                            createParameter("@TermDescription",theEnumerator.Current.EvaluationTermDescription),
                            createParameter("@ParentTermCode","")
                        };
                    try
                    {
                        ExecuteNonQuery(query, pars);
                    }
                    catch (Exception ex2)
                    {
                        logger.Info("Duplicate term entry");
                        logger.Error(ex2);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
                if (navisionConn.State == System.Data.ConnectionState.Open)
                    navisionConn.Close();
            }
        }

        public void updateEvaluationDetailsFromNavision_Partial(DateTime dateFrom, DateTime dateTo)
        {
            bool wasOpen = false;
            SqlConnection navisionConn = new SqlConnection(navisionConnectionString);

            try
            {
                EvaluationDetails theDetails = null;
                navisionConn.Open();
                string navisionQuery = "select *, convert (datetime, data_valutazione,103) as dataint from VALUTAZIONI_DETTAGLIO WHERE convert (datetime, data_valutazione,103) BETWEEN @DateFrom AND @DateTo ORDER BY dataint DESC";
                SqlCommand theCommand = navisionConn.CreateCommand();
                theCommand.CommandText = navisionQuery;
                theCommand.Parameters.Add(new SqlParameter("@DateFrom", dateFrom));
                theCommand.Parameters.Add(new SqlParameter("@DateTo", dateTo));
                SqlDataReader navisionReader = theCommand.ExecuteReader();
                if (navisionReader.HasRows)
                    theDetails = new EvaluationDetails();
                while (navisionReader.Read())
                {
                    theDetails.Add(new EvaluationDetail()
                    {
                        AssessorCode = (ValueOperations.DBToString(navisionReader["Codice_Valutatore"])),
                        EvaluationTermCode = ValueOperations.DBToString(navisionReader["Cod_Parametro_Valutazione"]),
                        UserCode = (ValueOperations.DBToString(navisionReader["Cod_Utente"])),
                        AssessorSurname = ValueOperations.DBToString(navisionReader["Cognome_Valutatore"]),
                        EvaluationDate = DateTime.ParseExact(ValueOperations.DBToString(navisionReader["Data_Valutazione"]), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture),
                        EvaluationTermDescription = ValueOperations.DBToString(navisionReader["Descrizione_Parametro_Valutazione"]),
                        AssessorName = ValueOperations.DBToString(navisionReader["Nome_Valutatore"]),
                        UserFullName = ValueOperations.DBToString(navisionReader["Nominativo_Utente"]),
                        Grade = ValueOperations.DBToDecimal(navisionReader["Voto"])
                    });
                }

                navisionReader.Close();

                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                string query = null;

                SqlParameter[] pars;
                IEnumerator<EvaluationDetail> theEnumerator = theDetails.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    query = @"INSERT INTO EvaluationDetails (AssessorCode,EvaluationTermCode,UserCode,AssessorSurname,
                            EvaluationDate,EvaluationTermDescription,AssessorName,UserFullName,Grade) VALUES (
                            @AssessorCode,@EvaluationTermCode,@UserCode,@AssessorSurname,
                            @EvaluationDate,@EvaluationTermDescription,@AssessorName,@UserFullName,@Grade)";

                    pars = new SqlParameter[]{
                        createParameter("@AssessorCode",theEnumerator.Current.AssessorCode),
                        createParameter("@EvaluationTermCode",theEnumerator.Current.EvaluationTermCode),
                        createParameter("@UserCode",theEnumerator.Current.UserCode),
                        createParameter("@AssessorSurname",theEnumerator.Current.AssessorSurname),
                        createParameter("@EvaluationDate",theEnumerator.Current.EvaluationDate),
                        createParameter("@EvaluationTermDescription",theEnumerator.Current.EvaluationTermDescription),
                        createParameter("@AssessorName",theEnumerator.Current.AssessorName),
                        createParameter("@UserFullName",theEnumerator.Current.UserFullName),
                        createParameter("@Grade",theEnumerator.Current.Grade)
                    };
                    ExecuteNonQuery(query, pars);

                    Evaluation ev = getEvaluationByDate(theEnumerator.Current.EvaluationDate);

                    query = @"INSERT INTO EvaluationTerms (TermCode, TermDescription, EvaluationId,
                              AssessorId, EvalueeId, Grade, ParentTermCode, LastModified) VALUES 
                                (@TermCode, @TermDescription, @EvaluationId,
                              @AssessorId, @EvalueeId, @Grade, @ParentTermCode, GETDATE())";
                    pars = new SqlParameter[]{
                            createParameter("@Grade",theEnumerator.Current.Grade),
                            createParameter("@TermCode",theEnumerator.Current.EvaluationTermCode),
                            createParameter("@EvaluationId",ev.EvaluationId),
                            createParameter("@AssessorId",theEnumerator.Current.AssessorCode),
                            createParameter("@EvalueeId",theEnumerator.Current.UserCode),
                            createParameter("@TermDescription",theEnumerator.Current.EvaluationTermDescription),
                            createParameter("@ParentTermCode","")
                        };
                    try
                    {
                        ExecuteNonQuery(query, pars);
                    }
                    catch (Exception ex2)
                    {
                        logger.Info("Duplicate term entry");
                        logger.Error(ex2);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
                if (navisionConn.State == System.Data.ConnectionState.Open)
                    navisionConn.Close();
            }
        }

        public void updateEvaluationEndDate(int evaluationId, DateTime endDate)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "UPDATE Evaluations SET EndDate=@EndDate WHERE EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EndDate",endDate),
                    createParameter("@EvaluationId",evaluationId),
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationIdInHistory(DateTime dt, int evaluationId)
        {
            bool wasOpen = false;
            try
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                string query = @"UPDATE EvaluationsReceived SET EvaluationId=@EvaluationId 
                                WHERE EvaluationDate=@EvaluationDate";

                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@EvaluationDate",dt)
                    };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }


        public EvaluationPairing getEvaluationPairing(int pairingId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "select * from EvaluationPairings where PairingId=@pairingId";
                SqlParameter[] pars = new SqlParameter[]{
                createParameter("@pairingId", pairingId)
                };
                SqlDataReader reader = ExecuteReader(query, pars);

                if (reader.Read())
                {
                    EvaluationPairing evP = new EvaluationPairing()
                    {
                        PairingId = ValueOperations.DBToInteger(reader["PairingId"]),
                        AssessorId = ValueOperations.DBToString(reader["AssessorId"]),
                        EvaluationId = ValueOperations.DBToInteger(reader["EvaluationId"]),
                        EvalueeId = ValueOperations.DBToString(reader["EvalueeId"])

                    };
                    return evP;
                }
                return null;

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationPairingNotes(int pairingId, string noteCode, string notes)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE EvaluationPairings SET Notes=@Notes, NoteCode=@NoteCode WHERE PairingId=@PairingId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@Notes",notes),
                    createParameter("@NoteCode",noteCode),
                    createParameter("@PairingId",pairingId),
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationPairingStatus(Dictionary<int, PairingStatus> pairings)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                if (pairings != null && pairings.Count > 0)
                {
                    string query = "UPDATE EvaluationPairings SET Status=@Status WHERE PairingId=@PairingId";

                    var pen = pairings.GetEnumerator();
                    while (pen.MoveNext())
                    {
                        SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@Status",pen.Current.Value.ToString()),
                            createParameter("@PairingId",pen.Current.Key),
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationRulesConfiguration(EvaluationRulesConfiguration conf)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE EvaluationRulesConfiguration SET MinimumAssignableRate=@MinimumAssignableRate, 
                                                                        MaximumAssignableRate=@MaximumAssignableRate";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@MinimumAssignableRate",conf.minimumAssignableRate),
                    createParameter("@MaximumAssignableRate",conf.maximumAssignableRate)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationStaffDate(int evaluationId, DateTime staffDate)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "UPDATE Evaluations SET StaffDate=@StaffDate WHERE EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@StaffDate",staffDate),
                    createParameter("@EvaluationId",evaluationId),
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationStartDate(int evaluationId, DateTime startDate)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "UPDATE Evaluations SET StartDate=@StartDate WHERE EvaluationId=@EvaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@StartDate",startDate),
                    createParameter("@EvaluationId",evaluationId),
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationStatus(int evaluationId, EvaluationStatus status)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "UPDATE Evaluations SET EvaluationStatus=@Status WHERE EvaluationId=@EvaluationId";
                logger.Debug("prima di aggiornare lo stato della valutazione:" + query);
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@Status",status.ToString()),
                    createParameter("@EvaluationId",evaluationId),
                };
                logger.Debug("prima di aggiornare lo stato della valutazione: la query è: " + query + "  i parametri sono  " + pars);
                ExecuteNonQuery(query, pars);

                if (status == EvaluationStatus.Closed)
                {
                    query = "UPDATE Evaluations SET ClosureDate=GETDATE() WHERE EvaluationId=@EvaluationId";
                    pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                    };
                    logger.Debug("Dopo aver aggiornato, la seconda query è " + query + "parametri" + pars);
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationTermsConfiguration(EvaluationTerms conf)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE PrivacyConfiguration SET PrivacyConfiguration=@PrivacyConfiguration";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@PrivacyConfiguration",conf.ToString())
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationType1Notes(int evaluationId, string note)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE Evaluations SET Notes=@Notes WHERE EvaluationId=@EvaluationId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@Notes",note)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvaluationType1Notes_old(int evaluationId, string userId, string note)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE EvaluationType1Notes SET Note=@Notes WHERE UserId=@User AND EvaluationId=@EvaluationId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@User",userId),
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@Notes",note)
                };
                int res = ExecuteNonQuery(query, pars);
                if (res == 0)
                {
                    query = "INSERT INTO EvaluationType1Notes (EvaluationId, UserId, Note) VALUES (@EvaluationId, @UserId, @Note)";

                    pars = new SqlParameter[]{
                        createParameter("@UserId",userId),
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@Note",note)
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateEvalueeHistoryFromNavision()
        {
            bool wasOpen = false;
            SqlConnection navisionConn = new SqlConnection(navisionConnectionString);

            try
            {
                EvaluationsReceived theHistory = null;
                navisionConn.Open();
                string navisionQuery = "select * from VALUTAZIONI_RIEPILOGO ORDER BY DataValutazione DESC";
                SqlCommand theCommand = navisionConn.CreateCommand();
                theCommand.CommandText = navisionQuery;
                SqlDataReader navisionReader = theCommand.ExecuteReader();
                if (navisionReader.HasRows)
                    theHistory = new EvaluationsReceived();
                while (navisionReader.Read())
                {
                    theHistory.Add(new EvaluationReceived()
                    {
                        ContractEndDate = ValueOperations.DBToDate(navisionReader["DataFineContratto"]),
                        ContractStartDate = ValueOperations.DBToDate(navisionReader["DataInizioContratto"]),
                        EvaluationDate = ValueOperations.DBToDate(navisionReader["DataValutazione"]),
                        ContractDuration = ValueOperations.DBToInteger(navisionReader["DurataContratto"]),
                        EvaluationNotes = ValueOperations.DBToString(navisionReader["NoteValutazione"]),
                        EmployeeNumber = (ValueOperations.DBToString(navisionReader["NumDipendente"])),
                        NumberOfAssessors = ValueOperations.DBToInteger(navisionReader["NumeroValutatori"]),
                        NextEvaluation = ValueOperations.DBToDate(navisionReader["ProxValutazione"]),
                        Result = ValueOperations.DBToDecimal(navisionReader["Risultato"]),
                        ContractType = ValueOperations.DBToString(navisionReader["TipoContratto"]),
                    });
                }

                navisionReader.Close();

                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                string query = "TRUNCATE TABLE EvaluationsReceived";
                ExecuteNonQuery(query);

                query = @"INSERT INTO EvaluationsReceived (ContractDuration, ContractEndDate,
                            ContractStartDate, ContractType, EmployeeNumber, EvaluationDate,
                            EvaluationNotes, NextEvaluation, Result, NumberOfAssessors) VALUES (
                            @ContractDuration, @ContractEndDate,
                            @ContractStartDate, @ContractType, @EmployeeNumber, @EvaluationDate,
                            @EvaluationNotes, @NextEvaluation, @Result, @NumberOfAssessors)";

                IEnumerator<EvaluationReceived> theEnumerator = theHistory.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@ContractDuration",theEnumerator.Current.ContractDuration),
                        createParameter("@ContractEndDate",theEnumerator.Current.ContractEndDate),
                        createParameter("@ContractStartDate",theEnumerator.Current.ContractStartDate),
                        createParameter("@ContractType",theEnumerator.Current.ContractType),
                        createParameter("@EmployeeNumber",theEnumerator.Current.EmployeeNumber),
                        createParameter("@EvaluationDate",theEnumerator.Current.EvaluationDate),
                        createParameter("@EvaluationNotes",theEnumerator.Current.EvaluationNotes),
                        createParameter("@NextEvaluation",theEnumerator.Current.NextEvaluation),
                        createParameter("@Result",theEnumerator.Current.Result),
                        createParameter("@NumberOfAssessors",theEnumerator.Current.NumberOfAssessors)
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
                if (navisionConn.State == System.Data.ConnectionState.Open)
                    navisionConn.Close();
            }
        }

        public void updateEvalueeHistoryFromNavision_Partial(DateTime dateFrom, DateTime dateTo)
        {
            bool wasOpen = false;
            SqlConnection navisionConn = new SqlConnection(navisionConnectionString);

            try
            {
                EvaluationsReceived theHistory = null;
                navisionConn.Open();
                string navisionQuery = "select * from VALUTAZIONI_RIEPILOGO WHERE DataValutazione BETWEEN @DateFrom AND @DateTo ORDER BY DataValutazione DESC";
                SqlCommand theCommand = navisionConn.CreateCommand();
                theCommand.CommandText = navisionQuery;
                theCommand.Parameters.Add(new SqlParameter("@DateFrom", dateFrom));
                theCommand.Parameters.Add(new SqlParameter("@DateTo", dateTo));
                SqlDataReader navisionReader = theCommand.ExecuteReader();
                if (navisionReader.HasRows)
                    theHistory = new EvaluationsReceived();
                while (navisionReader.Read())
                {
                    theHistory.Add(new EvaluationReceived()
                    {
                        ContractEndDate = ValueOperations.DBToDate(navisionReader["DataFineContratto"]),
                        ContractStartDate = ValueOperations.DBToDate(navisionReader["DataInizioContratto"]),
                        EvaluationDate = ValueOperations.DBToDate(navisionReader["DataValutazione"]),
                        ContractDuration = ValueOperations.DBToInteger(navisionReader["DurataContratto"]),
                        EvaluationNotes = ValueOperations.DBToString(navisionReader["NoteValutazione"]),
                        EmployeeNumber = (ValueOperations.DBToString(navisionReader["NumDipendente"])),
                        NumberOfAssessors = ValueOperations.DBToInteger(navisionReader["NumeroValutatori"]),
                        NextEvaluation = ValueOperations.DBToDate(navisionReader["ProxValutazione"]),
                        Result = ValueOperations.DBToDecimal(navisionReader["Risultato"]),
                        ContractType = ValueOperations.DBToString(navisionReader["TipoContratto"]),
                    });
                }

                navisionReader.Close();

                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                string query = @"INSERT INTO EvaluationsReceived (ContractDuration, ContractEndDate,
                            ContractStartDate, ContractType, EmployeeNumber, EvaluationDate,
                            EvaluationNotes, NextEvaluation, Result, NumberOfAssessors) VALUES (
                            @ContractDuration, @ContractEndDate,
                            @ContractStartDate, @ContractType, @EmployeeNumber, @EvaluationDate,
                            @EvaluationNotes, @NextEvaluation, @Result, @NumberOfAssessors)";

                IEnumerator<EvaluationReceived> theEnumerator = theHistory.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@ContractDuration",theEnumerator.Current.ContractDuration),
                        createParameter("@ContractEndDate",theEnumerator.Current.ContractEndDate),
                        createParameter("@ContractStartDate",theEnumerator.Current.ContractStartDate),
                        createParameter("@ContractType",theEnumerator.Current.ContractType),
                        createParameter("@EmployeeNumber",theEnumerator.Current.EmployeeNumber),
                        createParameter("@EvaluationDate",theEnumerator.Current.EvaluationDate),
                        createParameter("@EvaluationNotes",theEnumerator.Current.EvaluationNotes),
                        createParameter("@NextEvaluation",theEnumerator.Current.NextEvaluation),
                        createParameter("@Result",theEnumerator.Current.Result),
                        createParameter("@NumberOfAssessors",theEnumerator.Current.NumberOfAssessors)
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
                if (navisionConn.State == System.Data.ConnectionState.Open)
                    navisionConn.Close();
            }
        }

        public bool updateEvalueePromotion(int evaluationId, List<string> evalueeIdList)
        {
            bool ret = false;
            if (!ValueOperations.isNullOrEmpty(evalueeIdList))
            {
                bool wasOpen = false;
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                try
                {

                    string query = @"UPDATE EvaluationPromotions SET EmployeeType='Assessor', PromotionStatus='Accepted'  
                                        WHERE EvaluationId=@EvaluationId AND EmployeeId IN (" +
                                            ValueOperations.arrayToDbInClause(evalueeIdList) + ")"; // AND CanBePromoted='True'";
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId)
                    };
                    ret = ExecuteNonQuery(query, pars) > 0;
                }
                catch (SqlException ex)
                {
                    logger.Error(ex, "Error in updating the evaluee promotion, continuing");
                }
                finally
                {
                    if (!wasOpen)
                        connection.Close();
                }
            }
            return ret;
        }

        public void updateFilter(FilterModel theFilter)
        {
            bool wasOpen = false;

            try
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                string query = @"UPDATE Filters SET FilterName=@FilterName, IsAllEvaluation=@IsAllEvaluation, 
                                    IsAssessorHidden=@IsAssessorHidden, IsAverageEvaluation=@IsAverageEvaluation,
                                    IsDepartment=@IsDepartment, IsEvaluated=@IsEvaluated, 
                                    IsEvaluationDate=@IsEvaluationDate, IsEvaluationReceived=@IsEvaluationReceived,
                                    IsEvaluationReminded=@IsEvaluationReminded, IsEvaluationUnexpressed=@IsEvaluationUnexpressed,
                                    IsMaximumExcluded=@IsMaximumExcluded, IsMinimumExcluded=@IsMinimumExcluded,
                                    IsNames=@IsNames, IsNumberEvaluationReceived=@IsNumberEvaluationReceived,
                                    IsOnlySuggestedAssessors=@IsOnlySuggestedAssessors, IsSite=@IsSite,
                                    IsStaffMemberHidden=@IsStaffMemberHidden, 
                                    MaximumAverageEvaluationNumber=@MaximumAverageEvaluationNumber,
                                    MinimumAverageEvaluationNumber=@MinimumAverageEvaluationNumber,
                                    MinimumEvaluationNumber=@MinimumEvaluationNumber, 
                                    MaximumEvaluationNumber=@MaximumEvaluationNumber,
                                    NumberBeforeEvaluationDate=@NumberBeforeEvaluationDate,
									IsExcludeCompiledEvaluations=@IsExcludeCompiledEvaluations,
                                    IsOpenResearch=@IsOpenResearch,
                                    IsCommuter=@IsCommuter,
                                    IsContractType=@IsContractType,
                                    IsExternalEmployee=@IsExternalEmployee,
                                    IsWorkplaceAlgorithm=@IsWorkplaceAlgorithm,
                                    Commuter=@Commuter,
                                    ExternalEmployee=@ExternalEmployee,
                                    IsColorEnable=@IsColorEnable,
                                    IsGreen=@IsGreen,
                                    IsWhite=@IsWhite,
                                    IsOrange=@IsOrange,
                                    IsRed=@IsRed,
                                    IsPurple=@IsPurple
                                    WHERE FilterId=@FilterId";

                SqlParameter[] pars = new SqlParameter[] {
                    createParameter("@FilterName", theFilter.FilterName),
                    createParameter("@IsAllEvaluation", theFilter.IsAllEvaluation),
                    createParameter("@IsAssessorHidden", theFilter.IsAssessorHidden),
                    createParameter("@IsAverageEvaluation", theFilter.IsAverageEvaluation),
                    createParameter("@IsDepartment", theFilter.IsDepartment),
                    createParameter("@IsEvaluated", theFilter.IsEvaluated),
                    createParameter("@IsEvaluationDate", theFilter.IsEvaluationDate),
                    createParameter("@IsEvaluationReceived", theFilter.IsEvaluationReceived),
                    createParameter("@IsEvaluationReminded", theFilter.IsEvaluationReminded),
                    createParameter("@IsEvaluationUnexpressed", theFilter.IsEvaluationUnexpressed),
                    createParameter("@IsMaximumExcluded", theFilter.IsMaximumExcluded),
                    createParameter("@IsMinimumExcluded", theFilter.IsMinimumExcluded),
                    createParameter("@IsNames", theFilter.IsNames),
                    createParameter("@IsNumberEvaluationReceived", theFilter.IsNumberEvaluationReceived),
                    createParameter("@IsOnlySuggestedAssessors", theFilter.IsOnlySuggestedAssessors),
                    createParameter("@IsSite", theFilter.IsSite),
                    createParameter("@IsStaffMemberHidden", theFilter.IsStaffMemberHidden),
                    createParameter("@MaximumAverageEvaluationNumber", theFilter.MaximumAverageEvaluationNumber),
                    createParameter("@MinimumAverageEvaluationNumber", theFilter.MinimumAverageEvaluationNumber),
                    createParameter("@MinimumEvaluationNumber", theFilter.MinimumEvaluationNumber),
                    createParameter("@MaximumEvaluationNumber", theFilter.MaximumEvaluationNumber),
                    createParameter("@NumberBeforeEvaluationDate", theFilter.NumberBeforeEvaluationDate),
                    createParameter("@IsExternalEmployee", theFilter.IsExternalEmployee),
                    createParameter("@IsExcludeCompiledEvaluations", theFilter.IsExcludeCompiledEvaluations),
                    createParameter("@IsCommuter", theFilter.IsCommuter),
                    createParameter("@IsContractType", theFilter.IsContractType),
                    createParameter("@IsWorkplaceAlgorithm", theFilter.IsWorkplaceAlgorithm),
                    createParameter("@Commuter", theFilter.Commuter),
                    createParameter("@ExternalEmployee", theFilter.ExternalEmployee),
                    createParameter("@FilterId", theFilter.FilterId),
                    createParameter("@IsOpenResearch", theFilter.IsOpenResearch),
                    createParameter("@IsColorEnable", theFilter.IsColorEnable),
                    createParameter("@IsGreen", theFilter.IsGreen),
                    createParameter("@IsWhite", theFilter.IsWhite),
                    createParameter("@IsRed", theFilter.IsRed),
                    createParameter("@IsPurple", theFilter.IsPurple),
                    createParameter("@IsOrange", theFilter.IsOrange)
                };

                ExecuteNonQuery(query, pars);

                query = "DELETE FROM FilterNames WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.NamesList))
                {
                    foreach (string fs in theFilter.NamesList)
                    {
                        query = "INSERT INTO FilterNames(FilterId, FilterName) VALUES (@FilterId, @FilterName)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterName",fs)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }


                query = "DELETE FROM FilterDepartments WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.DepartmentList))
                {
                    foreach (string fs in theFilter.DepartmentList)
                    {
                        query = "INSERT INTO FilterDepartments(FilterId, FilterDepartment) VALUES (@FilterId, @FilterDepartment)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterDepartment",fs)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }


                query = "DELETE FROM FilterSites WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.SitesList))
                {
                    foreach (string fs in theFilter.SitesList)
                    {
                        query = "INSERT INTO FilterSites(FilterId, FilterSite) VALUES (@FilterId, @FilterSite)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterSite",fs)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }

                query = "DELETE FROM FilterAlgorithms WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.WorkplaceAlgorithm))
                {
                    foreach (int wa in theFilter.WorkplaceAlgorithm)
                    {
                        query = "INSERT INTO FilterAlgorithms(FilterId, FilterAlgorithm) VALUES (@FilterId, @FilterAlgorithm)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterAlgorithm",wa)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }

                query = "DELETE FROM FilterContractTypes WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.ContractType))
                {
                    foreach (string fs in theFilter.ContractType)
                    {
                        query = "INSERT INTO FilterContractTypes(FilterId, FilterContractType) VALUES (@FilterId, @FilterContractType)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterContractType",fs)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateCustomFilter(CustomFilter theCustomFilter)
        {
            bool wasOpen = false;

            try
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                FilterModel theFilter = theCustomFilter.customFilterModel;
                theFilter.FilterId = theCustomFilter.customFilterHeader.customFilterId;
                theFilter.FilterName = theCustomFilter.customFilterHeader.customFilterName;

                string query = @"UPDATE CustomFilters SET FilterName=@FilterName, IsAllEvaluation=@IsAllEvaluation, 
                                    IsAssessorHidden=@IsAssessorHidden, IsAverageEvaluation=@IsAverageEvaluation,
                                    IsDepartment=@IsDepartment, IsEvaluated=@IsEvaluated, 
                                    IsEvaluationDate=@IsEvaluationDate, IsEvaluationReceived=@IsEvaluationReceived,
                                    IsEvaluationReminded=@IsEvaluationReminded, IsEvaluationUnexpressed=@IsEvaluationUnexpressed,
                                    IsMaximumExcluded=@IsMaximumExcluded, IsMinimumExcluded=@IsMinimumExcluded,
                                    IsNames=@IsNames, IsNumberEvaluationReceived=@IsNumberEvaluationReceived,
                                    IsOnlySuggestedAssessors=@IsOnlySuggestedAssessors, IsSite=@IsSite,
                                    IsStaffMemberHidden=@IsStaffMemberHidden, 
                                    MaximumAverageEvaluationNumber=@MaximumAverageEvaluationNumber,
                                    MinimumAverageEvaluationNumber=@MinimumAverageEvaluationNumber,
                                    MinimumEvaluationNumber=@MinimumEvaluationNumber, 
                                    MaximumEvaluationNumber=@MaximumEvaluationNumber,
                                    NumberBeforeEvaluationDate=@NumberBeforeEvaluationDate,
									IsExcludeCompiledEvaluations=@IsExcludeCompiledEvaluations,
                                    IsOpenResearch=@IsOpenResearch,
                                    IsCommuter=@IsCommuter,
                                    IsContractType=@IsContractType,
                                    IsExternalEmployee=@IsExternalEmployee,
                                    IsWorkplaceAlgorithm=@IsWorkplaceAlgorithm,
                                    Commuter=@Commuter,
                                    ExternalEmployee=@ExternalEmployee
                                    WHERE FilterId=@FilterId";

                SqlParameter[] pars = new SqlParameter[] {
                    createParameter("@FilterName", theFilter.FilterName),
                    createParameter("@IsAllEvaluation", theFilter.IsAllEvaluation),
                    createParameter("@IsAssessorHidden", theFilter.IsAssessorHidden),
                    createParameter("@IsAverageEvaluation", theFilter.IsAverageEvaluation),
                    createParameter("@IsDepartment", theFilter.IsDepartment),
                    createParameter("@IsEvaluated", theFilter.IsEvaluated),
                    createParameter("@IsEvaluationDate", theFilter.IsEvaluationDate),
                    createParameter("@IsEvaluationReceived", theFilter.IsEvaluationReceived),
                    createParameter("@IsEvaluationReminded", theFilter.IsEvaluationReminded),
                    createParameter("@IsEvaluationUnexpressed", theFilter.IsEvaluationUnexpressed),
                    createParameter("@IsMaximumExcluded", theFilter.IsMaximumExcluded),
                    createParameter("@IsMinimumExcluded", theFilter.IsMinimumExcluded),
                    createParameter("@IsNames", theFilter.IsNames),
                    createParameter("@IsNumberEvaluationReceived", theFilter.IsNumberEvaluationReceived),
                    createParameter("@IsOnlySuggestedAssessors", theFilter.IsOnlySuggestedAssessors),
                    createParameter("@IsSite", theFilter.IsSite),
                    createParameter("@IsStaffMemberHidden", theFilter.IsStaffMemberHidden),
                    createParameter("@MaximumAverageEvaluationNumber", theFilter.MaximumAverageEvaluationNumber),
                    createParameter("@MinimumAverageEvaluationNumber", theFilter.MinimumAverageEvaluationNumber),
                    createParameter("@MinimumEvaluationNumber", theFilter.MinimumEvaluationNumber),
                    createParameter("@MaximumEvaluationNumber", theFilter.MaximumEvaluationNumber),
                    createParameter("@NumberBeforeEvaluationDate", theFilter.NumberBeforeEvaluationDate),
                    createParameter("@IsExternalEmployee", theFilter.IsExternalEmployee),
                    createParameter("@IsExcludeCompiledEvaluations", theFilter.IsExcludeCompiledEvaluations),
                    createParameter("@IsCommuter", theFilter.IsCommuter),
                    createParameter("@IsContractType", theFilter.IsContractType),
                    createParameter("@IsWorkplaceAlgorithm", theFilter.IsWorkplaceAlgorithm),
                    createParameter("@Commuter", theFilter.Commuter),
                    createParameter("@ExternalEmployee", theFilter.ExternalEmployee),
                    createParameter("@FilterId", theFilter.FilterId),
                    createParameter("@IsOpenResearch", theFilter.IsOpenResearch)
                };

                ExecuteNonQuery(query, pars);

                query = "DELETE FROM CustomFilterNames WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.NamesList))
                {
                    foreach (string fs in theFilter.NamesList)
                    {
                        query = "INSERT INTO CustomFilterNames(FilterId, FilterName) VALUES (@FilterId, @FilterName)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterName",fs)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }


                query = "DELETE FROM CustomFilterDepartments WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.DepartmentList))
                {
                    foreach (string fs in theFilter.DepartmentList)
                    {
                        query = "INSERT INTO CustomFilterDepartments(FilterId, FilterDepartment) VALUES (@FilterId, @FilterDepartment)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterDepartment",fs)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }


                query = "DELETE FROM CustomFilterSites WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.SitesList))
                {
                    foreach (string fs in theFilter.SitesList)
                    {
                        query = "INSERT INTO CustomFilterSites(FilterId, FilterSite) VALUES (@FilterId, @FilterSite)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterSite",fs)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }

                query = "DELETE FROM CustomFilterAlgorithms WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.WorkplaceAlgorithm))
                {
                    foreach (int wa in theFilter.WorkplaceAlgorithm)
                    {
                        query = "INSERT INTO CustomFilterAlgorithms(FilterId, FilterAlgorithm) VALUES (@FilterId, @FilterAlgorithm)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterAlgorithm",wa)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }

                query = "DELETE FROM CustomFilterContractTypes WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",theFilter.FilterId)
                };

                ExecuteNonQuery(query, pars);

                if (!ValueOperations.isNullOrEmpty(theFilter.ContractType))
                {
                    foreach (string fs in theFilter.ContractType)
                    {
                        query = "INSERT INTO CustomFilterContractTypes(FilterId, FilterContractType) VALUES (@FilterId, @FilterContractType)";

                        pars = new SqlParameter[]{
                            createParameter("@FilterId",theFilter.FilterId),
                            createParameter("@FilterContractType",fs)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }



        public void deleteCustomFilter(int customFilterId)
        {
            bool wasOpen = false;

            try
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                string query = @"DELETE FROM CustomFilters 
                                    WHERE FilterId=@FilterId";

                SqlParameter[] pars = new SqlParameter[] {
                    createParameter("@FilterId", customFilterId),
                };

                ExecuteNonQuery(query, pars);

                query = "DELETE FROM CustomFilterNames WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",customFilterId)
                };

                ExecuteNonQuery(query, pars);

                query = "DELETE FROM CustomFilterDepartments WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",customFilterId)
                };

                ExecuteNonQuery(query, pars);

                query = "DELETE FROM CustomFilterSites WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",customFilterId)
                };

                ExecuteNonQuery(query, pars);

                query = "DELETE FROM CustomFilterAlgorithms WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",customFilterId)
                };

                ExecuteNonQuery(query, pars);

                query = "DELETE FROM CustomFilterContractTypes WHERE FilterId=@FilterId";
                pars = new SqlParameter[]
                {
                    createParameter("@FilterId",customFilterId)
                };

                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateGrade(int evaluationId, string assessorId, string evalueeId, string termCode, decimal grade)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE EvaluationTerms SET Grade=@Grade, LastModified=GETDATE() WHERE TermCode=@TermCode AND 
                                EvaluationId=@EvaluationId AND AssessorId=@AssessorId AND
                                EvalueeId=@EvalueeId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@Grade",grade),
                    createParameter("@TermCode",termCode),
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@AssessorId",assessorId),
                    createParameter("@EvalueeId",evalueeId),
                };
                int res = ExecuteNonQuery(query, pars);
                if (res == 0)
                {
                    EvaluationCriteria_Multilanguage theCriteria = getEvaluationCriteriaConfiguration(evaluationId);
                    string parentCode;
                    EvaluationCriterion_Multilanguage theCrit = theCriteria.findByCode(termCode, out parentCode);
                    if (theCrit != null)
                    {
                        query = @"INSERT INTO EvaluationTerms (TermCode, TermDescription, EvaluationId,
                              AssessorId, EvalueeId, Grade, ParentTermCode, LastModified) VALUES 
                                (@TermCode, @TermDescription, @EvaluationId,
                              @AssessorId, @EvalueeId, @Grade, @ParentTermCode, GETDATE())";
                        pars = new SqlParameter[]{
                            createParameter("@Grade",grade),
                            createParameter("@TermCode",termCode),
                            createParameter("@EvaluationId",evaluationId),
                            createParameter("@AssessorId",assessorId),
                            createParameter("@EvalueeId",evalueeId),
                            createParameter("@TermDescription",""),
                            createParameter("@ParentTermCode",parentCode)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateGradeStatistics(int evaluationId, string evalueeId, decimal averageGrade, int gradesPlanned, int gradesDone)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE GradeStatistics SET AverageGrade=@AverageGrade, GradesPlanned=@GradesPlanned, 
                                GradesReceived=@GradesReceived WHERE 
                                EvaluationId=@EvaluationId AND EmployeeId=@EmployeeId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@AverageGrade",averageGrade),
                    createParameter("@GradesPlanned",gradesPlanned),
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@GradesReceived",gradesDone),
                    createParameter("@EmployeeId",evalueeId),
                };
                int res = ExecuteNonQuery(query, pars);
                if (res == 0)
                {
                    query = @"INSERT INTO GradeStatistics (AverageGrade, GradesPlanned, GradesReceived, EvaluationId, EmployeeId) 
                                                    VALUES (@AverageGrade, @GradesPlanned, @GradesReceived, @EvaluationId, @EmployeeId)";

                    pars = new SqlParameter[]{
                        createParameter("@AverageGrade",averageGrade),
                        createParameter("@GradesPlanned",gradesPlanned),
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@GradesReceived",gradesDone),
                        createParameter("@EmployeeId",evalueeId),
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }


        public void updateAssessorGradeStatistics(int evaluationId, string assessorId, decimal averageGrade, int gradesPlanned, int gradesDone)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE AssessorGradeStatistics SET AverageGrade=@AverageGrade, GradesPlanned=@GradesPlanned, 
                                GradesReceived=@GradesReceived WHERE 
                                EvaluationId=@EvaluationId AND EmployeeId=@EmployeeId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@AverageGrade",averageGrade),
                    createParameter("@GradesPlanned",gradesPlanned),
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@GradesReceived",gradesDone),
                    createParameter("@EmployeeId",assessorId),
                };
                int res = ExecuteNonQuery(query, pars);
                if (res == 0)
                {
                    query = @"INSERT INTO AssessorGradeStatistics (AverageGrade, GradesPlanned, GradesReceived, EvaluationId, EmployeeId) 
                                                    VALUES (@AverageGrade, @GradesPlanned, @GradesReceived, @EvaluationId, @EmployeeId)";

                    pars = new SqlParameter[]{
                        createParameter("@AverageGrade",averageGrade),
                        createParameter("@GradesPlanned",gradesPlanned),
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@GradesReceived",gradesDone),
                        createParameter("@EmployeeId", assessorId),
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }


        public void updateGridColumns(string gridName, GridColumns theColumns, string userId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "DELETE FROM GridColumns WHERE GridName=@GridName AND UserId=@UserId";
                ExecuteNonQuery(query, createParameter("@GridName", gridName), createParameter("@UserId", userId));

                query = "INSERT INTO GridColumns (ColumnName, Visible, GridName, UserId) VALUES (@ColumnName, @Visible, @GridName, @UserId)";

                IEnumerator<GridColumn> theEnumerator = theColumns.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@ColumnName",theEnumerator.Current.columnName),
                        createParameter("@Visible",theEnumerator.Current.visible),
                        createParameter("@GridName",gridName),
                        createParameter("@UserId",userId),
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }


        public void insertGridColumns(string gridName, GridColumns theColumns, string userId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "INSERT INTO GridColumns (ColumnName, Visible, GridName, UserId) VALUES (@ColumnName, @Visible, @GridName, @UserId)";
                IEnumerator<GridColumn> theEnumerator = theColumns.GetEnumerator();
                while (theEnumerator.MoveNext())
                {
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@ColumnName",theEnumerator.Current.columnName),
                        createParameter("@Visible",theEnumerator.Current.visible),
                        createParameter("@GridName",gridName),
                        createParameter("@UserId",userId),
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updatePrivacyConfiguration(PrivacyConfiguration conf)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "TRUNCATE TABLE PrivacyConfiguration";
                ExecuteNonQuery(query);

                query = "INSERT INTO PrivacyConfiguration (UserId, FullName) VALUES (@UserId, @FullName)";
                if (!ValueOperations.isNullOrEmpty(conf.employees))
                {
                    foreach (EmployeeHeader eh in conf.employees)
                    {
                        SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@UserId",eh.employeeId),
                            createParameter("@FullName",eh.fullName)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updatePromotionNotes(int evaluationId, string employeeId, string commentatorId, string notes)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"UPDATE PromotionNotes SET PromotionNotes=@PromotionNotes  
                                        WHERE EvaluationId=@EvaluationId AND EmployeeId=@EmployeeId AND CommentatorId=@CommentatorId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@EvaluationId",evaluationId),
                    createParameter("@PromotionNotes",notes),
                    createParameter("@EmployeeId",employeeId),
                    createParameter("@CommentatorId",commentatorId),
                };
                int res = ExecuteNonQuery(query, pars);
                if (res == 0)
                {
                    query = @"INSERT INTO PromotionNotes (PromotionNotes, EvaluationId, EmployeeId, CommentatorId) 
                              VALUES (@PromotionNotes, @EvaluationId, @EmployeeId, @CommentatorId)";
                    pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@PromotionNotes",notes),
                        createParameter("@EmployeeId",employeeId),
                        createParameter("@CommentatorId",commentatorId),
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updatePromotionRulesConfiguration(PromotionRulesConfiguration conf)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE PromotionRulesConfiguration SET MinimumAverageRate=@MinimumAverageRate,
                                MinimumSeniority=@MinimumSeniority";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@MinimumAverageRate",conf.minimumAverageRate),
                    createParameter("@MinimumSeniority",conf.minimumSeniority)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updatePromotionStatus(int evaluationId, List<string> employeeIdList, PromotionStatus status)
        {
            if (!ValueOperations.isNullOrEmpty(employeeIdList))
            {
                bool wasOpen = false;
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                try
                {

                    string query = @"UPDATE EvaluationPromotions SET PromotionStatus=@PromotionStatus  
                                        WHERE EvaluationId=@EvaluationId AND EmployeeId IN (" +
                                            ValueOperations.arrayToDbInClause(employeeIdList) + ") AND EmployeeType='Assessor'";
                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@PromotionStatus",status.ToString())
                    };
                    ExecuteNonQuery(query, pars);
                }
                catch (SqlException ex)
                {
                    logger.Error(ex, "Error in updating the promotion status, continuing");
                }
                finally
                {
                    if (!wasOpen)
                        connection.Close();
                }
            }
        }

        public void updateRemindersConfiguration(RemindersConfiguration conf)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"UPDATE RemindersConfiguration SET NumberDaysBeforeReminder=@NumberDaysBeforeReminder,
                                NumberEvaluationsBeforeReminder=@NumberEvaluationsBeforeReminder,IsEnabled=@IsEnabled";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@NumberDaysBeforeReminder",conf.numberDaysBeforeReminder),
                    createParameter("@NumberEvaluationsBeforeReminder",conf.numberEvaluationsBeforeReminder),
                    createParameter("@IsEnabled",conf.isEnabled)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateReminderSent(int reminderId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE Reminders SET Status='SENT' WHERE ReminderId=@ReminderId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@ReminderId",reminderId)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateSelfEvaluation(int evaluationId, bool allow)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE Evaluations SET AllowSelfEvaluation=@AllowSelfEvaluation WHERE EvaluationId=@EvaluationId";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@AllowSelfEvaluation",allow),
                    createParameter("@EvaluationId",evaluationId)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateStaffMembersConfiguration(StaffMembersConfiguration conf)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "TRUNCATE TABLE StaffMembersConfiguration";
                ExecuteNonQuery(query);

                query = "INSERT INTO StaffMembersConfiguration (UserId, FullName) VALUES (@UserId, @FullName)";
                if (!ValueOperations.isNullOrEmpty(conf.members))
                {
                    foreach (StaffMember sm in conf.members)
                    {
                        SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@UserId",sm.employeeId),
                            createParameter("@FullName",sm.fullName)
                        };
                        ExecuteNonQuery(query, pars);
                    }
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateTermParentCodes()
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"update evaluationterms set parenttermcode=
                                    (select top(1) ParentCode from EvaluationCriteria 
                                        where evaluationcriteria.criterioncode=evaluationterms.termcode 
                                        and EvaluationCriteria.EvaluationId=EvaluationTerms.EvaluationId)";
                ExecuteNonQuery(query);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateTermParentCodes(List<string> evaluationIdList)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"update evaluationterms set parenttermcode=
                                    (select top(1) ParentCode from EvaluationCriteria 
                                        where evaluationcriteria.criterioncode=evaluationterms.termcode 
                                        and EvaluationCriteria.EvaluationId=EvaluationTerms.EvaluationId)
                                        WHERE EvaluationId IN (" + ValueOperations.arrayToDbInClause(evaluationIdList) + ")";
                ExecuteNonQuery(query);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateType2Note(string noteCode, string noteText)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE Type2NoteDefinitions SET NoteText=@NoteText WHERE NoteCode=@NoteCode";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@NoteCode",noteCode),
                    createParameter("@NoteText",noteText)
                };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }
        public void updateUserUISettings(string employeeId, UserUISettings theSettings)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE UserSettingsUI SET AssessorsPageSize=@AssessorsPageSize, EvalueesPageSize=@EvalueesPageSize WHERE UserID=@UserID";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@UserID",employeeId),
                    createParameter("@EvalueesPageSize",theSettings.evalueesPageSize),
                    createParameter("@AssessorsPageSize",theSettings.assessorsPageSize)
                };
                int res = ExecuteNonQuery(query, pars);
                if (res == 0)
                {
                    query = "INSERT INTO UserSettingsUI (UserID, AssessorsPageSize, EvalueesPageSize) VALUES (@UserID, @AssessorsPageSize, @EvalueesPageSize)";

                    pars = new SqlParameter[]{
                        createParameter("@UserID",employeeId),
                        createParameter("@EvalueesPageSize",theSettings.evalueesPageSize),
                        createParameter("@AssessorsPageSize",theSettings.assessorsPageSize),
                    };
                    ExecuteNonQuery(query, pars);
                }
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }


        private Evaluation toEvaluation(SqlDataReader reader)
        {
            Evaluation ret = new Evaluation()
            {
                EndDate = ValueOperations.DBToDate(reader["EndDate"]),
                EvaluationId = ValueOperations.DBToInteger(reader["EvaluationId"]),
                StartDate = ValueOperations.DBToDate(reader["StartDate"]),
                Status = Enums.GetEnumFromName<EvaluationStatus>(reader["EvaluationStatus"].ToString()),
                StaffDate = ValueOperations.DBToDate(reader["StaffDate"]),
                ClosureDate = ValueOperations.DBToDate(reader["ClosureDate"]),
                Notes = ValueOperations.DBToString(reader["Notes"])
            };

            return ret;
        }

        public bool DowngradingAssessors(string userId, List<string> assessorsToDownIdList)
        {
            bool ret = false;
            if (!ValueOperations.isNullOrEmpty(assessorsToDownIdList))
            {
                bool wasOpen = false;
                if (connection.State == System.Data.ConnectionState.Open)
                    wasOpen = true;
                else
                    connection.Open();

                try
                {
                    foreach (string atd in assessorsToDownIdList)
                    {
                        try
                        {
                            string query = @"INSERT INTO DowngradedAssessors (AssessorId, InsertDate, UserId) VALUES (@AssId,@InsertDate,@UserId)";

                            SqlParameter[] pars = new SqlParameter[]{
                                    createParameter("@AssId", atd),
                                    createParameter("@InsertDate", DateTime.Now.ToShortDateString()),
                                    createParameter("@UserId", userId)
                                };
                            ret = ExecuteNonQuery(query, pars) > 0;
                        }
                        catch (SqlException ex)
                        {
                            logger.Error(ex, "Error in Downgrading Assessor " + atd);
                        }
                    }
                }
                finally
                {
                    if (!wasOpen)
                        connection.Close();
                }
            }
            return ret;
        }

        public List<string> GetDowngradedAssessorsList()
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"Select AssessorId from DowngradedAssessors";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(reader["AssessorId"].ToString());
                }
                reader.Close();

            }

            catch (SqlException ex)
            {
                logger.Error(ex, "Error in getting Downgraded Assessors");
                throw new Exception();
            }
            catch (Exception ex)
            {
                logger.Error("Errore durante la cancellazione dalla tabella di rimozione valutatori");
                throw new Exception();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public List<string> GetDowngradedAssessors(int evaluationId)
        {
            List<string> ret = new List<string>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"Select AssessorId from DowngradedAssessors 
                                EXCEPT 
                                SELECT DISTINCT AssessorId FROM EvaluationPairings WHERE EvaluationId=@EvaluationId";
                SqlDataReader reader = ExecuteReader(query, createParameter("@EvaluationId", evaluationId));
                while (reader.Read())
                {
                    ret.Add(reader["AssessorId"].ToString());
                }
                reader.Close();

            }

            catch (SqlException ex)
            {
                logger.Error(ex, "Error in getting Downgraded Assessors");
                throw new Exception();
            }
            catch (Exception ex)
            {
                logger.Error("Errore durante la cancellazione dalla tabella di rimozione valutatori");
                throw new Exception();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }



        public void RemoveFromDowngrade(List<string> idsToRemove)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                idsToRemove.ForEach(id =>
                {
                    string query = @"DELETE FROM DowngradedAssessors WHERE AssessorId=@AssessorId";

                    SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@AssessorId",id)
                };
                    ExecuteNonQuery(query, pars);
                });
            }
            catch (Exception ex)
            {
                logger.Error("Errore durante la cancellazione dalla tabella di rimozione valutatori");
                throw new Exception();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }
        #endregion

        #region Notifications

        public void getEmailConfiguration(UnitecConfiguration conf)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "SELECT * FROM EmailConfiguration";

                SqlDataReader reader = ExecuteReader(query);
                if (reader.Read())
                {
                    conf.EmailSmtpAlternativeAlertAddress = ValueOperations.DBToString(reader["SmtpAlternativeAlertAddress"]);
                    conf.EmailSmtpAlternativeHost = ValueOperations.DBToString(reader["SmtpAlternativeHost"]);
                    conf.EmailSmtpAlternativePassword = ValueOperations.DBToString(reader["SmtpAlternativePassword"]);
                    conf.EmailSmtpAlternativePort = ValueOperations.DBToString(reader["SmtpAlternativePort"]);
                    conf.EmailSmtpAlternativeSenderAddress = ValueOperations.DBToString(reader["SmtpAlternativeSenderAddress"]);
                    conf.EmailSmtpAlternativeSenderName = ValueOperations.DBToString(reader["SmtpAlternativeSenderName"]);
                    conf.EmailSmtpAlternativeUsername = ValueOperations.DBToString(reader["SmtpAlternativeUsername"]);
                    conf.EmailSmtpAlternativeUseSSL = ValueOperations.DBToString(reader["SmtpAlternativeUseSSL"]);
                    conf.EmailSmtpHost = ValueOperations.DBToString(reader["SmtpHost"]);
                    conf.EmailSmtpPassword = ValueOperations.DBToString(reader["SmtpPassword"]);
                    conf.EmailSmtpPort = ValueOperations.DBToString(reader["SmtpPort"]);
                    conf.EmailSmtpSenderAddress = ValueOperations.DBToString(reader["SmtpSenderAddress"]);
                    conf.EmailSmtpSenderName = ValueOperations.DBToString(reader["SmtpSenderName"]);
                    conf.EmailSmtpUsername = ValueOperations.DBToString(reader["SmtpUsername"]);
                    conf.EmailSmtpUseSSL = ValueOperations.DBToString(reader["SmtpUseSSL"]);
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }


        public CP_SystemNotification getNotification(string notiId)
        {
            CP_SystemNotification ret = new CP_SystemNotification();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();
            try
            {
                //Insert the parking into the DB
                string query = "SELECT * FROM SystemNotifications WHERE SystemNotificationId=@notiID";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@notiID",notiId)
                };


                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = ToSystemNotification(reader);
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
            return ret;
        }

        public CP_SystemNotification getNotificationByObjectId(string objectId)
        {
            CP_SystemNotification ret = new CP_SystemNotification();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                //Insert the parking into the DB
                string query = "SELECT * FROM SystemNotifications WHERE ObjectId=@ObjectId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@ObjectId",objectId)
                };

                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    ret = ToSystemNotification(reader);
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
            return ret;
        }

        public void getNotificationTemplate(string templateId, out string templateSubject, out string templateBody)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "SELECT * FROM NotificationTemplates WHERE TemplateId=@TemplateId";
                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@TemplateId",templateId)
                };

                templateSubject = "";
                templateBody = "";
                SqlDataReader reader = ExecuteReader(query, pars);
                if (reader.Read())
                {
                    templateSubject = ValueOperations.DBToString(reader["Subject"]);
                    templateBody = ValueOperations.DBToString(reader["Body"]);
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }
        public void insertSystemNotification(CP_SystemNotification noti)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                DateTime timestamp = DateTime.Now;
                //Insert the parking into the DB
                string query = @"INSERT INTO [dbo].[SystemNotifications]
                                ([TemplateId],[DateInserted],[Name],[Result],[Event],[Module],[Method],
                                [RecipientName],[RecipientAddress],[MessageSubject],[HTMLBody],[PlaintextBody],[ObjectId],
                                [CP_Username], ButtonOkText, ButtonKoText, ButtonFinalNoText, ConfirmedOn, 
                                ConfirmationStatus, SendOn, ButtonOkUrl) VALUES (@TemplateId,GETDATE(),@Name,@Result,
                                @Event,@Module,@Method,
                                @RecipientName,@RecipientAddress,@MessageSubject,@HTMLBody,@PlaintextBody,@ObjectId,
                                @CP_Username, @ButtonOkText, @ButtonKoText, @ButtonFinalNoText, @ConfirmedOn, 
                                @ConfirmationStatus, @SendOn, @ButtonOkUrl)";


                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@TemplateId",noti.templateId),
                    createParameter("@Name",noti.name),
                    createParameter("@Result","READY"),
                    createParameter("@Event",noti.notificationEvent),
                    createParameter("@Module",noti.notificationModule),
                    createParameter("@Method",noti.notificationMethod),
                    createParameter("@RecipientName", optionalEncryption( noti.recipientName)),
                    createParameter("@RecipientAddress",optionalEncryption(noti.recipientAddress)),
                    createParameter("@MessageSubject",optionalEncryption(noti.messageSubject)),
                    createParameter("@HTMLBody",optionalEncryption(noti.htmlBody)),
                    createParameter("@PlaintextBody",optionalEncryption(noti.plaintextBody)),
                    createParameter("@ObjectId",noti.objectId),
                    createParameter("@CP_Username",noti.cpUsername),
                    createParameter("@ButtonOkText",noti.buttonOkText),
                    createParameter("@ButtonKoText",noti.buttonKoText),
                    createParameter("@ButtonFinalNoText",noti.buttonFinalNoText),
                    createParameter("@ConfirmedOn",noti.confirmedOn),
                    createParameter("@ConfirmationStatus",(int)noti.confirmationStatus),
                    createParameter("@SendOn",noti.sendOn),
                    createParameter("@ButtonOkUrl",noti.buttonOkUrl)
                };

                ExecuteNonQuery(query, pars);
            }
            catch (SqlException sex)
            {
                logger.Info("Error inserting notification: " + sex.Message);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public List<CP_SystemNotification> lockSystemNotificationsToSend()
        {
            List<CP_SystemNotification> ret = new List<CP_SystemNotification>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                DateTime timestamp = DateTime.Now;
                //Insert the parking into the DB
                string query = "UPDATE SystemNotifications SET LockDate=@timestamp WHERE SystemNotificationId IN " +
                                "(SELECT top 4 ni.SystemNotificationId FROM SystemNotifications ni WHERE ni.Method IN ('EMAIL','PUSH') and " +
                                "(ni.Result='READY' OR " +
                                "(ni.Result='FAILED' and ni.attempts<30) OR " +
                                "(ni.Result='FAILED' and ni.FailureReason LIKE '%ppp1%')) AND " +
                                "(ni.LockDate is null OR ni.LockDate<DATEADD(mi, -30, GETDATE())) order by templateid, dateInserted)";

                SqlParameter[] pars = new SqlParameter[]{
                    createParameter("@timestamp",timestamp)
                };

                ExecuteNonQuery(query, pars);

                query = "SELECT * FROM SystemNotifications WHERE LockDate=@timestamp";
                pars = new SqlParameter[]{
                    createParameter("@timestamp",timestamp)
                };

                SqlDataReader reader = ExecuteReader(query, pars);
                while (reader.Read())
                {
                    ret.Add(new CP_SystemNotification()
                    {
                        cpUsername = reader["CP_Username"].ToString(),
                        dateInserted = ValueOperations.DBToDate(reader["DateInserted"]),
                        dateSent = ValueOperations.DBToDate(reader["DateSent"]),
                        failureReason = reader["FailureReason"].ToString(),
                        htmlBody = optionalDecryption(reader["HTMLBody"].ToString()),
                        plaintextBody = optionalDecryption(reader["PlaintextBody"].ToString()),
                        messageSubject = optionalDecryption(reader["MessageSubject"].ToString()),
                        name = reader["Name"].ToString(),
                        notificationEvent = reader["Event"].ToString(),
                        notificationId = ValueOperations.DBToInteger(reader["SystemNotificationId"]),
                        notificationMethod = reader["Method"].ToString(),
                        notificationModule = reader["Module"].ToString(),
                        objectId = reader["ObjectId"].ToString(),
                        recipientAddress = optionalDecryption(reader["RecipientAddress"].ToString()),
                        recipientName = optionalDecryption(reader["RecipientName"].ToString()),
                        result = reader["Result"].ToString(),
                        templateId = reader["TemplateId"].ToString()
                    });
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
            return ret;
        }

        public CP_SystemNotification ToSystemNotification(SqlDataReader reader)
        {
            CP_SystemNotification ret = new CP_SystemNotification()
            {
                cpUsername = reader["CP_Username"].ToString(),
                dateInserted = ValueOperations.DBToDate(reader["DateInserted"]),
                dateSent = ValueOperations.DBToDate(reader["DateSent"]),
                failureReason = reader["FailureReason"].ToString(),
                htmlBody = optionalDecryption(reader["HTMLBody"].ToString()),
                plaintextBody = optionalDecryption(reader["PlaintextBody"].ToString()),
                messageSubject = optionalDecryption(reader["MessageSubject"].ToString()),
                name = reader["Name"].ToString(),
                notificationEvent = reader["Event"].ToString(),
                notificationId = ValueOperations.DBToInteger(reader["SystemNotificationId"]),
                notificationMethod = reader["Method"].ToString(),
                notificationModule = reader["Module"].ToString(),
                objectId = reader["ObjectId"].ToString(),
                recipientAddress = optionalDecryption(reader["RecipientAddress"].ToString()),
                recipientName = optionalDecryption(reader["RecipientName"].ToString()),
                result = reader["Result"].ToString(),
                templateId = reader["TemplateId"].ToString(),
                buttonFinalNoText = reader["ButtonFinalNoText"].ToString(),
                buttonKoText = reader["ButtonKoText"].ToString(),
                buttonOkText = reader["ButtonOkText"].ToString(),
                confirmedOn = ValueOperations.DBToDate(reader["ConfirmedOn"]),
                confirmationStatus = (CP_NotificationConfirmationStatus)ValueOperations.DBToInteger(reader["ConfirmationStatus"]),
                sendOn = ValueOperations.DBToDate(reader["SendOn"]),
                buttonOkUrl = reader["ButtonOkUrl"].ToString(),
            };
            return ret;
        }
        public void updateSystemNotificationConfirmation(int notificationId, CP_NotificationConfirmationStatus status)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = "UPDATE SystemNotifications SET ConfirmationStatus=@status, ConfirmedOn=GETDATE() WHERE SystemNotificationId=@notificationId";
                ExecuteNonQuery(query, createParameter("@notificationId", notificationId),
                                        createParameter("@status", (int)status));

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }

        public void updateSystemNotificationResult(int notificationId, string result, string failureReason)
        {
            updateSystemNotificationResult(notificationId, result, failureReason, null);
        }

        public void updateSystemNotificationResult(int notificationId, string result, string failureReason, DateTime? sendOn)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                DateTime timestamp = DateTime.Now;
                //Insert the parking into the DB
                string query = null;
                SqlParameter[] pars = null;
                switch (result)
                {
                    case "SENT":
                        query = "UPDATE SystemNotifications SET LockDate=NULL, FailureReason=NULL, Result='SENT', DateSent=GETDATE() WHERE SystemNotificationId=@notificationId";
                        pars = new SqlParameter[]{
                            createParameter("@notificationId",notificationId)
                        };
                        break;
                    case "READY":
                        query = "UPDATE SystemNotifications SET LockDate=NULL, FailureReason=NULL, Result='READY', DateSent=NULL, SendOn=@SendOn WHERE SystemNotificationId=@notificationId";
                        pars = new SqlParameter[]{
                            createParameter("@notificationId",notificationId),
                            createParameter("@SendOn",sendOn)
                        };
                        break;
                    default:
                        query = "UPDATE SystemNotifications SET LockDate=NULL, FailureReason=@reason, Result='FAILED', ATTEMPTS=ATTEMPTS+1 WHERE SystemNotificationId=@notificationId";
                        pars = new SqlParameter[]{
                            createParameter("@notificationId",notificationId),
                            createParameter("@reason",failureReason)
                        };
                        break;
                }

                ExecuteNonQuery(query, pars);

            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }



        #region Utils
        public string GetTrend(string evalueeId, int evaluationId, decimal tollerancePerc)
        {
            try
            {
                decimal averageGrade = 0;
                decimal pastAverageGrade = 0;
                var gradeStats = getGradeStatistics(evaluationId, evalueeId);
                if (gradeStats != null)
                    averageGrade = gradeStats.averageGrade;
                var pastEvaluation = getLastEvalueeHistoryRecord(evalueeId, evaluationId);
                if (pastEvaluation != null)
                {
                    var pastEvalGrade = getGradeStatistics(pastEvaluation.EvaluationId, evalueeId);
                    pastAverageGrade = pastEvalGrade.averageGrade;
                }
                if (averageGrade != 0)
                {
                    var tolerance = tollerancePerc;
                    var tl = (pastAverageGrade * tolerance) / 100;
                    var pastAverageGradeUp = pastAverageGrade + tl;
                    var pastAverageGradedown = pastAverageGrade - tl;
                    if (averageGrade > pastAverageGradeUp)
                    {
                        return "up";
                    }
                    else if (averageGrade < pastAverageGradedown)
                    {
                        //evaluate.AverageRatingTrend =
                        return "down";
                    }
                    else
                    {
                        return "equal";
                    }
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public string GetTrendAbsolute(string evalueeId, int evaluationId, decimal tolleranceAbsolute)
        {
            try
            {
                decimal averageGrade = 0;
                decimal pastAverageGrade = 0;
                var gradeStats = getGradeStatistics(evaluationId, evalueeId);
                if (gradeStats != null)
                    averageGrade = gradeStats.averageGrade;
                var pastEvaluation = getLastEvalueeHistoryRecord(evalueeId, evaluationId);
                if (pastEvaluation != null)
                {
                    var pastEvalGrade = getGradeStatistics(pastEvaluation.EvaluationId, evalueeId);
                    pastAverageGrade = pastEvalGrade.averageGrade;
                }

                if (averageGrade == 0)
                    return "";

                if (averageGrade - pastAverageGrade == 0 || Math.Abs(averageGrade - pastAverageGrade) < tolleranceAbsolute)
                    return "equal";

                if (averageGrade < pastAverageGrade)
                    return "down";

                return "up";
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private decimal GetAvarageOnTerms(string criterionCode, List<EvaluationTermsAndNotes> grades)
        {
            decimal sum = 0;
            int count = 0;

            foreach (EvaluationTermsAndNotes etn in grades)
            {
                if (etn.Terms == null || etn.Terms.Count == 0)
                    continue;

                foreach (EvaluationTerm t in etn.Terms)
                {
                    if (t.TermCode == criterionCode)
                    {
                        if (t.Grade == 0)
                            break;

                        sum = sum + t.Grade;
                        count++;
                        continue;
                    }

                    foreach (EvaluationTerm k in t.SubTerms)
                    {
                        if (k.TermCode == criterionCode)
                        {
                            if (k.Grade == 0)
                                break;

                            sum = sum + k.Grade;
                            count++;
                            break;
                        }
                    }
                }
            }

            if (count == 0)     //non trovato neanche uno
                return 0;

            if (count == 1)     //trovato 1 solo.. la media è lui
                return sum;

            //faccio la media
            return (sum / count);
        }

        public string GetTrendAbsolutePerVoce(string evalueeId, int evaluationId, string criterionCode, decimal tolleranceAbsolute, List<EvaluationTermsAndNotes> currGrades, List<EvaluationTermsAndNotes> pastGrades)
        {
            try
            {
                //decimal averageGrade        = 0;
                decimal pastAverageGrade = 0;

                //EvaluationGrades currEvaluationGrades = this.getEvalueeGradesSimplified(getEvaluation(evaluationId), getEmployee(evalueeId, false), false, new Dictionary<string, Employee>(), new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), false);

                //EvaluationTermsAndNotes gradeStats = null;

                //decimal sum = 0;
                //int count = 0;
                //foreach (EvaluationTermsAndNotes etn in currGrades)
                //{
                //    if (etn.Terms == null || etn.Terms.Count == 0)
                //        continue;

                //    foreach (EvaluationTerm t in etn.Terms)
                //    {
                //        if (t.TermCode == criterionCode)
                //        {
                //            if (t.Grade == 0)
                //                break;

                //            sum = sum + t.Grade;
                //            count++;
                //            continue;
                //        }

                //        foreach (EvaluationTerm k in t.SubTerms)
                //        {
                //            if (k.TermCode == criterionCode)
                //            {
                //                if (k.Grade == 0)
                //                    break;

                //                sum = sum + k.Grade;
                //                count++;
                //                break;
                //            }
                //        }
                //    }
                //}

                //if (count == 0)
                //{
                //    //non trovato neanche uno
                //}
                //if (count == 1)
                //{
                //    //trovato 1 sol.. la media è lui
                //}
                //if (count > 1)
                //{
                //    //faccio la media

                //}

                //if (gradeStats != null)
                //    averageGrade = gradeStats.AverageGrade;

                decimal averageGrade = GetAvarageOnTerms(criterionCode, currGrades);
                if (averageGrade == 0)
                    return "";

                //EvaluationTermsAndNotes pastEvalGrade = null;
                //var pastEvaluation = getLastEvalueeHistoryRecord(evalueeId, evaluationId);
                //if (pastEvaluation != null)
                //{
                //    EvaluationGrades pastEvaluationGrades = this.getEvalueeGradesSimplified(getEvaluation(pastEvaluation.EvaluationId), getEmployee(evalueeId, false), false, new Dictionary<string, Employee>(), new Dictionary<string, bool>(), new Dictionary<string, AssessorStatistics>(), false);

                //    foreach (EvaluationTermsAndNotes etn in pastEvaluationGrades.grades)
                //    {
                //        EvaluationTerm pastEvaluationTerm = etn.Terms.Where(j => j.TermCode == criterionCode).SingleOrDefault();
                //        if (pastEvaluationTerm != null)
                //        {
                //            pastEvalGrade = etn;
                //            break;
                //        }
                //    }
                //}

                //if (pastEvalGrade != null)
                //    pastAverageGrade = pastEvalGrade.AverageGrade;
                if (pastGrades == null)
                    return "equal";

                if (pastGrades != null)
                    pastAverageGrade = GetAvarageOnTerms(criterionCode, pastGrades);

                //se nella valutazione precedente non c'è voto (pastAverageGrade=0) ritorniamo equal
                if (averageGrade > 0 && pastAverageGrade == 0)
                    return "equal";

                //se la valutazione precedente è uguale alla precedente o se la differenza è minore di un epsilon ritorniamo equal
                if (averageGrade - pastAverageGrade == 0 || Math.Abs(averageGrade - pastAverageGrade) < tolleranceAbsolute)
                    return "equal";

                if (averageGrade < pastAverageGrade)
                    return "down";

                return "up";
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public EvaluationGrades getEvalueeGradesSimplified(Evaluation theEvaluation, Employee theEvaluee, bool getSelfEvaluations,
                                                   Dictionary<string, Employee> assessors,
                                                   Dictionary<string, bool> assessorsCanSelfEvaluate,
                                                   Dictionary<string, AssessorStatistics> assessorStatisticsStore, bool getPhoto)
        {
            string evalueeId = theEvaluee.EmployeeNumber;
            int evaluationId = theEvaluation.EvaluationId;
            DateTime beforeStart = DateTime.Now;
            logger.Info("get evaluee grades of user " + evalueeId + " evaluation " + evaluationId + " selfEvaluation=" + getSelfEvaluations);
            EvaluationGrades ret = new EvaluationGrades();
            ret.evaluation = theEvaluation;
            ret.employeeId = evalueeId;
            ret.employeeData = theEvaluee;
            logger.Info("time for evaluee data (ms): " + (int)(DateTime.Now - beforeStart).TotalMilliseconds);
            DateTime beforePairings = DateTime.Now;
            //Chiamo il metodo per le accoppiate, ma senza calcolare i reminder che sono l'operazione più onerosa
            List<EvaluationPairing> pairings = this.getEvalueePairingsWithoutReminders(evaluationId, evalueeId, getSelfEvaluations);

            List<string> assessorsWhoGraded = this.getAssessorsWhoGraded(evaluationId, evalueeId);
            logger.Info("time for pairings (ms): " + (int)(DateTime.Now - beforePairings).TotalMilliseconds);
            DateTime beforeCriteria = DateTime.Now;
            EvaluationCriteria_Multilanguage criteriaMultilanguage = getEvaluationCriteriaConfiguration(evaluationId);
            logger.Info("time for criteria (ms): " + (int)(DateTime.Now - beforeCriteria).TotalMilliseconds);
            int gradesDone = 0;
            if (!ValueOperations.isNullOrEmpty(pairings))
            {
                foreach (EvaluationPairing ep in pairings)
                {
                    EvaluationTermsAndNotes theTerms = new EvaluationTermsAndNotes();
                    DateTime beforeTerms = DateTime.Now;
                    if (assessorsWhoGraded.Contains(ep.AssessorId))
                        theTerms.Terms = this.getEvaluationTerms(evaluationId, evalueeId, ep.AssessorId, criteriaMultilanguage);
                    else
                        theTerms.Terms = new EvaluationTerms();
                    logger.Info("time for terms (ms): " + (int)(DateTime.Now - beforeTerms).TotalMilliseconds);
                    DateTime beforeEmployees = DateTime.Now;
                    if (!assessors.ContainsKey(ep.AssessorId))
                        assessors[ep.AssessorId] = this.getEmployeeSimplified(ep.AssessorId);
                    theTerms.Assessor = assessors[ep.AssessorId];
                    if (theTerms.Assessor != null)
                    {
                        theTerms.Evaluee = theEvaluee;
                        logger.Info("time for employees (ms): " + (int)(DateTime.Now - beforeEmployees).TotalMilliseconds);
                        DateTime beforeSelfEvaluations = DateTime.Now;
                        if (!assessorsCanSelfEvaluate.ContainsKey(ep.AssessorId))
                            assessorsCanSelfEvaluate[ep.AssessorId] = canSelfEvaluate(evaluationId, ep.AssessorId);
                        theTerms.AssessorCanSelfEvaluate = assessorsCanSelfEvaluate[ep.AssessorId];
                        theTerms.Pairing = ep;
                        logger.Info("time for self evaluations (ms): " + (int)(DateTime.Now - beforeSelfEvaluations).TotalMilliseconds);
                        DateTime beforeAssessorStatistics = DateTime.Now;
                        if (theTerms.Terms.Count > 0)
                        {
                            theTerms.AverageGrade = computeAverage(theTerms.Terms);
                            if (theTerms.AverageGrade > 0)
                                gradesDone++;
                        }
                        ret.grades.Add(theTerms);
                        logger.Info("time for assessor statistics (ms): " + (int)(DateTime.Now - beforeAssessorStatistics).TotalMilliseconds);
                    }
                }
                if (ret.grades.Count > 0)
                    ret.grades = ret.grades.OrderBy(x => x.Assessor.Surname).ThenBy(x => x.Assessor.Name).ToList();
                if (theEvaluee.GradeStats != null && theEvaluee.GradeStats.averageGrade > 0)
                    ret.averageGrade = theEvaluee.GradeStats.averageGrade;
                else
                {
                    if (ret.grades.Count > 0 && ret.grades.Where(num => num.AverageGrade > 0).Any())
                        ret.averageGrade = ret.grades.Where(num => num.AverageGrade > 0).Average(num => num.AverageGrade);
                }

            }
            else
                logger.Info("no pairings");
            return ret;
        }

        public bool canSelfEvaluate(int evaluationId, string employeeId)
        {
            bool ret = false;
            List<EvaluationPairing> pairings = this.getEvalueePairings(evaluationId, employeeId, true);
            ret = !ValueOperations.isNullOrEmpty(pairings);
            return ret;
        }
        public decimal computeAverage(EvaluationTerms terms)
        {
            decimal total = 0;
            int numTerms = 0;
            double totalWeight = 0;
            foreach (EvaluationTerm et in terms)
            {
                decimal subtotal = 0;
                int numSubterms = 0;
                if (et.SubTerms.Any())
                {
                    foreach (EvaluationTerm st in et.SubTerms)
                    {
                        if (st.Grade > 0)
                        {
                            subtotal += st.Grade;
                            numSubterms++;
                        }
                    }
                }

                if (et.Grade > 0)
                {
                    subtotal += et.Grade;
                    numSubterms++;
                }
                else
                {
                    if (subtotal > 0 && numSubterms > 0)
                        et.Grade = subtotal / numSubterms;
                    else
                        et.Grade = 0;
                }

                if (et.CriterionWeight > 0 && numSubterms > 0)
                {
                    total += (subtotal / numSubterms) * Convert.ToDecimal(et.CriterionWeight);
                    totalWeight += et.CriterionWeight;
                    numTerms++;
                }

            }

            if (numTerms > 0 && totalWeight > 0)
                return total / Convert.ToDecimal(totalWeight);
            else
                return 0;
        }

        public EvaluationStatus GetStatusByString(string status)
        {
            switch (status)
            {
                case "Closed":
                    return EvaluationStatus.Closed;
                case "Expired":
                    return EvaluationStatus.Expired;
                case "Imminent":
                    return EvaluationStatus.Imminent;
                default:
                    return EvaluationStatus.Expired;
            }

            return EvaluationStatus.Expired;
        }
        #endregion

        #endregion

        #region Users
        public ICollection<User> getNoSystemUsers()
        {
            ICollection<User> ret = new List<User>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select e.EmployeeNumber, e.Email, e.Name, e.Surname, e.Password, e.RoleId from [dbo].Employees e where e.password is not null and len(e.password) > 0";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(new User
                    {
                        Username = reader["EmployeeNumber"].ToString(),
                        Password = optionalDecryption(reader["Password"].ToString()),
                        Email = optionalDecryption(reader["Email"].ToString()),
                        Id = reader["EmployeeNumber"].ToString(),
                        Lastname = (reader["Surname"] is DBNull ? default(string) : optionalDecryption(reader["Surname"].ToString())),
                        Name = (reader["Name"] is DBNull ? default(string) : optionalDecryption(reader["Name"].ToString())),
                        RoleId = (reader["RoleId"] is DBNull ? 0 : int.Parse(reader["RoleId"].ToString()))

                    });
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public ICollection<User> getNoSystemUsersToAdd()
        {
            ICollection<User> ret = new List<User>();

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select e.EmployeeNumber, e.Email, e.Name, e.Surname, e.Password from [dbo].Employees e where e.password is null or len(e.password) = 0";
                SqlDataReader reader = ExecuteReader(query);
                while (reader.Read())
                {
                    ret.Add(new User
                    {
                        Username = reader["EmployeeNumber"].ToString(),
                        Password = optionalDecryption(reader["Password"].ToString()),
                        Email = optionalDecryption(reader["Email"].ToString()),
                        Id = reader["EmployeeNumber"].ToString(),
                        Lastname = (reader["Surname"] is DBNull ? default(string) : optionalDecryption(reader["Surname"].ToString())),
                        Name = (reader["Name"] is DBNull ? default(string) : optionalDecryption(reader["Name"].ToString()))
                    });
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public User getNoSystemUserById(string id)
        {
            User ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select e.EmployeeNumber, e.Email, e.Name, e.Surname, e.Password from [dbo].Employees e where e.EmployeeNumber = @id";
                SqlDataReader reader = ExecuteReader(query, new SqlParameter("@id", id));
                while (reader.Read())
                {
                    ret = new User
                    {
                        Username = reader["EmployeeNumber"].ToString(),
                        Password = optionalDecryption(reader["Password"].ToString()),
                        Email = optionalDecryption(reader["Email"].ToString()),
                        Id = reader["EmployeeNumber"].ToString(),
                        Lastname = (reader["Surname"] is DBNull ? default(string) : optionalDecryption(reader["Surname"].ToString())),
                        Name = (reader["Name"] is DBNull ? default(string) : optionalDecryption(reader["Name"].ToString()))
                    };
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public User getNoSystemUserByUsernameAndPassword(string username, string password)
        {
            User ret = null;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"select e.EmployeeNumber, e.Email, e.Name, e.Surname, e.Password from [dbo].Employees e where e.EmployeeNumber = @username and e.password is not null and len(e.password) > 0";
                SqlDataReader reader = ExecuteReader(query, new SqlParameter("@username", username));
                while (reader.Read())
                {
                    if (optionalDecryption(reader["Password"].ToString()).Equals(password, StringComparison.InvariantCulture))
                    {
                        ret = new User
                        {
                            Username = reader["EmployeeNumber"].ToString(),
                            Password = optionalDecryption(reader["Password"].ToString()),
                            Email = optionalDecryption(reader["Email"].ToString()),
                            Id = reader["EmployeeNumber"].ToString(),
                            Lastname = (reader["Surname"] is DBNull ? default(string) : optionalDecryption(reader["Surname"].ToString())),
                            Name = (reader["Name"] is DBNull ? default(string) : optionalDecryption(reader["Name"].ToString()))
                        };
                    }
                }
                reader.Close();
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public bool deleteNoSystemUser(string id)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"update [dbo].Employees set password = null where EmployeeNumber = @id";
                var r = ExecuteNonQuery(query, new SqlParameter("@id", id));

                if (r > 0)
                    return true;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return false;
        }

        public bool updateOrCreateNoSystemUser(User user)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                string query = @"update [dbo].Employees set password = @password where EmployeeNumber = @id";

                var r = ExecuteNonQuery(query, new[] {
                    new SqlParameter("@id", user.Id),
                    new SqlParameter("@password", optionalEncryption(user.Password))
                });

                if (r > 0)
                    return true;
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return false;
        }

        public AuthenticationResult_LDAP getSelfEmprovementUser(string username, string password)
        {
            AuthenticationResult_LDAP ret = new AuthenticationResult_LDAP() { authenticationSuccessful = false };

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "select * from SelfEmprovmentUsers where Username=@username AND Password=@password";
                SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@username",username),
                            createParameter("@password",password),
                        };
                SqlDataReader reader = ExecuteReader(query, pars);

                while (reader.Read())
                {
                    ret = new AuthenticationResult_LDAP()
                    {
                        authenticationSuccessful = true,
                        employeeData = new Employee
                        {
                            Name = reader["Name"].ToString(),
                            Surname = reader["Surname"].ToString(),
                            RoleType = reader["Role"].ToString(),
                            EmployeeNumber = reader["UserID"].ToString()
                        }
                    };

                }
                reader.Close();


            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public void deleteEvaluationTerms(int evaluationId, string assessorId)
        {
            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = @"DELETE FROM EVALUATIONTERMS WHERE EvaluationId=@EvaluationId AND AssessorId=@AssessorId";

                SqlParameter[] pars = new SqlParameter[]{
                        createParameter("@EvaluationId",evaluationId),
                        createParameter("@AssessorId",assessorId)
                    };
                ExecuteNonQuery(query, pars);
            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }
        }
        #endregion



        public double getVoiceYearAverage(string userId, string yearRef, string criterionCode)
        {
            Double ret = 0.0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                const string start = "-11-01 00:00:00";
                const string end = "-10-31 23:59:59";
                var startDate = (int.Parse(yearRef) - 1) + start;
                var endDate = yearRef + end;
                string query = "select AVG(grade) AS voiceAverage from EvaluationTerms where lastModified>=@startDate and lastModified<=@endDate and evalueeid=@evalueeId and termCode=@termCode  and grade>0";
                SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@evalueeId",userId),
                            createParameter("@termCode",criterionCode),
                            createParameter("@startDate",startDate),
                            createParameter("@endDate",endDate)
                        };
                SqlDataReader reader = ExecuteReader(query, pars);

                while (reader.Read())
                {
                    double.TryParse(reader["voiceAverage"].ToString(), out ret);

                }
                reader.Close();


            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }

        public double getTotalYearAverage(string userId, string yearRef)
        {
            Double ret = 0.0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {
                const string start = "-11-01 00:00:00";
                const string end = "-10-31 23:59:59";
                var startDate = (int.Parse(yearRef) - 1) + start;
                var endDate = yearRef + end;
                string query = "select AVG(result) AS totalAverage from EvaluationsReceived where EvaluationDate>=@startDate and EvaluationDate<=@endDate and EmployeeNumber=@evalueeId  and Result>0";
                SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@evalueeId",userId),
                            createParameter("@startDate",startDate),
                            createParameter("@endDate",endDate)
                        };
                SqlDataReader reader = ExecuteReader(query, pars);

                while (reader.Read())
                {
                    double.TryParse(reader["totalAverage"].ToString(), out ret);

                }
                reader.Close();


            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public string FindAverageDifferences()
        {
            var ret = string.Empty;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                var sql = @"SELECT s.TermCode,  s.mediaSave, t.mediaCalc,s.AssessorId, s.EvalueeId,s.EvaluationId
FROM (
   (select distinct EvaluationId, AssessorId,EvalueeId,ROUND(AVG(CAST(grade AS float)), 2) as mediaSave, TermCode  from [EvaluationTerms] where parentTermCode is null and EvaluationId>257 group by TermCode,EvaluationId, AssessorId,EvalueeId having avg(grade)>0 )  s 
   left join
   (select distinct EvaluationId, AssessorId,EvalueeId,ROUND(AVG(CAST(grade AS float)), 2) as mediaCalc, ParentTermCode  from [EvaluationTerms] where parentTermCode is not null and grade>0 and EvaluationId>257 group by ParentTermCode,EvaluationId, AssessorId,EvalueeId having avg(grade)>0) t
   on (t.ParentTermCode=s.TermCode and t.EvalueeId=s.EvalueeId and t.AssessorId=s.AssessorId and t.EvaluationId=s.EvaluationId))
   where t.mediaCalc -s.mediaSave>0.1";

                List<TestClass> testList = new List<TestClass>();
                List<TestClass> differentAverage = new List<TestClass>();

                SqlDataReader reader = ExecuteReader(sql, null);
                while (reader.Read())
                {
                    TestClass t = new TestClass();
                    t.evaluationId = ValueOperations.DBToInteger(reader["EvaluationId"]);
                    t.evalueeId = ValueOperations.DBToInteger(reader["EvalueeId"]);
                    t.assessorId = ValueOperations.DBToInteger(reader["AssessorId"]);
                    t.parentCode = ValueOperations.DBToString(reader["TermCode"]);
                    t.calculategrade = ValueOperations.DBToDecimal(reader["mediaCalc"]);
                    t.grade = ValueOperations.DBToDecimal(reader["mediaSave"]);
                    testList.Add(t);

                }
                reader.Close();
                var seconSql = @"update EvaluationTerms set Grade=@grade  where EvaluationId=@evaluationId	and AssessorId=@assessorId	and TermCode=@parentTermCode and EvalueeId =@evalueeId";

                foreach (TestClass t in testList)
                {


                    SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@evalueeId",t.evalueeId),
                            createParameter("@parentTermCode",t.parentCode),
                            createParameter("@assessorId",t.assessorId),
                            createParameter("@evaluationId",t.evaluationId),
                            createParameter("@grade",t.calculategrade)

                        };
                    ExecuteScalar(seconSql, pars);




                }




            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }


        public double getAverage(string userId, string evaluationId)
        {
            Double ret = 0.0;

            bool wasOpen = false;
            if (connection.State == System.Data.ConnectionState.Open)
                wasOpen = true;
            else
                connection.Open();

            try
            {

                string query = "select result from EvaluationsReceived where EmployeeNumber=@evalueeId  and evaluationId=@evaluationId";
                SqlParameter[] pars = new SqlParameter[]{
                            createParameter("@evalueeId",userId),
                            createParameter("@evaluationId",evaluationId)

                        };
                SqlDataReader reader = ExecuteReader(query, pars);

                while (reader.Read())
                {
                    double.TryParse(reader["result"].ToString(), out ret);

                }
                reader.Close();


            }
            finally
            {
                if (!wasOpen)
                    connection.Close();
            }

            return ret;
        }
    }

    class TestClass
    {
        public int evaluationId { get; set; }
        public int assessorId { get; set; }
        public int evalueeId { get; set; }
        public string parentCode { get; set; }
        public decimal grade { get; set; }
        public decimal calculategrade { get; set; }
    }
}


