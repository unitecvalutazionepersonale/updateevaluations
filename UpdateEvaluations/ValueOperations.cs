﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
namespace UpdateEvaluations
{



    public class ValueOperations
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static bool CheckPasswordRequirements(string password)
        {
            /*
                Passwords must be at least min. 8 characters in length,
                minimum of 1 lower case letter [a-z] and 
                a minimum of 1 upper case letter [A-Z] and
                a minimum of 1 numeric character [0-9] and
                a minimum of 1 special character: $ @ ! % * ? & + = #
                PASSWORD EXAMPLE : @Password1
            */
            Regex regex = new Regex("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@!%*?&+=#])");

            return password.Length >= 8 && regex.IsMatch(password);
        }

        public static bool CheckUsernameRequirements(string password)
        {
            /*
                Username must be at least min. 6 characters in length,
                only lower case letter [a-z] or 
                upper case letter [A-Z] or
                numeric character [0-9] or
            */
            //Regex regex = new Regex("^[a-zA-Z0-9\-_.]+$");
            Regex regex = new Regex("^[a-zA-Z0-9]+$");

            return password.Length >= 6 && regex.IsMatch(password);
        }

        public static DateTime getEpoch()
        {
            return new DateTime(1970, 1, 1);
        }

        public static DateTime getCurrentTime()
        {
            DateTime now = DateTime.Now;
            DateTime ret = new DateTime(now.Year, now.Month, now.Day,
                                    now.Hour, now.Minute, now.Second);
            return ret;
        }

        public static DateTime convertToEF(DateTime dt)
        {
            DateTime ret = new DateTime(dt.Year, dt.Month, dt.Day,
                                    dt.Hour, dt.Minute, dt.Second);
            return ret;
        }

        /*public static bool isNullOrEmpty<T>(EntityCollection<T> theCollection)
        {
            return theCollection == null || theCollection.Count == 0;
        }*/

        public static bool isNullOrEmpty(ICollection theCollection)
        {
            return theCollection == null || theCollection.Count == 0;
        }

        public static bool containsAll<T>(List<T> theList, params T[] args)
        {
            foreach (T theArg in args)
                if (!theList.Contains(theArg))
                    return false;
            return true;
        }

        public static bool containsAny<T>(List<T> theList, params T[] args)
        {
            foreach (T theArg in args)
                if (theList.Contains(theArg))
                    return true;
            return false;
        }

        public static bool containsAny<T>(T[] theArray, params T[] args)
        {
            foreach (T theArg in args)
            {
                foreach (T arrayItem in theArray)
                {
                    if (arrayItem.Equals(theArg))
                        return true;
                }
            }
            return false;
        }

        public static string GetUniqueKey(int maxSize)
        {
            char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[maxSize];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }

        public static object stringToDB(string s)
        {
            object ret = DBNull.Value;
            if (!string.IsNullOrEmpty(s))
                ret = s;
            return ret;
        }

        /// <summary>
        /// Transforms a comma- or semicolon-separated list of strings to IN values, like this:
        /// a,b,c become 'a','b','c' so that they can be used in an IN clause of SQL
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string stringToDbInClause(string s)
        {
            StringBuilder ret = new StringBuilder("''");
            if (!string.IsNullOrEmpty(s))
            {
                string[] theArray = s.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                string conj = "'";
                ret = new StringBuilder("");
                foreach (string t in theArray)
                {
                    ret.Append(conj).Append(t);
                    conj = "','";
                }
                ret.Append("'");
            }
            return ret.ToString();
        }

        public static string arrayConcatWithSeparators(string[] theStrings, string separator)
        {
            if (isNullOrEmpty(theStrings))
                return null;

            StringBuilder ret = new StringBuilder();
            string conj = "";
            foreach (string s in theStrings)
            {
                if (!string.IsNullOrEmpty(s))
                {
                    ret.Append(conj).Append(s);
                    conj = separator;
                }
            }
            return ret.ToString();
        }

        /// <summary>
        /// Transforms an array of strings to IN values, like this:
        /// [a,b,c] become 'a','b','c' so that they can be used in an IN clause of SQL
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string arrayToDbInClause(IEnumerable s)
        {
            StringBuilder ret = new StringBuilder("''");
            if (s != null)
            {
                string conj = "'";
                ret = new StringBuilder("");
                foreach (var t in s)
                {
                    if (t != null)
                    {
                        ret.Append(conj).Append(t.ToString());
                        conj = "','";
                    }
                }
                ret.Append("'");
            }
            return ret.ToString();
        }

        /// <summary>
        /// Transforms a comma- or semicolon-separated list of strings to an array of strings, like this:
        /// a,b,c becomes ["a","b","c"]
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string[] stringToSplitArray(string s)
        {
            string[] ret = null;
            if (!string.IsNullOrEmpty(s))
            {
                ret = s.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
            return ret;
        }

        public static object dateToDB(DateTime d)
        {
            object ret = DBNull.Value;
            if (d > DateTime.MinValue && d.Year >= 1753)
                ret = d;
            return ret;
        }

        public static double DBToDouble(object o)
        {
            double ret = 0.0;
            if (o is double)
                ret = (double)o;
            else if (o != null)
            {
                double temp;
                if (double.TryParse(o.ToString(), out temp))
                    ret = temp;
            }
            return ret;
        }

        public static decimal DBToDecimal(object o)
        {
            decimal ret = 0.0M;
            if (o is decimal)
                ret = (decimal)o;
            else if (o != null)
            {
                decimal temp;
                if (decimal.TryParse(o.ToString(), out temp))
                    ret = temp;
            }
            return ret;
        }

        public static int DBToInteger(object o)
        {
            int ret = 0;
            if (o is int)
                ret = (int)o;
            else if (o != null)
            {
                int temp;
                if (int.TryParse(o.ToString(), out temp))
                    ret = temp;
            }
            return ret;
        }

        public static object doubleToDB(double? d)
        {
            object ret = DBNull.Value;
            if (d.HasValue)
                ret = d.Value;
            return ret;
        }

        public static object integerToDB(int? d)
        {
            object ret = DBNull.Value;
            if (d.HasValue)
                ret = d.Value;
            return ret;
        }

        public static object decimalToDB(decimal? d)
        {
            object ret = DBNull.Value;
            if (d.HasValue)
                ret = d.Value;
            return ret;
        }

        public static object boolToDB(bool? d)
        {
            object ret = DBNull.Value;
            if (d.HasValue)
                ret = d.Value;
            return ret;
        }

        public static object longToDB(long? d)
        {
            object ret = DBNull.Value;
            if (d.HasValue)
                ret = d.Value;
            return ret;
        }

        public static object bytesToDB(Byte[] d)
        {
            object ret = DBNull.Value;
            if (d != null && d.Length > 0)
                ret = d;
            return ret;
        }

        public static DateTime DBToDate(object dbvalue)
        {
            DateTime ret = DateTime.MinValue;
            if (dbvalue != null && dbvalue != DBNull.Value)
                ret = Convert.ToDateTime(dbvalue);
            return ret;
        }

        public static DateTime DBToNormalizedDate(object dbvalue)
        {
            DateTime ret = DateTime.MinValue;
            DateTime epoch = new DateTime(1901, 1, 1);
            if (dbvalue != null && dbvalue != DBNull.Value)
                ret = Convert.ToDateTime(dbvalue);
            if (ret < epoch)
                ret = DateTime.MinValue;
            return ret;
        }


        public static Byte[] DBToByteArray(object dbvalue)
        {
            if (!(dbvalue == null) && !(dbvalue is DBNull))
            {
                Byte[] ret = (Byte[])dbvalue;
                if (ret != null && ret.Length > 0)
                    return ret;
            }
            return null;
        }

        public static string DBToString(object dbvalue)
        {
            string ret = null;
            if (dbvalue != null && dbvalue != DBNull.Value)
                ret = dbvalue.ToString();
            return ret;
        }

        public static bool DBToBool(object dbvalue)
        {
            bool ret = false;
            if (dbvalue != null && dbvalue != DBNull.Value &&
                ("Y".Equals(dbvalue.ToString()) ||
                 "1".Equals(dbvalue.ToString()) ||
                 "True".Equals(dbvalue.ToString())))
                ret = true;
            return ret;
        }

        public static string generateRandomPassword(int length)
        {
            string ret = "";
            Random r = new Random();
            for (int ii = 0; ii < length + 10; ii++)
                ret += r.Next(1, 100).ToString();
            ret = ret.Substring(0, length);
            return ret;
        }

        /// <summary>
        /// Returns a/b if a and b are not zero, and zero otherwise
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double safeDivideBy(double a, double b)
        {
            double ret = 0;
            if (a != 0 && b != 0)
                ret = a / b;
            return ret;
        }

        public static string safeToString(object ob)
        {
            if (ob != null)
                return ob.ToString();
            return String.Empty;
        }

        public static string safeInnerText(System.Xml.XmlNode node)
        {
            if (node != null)
                return node.InnerText;
            return String.Empty;
        }

        public static DateTime safeToDate(object ob)
        {
            if (ob != null)
            {
                if (ob is DateTime)
                    return (DateTime)ob;

                DateTime ret;
                if (DateTime.TryParse(ob.ToString(), out ret))
                    return ret;
            }
            return DateTime.MinValue;
        }


        public static double safeLogarithm(double operand, double newBase)
        {
            if (operand <= 1 || newBase < 1)
                return 0;
            else
            {
                return Math.Log(operand, newBase);
            }
        }

        public static double safeLogarithm(double operand)
        {
            if (operand <= 1)
                return 0;
            else
            {
                return Math.Log(operand);
            }
        }

        public static string dump(object ob)
        {
            if (ob != null)
            {
                if (ob is int || ob is string || ob is bool || ob is DateTime || ob is decimal ||
                                ob is double || ob is float || ob is long || ob is Enum || ob is byte[])
                {
                    return ob.ToString();
                }
                else if (ob is System.Collections.IEnumerable)
                {
                    System.Text.StringBuilder sbien = new StringBuilder();
                    int ienCounter = 0;
                    System.Collections.IEnumerator ien = ((System.Collections.IEnumerable)ob).GetEnumerator();
                    while (ien.MoveNext())
                        sbien.Append("[").Append(ienCounter++).Append("]=[").Append(dump(ien.Current)).AppendLine("]");
                    return sbien.ToString();
                }
                else
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    bool hasProperties = false;
                    Type rt = ob.GetType();
                    foreach (System.Reflection.PropertyInfo prop in rt.GetProperties())
                    {
                        if (!prop.Name.Equals("Password"))
                        {
                            object propValue = prop.GetValue(ob, null);
                            if (propValue != null)
                            {
                                if (propValue is int || propValue is string || propValue is bool || propValue is DateTime || propValue is decimal ||
                                    propValue is double || propValue is float || propValue is long || propValue is Enum || propValue is byte[])
                                    sb.Append(prop.Name).Append("=").AppendLine(safeToString(propValue));
                                else
                                    sb.Append(prop.Name).Append("={").AppendLine(dump(propValue)).AppendLine("}");
                            }
                            hasProperties = true;
                        }
                    }

                    if (!hasProperties)
                    {
                        foreach (System.Reflection.FieldInfo field in rt.GetFields())
                        {
                            if (!field.Name.Equals("Password"))
                            {
                                object fieldValue = field.GetValue(ob);
                                if (fieldValue != null)
                                {
                                    if (fieldValue is int || fieldValue is string || fieldValue is bool || fieldValue is DateTime || fieldValue is decimal ||
                                        fieldValue is double || fieldValue is float || fieldValue is long || fieldValue is Enum || fieldValue is byte[])
                                        sb.Append(field.Name).Append("=").AppendLine(safeToString(fieldValue));
                                    else
                                        sb.Append(field.Name).Append("={").AppendLine(dump(fieldValue)).AppendLine("}");
                                }
                            }
                        }
                    }

                    return sb.ToString();
                }
            }
            return String.Empty;
        }

        public static void WriteTransmissionLog(string method, string timeMark, object req, double timeElapsed = 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Method name: ").AppendLine(method);
            sb.Append("Time mark: ").AppendLine(timeMark);

            if (req != null)
            {
                Type rt = req.GetType();
                sb.Append("Wrapper name: ").AppendLine(rt.Name);

                foreach (System.Reflection.FieldInfo field in rt.GetFields())
                {
                    if (field.Name.Equals("Username"))
                    {
                        object fieldValue = field.GetValue(req);
                        if (fieldValue != null)
                        {
                            sb.Append(field.Name).Append("=").AppendLine(safeToString(fieldValue));
                        }
                    }
                }

                foreach (System.Reflection.PropertyInfo prop in rt.GetProperties())
                {
                    if (prop.Name.Equals("Username"))
                    {
                        object propValue = prop.GetValue(req, null);
                        if (propValue != null)
                        {
                            sb.Append(prop.Name).Append("=").AppendLine(safeToString(propValue));
                        }
                    }
                }

                sb.AppendLine(dump(req));
            }

            if (timeElapsed > 0)
            {
                sb.Append("Time Elapsed (ms):").Append(timeElapsed).Append(" ").Append(method).Append(" ").Append(timeMark).AppendLine();
            }
            logger.Info(sb.ToString());
        }

        public static double moneyCeiling(double amount)
        {
            return Math.Round(Math.Ceiling((Math.Floor(amount * 1000000) / 1000000) * 100.0) / 100.0, 2, MidpointRounding.AwayFromZero);
        }

        public static double roundToNextMoneyValue(double d)
        {
            return Math.Ceiling(d * 100.0) / 100.0;
        }

        public static double addVAT(double amount)
        {
            amount += (amount * 22.0) / 100.0;
            return amount;
        }

        public static DateTime roundTimeToNextMinute(DateTime d)
        {
            DateTime ret = new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, 0);
            if (d.Second > 0)
                ret = ret.AddMinutes(1);

            return ret;
        }

        public static DateTime roundTimeToLastMinute(DateTime d)
        {
            DateTime ret = new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, 0);
            return ret;
        }


        public static string ByteArrayToBase64(Byte[] obj)
        {
            if (obj != null && obj.Length > 0)
            {
                string base64String = Convert.ToBase64String(obj, 0, obj.Length);

                return "data:image/jpeg;base64," + base64String;
            }
            return string.Empty;
        }

        public static string ObjectToBase64(object obj)
        {
            return ByteArrayToBase64((Byte[])(obj));
        }


        public static bool isAllUppercase(string s)
        {
            char[] theChars = s.ToCharArray();
            int contingentUppercaseChars = 0;
            foreach (char c in theChars)
            {
                if (Char.IsLetter(c))
                {
                    if (Char.IsUpper(c))
                    {
                        contingentUppercaseChars++;
                        if (contingentUppercaseChars > 5)
                            return true;
                    }
                    else
                        contingentUppercaseChars = 0;
                }
            }
            return contingentUppercaseChars > 5;
        }

        public static string setLowercaseIfAllUppercase(string s)
        {
            if (isAllUppercase(s))
                return s.ToLower();
            else
                return s;
        }

        public static string keepOnlyLettersAndDigits(string original)
        {
            StringBuilder ret = new StringBuilder();
            int l = original.Length;
            for (int i = 0; i < l; i++)
            {
                if (Char.IsLetterOrDigit(original, i))
                    ret.Append(original[i]);
            }
            return ret.ToString();
        }

        public static string setOnlyFirtUpper(string s)
        {
            if (string.IsNullOrEmpty(s))
                return null;

            char[] ret = s.ToLower().ToCharArray();
            ret[0] = char.ToUpper(ret[0]);
            return new string(ret);
        }

        public static string deleteAsterisks(string s)
        {
            if (string.IsNullOrEmpty(s))
                return null;

            string ret = s.Replace("(*)", "");
            ret = ret.Replace("(**)", "");

            return ret.TrimEnd();
        }

        public static string getCarLicenseApproximations(string carLicense)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < carLicense.Length; i++)
            {
                char[] theChars = carLicense.ToCharArray();
                theChars[i] = '%';
                string approx = new string(theChars);
                sb.Append(" OR Parkings.CarLicense LIKE '").Append(approx).Append("'");
            }
            sb.Append(" OR Parkings.CarLicense LIKE '[").Append(carLicense.Replace("-", "")).Append("]'");

            for (int i = 0; i < carLicense.Length - 1; i++)
            {
                for (int j = i + 1; j < carLicense.Length; j++)
                {
                    char[] theChars = carLicense.ToCharArray();
                    char temp = theChars[i];
                    theChars[i] = theChars[j];
                    theChars[j] = temp;
                    string approx = new string(theChars);
                    sb.Append(" OR Parkings.CarLicense LIKE '").Append(approx).Append("'");
                }
            }
            return sb.ToString();
        }

        public static string getLastPartOfUrl(string url)
        {
            url = url.TrimEnd('/');
            return url.Substring(url.LastIndexOf('/') + 1);
        }

        public static string hex_SHA1(string strPlain)
        {
            UTF8Encoding encode = new UTF8Encoding();
            byte[] HashValue, MessageBytes = encode.GetBytes(strPlain);
            SHA1Managed SHhash = new SHA1Managed();
            string strHex = "";

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += string.Format("{0:x2}", b);
            }

            return strHex;
        }
    }

    /*public static class Encryption
    {
        public static string Encrypt(string input, string key, string IV)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.IV = UTF8Encoding.UTF8.GetBytes(IV);
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.CBC;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string input, string key, string IV)
        {
            byte[] inputArray = Convert.FromBase64String(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.IV = UTF8Encoding.UTF8.GetBytes(IV);
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.CBC;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            string key = AddUpConstants.EncryptionKey;
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string toDecrypt, bool useHashing)
        {
            try
            {
                byte[] keyArray;
                byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);
                string key = AddUpConstants.EncryptionKey;

                if (useHashing)
                {
                    MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                    keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    hashmd5.Clear();
                }
                else
                    keyArray = UTF8Encoding.UTF8.GetBytes(key);

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tdes.Clear();
                return UTF8Encoding.UTF8.GetString(resultArray);
            }
            catch
            {
                return toDecrypt;
            }
        }
    }*/



    public static class Encryption
    {
        private static string AesEncryptionKey = "Gr7DLnbP5gQBsfZA";
        private static string AesEncryptionIV = "YejpSaRHn3qGTJ6X";

        public static string AesEncrypt(string input)
        {
            return AesEncrypt(input, AesEncryptionKey, AesEncryptionIV);
        }

        public static string AesDecrypt(string input)
        {
            return AesDecrypt(input, AesEncryptionKey, AesEncryptionIV);
        }

        public static string AesEncrypt(string input, string key, string IV)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            AesCryptoServiceProvider theAes = new AesCryptoServiceProvider();
            theAes.IV = UTF8Encoding.UTF8.GetBytes(IV);
            theAes.Key = UTF8Encoding.UTF8.GetBytes(key);
            theAes.Mode = CipherMode.CBC;
            theAes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = theAes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            theAes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string AesDecrypt(string input, string key, string IV)
        {
            byte[] inputArray = Convert.FromBase64String(input);
            AesCryptoServiceProvider theAes = new AesCryptoServiceProvider();
            theAes.IV = UTF8Encoding.UTF8.GetBytes(IV);
            theAes.Key = UTF8Encoding.UTF8.GetBytes(key);
            theAes.Mode = CipherMode.CBC;
            theAes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = theAes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            theAes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}

