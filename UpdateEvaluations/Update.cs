﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpdateEvaluations.BE;
using UpdateEvaluations.BL;

namespace UpdateEvaluations
{
    public class Update
    {
        LogFactory logFactory;
        Logger logger;

        UnitecBL bl = null;
        public Update()
        {
            logFactory = new LogFactory();
            logger = logFactory.GetCurrentClassLogger();
            UnitecConfiguration conf = new UnitecConfiguration();
            conf.AvailableLanguages = "it|en|de|ru";
            conf.DefaultLanguage = "en";
            conf.ZeroLanguage = "it";
            conf.useDbEncryption = true;
            conf.reminderEmailBody = "Buongiorno ((REM01)), ricordati di valutare queste persone ((REM02))";
            conf.reminderEmailSubject = "Promemoria di valutazione";
            //bl = new UnitecBL("Server=77.81.232.71,1433;Database=UnitecDB;User Id=cityphoneAdmin;Password=cityPhonzie2016!;", conf);
            //string x = bl.optionalEncryption_test("bacchi@lenis.tech");
            bl = new UnitecBL("Data Source=SRVVALUTAZIONI;Initial Catalog=VALUTAZIONI;Persist Security Info=True;User ID=lenis;Password=LNS639kwq!", conf);
            //"Server=172.16.0.47;Database=UnitecReportDB_DEMO;User Id=lenis;Password=LNS639kwq!;", null);
        
        }

        public void UpdateEvaluationList()
        {
            try {
                var ids = ConfigurationManager.AppSettings["evaluationsIds"].Split(',').ToList();
             

                foreach (var id in ids)
                {
                    Console.WriteLine("Inizio bonifica valutazione: " + id);
                    logger.Warn("Inzio bonifica valutazione: " + id);
                    bl.updateAllGradeStatistics(int.Parse(id));
                    Console.WriteLine("Fine metodo  bl.updateAllGradeStatistics(id)");
                    bl.updateAllAssessorGradeStatistics(int.Parse(id));
                    Console.WriteLine("Fine metodo  bl.updateAllAssessorGradeStatistics(id)");
                    logger.Warn("Fine metodo  bl.updateAllAssessorGradeStatistics(id)");
                    Employees allEmp = bl.getAllEvaluees(int.Parse(id), false);
                    Employees evaluees = new Employees();
                    List<string> evalueesIdList = new List<string>();
                    foreach (Employee e in allEmp)
                    {
                        if (!("debug".Equals(e.EmployeeNumber) ||
                             "demo".Equals(e.EmployeeNumber) ||
                             "lenis".Equals(e.EmployeeNumber)))
                        {
                            EvaluationGrades grades = bl.getEvalueeGrades(int.Parse(id), e.EmployeeNumber, false, false);
                            logger.Warn("Fine metodo  bl.getEvalueeGrades");
                            Console.WriteLine("Fine metodo  bl.getEvalueeGrades("+id+","+e.EmployeeNumber+")");
                            bl.updateHistoryRecordAndAssessor(e.EmployeeNumber, int.Parse(id), grades.averageGrade, grades.gradesDone);
                            Console.WriteLine("Fine metodo  bl.updateHistoryRecordAndAssessor(" + e.EmployeeNumber + "," + id + "," + grades.averageGrade + ","+grades.gradesDone+")");
                            logger.Warn("Fine metodo  bl.updateHistoryRecordAndAssessor");
                        }
                    }

                    logger.Warn("Fine bonifica valutazione: " + id);
                    Console.WriteLine("Fine bonifica valutazione: " + id);
                }
                Console.ReadLine();

            }
            catch(Exception ex)
            {
                logger.Error("Errore " + ex.Message);
                 logger.Error("Errore " + ex.StackTrace);
            }
        }
    }
}
